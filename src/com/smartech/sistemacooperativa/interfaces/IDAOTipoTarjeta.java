package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.TipoTarjeta;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOTipoTarjeta {
    
    public TipoTarjeta getTipoTarjeta(int id);
    
    public List<TipoTarjeta> getAllTiposTarjeta();
}
