package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.TasaInteres;
import com.smartech.sistemacooperativa.dominio.TipoInteres;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOTasaInteres {

    public TasaInteres getTasaInteres(int id);

    public TasaInteres getTasaInteres(SolicitudPrestamo objSolicitudPrestamo);

    public TasaInteres getTasaInteres(TipoInteres objTipoInteres);

    public List<TasaInteres> getAllTasasIntereses();

    public List<TasaInteres> getAllTasasIntereses(TipoInteres objTipoInteres);

    public boolean insert(TasaInteres objTasaInteres);

    public boolean update(TasaInteres objTasaInteres);

    public boolean delete(TasaInteres objTasaInteres);
    
    public boolean physicalDelete(TasaInteres objTasaInteres);
}
