package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.HabilitacionCuotas;
import com.smartech.sistemacooperativa.dominio.Pago;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOHabilitacionCuotas {

    public HabilitacionCuotas getHabilitacionCuotas(long id);

    public HabilitacionCuotas getHabilitacionCuotas(Pago objPago);

    public List<HabilitacionCuotas> getAllHabilitacionesCuotasHabilitados(Timestamp fechaDesde, Timestamp fechaHasta);

    public List<HabilitacionCuotas> getAllHabilitacionesCuotasPagados(Timestamp fechaDesde, Timestamp fechaHasta);

    public boolean insert(HabilitacionCuotas objHabilitacionCuotas);

    public boolean update(HabilitacionCuotas objHabilitacionCuotas);

    public boolean insertDetalleHabilitacionCuota(HabilitacionCuotas objHabilitacionCuotas);

    public boolean insertDetalleHabilitacionCuotaPago(Pago objPago, HabilitacionCuotas objHabilitacionCuotas);

    public boolean delete(HabilitacionCuotas objHabilitacionCuotas);
}
