package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.TipoPrestamo;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOTipoPrestamo {
    
    public TipoPrestamo getTipoPrestamo(int id);
    
    public List<TipoPrestamo> getAllTiposPrestamo();
}
