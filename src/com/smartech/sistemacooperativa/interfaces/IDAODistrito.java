/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Distrito;
import com.smartech.sistemacooperativa.dominio.Provincia;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAODistrito {
    
    public Distrito getDistrito(int id);
    
    public List<Distrito> getAllDistritos();
    
    public List<Distrito> getAllDistritos(Provincia objProvincia);
    
}
