package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Solicitud;

/**
 *
 * @author Smartech
 */
public interface IDAOSolicitud {
    
    public long getUltimoCodigo(int year);
    
    public Solicitud getSolicitud(long id);
    
    public boolean insert(Solicitud objSolicitud);

    public boolean update(Solicitud objSolicitud);

    public boolean physicalDelete(Solicitud objSolicitud);
}
