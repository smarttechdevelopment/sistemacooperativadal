package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Concepto;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.ITransaccion;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOTransaccion {

    public boolean insert(ITransaccion objTransaccion);

    public boolean update(ITransaccion objTransaccion);

    public ITransaccion getTransaccion(long id, Concepto objConcepto);
    
    public ITransaccion getTransaccion(Pago objPago);

    public List<ITransaccion> getAllTransacciones(Concepto objConcepto, Timestamp fechaDesde, Timestamp fechaHasta, int modo);

    public List<ITransaccion> getAllTransacciones(Cuenta objCuenta, Timestamp fechaDesde, Timestamp fechaHasta, int modo);
}
