/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Moneda;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOMoneda {

    public Moneda getMoneda(int id);

    public Moneda getMonedaByAbreviatura(String abreviatura);
    
    public Moneda getMonedaBase();

    public List<Moneda> getAllMonedas();
    
    public List<Moneda> getAllMonedasCambio();

    public boolean insert(Moneda objMoneda);

    public boolean update(Moneda objMoneda);

    public boolean insertTipoCambio(Moneda objMoneda);

    public boolean physicalDeleteMoneda(Moneda objMoneda);

    public boolean physicalDeleteLastTipoCambio(Moneda objMoneda);
}
