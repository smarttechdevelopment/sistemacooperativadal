/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.DetalleSolicitudRetiroSocio;
import com.smartech.sistemacooperativa.dominio.Solicitud;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAODetalleSolicitudRetiroSocio {
    
    public boolean insert(Solicitud objSolicitud, DetalleSolicitudRetiroSocio objDetalleSolicitudRetiroSocio);
    
    public boolean update(Solicitud objSolicitud, DetalleSolicitudRetiroSocio objDetalleSolicitudRetiroSocio);
    
    public List<DetalleSolicitudRetiroSocio> getAllDetalleSolicitudRetiroSocio(Solicitud objSolicitud);
}
