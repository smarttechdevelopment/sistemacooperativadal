package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.MovimientoAdministrativo;
import com.smartech.sistemacooperativa.dominio.Pago;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOMovimientoAdministrativo {

    public MovimientoAdministrativo getMovimientoAdministrativo(long id);

    public MovimientoAdministrativo getMovimientoAdministrativo(Pago objPago);

    public List<MovimientoAdministrativo> getAllMovimientosAdministrativosHabilitados(Timestamp fechaDesde, Timestamp fechaHasta, boolean ingreso);

    public List<MovimientoAdministrativo> getAllMovimientosAdministrativosPagados(Timestamp fechaDesde, Timestamp fechaHasta, boolean ingreso);

    public boolean insert(MovimientoAdministrativo objMovimientoAdministrativo);

    public boolean update(MovimientoAdministrativo objMovimientoAdministrativo);

    public boolean insertDetallePagoMovimientoAdministrativo(Pago objPago, MovimientoAdministrativo objMovimientoAdministrativo);

    public boolean delete(MovimientoAdministrativo objMovimientoAdministrativo);
}
