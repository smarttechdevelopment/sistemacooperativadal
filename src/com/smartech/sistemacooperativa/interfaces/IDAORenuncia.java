/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Renuncia;
import com.smartech.sistemacooperativa.dominio.Socio;

/**
 *
 * @author Smartech
 */
public interface IDAORenuncia {
    
    public boolean insert(Renuncia objRenuncia);
    
    public boolean update(Renuncia objRenuncia);
    
    public Renuncia getRenuncia(Socio objSocio);
}