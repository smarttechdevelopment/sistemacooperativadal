package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.AccesoSistema;
import com.smartech.sistemacooperativa.dominio.Usuario;

/**
 *
 * @author Smartech
 */
public interface IDAOAccesoSistema {
    
    public boolean insert(AccesoSistema objAccesoSistema);
    
    public AccesoSistema getLastAccesoSistema(Usuario objUsuario);
    
    public AccesoSistema getLastAccesoSistema();
    
}
