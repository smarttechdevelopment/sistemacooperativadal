package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Documento;
import com.smartech.sistemacooperativa.dominio.TipoSolicitud;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAODocumento {

    public Documento getDocumento(int id);

    public List<Documento> getAllDocumentos();
}
