/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Caja;
import com.smartech.sistemacooperativa.dominio.Establecimiento;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOCaja {
    
    public boolean update(Caja objCaja);
    
    public Caja getCaja(int id);
    
    public List<Caja> getAllCajas(Establecimiento objEstablecimiento);
    
    public List<Caja> getAllCajasAbiertas(Establecimiento objEstablecimiento);
    
    public List<Caja> getAllCajasCerradas(Establecimiento objEstablecimiento);
}
