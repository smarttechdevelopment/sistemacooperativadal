package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Comprobante;
import com.smartech.sistemacooperativa.dominio.Concepto;
import com.smartech.sistemacooperativa.dominio.Deposito;
import com.smartech.sistemacooperativa.dominio.HabilitacionCuotas;
import com.smartech.sistemacooperativa.dominio.MovimientoAdministrativo;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Retiro;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.Transferencia;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOPago {

    public boolean insert(Pago objPago);

    public boolean update(Pago objPago);

    public boolean deletePhysicallyPago(Pago objPago);

    public List<Pago> getAllPagos(Timestamp fechaDesde, Timestamp fechaHasta);

    public List<Pago> getAllPagos(Aporte objAporte);

    public List<Pago> getAllPagos(Deposito objDeposito);

    public List<Pago> getAllPagos(Retiro objRetiro);

    public List<Pago> getAllPagos(Transferencia objTransferencia);

    public List<Pago> getAllPagos(HabilitacionCuotas objHabilitacionCuotas);

    public List<Pago> getAllPagos(MovimientoAdministrativo objMovimientoAdministrativo);

    public List<Pago> getAllPagos(SolicitudPrestamo objSolicitudPrestamo);

    public Pago getPago(long id);

    public List<Pago> getAllPagos(Deposito objDeposito, Timestamp fechaDesde, Timestamp fechaHasta);

    public List<Pago> getAllPagos(Retiro objRetiro, Timestamp fechaDesde, Timestamp fechaHasta);

    public List<Pago> getAllPagos(Aporte objAporte, Timestamp fechaDesde, Timestamp fechaHasta);

    public List<Pago> getAllPagos(Concepto objConcepto, Timestamp fechaDesde, Timestamp fechaHasta);

    public List<Pago> getAllPagos(Comprobante objComprobante);
}
