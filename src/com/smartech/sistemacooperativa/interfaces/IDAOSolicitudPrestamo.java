package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOSolicitudPrestamo extends IDAOSolicitud{

    public SolicitudPrestamo getSolicitudPrestamoVigente(Socio objSocio);
    
    public SolicitudPrestamo getSolicitudPrestamo(Pago objPago);

    public List<SolicitudPrestamo> getAllSolicitudesPrestamoPorAprobar();

    public List<SolicitudPrestamo> getAllSolicitudesPrestamosPorAprobarAsFiador(Socio objSocio);
    
    public List<SolicitudPrestamo> getAllSolicitudesPrestamoHabilitados(Timestamp fechaDesde, Timestamp fechaHasta);

    public List<SolicitudPrestamo> getAllSolicitudesPrestamoPagados(Timestamp fechaDesde, Timestamp fechaHasta);

    public boolean insertDetallePagoSolicitudPrestamo(Pago objPago, SolicitudPrestamo objSolicitudPrestamo);
}
