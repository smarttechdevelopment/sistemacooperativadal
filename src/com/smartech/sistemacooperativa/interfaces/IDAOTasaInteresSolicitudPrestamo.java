package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.TasaInteresSolicitudPrestamo;

/**
 *
 * @author Smartech
 */
public interface IDAOTasaInteresSolicitudPrestamo {

    public boolean insert(TasaInteresSolicitudPrestamo objTasaInteresSolicitudPrestamo);

    public boolean updateTasasIntereses(long oldTasaInteresId, long newTasaInteresId);

    public boolean physicalDelete(SolicitudPrestamo objSolicitudPrestamo);
}
