/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Concepto;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOConcepto {

    public Concepto getConcepto(int id);
    
    public Concepto getConcepto(String nombre);
    
    public List<Concepto> getAllConceptosOnly();
    
    public List<Concepto> getAllConceptos();
}
