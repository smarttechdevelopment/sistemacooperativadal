package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Huella;
import com.smartech.sistemacooperativa.dominio.Usuario;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOHuella {
    
    public List<Huella> getAllHuellas(Usuario objUsuario);
    
    public boolean insert(Huella objHuella, Usuario objUsuario);
    
    public boolean physicalDelete(Usuario objUsuario);
}
