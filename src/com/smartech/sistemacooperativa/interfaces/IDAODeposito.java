package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Deposito;
import com.smartech.sistemacooperativa.dominio.Pago;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAODeposito {

    public Deposito getDeposito(long id);
    
    public Deposito getDeposito(Pago objPago);

    public boolean insert(Deposito objDeposito);

    public boolean insertDetallePago(Deposito objDeposito, Pago objPago);

    public boolean update(Deposito objDeposito);
    
    public boolean logicalDeleteDeposito(Deposito objDeposito);

    public boolean deletePhysicallyDeposito(Deposito objDeposito);

    public boolean deletePhysicallyAllDetallePagoByDeposito(Deposito objDeposito);

    public List<Deposito> getAllDepositos(Cuenta objCuenta);

    public List<Deposito> getAllDepositos(Timestamp fechaDesde, Timestamp fechaHasta);

    public List<Deposito> getAllDepositosHabilitados(Timestamp fechaDesde, Timestamp fechaHasta);

    public List<Deposito> getAllDepositosHabilitados(Cuenta objCuenta, Timestamp fechaDesde, Timestamp fechaHasta);

    public List<Deposito> getAllDepositosPagados(Timestamp fechaDesde, Timestamp fechaHasta);

    public List<Deposito> getAllDepositosPagados(Cuenta objCuenta, Timestamp fechaDesde, Timestamp fechaHasta);
}
