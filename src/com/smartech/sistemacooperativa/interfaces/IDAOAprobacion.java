package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Aprobacion;
import com.smartech.sistemacooperativa.dominio.Solicitud;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOAprobacion {

    public List<Aprobacion> getAllAprobaciones(Solicitud objSolicitud);

    public boolean insert(Aprobacion objAprobacion);

    public boolean update(Aprobacion objAprobacion);
    
    public boolean physicalDelete(Solicitud objSolicitud);
}
