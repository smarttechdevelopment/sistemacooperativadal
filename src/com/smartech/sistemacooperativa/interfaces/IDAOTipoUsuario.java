/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.TipoUsuario;
import com.smartech.sistemacooperativa.dominio.Usuario;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOTipoUsuario {
 
    public TipoUsuario getTipoUsuario(int id);
    
    public List<TipoUsuario> getAllTiposUsuario();
    
    public List<TipoUsuario> getAllTiposUsuarioEmpleadosDebajoNivel(Usuario objUsuario);
}
