package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.Tarjeta;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOTarjeta {

    public Tarjeta getTarjeta(long id);

    public Tarjeta getTarjeta(Socio objSocio);
    
    public Tarjeta getTarjeta(String codigo);

    public List<Tarjeta> getAllTarjetas(Socio objSocio);

    public boolean insert(Tarjeta objTarjeta);
    
    public boolean verificar(Tarjeta objTarjeta, String clave);
    
    public boolean update(Tarjeta objTarjeta);
    
    public boolean physicalDelete(Tarjeta objTarjeta);
    
    public String getUltimoCodigoTarjeta();
}
