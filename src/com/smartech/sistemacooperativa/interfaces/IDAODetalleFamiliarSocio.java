/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.DetalleFamiliarSocio;
import com.smartech.sistemacooperativa.dominio.Socio;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAODetalleFamiliarSocio {
    
    public List<DetalleFamiliarSocio> getAllDetalleFamiliarSocio(Socio objSocio);
    
    public boolean insert(DetalleFamiliarSocio objDetalleFamiliarSocio);
    
    public boolean physicalDelete(Socio objSocio);
}
