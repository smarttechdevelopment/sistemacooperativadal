package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.TipoDocumentoIdentidad;
import com.smartech.sistemacooperativa.dominio.Usuario;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOSocio {

    public long getUltimoCodigo();
    
    public Socio getSocio(long id, boolean cargarFoto);

    public Socio getSocio(Usuario objUsuario, boolean cargarFoto);

    public Socio getSocioByDocumentoIdentidad(String documentoIdentidad, boolean cargarFoto);
    
    public Socio getSocioByDocumentoIdentidad(String documentoIdentidad, TipoDocumentoIdentidad objTipoDocumentoIdentidad, boolean cargarFoto);
    
    public Socio getSocioByRUC(String ruc, boolean cargarFoto);

    public Socio getSocioByCodigo(String codigo, boolean cargarFoto);

    public List<Socio> getAllSocios(boolean cargarFoto);

    public List<Socio> getAllSociosActivos(boolean cargarFoto);
    
    public List<Socio> getAllSociosPasivos(Timestamp fechaLimite, boolean cargarFoto);

    public List<Socio> getAllSocios(Cuenta objCuenta, boolean cargarFoto);

    public List<Socio> getAllSociosOnly(Cuenta objCuenta, boolean cargarFoto);

    public boolean insert(Socio objSocio);

    public boolean update(Socio objSocio);
    
    public boolean physicalDelete(Socio objSocio);
}
