package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.DetalleSolicitudPrestamoSocio;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAODetalleSolicitudPrestamoSocio {
    
    public List<DetalleSolicitudPrestamoSocio> getAllDetalleSolicitudPrestamoSocio(SolicitudPrestamo objSolicitudPrestamo);
    
    public boolean insert(DetalleSolicitudPrestamoSocio objDetalleSolicitudPrestamoSocio);
    
    public boolean physicalDelete(SolicitudPrestamo objSolicitudPrestamo);
}
