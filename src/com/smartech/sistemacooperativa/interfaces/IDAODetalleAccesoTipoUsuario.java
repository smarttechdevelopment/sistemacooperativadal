package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.DetalleAccesoTipoUsuario;
import com.smartech.sistemacooperativa.dominio.TipoUsuario;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAODetalleAccesoTipoUsuario {
    
    public List<DetalleAccesoTipoUsuario> getAllDetalleAccesoTipoUsuario(TipoUsuario objTipoUsuario);
}
