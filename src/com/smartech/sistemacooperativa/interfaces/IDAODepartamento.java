/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Departamento;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAODepartamento {
    
    public List<Departamento> getAllDepartamentos();
    
    public Departamento getDepartamento(int id);
    
}
