/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Familiar;
import com.smartech.sistemacooperativa.dominio.Socio;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOFamiliar {
    
    public List<Familiar> getAllFamiliares(Socio objSocio);
    
    public Familiar getFamiliar(int id);
    
    public boolean insert(Familiar objFamiliar);
}
