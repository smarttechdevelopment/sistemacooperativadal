/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.Transferencia;
import com.smartech.sistemacooperativa.dominio.TransferenciaCuenta;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOTransferenciaCuenta extends IDAOTransferencia {
        
    public TransferenciaCuenta getTransferenciaCuenta(Transferencia objTransferencia);
    
    public boolean insertDetallePagoTransferenciaCuenta(Pago objPago, TransferenciaCuenta objtraTransferenciaCuenta);
    
    public List<TransferenciaCuenta> getAllTransferenciasCuenta(Cuenta objCuenta);
    
    public List<TransferenciaCuenta> getAllTransferenciasCuenta(Socio objSocio);
}
