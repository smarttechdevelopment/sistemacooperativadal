package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.HistoricoCuenta;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOHistoricoCuenta {
 
    public List<HistoricoCuenta> getAllHistoricoCuenta(Cuenta objCuenta);
    
    public List<HistoricoCuenta> getAllHistoricoCuenta(Cuenta objCuenta, Timestamp fechaDesde, Timestamp fechaHasta);
    
    public boolean insertHistoricoCuenta(HistoricoCuenta objHistoricoCuenta);
}
