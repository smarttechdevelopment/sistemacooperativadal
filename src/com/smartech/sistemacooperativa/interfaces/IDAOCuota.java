package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Cuota;
import com.smartech.sistemacooperativa.dominio.HabilitacionCuotas;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Prestamo;
import com.smartech.sistemacooperativa.dominio.Socio;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOCuota {

    public Cuota getCuota(long id);

    public List<Cuota> getAllCuotas(Prestamo objPrestamo);

    public List<Cuota> getAllCuotasPorPagar(Socio objSocio);

    public List<Cuota> getAllCuotasPorPagar(Timestamp fechaDesde, Timestamp fechaHasta);

    public List<Cuota> getAllCuotasPagadas(Timestamp fechaDesde, Timestamp fechaHasta);

    public List<Cuota> getAllCuotas(HabilitacionCuotas objHabilitacionCuotas);

    public boolean insert(Cuota objCuota);

    public boolean insertDetallePagoCuota(Cuota objCuota, Pago objPago);

    public boolean update(Cuota objCuota);

    public boolean physicalDelete(Prestamo objPrestamo);
}
