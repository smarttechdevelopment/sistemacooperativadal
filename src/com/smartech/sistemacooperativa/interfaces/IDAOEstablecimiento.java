package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Establecimiento;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOEstablecimiento {

    public Establecimiento getEstablecimiento(int id);

    public List<Establecimiento> getAllEstablecimientos();
}
