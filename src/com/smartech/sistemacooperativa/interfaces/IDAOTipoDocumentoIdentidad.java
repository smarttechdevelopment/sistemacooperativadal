package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.TipoDocumentoIdentidad;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOTipoDocumentoIdentidad {

    public TipoDocumentoIdentidad getTipoDocumentoIdentidad(int id);

    public List<TipoDocumentoIdentidad> getAllTiposDocumentoIdentidad();
}
