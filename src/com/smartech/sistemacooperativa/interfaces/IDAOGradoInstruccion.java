package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.GradoInstruccion;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOGradoInstruccion {

    public GradoInstruccion getGradoInstruccion(int id);

    public List<GradoInstruccion> getAllGradosInstruccion();
}
