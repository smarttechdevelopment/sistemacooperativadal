package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Prestamo;
import com.smartech.sistemacooperativa.dominio.Socio;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOPrestamo {

    public Prestamo getPrestamo(long id);

    public Prestamo getPrestamoOnly(long id);

    public List<Prestamo> getAllPrestamos(Socio objSocio, boolean pagado);

    public List<Prestamo> getAllPrestamos(Timestamp fechaInicio, Timestamp fechaFin, boolean pagado);

    public List<Prestamo> getAllPrestamos(Socio objSocio, Timestamp fechaInicio, Timestamp fechaFin, boolean pagado);

    public List<Prestamo> getAllPrestamosAsGarante(Socio objSocio, boolean pagado);

    public List<Prestamo> getAllPrestamosAsGarante(Socio objSocio, Timestamp fechaInicio, Timestamp fechaFin, boolean pagado);

    public boolean insert(Prestamo objPrestamo);

    public boolean update(Prestamo objPrestamo);

    public boolean physicalDelete(Prestamo objPrestamo);
}
