package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.DetalleDocumentoSolicitud;
import com.smartech.sistemacooperativa.dominio.Solicitud;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAODetalleDocumentoSolicitud {
    
    public List<DetalleDocumentoSolicitud> getAllDetalleDocumentoSolicitud(Solicitud objSolicitud);
    
    public boolean insert(DetalleDocumentoSolicitud objDetalleDocumentoSolicitud);
    
    public boolean update(DetalleDocumentoSolicitud objDetalleDocumentoSolicitud);
    
    public boolean discardAllDetalleDocumentoSolicitud(Solicitud objSolicitud);
    
    public boolean physicalDelete(Solicitud objSolicitud);
}
