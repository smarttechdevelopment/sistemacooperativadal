package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Caja;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.HistoricoCaja;

/**
 *
 * @author Smartech
 */
public interface IDAOHistoricoCaja {

    public HistoricoCaja getLastHistoricoCaja(Caja objCaja);
    
    public HistoricoCaja getLastHistoricoCaja(Empleado objEmpleado);

    public boolean insert(HistoricoCaja objHistoricoCaja);

    public boolean update(HistoricoCaja objHistoricoCaja);
}
