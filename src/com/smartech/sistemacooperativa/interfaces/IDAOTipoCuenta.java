package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.TipoCuenta;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOTipoCuenta {

    public TipoCuenta getTipoCuenta(int id);

    public List<TipoCuenta> getAllTiposCuenta();
}
