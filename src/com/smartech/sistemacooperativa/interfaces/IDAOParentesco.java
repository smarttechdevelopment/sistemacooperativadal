/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Parentesco;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOParentesco {
    
    public List<Parentesco> getAllParentescos();
    
    public Parentesco getParentesco(int id);
    
}
