package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Permiso;
import com.smartech.sistemacooperativa.dominio.TipoUsuario;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOPermiso {
    
    public Permiso getPermiso(int id);
    
    public List<Permiso> getAllPermisos();
}
