package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Transferencia;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOTransferencia {
    
    public Transferencia getTransferencia(long id);
    
    public Transferencia getTransferencia(Pago objPago);
    
    public List<Transferencia> getAllTransferencias(Cuenta objCuenta);
    
    public List<Transferencia> getAllTransferencias(Timestamp fechaDesde, Timestamp fechaFin);
    
    public boolean insert(Transferencia objTransferencia);
    
    public boolean update(Transferencia objTransferencia);
    
    public boolean delete(Transferencia objTransferencia);
}
