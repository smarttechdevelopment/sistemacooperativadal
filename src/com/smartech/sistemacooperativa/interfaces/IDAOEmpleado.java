package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.Establecimiento;
import com.smartech.sistemacooperativa.dominio.TipoUsuario;
import com.smartech.sistemacooperativa.dominio.Usuario;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOEmpleado {

    public Empleado getEmpleado(long id);

    public Empleado getEmpleado(Usuario objUsuario);
    
    public Empleado getEmpleado(int idTipoUsuario);

    public List<Empleado> getAllEmpleados();

    public List<Empleado> getAllEmpleadosDebajoNivel(Usuario objUsuario, Establecimiento objEstablecimiento);

    public List<Empleado> getAllEmpleadosSobreNivel(int nivel, Establecimiento objEstablecimiento);

    public boolean insert(Empleado objEmpleado);

    public boolean update(Empleado objEmpleado);

    public boolean delete(Empleado objEmpleado);
}
