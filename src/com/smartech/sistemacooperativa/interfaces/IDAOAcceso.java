package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Acceso;
import com.smartech.sistemacooperativa.dominio.TipoUsuario;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOAcceso {
    
    public Acceso getAcceso(int id);
    
    public List<Acceso> getAllAccesos();
}
