package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.TipoCuenta;
import com.smartech.sistemacooperativa.dominio.TipoInteres;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOTipoInteres {

    public TipoInteres getTipoInteres(int id);

    public List<TipoInteres> getAllTiposInteres();

    public List<TipoInteres> getAllTiposInteres(TipoCuenta objTipoCuenta);

    public boolean insert(TipoInteres objTipoInteres);

    public boolean update(TipoInteres objTipoInteres);

    public boolean delete(TipoInteres objTipoInteres);
}
