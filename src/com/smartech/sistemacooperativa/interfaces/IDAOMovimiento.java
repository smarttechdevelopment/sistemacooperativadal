package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Movimiento;
import com.smartech.sistemacooperativa.dominio.Pago;

/**
 *
 * @author Smartech
 */
public interface IDAOMovimiento {
    
    public boolean insert(Movimiento objMovimiento);
    
    public Movimiento getMovimiento(Pago objPago);
}
