package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Establecimiento;
import com.smartech.sistemacooperativa.dominio.TipoSolicitud;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOTipoSolicitud {

    public TipoSolicitud getTipoSolicitud(int id);

    public TipoSolicitud getTipoSolicitud(int id, Establecimiento objEstablecimiento);

    public List<TipoSolicitud> getAllTiposSolicitud();

    public List<TipoSolicitud> getAllTiposSolicitud(Establecimiento objEstablecimiento);
}
