package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.SolicitudRetiroSocio;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOSolicitudRetiroSocio extends IDAOSolicitud{
    
    public SolicitudRetiroSocio getLastSolicitudRetiroSocio(Socio objSocio);
    
    public List<SolicitudRetiroSocio> getAllSolicitudesRetiroAprobadas();
}
