package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.TipoUsuario;
import com.smartech.sistemacooperativa.dominio.Usuario;
import java.util.List;

/**
 *
 * @author Usuario
 */
public interface IDAOUsuario {

    public Usuario getUsuario(String username, String password);

    public Usuario getUsuario(long id);

    public List<Usuario> getAllUsuarios(TipoUsuario objTipoUsuario);
    
    public List<Usuario> getAllUsuarios(String username, String password);

    public boolean insert(Usuario objUsuario);

    public boolean update(Usuario objUsuario);
    
    public boolean delete(Usuario objUsuario);

    public boolean physicalDelete(Usuario objUsuario);
}
