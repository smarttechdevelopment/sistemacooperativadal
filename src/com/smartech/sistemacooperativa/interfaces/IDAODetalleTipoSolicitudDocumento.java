package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.DetalleTipoSolicitudDocumento;
import com.smartech.sistemacooperativa.dominio.Establecimiento;
import com.smartech.sistemacooperativa.dominio.TipoSolicitud;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAODetalleTipoSolicitudDocumento {
    
    public List<DetalleTipoSolicitudDocumento> getAllDetalleTipoSolicitudDocumento(TipoSolicitud objTipoSolicitud);
    
    public List<DetalleTipoSolicitudDocumento> getAllDetalleTipoSolicitudDocumento(TipoSolicitud objTipoSolicitud, Establecimiento objEstablecimiento);
}
