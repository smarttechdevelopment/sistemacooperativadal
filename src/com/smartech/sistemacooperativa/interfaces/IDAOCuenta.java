/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.TasaInteres;
import com.smartech.sistemacooperativa.dominio.TipoCuenta;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOCuenta {
    
    public String getLastCodigoCuenta();

    public Cuenta getCuenta(Long id);
    
    public Cuenta getCuenta(String numeroCuenta);
    
    public BigDecimal getSaldoCuentas(Socio objSocio);
    
    public List<Cuenta> getAllCuentas();
    
    public List<Cuenta> getAllCuentasHabilitadas();

    public List<Cuenta> getAllCuentas(Socio objSocio);
    
    public List<Cuenta> getAllCuentasOnly(Socio objSocio);
    
    public List<Cuenta> getCuentasSocios(TipoCuenta objTipoCuenta, List<Socio> lstSocios);
    
    public boolean insert(Cuenta objCuenta);
    
    public boolean insertDetalleCuentaSocio(Cuenta objCuenta, Socio objSocio);
    
    public boolean update(Cuenta objCuenta);
    
    public boolean updateTasasInteres(long oldTasaInteresId, long newTasaInteresId);
    
    public boolean updateIntereses(Timestamp fechaInteres);
}
