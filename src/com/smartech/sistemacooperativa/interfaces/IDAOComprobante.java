package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Comprobante;
import com.smartech.sistemacooperativa.dominio.Pago;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOComprobante {

    public Comprobante getComprobante(long id);

    public Comprobante getComprobante(String codigo);
    
    public Comprobante getComprobante(Pago objPago);

    public List<Comprobante> getAllComprobantes(Timestamp fechaDesde, Timestamp fechaHasta);

    public boolean insert(Comprobante objComprobante);

    public boolean insertDetalleComprobantePago(Comprobante objComprobante);
    
    public long getUltimoCodigo(boolean ingreso);
}
