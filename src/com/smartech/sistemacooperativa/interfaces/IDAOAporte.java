package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Socio;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOAporte {
    
    public Aporte getAporte(long id);

    public Aporte getAporte(Pago objPago);
    
    public Aporte getLastAporte(Socio objSocio);
    
    public BigDecimal getSaldoAportes(Socio objSocio);
    
    public List<Aporte> getAllAportes(Socio objSocio);
    
    public List<Aporte> getAllAportesHabilitados(Timestamp fechaDesde, Timestamp fechaHasta);    
    
    public List<Aporte> getAllAportesPagados(Timestamp fechaDesde, Timestamp fechaHasta);
    
    public List<Aporte> getAllAportesPagados(Socio objSocio, Timestamp fechaDesde, Timestamp fechaHasta);
    
    public Aporte getAporteIngresoImpago(Socio objSocio);
    
    public boolean insert(Aporte objAporte);
    
    public boolean update(Aporte objAporte);
    
    public boolean updateAllAportes(Aporte objAporte);
        
    public boolean insertDetallePagoAporte(Pago objPago, Aporte objAporte);
    
    public boolean delete(Aporte objAporte);
    
    public boolean physicalDelete(Aporte objAporte);
}
