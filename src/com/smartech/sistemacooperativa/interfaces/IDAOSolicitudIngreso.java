package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.SolicitudIngreso;
import com.smartech.sistemacooperativa.dominio.TipoSolicitud;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOSolicitudIngreso extends IDAOSolicitud {

    public SolicitudIngreso getSolicitudIngreso(String codigo);

    public SolicitudIngreso getSolicitudIngresoVigente(String dni);

    public SolicitudIngreso getSolicitudIngresoVigente(Socio objSocio);

    public List<SolicitudIngreso> getAllSolicitudesIngreso(Socio objSocio);

    public List<SolicitudIngreso> getAllSolicitudesIngresoPorPagar(Socio objSocio);

    public List<SolicitudIngreso> getAllSolicitudesIngresoPorPagar();

    public List<SolicitudIngreso> getAllSolicitudesIngresoPorAprobarDocumentos(Socio objSocio);

    public List<SolicitudIngreso> getAllSolicitudesIngresoPorAprobarDocumentos();

    public List<SolicitudIngreso> getAllSolicitudesIngreso(Socio objSocio, TipoSolicitud objTipoSolicitud);

    public List<SolicitudIngreso> getAllSolicitudesIngreso(Socio objSocio, TipoSolicitud objTipoSolicitud, boolean aprobado);

    public List<SolicitudIngreso> getAllSolicitudesIngreso(Socio objSocio, TipoSolicitud objTipoSolicitud, boolean aprobado, Timestamp fechaInicio, Timestamp fechaFin);
}
