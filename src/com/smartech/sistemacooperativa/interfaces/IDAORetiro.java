/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Retiro;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAORetiro {

    public Retiro get(long id);
    
    public Retiro get(Pago objPago);
    
    public boolean insert(Retiro objRetiro);

    public boolean insertDetalleRetiro(Retiro objRetiro, Pago objPago);

    public boolean update(Retiro objRetiro);
    
    public boolean logicalDeleteRetiro(Retiro objRetiro);

    public boolean deletePhysicallyRetiro(Retiro objRetiro);

    public boolean deletePhysicallyAllDetalleRetiroByRetiro(Retiro objRetiro);

    public List<Retiro> getAllRetiros(Timestamp fechaDesde, Timestamp fechaHasta);

    public List<Retiro> getAllRetirosHabilitados(Timestamp fechaDesde, Timestamp fechaHasta);

    public List<Retiro> getAllRetirosHabilitados(Cuenta objCuenta, Timestamp fechaDesde, Timestamp fechaHasta);

    public List<Retiro> getAllRetirosPagados(Timestamp fechaDesde, Timestamp fechaHasta);

    public List<Retiro> getAllRetirosPagados(Cuenta objCuenta, Timestamp fechaDesde, Timestamp fechaHasta);
}
