package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.EstadoCivil;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOEstadoCivil {
    
    public EstadoCivil getEstadoCivil(int id);
    
    public List<EstadoCivil> getAllEstadosCiviles();
}
