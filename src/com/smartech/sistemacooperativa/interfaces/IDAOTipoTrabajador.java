/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.interfaces;

import com.smartech.sistemacooperativa.dominio.TipoTrabajador;
import java.util.List;

/**
 *
 * @author Smartech
 */
public interface IDAOTipoTrabajador {
 
    public TipoTrabajador getTipoTrabajador(int id);
    
    public List<TipoTrabajador> getAllTiposTrabajador();
}
