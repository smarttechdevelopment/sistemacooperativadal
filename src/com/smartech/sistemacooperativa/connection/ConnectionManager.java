package com.smartech.sistemacooperativa.connection;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Usuario
 */
public class ConnectionManager {

    private static boolean started = false;
    private static Connection CONNECTION = null;

    public static Connection getConnection() {
        try {
            if (CONNECTION == null || CONNECTION.isClosed()) {
                Class.forName("org.postgresql.Driver");
                CONNECTION = DriverManager.getConnection("jdbc:postgresql://"
                        + java.util.ResourceBundle.getBundle("com/smartech/sistemacooperativa/connection/config/PostgreSQLConnection").getString("server")
                        + ":"
                        + java.util.ResourceBundle.getBundle("com/smartech/sistemacooperativa/connection/config/PostgreSQLConnection").getString("port")
                        + "/"
                        + java.util.ResourceBundle.getBundle("com/smartech/sistemacooperativa/connection/config/PostgreSQLConnection").getString("database"),
                        java.util.ResourceBundle.getBundle("com/smartech/sistemacooperativa/connection/config/PostgreSQLConnection").getString("user"),
                        java.util.ResourceBundle.getBundle("com/smartech/sistemacooperativa/connection/config/PostgreSQLConnection").getString("password"));
            }else{
                CONNECTION.setAutoCommit(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return CONNECTION;
    }

    public static void beginTransaction() {
        try {
            getConnection();
            CONNECTION.setAutoCommit(false);
            started = true;
            System.out.println("Transaction begun!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void commitTransaction() {
        try {
            if (started) {
                getConnection();
                CONNECTION.commit();
                CONNECTION.setAutoCommit(true);
                started = false;
                System.out.println("Transaction commited!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void rollbackTransaction() {
        try {
            if (started) {
                getConnection();
                CONNECTION.rollback();
                CONNECTION.setAutoCommit(true);
                started = false;
                System.out.println("Transaction rolled back!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
