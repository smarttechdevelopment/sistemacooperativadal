package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Aprobacion;
import com.smartech.sistemacooperativa.dominio.Solicitud;
import com.smartech.sistemacooperativa.dominio.SolicitudIngreso;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.interfaces.IDAOAprobacion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOAprobacion implements IDAOAprobacion {

    private static DAOAprobacion instance = null;

    private DAOAprobacion() {
    }

    public static DAOAprobacion getInstance() {
        if (instance == null) {
            instance = new DAOAprobacion();
        }

        return instance;
    }

    @Override
    public List<Aprobacion> getAllAprobaciones(Solicitud objSolicitud) {
        List<Aprobacion> lstAprobaciones = new ArrayList<>();
        List<Integer> lstIdsTiposUsuario = new ArrayList<>();
        List<Integer> lstIdsUsuarios = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT a.id, a.aprobado, a.idtipousuario, a.idusuario, a." + objSolicitud.getForeignKeyName() + " "
                + "FROM " + objSolicitud.getAprobacionTableName() + " a "
                + "WHERE " + objSolicitud.getForeignKeyName() + " = ?;")) {
            preparedStatement.setLong(1, objSolicitud.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsTiposUsuario.add(resultSet.getInt("idtipousuario"));
                lstIdsUsuarios.add(resultSet.getInt("idusuario"));
                Aprobacion objAprobacion = new Aprobacion(
                        resultSet.getLong("id"),
                        resultSet.getBoolean("aprobado"),
                        objSolicitud,
                        null,
                        null);
                lstAprobaciones.add(objAprobacion);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstAprobaciones.isEmpty()) {
            DAOTipoUsuario objDAOTipoUsuario = DAOTipoUsuario.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();

            int size = lstAprobaciones.size();
            for (int i = 0; i < size; i++) {
                lstAprobaciones.get(i).setObjTipoUsuario(objDAOTipoUsuario.getTipoUsuario(lstIdsTiposUsuario.get(i)));
                lstAprobaciones.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
            }
        }

        return lstAprobaciones;
    }

    @Override
    public boolean insert(Aprobacion objAprobacion) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO " + objAprobacion.getObjSolicitud().getAprobacionTableName() + "(aprobado, idtipousuario, idusuario, " + objAprobacion.getObjSolicitud().getForeignKeyName() + ") "
                + "VALUES(?, ?, ?, ?);")) {
            preparedStatement.setBoolean(1, objAprobacion.isAprobado());
            preparedStatement.setInt(2, objAprobacion.getObjTipoUsuario().getId());
            if (objAprobacion.getObjUsuario() == null) {
                preparedStatement.setNull(3, Types.BIGINT);
            } else {
                preparedStatement.setLong(3, objAprobacion.getObjUsuario().getId());
            }
            preparedStatement.setLong(4, objAprobacion.getObjSolicitud().getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean update(Aprobacion objAprobacion) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE " + objAprobacion.getObjSolicitud().getAprobacionTableName() + " SET aprobado = ?, idtipousuario = ?, idusuario = ?, " + objAprobacion.getObjSolicitud().getForeignKeyName() + " = ? "
                + "WHERE id = ?;")) {
            preparedStatement.setBoolean(1, objAprobacion.isAprobado());
            preparedStatement.setInt(2, objAprobacion.getObjTipoUsuario().getId());
            if (objAprobacion.getObjUsuario() == null) {
                preparedStatement.setLong(3, Types.BIGINT);
            } else {
                preparedStatement.setLong(3, objAprobacion.getObjUsuario().getId());
            }
            preparedStatement.setLong(4, objAprobacion.getObjSolicitud().getId());
            preparedStatement.setLong(5, objAprobacion.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean physicalDelete(Solicitud objSolicitud) {
        boolean result = false;

        String table = "";
        String foreignKey = "";
        if (objSolicitud instanceof SolicitudIngreso) {
            table = "aprobacionesingreso";
            foreignKey = "idsolicitudingreso";
        }
        if (objSolicitud instanceof SolicitudPrestamo) {
            table = "aprobacionesprestamo";
            foreignKey = "idsolicitudprestamo";
        }

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM " + table + " "
                + "WHERE " + foreignKey + " = ?;")) {
            preparedStatement.setLong(1, objSolicitud.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
