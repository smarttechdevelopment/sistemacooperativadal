package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.TipoTarjeta;
import com.smartech.sistemacooperativa.interfaces.IDAOTipoTarjeta;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAOTipoTarjeta implements IDAOTipoTarjeta{
    
    private static DAOTipoTarjeta instance = null;
    private Map<Integer, TipoTarjeta> cache;
    
    private DAOTipoTarjeta(){
        this.cache = new HashMap<>();
    }
    
    public static DAOTipoTarjeta getInstance(){
        if(instance == null){
            instance = new DAOTipoTarjeta();
        }
        
        return instance;
    }

    @Override
    public TipoTarjeta getTipoTarjeta(int id) {
        TipoTarjeta objTipoTarjeta = this.cache.get(id);
        
        if(objTipoTarjeta == null){
            this.cache.clear();
            this.getAllTiposTarjeta();
            objTipoTarjeta = this.cache.get(id);
        }
        
        return objTipoTarjeta;
    }

    @Override
    public List<TipoTarjeta> getAllTiposTarjeta() {
        List<TipoTarjeta> lstTiposTarjeta;
        
        if(this.cache.isEmpty()){
            try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT tt.id, tt.nombre "
                    + "FROM tipostarjeta tt;")){
                
                ResultSet resultSet = preparedStatement.executeQuery();
                
                while(resultSet.next()){
                    TipoTarjeta objTipoTarjeta = new TipoTarjeta(
                            resultSet.getInt("id"),
                            resultSet.getString("nombre"));
                    this.cache.put(objTipoTarjeta.getId(), objTipoTarjeta);
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        
        Collection collection = this.cache.values();
        
        lstTiposTarjeta = new ArrayList<>(collection);
        
        return lstTiposTarjeta;
    }
}
