package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.TasaInteres;
import com.smartech.sistemacooperativa.dominio.TipoInteres;
import com.smartech.sistemacooperativa.interfaces.IDAOTasaInteres;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOTasaInteres implements IDAOTasaInteres {

    private static DAOTasaInteres instance = null;

    private DAOTasaInteres() {
    }

    public static DAOTasaInteres getInstance() {
        if (instance == null) {
            instance = new DAOTasaInteres();
        }

        return instance;
    }

    @Override
    public TasaInteres getTasaInteres(int id) {
        TasaInteres objTasaInteres = null;
        int idTipoInteres = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT ti.id, ti.nombre, ti.estado, ti.tasa, ti.tea, ti.tim, ti.montoplazofijo, ti.fecharegistro, ti.fechamodificacion, ti.idtipointeres "
                + "FROM tasasinteres ti "
                + "WHERE id = ?;")) {

            preparedStatement.setInt(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idTipoInteres = resultSet.getInt("idtipointeres");
                objTasaInteres = new TasaInteres(
                        resultSet.getInt("id"),
                        resultSet.getString("nombre"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBigDecimal("tasa"),
                        resultSet.getBigDecimal("tea"),
                        resultSet.getBigDecimal("tim"),
                        resultSet.getBigDecimal("montoplazofijo"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objTasaInteres != null) {
            objTasaInteres.setObjTipoInteres(DAOTipoInteres.getInstance().getTipoInteres(idTipoInteres));
        }

        return objTasaInteres;
    }

    @Override
    public TasaInteres getTasaInteres(SolicitudPrestamo objSolicitudPrestamo) {
        TasaInteres objTasaInteres = null;
        int idTipoInteres = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT ti.id, ti.nombre, ti.estado, ti.tasa, ti.tea, ti.tim, ti.montoplazofijo, ti.fecharegistro, ti.fechamodificacion, ti.idtipointeres "
                + "FROM tasasinteres ti "
                + "WHERE ti.id = (SELECT MAX(tisp.idtasainteres) idtasainteres "
                + "FROM tasainteressolicitudprestamo tisp "
                + "WHERE idsolicitudprestamo = ?);")) {
            preparedStatement.setLong(1, objSolicitudPrestamo.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idTipoInteres = resultSet.getInt("idtipointeres");

                objTasaInteres = new TasaInteres(
                        resultSet.getInt("id"),
                        resultSet.getString("nombre"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBigDecimal("tasa"),
                        resultSet.getBigDecimal("tea"),
                        resultSet.getBigDecimal("tim"),
                        resultSet.getBigDecimal("montoplazofijo"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objTasaInteres != null) {
            objTasaInteres.setObjTipoInteres(DAOTipoInteres.getInstance().getTipoInteres(idTipoInteres));
        }

        return objTasaInteres;
    }

    @Override
    public TasaInteres getTasaInteres(TipoInteres objTipoInteres) {
        TasaInteres objTasaInteres = null;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT ti.id , ti.nombre, ti.estado, ti.tasa, ti.tea, ti.tim, ti.montoplazofijo, ti.fecharegistro, ti.fechamodificacion "
                + " FROM tasasinteres ti "
                + " WHERE ti.idtipointeres = ?"
                + " ORDER BY ti.id desc"
                + " LIMIT 1;")) {

            preparedStatement.setInt(1, objTipoInteres.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                objTasaInteres = new TasaInteres(
                        resultSet.getInt("id"),
                        resultSet.getString("nombre"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBigDecimal("tasa"),
                        resultSet.getBigDecimal("tea"),
                        resultSet.getBigDecimal("tim"),
                        resultSet.getBigDecimal("montoplazofijo"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        objTipoInteres);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return objTasaInteres;
    }

    @Override
    public List<TasaInteres> getAllTasasIntereses() {
        List<TasaInteres> lstTasasInteres = new ArrayList<>();
        List<Integer> lstIdsTiposInteres = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT ti.id, ti.nombre, ti.estado, ti.tasa, ti.tea, ti.tim, ti.montoplazofijo, ti.fecharegistro, ti.fechamodificacion, ti.idtipointeres "
                + "FROM tasasinteres ti;")) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsTiposInteres.add(resultSet.getInt("idtipointeres"));
                TasaInteres objTasaInteres = new TasaInteres(
                        resultSet.getInt("id"),
                        resultSet.getString("nombre"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBigDecimal("tasa"),
                        resultSet.getBigDecimal("tea"),
                        resultSet.getBigDecimal("tim"),
                        resultSet.getBigDecimal("montoplazofijo"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        null);
                lstTasasInteres.add(objTasaInteres);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstTasasInteres.isEmpty()) {
            DAOTipoInteres objDAOTipoInteres = DAOTipoInteres.getInstance();

            int size = lstTasasInteres.size();
            for (int i = 0; i < size; i++) {
                lstTasasInteres.get(i).setObjTipoInteres(objDAOTipoInteres.getTipoInteres(lstIdsTiposInteres.get(i)));
            }
        }

        return lstTasasInteres;
    }

    @Override
    public List<TasaInteres> getAllTasasIntereses(TipoInteres objTipoInteres) {
        List<TasaInteres> lstTasasInteres = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT ti.id, ti.nombre, ti.estado, ti.tasa, ti.tea, ti.tim, ti.montoplazofijo, ti.fecharegistro, ti.fechamodificacion, ti.idtipointeres "
                + "FROM tasasinteres ti "
                + "WHERE ti.idtipointeres = ?;")) {
            preparedStatement.setInt(1, objTipoInteres.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                TasaInteres objTasaInteres = new TasaInteres(
                        resultSet.getInt("id"),
                        resultSet.getString("nombre"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBigDecimal("tasa"),
                        resultSet.getBigDecimal("tea"),
                        resultSet.getBigDecimal("tim"),
                        resultSet.getBigDecimal("montoplazofijo"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        objTipoInteres);
                lstTasasInteres.add(objTasaInteres);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lstTasasInteres;
    }

    @Override
    public boolean insert(TasaInteres objTasaInteres) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO tasasinteres(nombre, tasa, tea, tim, montoplazofijo, estado, fecharegistro, idtipointeres) "
                + "VALUES(?, ?, ?, ?, ?, true, now(), ?);", PreparedStatement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, objTasaInteres.getNombre());
            preparedStatement.setBigDecimal(2, objTasaInteres.getTasa());
            preparedStatement.setBigDecimal(3, objTasaInteres.getTea());
            preparedStatement.setBigDecimal(4, objTasaInteres.getTim());
            preparedStatement.setBigDecimal(5, objTasaInteres.getMontoPlazoFijo());
            preparedStatement.setInt(6, objTasaInteres.getObjTipoInteres().getId());

            preparedStatement.execute();
            
            ResultSet keySet = preparedStatement.getGeneratedKeys();
            while(keySet.next()){
                objTasaInteres.setId(keySet.getInt(1));
            }

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean update(TasaInteres objTasaInteres) {
        boolean result = false;
        
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO tasasinteres(nombre, tasa, tea, tim, montoplazofijo, estado, fecharegistro, idtipointeres) "
                + "VALUES(?, ?, ?, ?, ?, true, now(), ?);", PreparedStatement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, objTasaInteres.getNombre());
            preparedStatement.setBigDecimal(2, objTasaInteres.getTasa());
            preparedStatement.setBigDecimal(3, objTasaInteres.getTea());
            preparedStatement.setBigDecimal(4, objTasaInteres.getTim());
            preparedStatement.setBigDecimal(5, objTasaInteres.getMontoPlazoFijo());
            preparedStatement.setInt(6, objTasaInteres.getObjTipoInteres().getId());

            preparedStatement.execute();
            
            ResultSet keySet = preparedStatement.getGeneratedKeys();
            while(keySet.next()){
                objTasaInteres.setId(keySet.getInt(1));
            }

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return result;
    }

    @Override
    public boolean delete(TasaInteres objTasaInteres) {
        boolean result = false;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE tasasinteres SET estado = false "
                + "WHERE id = ?;")){
            preparedStatement.setInt(1, objTasaInteres.getId());
            
            preparedStatement.execute();
            
            result = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
    }

    @Override
    public boolean physicalDelete(TasaInteres objTasaInteres) {
        boolean result = false;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM tasasinteres "
                + "WHERE id = ?;")){
            preparedStatement.setInt(1, objTasaInteres.getId());
            
            preparedStatement.execute();
            
            result = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
    }
}
