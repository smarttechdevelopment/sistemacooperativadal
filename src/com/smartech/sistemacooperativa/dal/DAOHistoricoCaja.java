/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Caja;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.HistoricoCaja;
import com.smartech.sistemacooperativa.interfaces.IDAOHistoricoCaja;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

/**
 *
 * @author Smartech
 */
public class DAOHistoricoCaja implements IDAOHistoricoCaja {

    private static DAOHistoricoCaja instance = null;

    private DAOHistoricoCaja() {
    }

    public static DAOHistoricoCaja getInstance() {
        if (instance == null) {
            instance = new DAOHistoricoCaja();
        }

        return instance;
    }

    @Override
    public HistoricoCaja getLastHistoricoCaja(Caja objCaja) {
        HistoricoCaja objHistoricoCaja = null;
        Long idEmpleado = new Long(0);
        
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT hc.id, hc.fechainicio, hc.fechafin, hc.montoinicio, hc.montofin, hc.idcaja, hc.idempleado "
                + "FROM historicocaja hc "
                + "WHERE hc.id = (SELECT MAX(hcsub.id) id FROM historicocaja hcsub WHERE hcsub.idcaja = ?);")) {
            preparedStatement.setInt(1, objCaja.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idEmpleado = resultSet.getLong("idempleado");
                objHistoricoCaja = new HistoricoCaja(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fechainicio"),
                        resultSet.getTimestamp("fechafin"),
                        resultSet.getBigDecimal("montoinicio"),
                        resultSet.getBigDecimal("montofin"),
                        objCaja,
                        null);
            }
            
            if(objHistoricoCaja != null){
                objHistoricoCaja.setObjEmpleado(DAOEmpleado.getInstance().getEmpleado(idEmpleado));
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return objHistoricoCaja;
    }

    @Override
    public HistoricoCaja getLastHistoricoCaja(Empleado objEmpleado) {
        HistoricoCaja objHistoricoCaja = null;
        int idCaja = 0;
        
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT hc.id, hc.fechainicio, hc.fechafin, hc.montoinicio, hc.montofin, hc.idcaja, hc.idempleado "
                + "FROM historicocaja hc "
                + "WHERE hc.id = (SELECT MAX(hcsub.id) id FROM historicocaja hcsub WHERE hcsub.idempleado = ?);")) {
            preparedStatement.setLong(1, objEmpleado.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idCaja = resultSet.getInt("idcaja");
                objHistoricoCaja = new HistoricoCaja(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fechainicio"),
                        resultSet.getTimestamp("fechafin"),
                        resultSet.getBigDecimal("montoinicio"),
                        resultSet.getBigDecimal("montofin"),
                        null,
                        objEmpleado);
            }
            
            if(objHistoricoCaja != null){
                objHistoricoCaja.setObjCaja(DAOCaja.getInstance().getCaja(idCaja));
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        return objHistoricoCaja;
    }

    @Override
    public boolean insert(HistoricoCaja objHistoricoCaja) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO historicocaja(fechainicio, fechafin, montoinicio, montofin, idcaja, idempleado) "
                + "VALUES(?, ?, ?, ?, ?, ?);")) {
            preparedStatement.setTimestamp(1, objHistoricoCaja.getFechaInicio());
            if (objHistoricoCaja.getFechaFin() == null) {
                preparedStatement.setNull(2, Types.TIMESTAMP);
            } else {
                preparedStatement.setTimestamp(2, objHistoricoCaja.getFechaFin());
            }
            preparedStatement.setBigDecimal(3, objHistoricoCaja.getMontoInicio());
            if (objHistoricoCaja.getMontoFin() == null) {
                preparedStatement.setNull(4, Types.NUMERIC);
            } else {
                preparedStatement.setBigDecimal(4, objHistoricoCaja.getMontoFin());
            }
            preparedStatement.setInt(5, objHistoricoCaja.getObjCaja().getId());
            preparedStatement.setLong(6, objHistoricoCaja.getObjEmpleado().getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean update(HistoricoCaja objHistoricoCaja) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE historicocaja SET fechainicio = ?, fechafin = ?, montoinicio = ?, montofin = ?, idcaja = ?, idempleado = ? "
                + "WHERE id = ?;", PreparedStatement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setTimestamp(1, objHistoricoCaja.getFechaInicio());
            if (objHistoricoCaja.getFechaFin() == null) {
                preparedStatement.setNull(2, Types.TIMESTAMP);
            } else {
                preparedStatement.setTimestamp(2, objHistoricoCaja.getFechaFin());
            }
            preparedStatement.setBigDecimal(3, objHistoricoCaja.getMontoInicio());
            if (objHistoricoCaja.getMontoFin() == null) {
                preparedStatement.setNull(4, Types.NUMERIC);
            } else {
                preparedStatement.setBigDecimal(4, objHistoricoCaja.getMontoFin());
            }
            preparedStatement.setInt(5, objHistoricoCaja.getObjCaja().getId());
            preparedStatement.setLong(6, objHistoricoCaja.getObjEmpleado().getId());
            preparedStatement.setLong(7, objHistoricoCaja.getId());

            preparedStatement.execute();

            ResultSet keySet = preparedStatement.getGeneratedKeys();

            while (keySet.next()) {
                objHistoricoCaja.setId(keySet.getLong(1));
            }

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
