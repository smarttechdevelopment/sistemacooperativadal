package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.GradoInstruccion;
import com.smartech.sistemacooperativa.interfaces.IDAOGradoInstruccion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAOGradoInstruccion implements IDAOGradoInstruccion {

    private static DAOGradoInstruccion instance = null;
    private Map<Integer, GradoInstruccion> cache;

    private DAOGradoInstruccion() {
        this.cache = new HashMap<>();
    }

    public static DAOGradoInstruccion getInstance() {
        if (instance == null) {
            instance = new DAOGradoInstruccion();
        }

        return instance;
    }

    @Override
    public GradoInstruccion getGradoInstruccion(int id) {
        GradoInstruccion objGradoInstruccion = this.cache.get(id);

        if (objGradoInstruccion == null) {
            this.cache.clear();
            this.getAllGradosInstruccion();
            objGradoInstruccion = this.cache.get(id);
        }

        return objGradoInstruccion;
    }

    @Override
    public List<GradoInstruccion> getAllGradosInstruccion() {
        List<GradoInstruccion> lstGradosIntruccion;

        if (this.cache.isEmpty()) {
            try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareCall("SELECT gi.id, gi.nombre "
                    + "FROM gradosinstruccion gi;")) {

                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    GradoInstruccion objGradoInstruccion = new GradoInstruccion(
                            resultSet.getInt("id"),
                            resultSet.getString("nombre"));
                    this.cache.put(objGradoInstruccion.getId(), objGradoInstruccion);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Collection collection = this.cache.values();

        lstGradosIntruccion = new ArrayList<>(collection);

        return lstGradosIntruccion;
    }
}
