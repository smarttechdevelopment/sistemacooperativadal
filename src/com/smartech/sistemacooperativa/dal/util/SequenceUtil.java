package com.smartech.sistemacooperativa.dal.util;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Smartech
 */
public class SequenceUtil {
    
    private SequenceUtil(){
    }
    
    public static int getTableSequenceCurrentValue(String tableName){
        int currVal = 0;
        
        String sequenceName = tableName.toLowerCase() + "_id_seq";
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT last_value FROM " + sequenceName)){            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                currVal = resultSet.getInt("last_value");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return currVal;
    }
    
    public static int getTableSequenceNextValue(String tableName){
        return getTableSequenceCurrentValue(tableName) + 1;
    }
}
