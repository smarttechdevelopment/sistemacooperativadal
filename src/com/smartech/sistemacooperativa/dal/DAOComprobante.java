package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Comprobante;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.interfaces.IDAOComprobante;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOComprobante implements IDAOComprobante {

    private static DAOComprobante instance = null;

    private DAOComprobante() {
    }

    public static DAOComprobante getInstance() {
        if (instance == null) {
            instance = new DAOComprobante();
        }

        return instance;
    }

    @Override
    public Comprobante getComprobante(long id) {
        Comprobante objComprobante = null;
        int idEstablecimiento = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT c.id, c.codigo, c.descripcion, c.fecha, c.monto, c.interescuenta, c.idestablecimiento, c.ingreso "
                + "FROM comprobantes c "
                + "WHERE c.id = ?;")) {
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idEstablecimiento = resultSet.getInt("idestablecimiento");
                objComprobante = new Comprobante(
                        id,
                        resultSet.getString("codigo"),
                        resultSet.getString("descripcion"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getBoolean("ingreso"),
                        resultSet.getTimestamp("fecha"),
                        resultSet.getBoolean("interescuenta"),
                        null,
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objComprobante != null) {
            objComprobante.setObjEstablecimiento(DAOEstablecimiento.getInstance().getEstablecimiento(idEstablecimiento));
            objComprobante.setLstPagos(DAOPago.getInstance().getAllPagos(objComprobante));
        }

        return objComprobante;
    }

    @Override
    public Comprobante getComprobante(String codigo) {
        Comprobante objComprobante = null;
        int idEstablecimiento = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT c.id, c.codigo, c.descripcion, c.fecha, c.monto, c.interescuenta, c.idestablecimiento, c.ingreso "
                + "FROM comprobantes c "
                + "WHERE c.codigo = ?;")) {
            preparedStatement.setString(1, codigo);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idEstablecimiento = resultSet.getInt("idestablecimiento");
                objComprobante = new Comprobante(
                        resultSet.getLong("id"),
                        codigo,
                        resultSet.getString("descripcion"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getBoolean("ingreso"),
                        resultSet.getTimestamp("fecha"),
                        resultSet.getBoolean("interescuenta"),
                        null,
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objComprobante != null) {
            objComprobante.setObjEstablecimiento(DAOEstablecimiento.getInstance().getEstablecimiento(idEstablecimiento));
            objComprobante.setLstPagos(DAOPago.getInstance().getAllPagos(objComprobante));
        }

        return objComprobante;
    }

    @Override
    public Comprobante getComprobante(Pago objPago) {
        Comprobante objComprobante = null;
        int idEstablecimiento = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT c.id, c.codigo, c.descripcion, c.fecha, c.monto, c.interescuenta, c.idestablecimiento, c.ingreso "
                + "FROM comprobantes c INNER JOIN detallecomprobantepago dcp ON dcp.idcomprobante = c.id "
                + "INNER JOIN pagos p ON dcp.idpago = p.id "
                + "WHERE p.id = ?;")) {
            preparedStatement.setLong(1, objPago.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idEstablecimiento = resultSet.getInt("idestablecimiento");
                objComprobante = new Comprobante(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        resultSet.getString("descripcion"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getBoolean("ingreso"),
                        resultSet.getTimestamp("fecha"),
                        resultSet.getBoolean("interescuenta"),
                        null,
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objComprobante != null) {
            objComprobante.setObjEstablecimiento(DAOEstablecimiento.getInstance().getEstablecimiento(idEstablecimiento));
            objComprobante.setLstPagos(DAOPago.getInstance().getAllPagos(objComprobante));
        }

        return objComprobante;
    }

    @Override
    public List<Comprobante> getAllComprobantes(Timestamp fechaDesde, Timestamp fechaHasta) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insert(Comprobante objComprobante) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO comprobantes(codigo, descripcion, fecha, monto, interescuenta, idestablecimiento, ingreso) "
                + "VALUES(?, ?, ?, ?, ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, objComprobante.getCodigo());
            preparedStatement.setString(2, objComprobante.getDescripcion());
            preparedStatement.setTimestamp(3, objComprobante.getFecha());
            preparedStatement.setBigDecimal(4, objComprobante.getMonto());
            preparedStatement.setBoolean(5, objComprobante.isInteresCuenta());
            preparedStatement.setInt(6, objComprobante.getObjEstablecimiento().getId());
            preparedStatement.setBoolean(7, objComprobante.isIngreso());

            preparedStatement.execute();

            ResultSet keySet = preparedStatement.getGeneratedKeys();

            while (keySet.next()) {
                objComprobante.setId(keySet.getLong(1));
            }

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean insertDetalleComprobantePago(Comprobante objComprobante) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO detallecomprobantepago(idpago, idcomprobante) "
                + "VALUES(?, ?);")) {
            for (Pago objPago : objComprobante.getLstPagos()) {
                preparedStatement.setLong(1, objPago.getId());
                preparedStatement.setLong(2, objComprobante.getId());

                preparedStatement.execute();
            }

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public long getUltimoCodigo(boolean ingreso) {
        long codigo = 0;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT COALESCE((SELECT MAX(c.id) id FROM comprobantes c WHERE c.ingreso = ?), 1) id;")){
            preparedStatement.setBoolean(1, ingreso);
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                codigo = resultSet.getLong("id");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return codigo;
    }
}
