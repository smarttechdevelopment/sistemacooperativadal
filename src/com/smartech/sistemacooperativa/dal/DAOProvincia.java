/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Departamento;
import com.smartech.sistemacooperativa.dominio.Provincia;
import com.smartech.sistemacooperativa.interfaces.IDAOProvincia;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAOProvincia implements IDAOProvincia {

    private static DAOProvincia instance = null;
    private Map<Integer, Provincia> cache;

    private DAOProvincia() {
        this.cache = new HashMap<>();
    }

    public static DAOProvincia getInstance() {
        if (instance == null) {
            instance = new DAOProvincia();
        }

        return instance;
    }

    @Override
    public Provincia getProvincia(int id) {
        Provincia objProvincia = this.cache.get(id);
        
        if(objProvincia == null){
            this.cache.clear();
            this.getAllProvincias();
            objProvincia = this.cache.get(id);
        }
        
        return objProvincia;
    }

    @Override
    public List<Provincia> getAllProvincias() {
        List<Provincia> lstProvincias;
        Map<Integer, Integer> mapIdsDepartamentos = new HashMap<>();

        if (this.cache.isEmpty()) {
            try (PreparedStatement pst = ConnectionManager.getConnection().prepareStatement("SELECT id, nombre, iddepartamento FROM provincias ORDER BY id;");){
                
                ResultSet rs = pst.executeQuery();
                
                while(rs.next()){
                    Provincia objProvincia = new Provincia(rs.getInt("id"), rs.getString("nombre"), null, new ArrayList<>());
                    mapIdsDepartamentos.put(objProvincia.getId(), rs.getInt("iddepartamento"));
                    this.cache.put(objProvincia.getId(), objProvincia);
                }
                
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!this.cache.isEmpty()) {
                DAODistrito objDAODistrito = DAODistrito.getInstance();
                DAODepartamento objDAODepartamento = DAODepartamento.getInstance();
                for(Map.Entry<Integer, Provincia> entry : this.cache.entrySet()){
                    Provincia objProvincia = entry.getValue();
                    objProvincia.setObjDepartamento(objDAODepartamento.getDepartamento(mapIdsDepartamentos.get(objProvincia.getId())));
                    objProvincia.setLstDistritos(objDAODistrito.getAllDistritos(objProvincia));
                }
            }
        }
        
        Collection<Provincia> collection = this.cache.values();
        
        lstProvincias = new ArrayList<>(collection);

        return lstProvincias;
    }

    @Override
    public List<Provincia> getAllProvincias(Departamento objDepartamento) {
        List<Provincia> lstProvincias = new ArrayList<>();

        if (this.cache.isEmpty()) {
            this.getAllProvincias();
        }

        for (Map.Entry<Integer, Provincia> entry : this.cache.entrySet()) {
            Provincia objProvincia = entry.getValue();
            if (objProvincia.getObjDepartamento().getId() == objDepartamento.getId()) {
                lstProvincias.add(objProvincia);
            }
        }

        return lstProvincias;
    }

}
