/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Departamento;
import com.smartech.sistemacooperativa.interfaces.IDAODepartamento;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAODepartamento implements IDAODepartamento{

    private static DAODepartamento instance = null;
    private Map<Integer,Departamento> cache ;
    
    private DAODepartamento(){
        this.cache = new HashMap<>();
    }
    public static DAODepartamento getInstance(){
        if(instance == null){
            instance = new DAODepartamento();
        }
        
        return instance;
    }    
    
    @Override
    public List<Departamento> getAllDepartamentos() {
        List<Departamento> lstDepartamentos;
        
        if(this.cache.isEmpty()){
            PreparedStatement pst = null;
            try{
                pst = ConnectionManager.getConnection().prepareStatement("SELECT id, nombre FROM departamentos ORDER BY id;");
                ResultSet rs = pst.executeQuery();
                while(rs.next()){
                    this.cache.put(rs.getInt("id"), new Departamento(rs.getInt("id"), rs.getString("nombre"), new ArrayList<>()));
                }
                
            }catch(Exception e){
                e.printStackTrace();
            }
            
            if(!this.cache.isEmpty()){
                for(Map.Entry<Integer, Departamento> entry : this.cache.entrySet()){
                    Departamento objDepartamento = entry.getValue();
                    objDepartamento.setLstProvincias(DAOProvincia.getInstance().getAllProvincias(objDepartamento));
                }
            }
        }
        
        Collection<Departamento> collection = this.cache.values();
        
        lstDepartamentos = new ArrayList<>(collection);
        
        return lstDepartamentos;
    }

    @Override
    public Departamento getDepartamento(int id) {
        Departamento objDepartamento = this.cache.get(id);
        
        if(objDepartamento == null){
            this.cache.clear();
            this.getAllDepartamentos();
            objDepartamento = this.cache.get(id);
        }        
        return objDepartamento;
    }
    
}
