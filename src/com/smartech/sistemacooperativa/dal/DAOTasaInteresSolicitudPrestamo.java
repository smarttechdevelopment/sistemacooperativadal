/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.TasaInteresSolicitudPrestamo;
import com.smartech.sistemacooperativa.interfaces.IDAOTasaInteresSolicitudPrestamo;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Smartech
 */
public class DAOTasaInteresSolicitudPrestamo implements IDAOTasaInteresSolicitudPrestamo{
    
    private static DAOTasaInteresSolicitudPrestamo instance = null;
    
    private DAOTasaInteresSolicitudPrestamo(){
    }
    
    public static DAOTasaInteresSolicitudPrestamo getInstance(){
        if(instance == null){
            instance = new DAOTasaInteresSolicitudPrestamo();
        }
        
        return instance;
    }
    
    @Override
    public boolean insert(TasaInteresSolicitudPrestamo objTasaInteresSolicitudPrestamo) {
        boolean result = false;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO tasainteressolicitudprestamo(fecharegistro, estado, idtasainteres, idsolicitudprestamo) "
                + "VALUES(?, ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS)){
            preparedStatement.setTimestamp(1, objTasaInteresSolicitudPrestamo.getFechaRegistro());
            preparedStatement.setBoolean(2, true);
            preparedStatement.setInt(3, objTasaInteresSolicitudPrestamo.getObjTasaInteres().getId());
            preparedStatement.setLong(4, objTasaInteresSolicitudPrestamo.getObjSolicitudPrestamo().getId());
            
            preparedStatement.execute();
            
            ResultSet keySet = preparedStatement.getGeneratedKeys();
            
            while(keySet.next()){
                objTasaInteresSolicitudPrestamo.setId(keySet.getLong(1));
            }
            
            result = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
    }

    @Override
    public boolean updateTasasIntereses(long oldTasaInteresId, long newTasaInteresId) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO tasainteressolicitudprestamo(idsolicitudprestamo, fecharegistro, estado, idtasainteres)"
                + "SELECT sp.id, now(), true, ? FROM solicitudesprestamo sp INNER JOIN tasainteressolicitudprestamo tisp ON sp.id = tisp.idsolicitudprestamo WHERE tisp.idtasainteres = ? AND sp.aprobado is false AND sp.estado is true;")) {
            preparedStatement.setLong(1, newTasaInteresId);
            preparedStatement.setLong(2, oldTasaInteresId);

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean physicalDelete(SolicitudPrestamo objSolicitudPrestamo) {
        boolean result = false;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM tasainteressolicitudprestamo WHERE idsolicitudprestamo = ?;")){
            preparedStatement.setLong(1, objSolicitudPrestamo.getId());
            
            preparedStatement.execute();
            
            result = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
    }
}
