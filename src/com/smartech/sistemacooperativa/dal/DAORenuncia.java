/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Renuncia;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.SolicitudRetiroSocio;
import com.smartech.sistemacooperativa.interfaces.IDAORenuncia;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

/**
 *
 * @author Smartech
 */
public class DAORenuncia implements IDAORenuncia {

    private static DAORenuncia instance = null;

    private DAORenuncia() {
    }

    public static DAORenuncia getInstance() {
        if (instance == null) {
            instance = new DAORenuncia();
        }
        return instance;
    }

    @Override
    public boolean insert(Renuncia objRenuncia) {
        boolean result = false;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO renuncias (fecharegistro,fechamodificacion,estado,idusuario,habilitado,pagado,idpago,monto,idsolicitudretirosocio) "
                + "VALUES (?,?,?,?,?,?,?,?,?);", PreparedStatement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setTimestamp(1, objRenuncia.getFechaRegistro());
            if (objRenuncia.getFechaModificacion() != null) {
                preparedStatement.setTimestamp(2, objRenuncia.getFechaModificacion());
            } else {
                preparedStatement.setNull(2, Types.TIMESTAMP);
            }
            preparedStatement.setBoolean(3, objRenuncia.isEstado());
            preparedStatement.setLong(4, objRenuncia.getObjUsuario().getId());
            preparedStatement.setBoolean(5, objRenuncia.isHabilitado());
            preparedStatement.setBoolean(6, objRenuncia.isPagado());
            if (objRenuncia.getObjPago() == null) {
                preparedStatement.setNull(7, Types.BIGINT);
            } else {
                preparedStatement.setLong(7, objRenuncia.getObjPago().getId());
            }
            preparedStatement.setBigDecimal(8, objRenuncia.getMonto());
            preparedStatement.setLong(9, objRenuncia.getObjSolicitudRetiroSocio().getId());

            preparedStatement.execute();
            ResultSet keySet = preparedStatement.getGeneratedKeys();
            while (keySet.next()) {
                objRenuncia.setId(keySet.getLong("id"));
            }
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean update(Renuncia objRenuncia) {
        boolean result = false;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE renuncias "
                + "SET fecharegistro = ?, "
                + "fechamodificacion = ?, "
                + "estado = ?, "
                + "idusuario = ?, "
                + "habilitado = ?, "
                + "pagado = ?, "
                + "idpago = ?, "
                + "monto = ?, "
                + "idsolicitudretirosocio = ? "
                + "WHERE id = ?;")) {

            preparedStatement.setTimestamp(1, objRenuncia.getFechaRegistro());
            if (objRenuncia.getFechaModificacion() != null) {
                preparedStatement.setTimestamp(2, objRenuncia.getFechaModificacion());
            } else {
                preparedStatement.setNull(2, Types.TIMESTAMP);
            }
            preparedStatement.setBoolean(3, objRenuncia.isEstado());
            preparedStatement.setLong(4, objRenuncia.getObjUsuario().getId());
            preparedStatement.setBoolean(5, objRenuncia.isHabilitado());
            preparedStatement.setBoolean(6, objRenuncia.isPagado());
            preparedStatement.setLong(7, objRenuncia.getObjPago().getId());
            preparedStatement.setBigDecimal(8, objRenuncia.getMonto());
            preparedStatement.setLong(9, objRenuncia.getObjSolicitudRetiroSocio().getId());
            preparedStatement.setLong(10, objRenuncia.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Renuncia getRenuncia(Socio objSocio) {
        Renuncia objRenuncia = null;
        long idpago = 0;
        long idUsuario = 0;
        long idSolicitudRetiroSocio = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT r.id, r.fecharegistro, r.fechamodificacion, r.estado, r.idusuario, r.habilitado, r.pagado, r.idpago, r.monto, r.idsolicitudretirosocio "
                + "FROM renuncias r "
                + "INNER JOIN solicitudesretirosocio srs on srs.id = r.idsolicitudretirosocio "
                + "WHERE srs.idsocio = ?;")) {

            preparedStatement.setLong(1, objSocio.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idpago = resultSet.getLong("idpago");
                idUsuario = resultSet.getLong("idusuario");
                idSolicitudRetiroSocio = resultSet.getLong("idsolicitudretirosocio");

                objRenuncia = new Renuncia(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        null,
                        null,
                        null
                );
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objRenuncia != null) {
            objRenuncia.setObjPago(DAOPago.getInstance().getPago(idpago));
            objRenuncia.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objRenuncia.setObjSolicitudRetiroSocio((SolicitudRetiroSocio) DAOSolicitudRetiroSocio.getInstance().getSolicitud(idSolicitudRetiroSocio));
        }

        return objRenuncia;
    }

}
