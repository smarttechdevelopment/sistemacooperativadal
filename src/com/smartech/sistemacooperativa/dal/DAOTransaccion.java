package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Concepto;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Cuota;
import com.smartech.sistemacooperativa.dominio.Deposito;
import com.smartech.sistemacooperativa.dominio.HabilitacionCuotas;
import com.smartech.sistemacooperativa.dominio.MovimientoAdministrativo;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Retiro;
import com.smartech.sistemacooperativa.dominio.ITransaccion;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.Transferencia;
import com.smartech.sistemacooperativa.dominio.TransferenciaCuenta;
import com.smartech.sistemacooperativa.interfaces.IDAOTransaccion;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOTransaccion implements IDAOTransaccion {

    private static DAOTransaccion instance = null;

    private DAOTransaccion() {
    }

    public static DAOTransaccion getInstance() {
        if (instance == null) {
            instance = new DAOTransaccion();
        }

        return instance;
    }

    @Override
    public boolean insert(ITransaccion objTransaccion) {
        if (objTransaccion instanceof Aporte) {
            return DAOAporte.getInstance().insert((Aporte) objTransaccion);
        } else if (objTransaccion instanceof Retiro) {
            return DAORetiro.getInstance().insert((Retiro) objTransaccion);
        } else if (objTransaccion instanceof Deposito) {
            return DAODeposito.getInstance().insert((Deposito) objTransaccion);
        } else if (objTransaccion instanceof Transferencia) {
            if (objTransaccion instanceof TransferenciaCuenta) {
                return DAOTransferenciaCuenta.getInstance().insert((TransferenciaCuenta) objTransaccion);
            }
        }
        return false;
    }

    @Override
    public boolean update(ITransaccion objTransaccion) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ITransaccion getTransaccion(long id, Concepto objConcepto) {
        ITransaccion objTransaccion = null;

        try {
            switch (objConcepto.getId()) {
                case Concepto.CONCEPTO_APORTES:
                    objTransaccion = DAOAporte.getInstance().getAporte(id);
                    break;
                case Concepto.CONCEPTO_DEPOSITOS:
                    objTransaccion = DAODeposito.getInstance().getDeposito(id);
                    break;
                case Concepto.CONCEPTO_TRANSFERENCIAS:
                    break;
                case Concepto.CONCEPTO_RETIROS_CUENTA:
                    objTransaccion = DAORetiro.getInstance().get(id);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return objTransaccion;
    }

    @Override
    public ITransaccion getTransaccion(Pago objPago) {
        ITransaccion objTransaccion = null;

        try {
            switch (objPago.getObjConcepto().getId()) {
                case Concepto.CONCEPTO_APORTES:
                    objTransaccion = DAOAporte.getInstance().getAporte(objPago);
                    break;
                case Concepto.CONCEPTO_DEPOSITOS:
                    objTransaccion = DAODeposito.getInstance().getDeposito(objPago);
                    break;
                case Concepto.CONCEPTO_TRANSFERENCIAS:
                    objTransaccion = DAOTransferencia.getInstance().getTransferencia(objPago);
                    break;
                case Concepto.CONCEPTO_RETIROS_CUENTA:
                    objTransaccion = DAORetiro.getInstance().get(objPago);
                    break;
                case Concepto.CONCEPTO_CUOTAS:
                    objTransaccion = DAOHabilitacionCuotas.getInstance().getHabilitacionCuotas(objPago);
                    break;
                case Concepto.CONCEPTO_INGRESO_CAJA_ADMINISTRATIVO:
                    objTransaccion = DAOMovimientoAdministrativo.getInstance().getMovimientoAdministrativo(objPago);
                    break;
                case Concepto.CONCEPTO_EGRESO_CAJA_ADMINISTRATIVO:
                    objTransaccion = DAOMovimientoAdministrativo.getInstance().getMovimientoAdministrativo(objPago);
                    break;
                case Concepto.CONCEPTO_SOLICITUD_PRESTAMO:
                    objTransaccion = DAOSolicitudPrestamo.getInstance().getSolicitudPrestamo(objPago);
                    break;
                case Concepto.CONCEPTO_RETIROS_SOCIO:
                    objTransaccion = DAOAporte.getInstance().getAporte(objPago);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return objTransaccion;
    }

    @Override
    public List<ITransaccion> getAllTransacciones(Concepto objConcepto, Timestamp fechaDesde, Timestamp fechaHasta, int modo) {
        List<ITransaccion> lstTransacciones = new ArrayList<>();

        try {
            switch (objConcepto.getId()) {
                case Concepto.CONCEPTO_APORTES:
                    List<Aporte> lstAportes = new ArrayList<>();
                    if (modo == ITransaccion.HABILITADO) {
                        lstAportes = DAOAporte.getInstance().getAllAportesHabilitados(fechaDesde, fechaHasta);
                        int size = lstAportes.size();
                        for (int i = 0; i < size; i++) {
                            if (lstAportes.get(i).isRetiro()) {
                                lstAportes.remove(i);
                                size--;
                                i--;
                            }
                        }
                    }
                    if (modo == ITransaccion.PAGADO) {
                        lstAportes = DAOAporte.getInstance().getAllAportesPagados(fechaDesde, fechaHasta);
                        int size = lstAportes.size();
                        for (int i = 0; i < size; i++) {
                            if (lstAportes.get(i).isRetiro()) {
                                lstAportes.remove(i);
                                size--;
                                i--;
                            }
                        }
                    }
                    lstTransacciones.addAll(lstAportes);
                    break;
                case Concepto.CONCEPTO_DEPOSITOS:
                    List<Deposito> lstDepositos = new ArrayList<>();
                    if (modo == ITransaccion.HABILITADO) {
                        lstDepositos = DAODeposito.getInstance().getAllDepositosHabilitados(fechaDesde, fechaHasta);
                    }
                    if (modo == ITransaccion.PAGADO) {
                        lstDepositos = DAODeposito.getInstance().getAllDepositosPagados(fechaDesde, fechaHasta);
                    }
                    lstTransacciones.addAll(lstDepositos);
                    break;
                case Concepto.CONCEPTO_TRANSFERENCIAS:
                    lstTransacciones.addAll(DAOTransferencia.getInstance().getAllTransferencias(fechaDesde, fechaHasta));
                    break;
                case Concepto.CONCEPTO_RETIROS_CUENTA:
                    List<Retiro> lstRetiros = new ArrayList<>();
                    if (modo == ITransaccion.HABILITADO) {
                        lstRetiros = DAORetiro.getInstance().getAllRetirosHabilitados(fechaDesde, fechaHasta);
                    }
                    if (modo == ITransaccion.PAGADO) {
                        lstRetiros = DAORetiro.getInstance().getAllRetirosPagados(fechaDesde, fechaHasta);
                    }

                    lstTransacciones.addAll(lstRetiros);
                    break;
                case Concepto.CONCEPTO_CUOTAS:
                    List<HabilitacionCuotas> lstHabilitacionesCuotas = new ArrayList<>();
                    if (modo == ITransaccion.HABILITADO) {
                        lstHabilitacionesCuotas = DAOHabilitacionCuotas.getInstance().getAllHabilitacionesCuotasHabilitados(fechaDesde, fechaHasta);
                    }
                    if (modo == ITransaccion.PAGADO) {
                        lstHabilitacionesCuotas = DAOHabilitacionCuotas.getInstance().getAllHabilitacionesCuotasPagados(fechaDesde, fechaHasta);
                    }

                    lstTransacciones.addAll(lstHabilitacionesCuotas);
                    break;
                case Concepto.CONCEPTO_RETIROS_SOCIO:
                    lstAportes = new ArrayList<>();
                    if (modo == ITransaccion.HABILITADO) {
                        lstAportes = DAOAporte.getInstance().getAllAportesHabilitados(fechaDesde, fechaHasta);
                        int size = lstAportes.size();
                        for (int i = 0; i < size; i++) {
                            if (!lstAportes.get(i).isRetiro()) {
                                lstAportes.remove(i);
                                size--;
                                i--;
                            }
                        }
                    }
                    if (modo == ITransaccion.PAGADO) {
                        lstAportes = DAOAporte.getInstance().getAllAportesPagados(fechaDesde, fechaHasta);
                        int size = lstAportes.size();
                        for (int i = 0; i < size; i++) {
                            if (!lstAportes.get(i).isRetiro()) {
                                lstAportes.remove(i);
                                size--;
                                i--;
                            }
                        }
                    }
                    lstTransacciones.addAll(lstAportes);
                    break;
                case Concepto.CONCEPTO_INGRESO_CAJA_ADMINISTRATIVO:
                    List<MovimientoAdministrativo> lstMovimientosAdministrativosIngreso = new ArrayList<>();
                    if (modo == ITransaccion.HABILITADO) {
                        lstMovimientosAdministrativosIngreso = DAOMovimientoAdministrativo.getInstance().getAllMovimientosAdministrativosHabilitados(fechaDesde, fechaHasta, true);
                    }
                    if (modo == ITransaccion.PAGADO) {
                        lstMovimientosAdministrativosIngreso = DAOMovimientoAdministrativo.getInstance().getAllMovimientosAdministrativosPagados(fechaDesde, fechaHasta, true);
                    }

                    lstTransacciones.addAll(lstMovimientosAdministrativosIngreso);
                    break;
                case Concepto.CONCEPTO_EGRESO_CAJA_ADMINISTRATIVO:
                    List<MovimientoAdministrativo> lstMovimientosAdministrativosEgreso = new ArrayList<>();
                    if (modo == ITransaccion.HABILITADO) {
                        lstMovimientosAdministrativosEgreso = DAOMovimientoAdministrativo.getInstance().getAllMovimientosAdministrativosHabilitados(fechaDesde, fechaHasta, false);
                    }
                    if (modo == ITransaccion.PAGADO) {
                        lstMovimientosAdministrativosEgreso = DAOMovimientoAdministrativo.getInstance().getAllMovimientosAdministrativosPagados(fechaDesde, fechaHasta, false);
                    }

                    lstTransacciones.addAll(lstMovimientosAdministrativosEgreso);
                    break;
                case Concepto.CONCEPTO_SOLICITUD_PRESTAMO:
                    List<SolicitudPrestamo> lstSolicitudesPrestamo = new ArrayList<>();
                    if(modo == ITransaccion.HABILITADO){
                        lstSolicitudesPrestamo = DAOSolicitudPrestamo.getInstance().getAllSolicitudesPrestamoHabilitados(fechaDesde, fechaHasta);
                    }
                    if(modo == ITransaccion.PAGADO){
                        lstSolicitudesPrestamo = DAOSolicitudPrestamo.getInstance().getAllSolicitudesPrestamoPagados(fechaDesde, fechaHasta);
                    }
                    
                    lstTransacciones.addAll(lstSolicitudesPrestamo);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lstTransacciones;
    }

    @Override
    public List<ITransaccion> getAllTransacciones(Cuenta objCuenta, Timestamp fechaDesde, Timestamp fechaHasta, int modo) {
        List<ITransaccion> lstTransacciones = new ArrayList<>();

        try {
            if (modo == ITransaccion.HABILITADO) {
                //faltan aportes
                //faltan transferencias
                lstTransacciones.addAll(DAORetiro.getInstance().getAllRetirosPagados(objCuenta, fechaDesde, fechaHasta));
                lstTransacciones.addAll(DAODeposito.getInstance().getAllDepositosPagados(objCuenta, fechaDesde, fechaHasta));
            }
            if (modo == ITransaccion.PAGADO) {
                //faltan aportes
                //faltan transferencias
                lstTransacciones.addAll(DAORetiro.getInstance().getAllRetirosHabilitados(objCuenta, fechaDesde, fechaHasta));
                lstTransacciones.addAll(DAODeposito.getInstance().getAllDepositosHabilitados(objCuenta, fechaDesde, fechaHasta));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lstTransacciones;
    }
}
