package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Huella;
import com.smartech.sistemacooperativa.dominio.Usuario;
import com.smartech.sistemacooperativa.interfaces.IDAOHuella;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOHuella implements IDAOHuella{

    private static DAOHuella instance = null;

    private DAOHuella() {
    }

    public static DAOHuella getInstance() {
        if (instance == null) {
            instance = new DAOHuella();
        }

        return instance;
    }

    @Override
    public List<Huella> getAllHuellas(Usuario objUsuario) {
        List<Huella> lstHuellas = new ArrayList<>();
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT h.id, h.huella, h.dedo, h.mano, h.idusuario "
                + "FROM huellas h "
                + "WHERE h.idusuario = ?;")){
            preparedStatement.setLong(1, objUsuario.getId());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                Huella objHuella = new Huella(
                        resultSet.getLong("id"),
                        resultSet.getBytes("huella"),
                        resultSet.getString("dedo"),
                        resultSet.getBoolean("mano"));
                lstHuellas.add(objHuella);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return lstHuellas;
    }

    @Override
    public boolean insert(Huella objHuella, Usuario objUsuario) {
        boolean result = false;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO huellas(huella, dedo, mano, idusuario) "
                + "VALUES(?, ?, ?, ?);")){
            preparedStatement.setBytes(1, objHuella.getHuella());
            preparedStatement.setString(2, objHuella.getDedo());
            preparedStatement.setBoolean(3, objHuella.isMano());
            preparedStatement.setLong(4, objUsuario.getId());
            
            preparedStatement.execute();
            
            result = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
    }

    @Override
    public boolean physicalDelete(Usuario objUsuario) {
        boolean result = false;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM huellas WHERE idusuario = ?;")){
            preparedStatement.setLong(1, objUsuario.getId());
            
            preparedStatement.execute();
            
            result = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
    }
}
