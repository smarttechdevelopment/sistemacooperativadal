package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.AccesoSistema;
import com.smartech.sistemacooperativa.dominio.Usuario;
import com.smartech.sistemacooperativa.interfaces.IDAOAccesoSistema;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Smartech
 */
public class DAOAccesoSistema implements IDAOAccesoSistema{
    
    private static DAOAccesoSistema instance = null;
    
    private DAOAccesoSistema(){
    }
    
    public static DAOAccesoSistema getInstance(){
        if(instance == null){
            instance = new DAOAccesoSistema();
        }
        
        return instance;
    }

    @Override
    public boolean insert(AccesoSistema objAccesoSistema) {
        boolean result = false;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO accesossistema "
                + "(fecha,idusuario) "
                + "VALUES (now(),?)",PreparedStatement.RETURN_GENERATED_KEYS)){
            
            preparedStatement.setLong(1, objAccesoSistema.getObjUsuario().getId());
            
            preparedStatement.execute();
            
            ResultSet keysSet = preparedStatement.getGeneratedKeys();
            
            while(keysSet.next()){
                objAccesoSistema.setId(keysSet.getLong("id"));
            }
            
            result  = true;
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return result;
    }

    @Override
    public AccesoSistema getLastAccesoSistema(Usuario objUsuario) {
        AccesoSistema objAccesoSistema = null;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT a.id, a.fecha, a.idusuario "
                + "FROM accesossistema a "
                + "WHERE a.id = (SELECT MAX(asub.id) FROM accesossistema asub WHERE asub.idusuario = ?);")){
            preparedStatement.setLong(1, objUsuario.getId());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                objAccesoSistema = new AccesoSistema(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fecha"),
                        objUsuario);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return objAccesoSistema;
    }

    @Override
    public AccesoSistema getLastAccesoSistema() {
        AccesoSistema objAccesoSistema = null;
        long idUsuario = 0;
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT a.id, a.fecha, a.idusuario "
                + "FROM accesossistema a "
                + "ORDER BY a.fecha desc "
                + "LIMIT 1;")){            
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                idUsuario = resultSet.getLong("idusuario");
                objAccesoSistema = new AccesoSistema(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fecha"),
                        null);
            }
            
        }catch(Exception e){
            e.printStackTrace();
        }
        
        if(objAccesoSistema != null){
            objAccesoSistema.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
        }
        
        return objAccesoSistema;
    }
    
}
