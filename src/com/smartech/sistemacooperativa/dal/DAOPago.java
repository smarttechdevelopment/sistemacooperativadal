/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Comprobante;
import com.smartech.sistemacooperativa.dominio.Concepto;
import com.smartech.sistemacooperativa.dominio.Cuota;
import com.smartech.sistemacooperativa.dominio.Deposito;
import com.smartech.sistemacooperativa.dominio.HabilitacionCuotas;
import com.smartech.sistemacooperativa.dominio.MovimientoAdministrativo;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Retiro;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.Transferencia;
import com.smartech.sistemacooperativa.interfaces.IDAOPago;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOPago implements IDAOPago {

    private static DAOPago instance = null;

    private DAOPago() {
    }

    public static DAOPago getInstance() {
        if (instance == null) {
            instance = new DAOPago();
        }

        return instance;
    }

    @Override
    public boolean insert(Pago objPago) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO pagos (fecha, monto, fecharegistro, fechamodificacion, estado, idtipopago, idconcepto, idmoneda) "
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setTimestamp(1, objPago.getFecha());
            preparedStatement.setBigDecimal(2, objPago.getMonto());
            preparedStatement.setTimestamp(3, objPago.getFechaRegistro());
            if (objPago.getFechaModificacion() == null) {
                preparedStatement.setNull(4, Types.TIMESTAMP);
            } else {
                preparedStatement.setTimestamp(4, objPago.getFechaModificacion());
            }
            preparedStatement.setBoolean(5, objPago.isEstado());
            preparedStatement.setInt(6, objPago.getObjTipoPago().getId());
            preparedStatement.setInt(7, objPago.getObjConcepto().getId());
            preparedStatement.setInt(8, objPago.getObjMoneda().getId());

            preparedStatement.execute();

            ResultSet keySet = preparedStatement.getGeneratedKeys();

            while (keySet.next()) {
                objPago.setId(keySet.getLong(1));
            }

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean update(Pago objPago) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE pagos "
                + " SET fecha = ?,"
                + " monto = ?,"
                + " fecharegistro = ?,"
                + " fechamodificacion = ?,"
                + " estado = ?,"
                + " idtipopago = ?,"
                + " idconcepto = ?,"
                + " idmoneda = ? "
                + " WHERE id = ?;")) {

            preparedStatement.setTimestamp(1, objPago.getFecha());
            preparedStatement.setBigDecimal(2, objPago.getMonto());
            preparedStatement.setTimestamp(3, objPago.getFechaRegistro());
            if (objPago.getFechaModificacion() == null) {
                preparedStatement.setNull(4, Types.TIMESTAMP);
            } else {
                preparedStatement.setTimestamp(4, objPago.getFechaModificacion());
            }
            preparedStatement.setBoolean(5, objPago.isEstado());
            preparedStatement.setInt(6, objPago.getObjTipoPago().getId());
            preparedStatement.setInt(7, objPago.getObjConcepto().getId());
            preparedStatement.setInt(8, objPago.getObjMoneda().getId());
            preparedStatement.setLong(9, objPago.getId());

            preparedStatement.execute();

            result = true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean deletePhysicallyPago(Pago objPago) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM pagos WHERE id = ?;")) {
            preparedStatement.setLong(1, objPago.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Pago> getAllPagos(Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Pago> lstPagos = new ArrayList<>();
        List<Integer> lstIdTipoPago = new ArrayList<>();
        List<Integer> lstIdConcepto = new ArrayList<>();
        List<Integer> lstIdMoneda = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id, p.fecha, p.monto, p.fecharegistro, "
                + "p.fechamodificacion, p.estado, p.idtipopago, p.idconcepto, p.idmoneda "
                + "FROM pagos as p INNER JOIN detallepagodeposito as dpd ON dpd.idpago = p.id "
                + "WHERE p.fecharegistro BETWEEN ? AND ?;")) {
            preparedStatement.setTimestamp(1, fechaDesde);
            preparedStatement.setTimestamp(2, fechaHasta);

            ResultSet resultSet = preparedStatement.executeQuery();

            Pago objPago = null;
            while (resultSet.next()) {

                lstIdTipoPago.add(resultSet.getInt("idtipopago"));
                lstIdConcepto.add(resultSet.getInt("idconcepto"));
                lstIdMoneda.add(resultSet.getInt("idmoneda"));

                objPago = new Pago(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fecha"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null);
                lstPagos.add(objPago);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPagos.isEmpty()) {
            DAOTipoPago objDAOTipoPago = DAOTipoPago.getInstance();
            DAOConcepto objDAOConcepto = DAOConcepto.getInstance();
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();

            int size = lstIdTipoPago.size();
            for (int i = 0; i < size; i++) {
                lstPagos.get(i).setObjTipoPago(objDAOTipoPago.getTipoPago(lstIdTipoPago.get(i)));
                lstPagos.get(i).setObjConcepto(objDAOConcepto.getConcepto(lstIdConcepto.get(i)));
                lstPagos.get(i).setObjMoneda(objDAOMoneda.getMoneda(lstIdMoneda.get(i)));
            }
        }

        return lstPagos;
    }

    @Override
    public List<Pago> getAllPagos(Aporte objAporte) {
        List<Pago> lstPagos = new ArrayList<>();
        List<Integer> lstIdTipoPago = new ArrayList<>();
        List<Integer> lstIdConcepto = new ArrayList<>();
        List<Integer> lstIdMoneda = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id,p.fecha,p.monto,p.fecharegistro,p.fechamodificacion,p.estado,p.idtipopago,p.idconcepto, p.idmoneda "
                + "FROM pagos as p INNER JOIN detallepagoaporte as dpa ON dpa.idpago = p.id "
                + "WHERE dpa.idaporte = ?;")) {

            preparedStatement.setLong(1, objAporte.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            Pago objPago = null;
            while (resultSet.next()) {
                lstIdTipoPago.add(resultSet.getInt("idtipopago"));
                lstIdConcepto.add(resultSet.getInt("idconcepto"));
                lstIdMoneda.add(resultSet.getInt("idmoneda"));

                objPago = new Pago(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fecha"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null);
                lstPagos.add(objPago);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPagos.isEmpty()) {
            DAOTipoPago objDAOTipoPago = DAOTipoPago.getInstance();
            DAOConcepto objDAOConcepto = DAOConcepto.getInstance();
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();

            int size = lstIdTipoPago.size();
            for (int i = 0; i < size; i++) {
                lstPagos.get(i).setObjTipoPago(objDAOTipoPago.getTipoPago(lstIdTipoPago.get(i)));
                lstPagos.get(i).setObjConcepto(objDAOConcepto.getConcepto(lstIdConcepto.get(i)));
                lstPagos.get(i).setObjMoneda(objDAOMoneda.getMoneda(lstIdMoneda.get(i)));
            }
        }

        return lstPagos;
    }

    @Override
    public List<Pago> getAllPagos(Deposito objDeposito) {
        List<Pago> lstPagos = new ArrayList<>();
        List<Integer> lstIdTipoPago = new ArrayList<>();
        List<Integer> lstIdConcepto = new ArrayList<>();
        List<Integer> lstIdMoneda = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id,p.fecha,p.monto,p.fecharegistro,p.fechamodificacion,p.estado,p.idtipopago,p.idconcepto, p.idmoneda FROM pagos as p INNER JOIN detallepagodeposito as dpd ON dpd.idpago = p.id WHERE iddeposito = ?;")) {

            preparedStatement.setLong(1, objDeposito.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            Pago objPago = null;
            while (resultSet.next()) {
                lstIdTipoPago.add(resultSet.getInt("idtipopago"));
                lstIdConcepto.add(resultSet.getInt("idconcepto"));
                lstIdMoneda.add(resultSet.getInt("idmoneda"));

                objPago = new Pago(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fecha"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null);
                lstPagos.add(objPago);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPagos.isEmpty()) {
            DAOTipoPago objDAOTipoPago = DAOTipoPago.getInstance();
            DAOConcepto objDAOConcepto = DAOConcepto.getInstance();
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();

            int size = lstIdTipoPago.size();
            for (int i = 0; i < size; i++) {
                lstPagos.get(i).setObjTipoPago(objDAOTipoPago.getTipoPago(lstIdTipoPago.get(i)));
                lstPagos.get(i).setObjConcepto(objDAOConcepto.getConcepto(lstIdConcepto.get(i)));
                lstPagos.get(i).setObjMoneda(objDAOMoneda.getMoneda(lstIdMoneda.get(i)));
            }
        }

        return lstPagos;
    }

    @Override
    public List<Pago> getAllPagos(Retiro objRetiro) {
        List<Pago> lstPagos = new ArrayList<>();
        List<Integer> lstIdTipoPago = new ArrayList<>();
        List<Integer> lstIdConcepto = new ArrayList<>();
        List<Integer> lstIdMoneda = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id,p.fecha,p.monto,p.fecharegistro,p.fechamodificacion,p.estado,p.idtipopago,p.idconcepto, p.idmoneda FROM pagos as p INNER JOIN detallepagoretiro as dpr ON dpr.idpago = p.id WHERE idretiro = ?;")) {

            preparedStatement.setLong(1, objRetiro.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            Pago objPago = null;
            while (resultSet.next()) {

                lstIdTipoPago.add(resultSet.getInt("idtipopago"));
                lstIdConcepto.add(resultSet.getInt("idconcepto"));
                lstIdMoneda.add(resultSet.getInt("idmoneda"));

                objPago = new Pago(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fecha"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null);
                lstPagos.add(objPago);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPagos.isEmpty()) {
            DAOTipoPago objDAOTipoPago = DAOTipoPago.getInstance();
            DAOConcepto objDAOConcepto = DAOConcepto.getInstance();
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();

            int size = lstIdTipoPago.size();
            for (int i = 0; i < size; i++) {
                lstPagos.get(i).setObjTipoPago(objDAOTipoPago.getTipoPago(lstIdTipoPago.get(i)));
                lstPagos.get(i).setObjConcepto(objDAOConcepto.getConcepto(lstIdConcepto.get(i)));
                lstPagos.get(i).setObjMoneda(objDAOMoneda.getMoneda(lstIdMoneda.get(i)));
            }
        }

        return lstPagos;
    }

    @Override
    public List<Pago> getAllPagos(Transferencia objTransferencia) {
        List<Pago> lstPagos = new ArrayList<>();
        List<Integer> lstIdTipoPago = new ArrayList<>();
        List<Integer> lstIdConcepto = new ArrayList<>();
        List<Integer> lstIdMoneda = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id,p.fecha,p.monto,p.fecharegistro,p.fechamodificacion,p.estado,p.idtipopago,p.idconcepto, p.idmoneda "
                + "FROM pagos p INNER JOIN detallepagotransferencia dpt ON dpt.idpago = p.id "
                + "WHERE dpt.idtransferencia = ?;")) {

            preparedStatement.setLong(1, objTransferencia.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            Pago objPago = null;
            while (resultSet.next()) {
                lstIdTipoPago.add(resultSet.getInt("idtipopago"));
                lstIdConcepto.add(resultSet.getInt("idconcepto"));
                lstIdMoneda.add(resultSet.getInt("idmoneda"));

                objPago = new Pago(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fecha"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null);
                lstPagos.add(objPago);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPagos.isEmpty()) {
            DAOTipoPago objDAOTipoPago = DAOTipoPago.getInstance();
            DAOConcepto objDAOConcepto = DAOConcepto.getInstance();
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();

            int size = lstIdTipoPago.size();
            for (int i = 0; i < size; i++) {
                lstPagos.get(i).setObjTipoPago(objDAOTipoPago.getTipoPago(lstIdTipoPago.get(i)));
                lstPagos.get(i).setObjConcepto(objDAOConcepto.getConcepto(lstIdConcepto.get(i)));
                lstPagos.get(i).setObjMoneda(objDAOMoneda.getMoneda(lstIdMoneda.get(i)));
            }
        }

        return lstPagos;
    }

    @Override
    public List<Pago> getAllPagos(HabilitacionCuotas objHabilitacionCuotas) {
        List<Pago> lstPagos = new ArrayList<>();
        List<Integer> lstIdTipoPago = new ArrayList<>();
        List<Integer> lstIdConcepto = new ArrayList<>();
        List<Integer> lstIdMoneda = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id,p.fecha,p.monto,p.fecharegistro,p.fechamodificacion,p.estado,p.idtipopago,p.idconcepto, p.idmoneda "
                + "FROM pagos p INNER JOIN detallehabilitacioncuotapago dhcp ON dhcp.idpago = p.id "
                + "WHERE dhcp.idhabilitacioncuota = ?;")) {

            preparedStatement.setLong(1, objHabilitacionCuotas.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            Pago objPago = null;
            while (resultSet.next()) {
                lstIdTipoPago.add(resultSet.getInt("idtipopago"));
                lstIdConcepto.add(resultSet.getInt("idconcepto"));
                lstIdMoneda.add(resultSet.getInt("idmoneda"));

                objPago = new Pago(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fecha"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null);
                lstPagos.add(objPago);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPagos.isEmpty()) {
            DAOTipoPago objDAOTipoPago = DAOTipoPago.getInstance();
            DAOConcepto objDAOConcepto = DAOConcepto.getInstance();
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();

            int size = lstIdTipoPago.size();
            for (int i = 0; i < size; i++) {
                lstPagos.get(i).setObjTipoPago(objDAOTipoPago.getTipoPago(lstIdTipoPago.get(i)));
                lstPagos.get(i).setObjConcepto(objDAOConcepto.getConcepto(lstIdConcepto.get(i)));
                lstPagos.get(i).setObjMoneda(objDAOMoneda.getMoneda(lstIdMoneda.get(i)));
            }
        }

        return lstPagos;
    }

    @Override
    public List<Pago> getAllPagos(MovimientoAdministrativo objMovimientoAdministrativo) {
        List<Pago> lstPagos = new ArrayList<>();
        List<Integer> lstIdTipoPago = new ArrayList<>();
        List<Integer> lstIdConcepto = new ArrayList<>();
        List<Integer> lstIdMoneda = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id,p.fecha,p.monto,p.fecharegistro,p.fechamodificacion,p.estado,p.idtipopago,p.idconcepto, p.idmoneda "
                + "FROM pagos p INNER JOIN detallepagomovimientoadministrativo dpma ON dpma.idpago = p.id "
                + "WHERE dpma.idmovimientoadministrativo = ?;")) {

            preparedStatement.setLong(1, objMovimientoAdministrativo.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            Pago objPago = null;
            while (resultSet.next()) {
                lstIdTipoPago.add(resultSet.getInt("idtipopago"));
                lstIdConcepto.add(resultSet.getInt("idconcepto"));
                lstIdMoneda.add(resultSet.getInt("idmoneda"));

                objPago = new Pago(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fecha"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null);
                lstPagos.add(objPago);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPagos.isEmpty()) {
            DAOTipoPago objDAOTipoPago = DAOTipoPago.getInstance();
            DAOConcepto objDAOConcepto = DAOConcepto.getInstance();
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();

            int size = lstIdTipoPago.size();
            for (int i = 0; i < size; i++) {
                lstPagos.get(i).setObjTipoPago(objDAOTipoPago.getTipoPago(lstIdTipoPago.get(i)));
                lstPagos.get(i).setObjConcepto(objDAOConcepto.getConcepto(lstIdConcepto.get(i)));
                lstPagos.get(i).setObjMoneda(objDAOMoneda.getMoneda(lstIdMoneda.get(i)));
            }
        }

        return lstPagos;
    }

    @Override
    public List<Pago> getAllPagos(SolicitudPrestamo objSolicitudPrestamo) {
        List<Pago> lstPagos = new ArrayList<>();
        List<Integer> lstIdTipoPago = new ArrayList<>();
        List<Integer> lstIdConcepto = new ArrayList<>();
        List<Integer> lstIdMoneda = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id,p.fecha,p.monto,p.fecharegistro,p.fechamodificacion,p.estado,p.idtipopago,p.idconcepto, p.idmoneda "
                + "FROM pagos p INNER JOIN detallepagosolicitudprestamo dpsp ON dpsp.idpago = p.id "
                + "WHERE dpsp.idsolicitudprestamo = ?;")) {

            preparedStatement.setLong(1, objSolicitudPrestamo.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            Pago objPago = null;
            while (resultSet.next()) {
                lstIdTipoPago.add(resultSet.getInt("idtipopago"));
                lstIdConcepto.add(resultSet.getInt("idconcepto"));
                lstIdMoneda.add(resultSet.getInt("idmoneda"));

                objPago = new Pago(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fecha"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null);
                lstPagos.add(objPago);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPagos.isEmpty()) {
            DAOTipoPago objDAOTipoPago = DAOTipoPago.getInstance();
            DAOConcepto objDAOConcepto = DAOConcepto.getInstance();
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();

            int size = lstIdTipoPago.size();
            for (int i = 0; i < size; i++) {
                lstPagos.get(i).setObjTipoPago(objDAOTipoPago.getTipoPago(lstIdTipoPago.get(i)));
                lstPagos.get(i).setObjConcepto(objDAOConcepto.getConcepto(lstIdConcepto.get(i)));
                lstPagos.get(i).setObjMoneda(objDAOMoneda.getMoneda(lstIdMoneda.get(i)));
            }
        }

        return lstPagos;
    }

    @Override
    public Pago getPago(long id) {
        Pago objPago = null;
        int idTipoPago = 0;
        int idConcepto = 0;
        int idMoneda = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id,p.fecha,p.monto,p.fecharegistro,p.fechamodificacion,p.estado,p.idtipopago,p.idconcepto, p.idmoneda "
                + "FROM pagos p "
                + "WHERE id = ?;")) {

            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idTipoPago = resultSet.getInt("idtipopago");
                idConcepto = resultSet.getInt("idconcepto");
                idMoneda = resultSet.getInt("idmoneda");

                objPago = new Pago(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fecha"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objPago != null) {
            objPago.setObjTipoPago(DAOTipoPago.getInstance().getTipoPago(idTipoPago));
            objPago.setObjConcepto(DAOConcepto.getInstance().getConcepto(idConcepto));
            objPago.setObjMoneda(DAOMoneda.getInstance().getMoneda(idMoneda));
        }

        return objPago;
    }

    @Override
    public List<Pago> getAllPagos(Deposito objDeposito, Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Pago> lstPagos = new ArrayList<>();
        List<Integer> lstIdTipoPago = new ArrayList<>();
        List<Integer> lstIdConcepto = new ArrayList<>();
        List<Integer> lstIdMoneda = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id, p.fecha, p.monto, p.fecharegistro, "
                + "p.fechamodificacion, p.estado, p.idtipopago, p.idconcepto, p.idmoneda "
                + "FROM pagos as p INNER JOIN detallepagodeposito as dpd ON dpd.idpago = p.id "
                + "WHERE iddeposito = ? AND p.fecharegistro BETWEEN ? AND ?;")) {
            preparedStatement.setLong(1, objDeposito.getId());
            preparedStatement.setTimestamp(2, fechaDesde);
            preparedStatement.setTimestamp(3, fechaHasta);

            ResultSet resultSet = preparedStatement.executeQuery();

            Pago objPago = null;
            while (resultSet.next()) {
                lstIdTipoPago.add(resultSet.getInt("idtipopago"));
                lstIdConcepto.add(resultSet.getInt("idconcepto"));
                lstIdMoneda.add(resultSet.getInt("idmoneda"));

                objPago = new Pago(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fecha"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null);
                lstPagos.add(objPago);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPagos.isEmpty()) {
            DAOTipoPago objDAOTipoPago = DAOTipoPago.getInstance();
            DAOConcepto objDAOConcepto = DAOConcepto.getInstance();
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();

            int size = lstIdTipoPago.size();
            for (int i = 0; i < size; i++) {
                lstPagos.get(i).setObjTipoPago(objDAOTipoPago.getTipoPago(lstIdTipoPago.get(i)));
                lstPagos.get(i).setObjConcepto(objDAOConcepto.getConcepto(lstIdConcepto.get(i)));
                lstPagos.get(i).setObjMoneda(objDAOMoneda.getMoneda(lstIdMoneda.get(i)));
            }
        }

        return lstPagos;
    }

    @Override
    public List<Pago> getAllPagos(Retiro objRetiro, Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Pago> lstPagos = new ArrayList<>();
        List<Integer> lstIdTipoPago = new ArrayList<>();
        List<Integer> lstIdConcepto = new ArrayList<>();
        List<Integer> lstIdMoneda = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id, p.fecha, p.monto, p.fecharegistro, "
                + "p.fechamodificacion, p.estado, p.idtipopago, p.idconcepto, p.idmoneda "
                + "FROM pagos as p INNER JOIN detallepagoretiro as dpr ON dpr.idpago = p.id "
                + "WHERE idretiro = ? AND fecharegistro BETWEEN ? AND ?;")) {
            preparedStatement.setLong(1, objRetiro.getId());
            preparedStatement.setTimestamp(2, fechaDesde);
            preparedStatement.setTimestamp(3, fechaHasta);

            ResultSet resultSet = preparedStatement.executeQuery();

            Pago objPago = null;
            while (resultSet.next()) {

                lstIdTipoPago.add(resultSet.getInt("idtipopago"));
                lstIdConcepto.add(resultSet.getInt("idconcepto"));
                lstIdMoneda.add(resultSet.getInt("idmoneda"));

                objPago = new Pago(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fecha"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null);
                lstPagos.add(objPago);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPagos.isEmpty()) {
            DAOTipoPago objDAOTipoPago = DAOTipoPago.getInstance();
            DAOConcepto objDAOConcepto = DAOConcepto.getInstance();
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();

            int size = lstIdTipoPago.size();
            for (int i = 0; i < size; i++) {
                lstPagos.get(i).setObjTipoPago(objDAOTipoPago.getTipoPago(lstIdTipoPago.get(i)));
                lstPagos.get(i).setObjConcepto(objDAOConcepto.getConcepto(lstIdConcepto.get(i)));
                lstPagos.get(i).setObjMoneda(objDAOMoneda.getMoneda(lstIdMoneda.get(i)));
            }
        }

        return lstPagos;
    }

    @Override
    public List<Pago> getAllPagos(Aporte objAporte, Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Pago> lstPagos = new ArrayList<>();
        List<Integer> lstIdTipoPago = new ArrayList<>();
        List<Integer> lstIdConcepto = new ArrayList<>();
        List<Integer> lstIdMoneda = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id, p.fecha, p.monto, p.fecharegistro, "
                + "p.fechamodificacion, p.estado, p.idtipopago, p.idconcepto, p.idmoneda "
                + "FROM pagos as p INNER JOIN detallepagoaporte as dpa ON dpa.idpago = p.id "
                + "WHERE idaporte = ? AND fecharegistro BETWEEN ? AND ?;")) {
            preparedStatement.setLong(1, objAporte.getId());
            preparedStatement.setTimestamp(2, fechaDesde);
            preparedStatement.setTimestamp(3, fechaHasta);

            ResultSet resultSet = preparedStatement.executeQuery();

            Pago objPago = null;
            while (resultSet.next()) {

                lstIdTipoPago.add(resultSet.getInt("idtipopago"));
                lstIdConcepto.add(resultSet.getInt("idconcepto"));
                lstIdMoneda.add(resultSet.getInt("idmoneda"));

                objPago = new Pago(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fecha"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null);
                lstPagos.add(objPago);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPagos.isEmpty()) {
            DAOTipoPago objDAOTipoPago = DAOTipoPago.getInstance();
            DAOConcepto objDAOConcepto = DAOConcepto.getInstance();
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();

            int size = lstIdTipoPago.size();
            for (int i = 0; i < size; i++) {
                lstPagos.get(i).setObjTipoPago(objDAOTipoPago.getTipoPago(lstIdTipoPago.get(i)));
                lstPagos.get(i).setObjConcepto(objDAOConcepto.getConcepto(lstIdConcepto.get(i)));
                lstPagos.get(i).setObjMoneda(objDAOMoneda.getMoneda(lstIdMoneda.get(i)));
            }
        }

        return lstPagos;
    }

    @Override
    public List<Pago> getAllPagos(Concepto objConcepto, Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Pago> lstPagos = new ArrayList<>();
        List<Integer> lstIdTipoPago = new ArrayList<>();
        List<Integer> lstIdMoneda = new ArrayList<>();

        String conceptoQuery = "";
        switch (objConcepto.getId()) {
            case 1:
                conceptoQuery = "detallepagoaporte dpa ON dpa.idpago = p.id ";
                break;
            case 2:
                conceptoQuery = "detallepagodeposito dpd ON dpd.idpago = p.id ";
                break;
            case 3:
                conceptoQuery = "detallepagotransferencia dpt ON dpt.idpago = p.id ";
                break;
            case 4:
                conceptoQuery = "detallepagoretiro dpr ON dpr.idpago = p.id ";
                break;
        }

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id, p.fecha, p.monto, p.fecharegistro, "
                + "p.fechamodificacion, p.estado, p.idtipopago, p.idconcepto, p.idmoneda "
                + "FROM pagos p INNER JOIN " + conceptoQuery
                + "WHERE (p.fecharegistro BETWEEN ? AND ?) AND p.idconcepto = ?;")) {
            preparedStatement.setTimestamp(1, fechaDesde);
            preparedStatement.setTimestamp(2, fechaHasta);
            preparedStatement.setInt(3, objConcepto.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            Pago objPago = null;
            while (resultSet.next()) {
                lstIdTipoPago.add(resultSet.getInt("idtipopago"));
                lstIdMoneda.add(resultSet.getInt("idmoneda"));

                objPago = new Pago(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fecha"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        objConcepto,
                        null);
                lstPagos.add(objPago);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPagos.isEmpty()) {
            DAOTipoPago objDAOTipoPago = DAOTipoPago.getInstance();
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();

            int size = lstIdTipoPago.size();
            for (int i = 0; i < size; i++) {
                lstPagos.get(i).setObjTipoPago(objDAOTipoPago.getTipoPago(lstIdTipoPago.get(i)));
                lstPagos.get(i).setObjMoneda(objDAOMoneda.getMoneda(lstIdMoneda.get(i)));
            }
        }

        return lstPagos;
    }

    @Override
    public List<Pago> getAllPagos(Comprobante objComprobante) {
        List<Pago> lstPagos = new ArrayList<>();
        List<Integer> lstIdTipoPago = new ArrayList<>();
        List<Integer> lstIdConcepto = new ArrayList<>();
        List<Integer> lstIdMoneda = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id, p.fecha, p.monto, p.fecharegistro, "
                + "p.fechamodificacion, p.estado, p.idtipopago, p.idconcepto, p.idmoneda "
                + "FROM pagos p INNER JOIN detallecomprobantepago dcp ON dcp.idpago = p.id "
                + "WHERE dcp.idcomprobante = ?;")) {
            preparedStatement.setLong(1, objComprobante.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdTipoPago.add(resultSet.getInt("idtipopago"));
                lstIdConcepto.add(resultSet.getInt("idconcepto"));
                lstIdMoneda.add(resultSet.getInt("idmoneda"));

                Pago objPago = new Pago(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fecha"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null);
                lstPagos.add(objPago);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPagos.isEmpty()) {
            DAOTipoPago objDAOTipoPago = DAOTipoPago.getInstance();
            DAOConcepto objDAOConcepto = DAOConcepto.getInstance();
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();

            int size = lstIdTipoPago.size();
            for (int i = 0; i < size; i++) {
                lstPagos.get(i).setObjTipoPago(objDAOTipoPago.getTipoPago(lstIdTipoPago.get(i)));
                lstPagos.get(i).setObjConcepto(objDAOConcepto.getConcepto(lstIdConcepto.get(i)));
                lstPagos.get(i).setObjMoneda(objDAOMoneda.getMoneda(lstIdMoneda.get(i)));
            }
        }

        return lstPagos;
    }
}
