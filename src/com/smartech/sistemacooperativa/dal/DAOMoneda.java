package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Moneda;
import com.smartech.sistemacooperativa.interfaces.IDAOMoneda;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAOMoneda implements IDAOMoneda {

    private static DAOMoneda instance = null;
    private Map<Integer, Moneda> cache;

    private DAOMoneda() {
        this.cache = new HashMap<>();
    }

    public static DAOMoneda getInstance() {
        if (instance == null) {
            instance = new DAOMoneda();
        }

        return instance;
    }

    @Override
    public Moneda getMoneda(int id) {
        Moneda objMoneda = this.cache.get(id);

        if (objMoneda == null) {
            this.cache.clear();
            this.getAllMonedas();
            objMoneda = this.cache.get(id);
        }

        return objMoneda;
    }

    @Override
    public Moneda getMonedaByAbreviatura(String abreviatura) {
        if (this.cache.values().isEmpty()) {
            this.getAllMonedas();
        }
        for (Moneda objMoneda : this.cache.values()) {
            if (objMoneda.getAbreviatura().equals(abreviatura)) {
                return objMoneda;
            }
        }

        return null;
    }

    @Override
    public Moneda getMonedaBase() {
        if (this.cache.isEmpty()) {
            this.getAllMonedas();
        }
        for (Moneda objMoneda : this.cache.values()) {
            if (objMoneda.isBase()) {
                return objMoneda;
            }
        }
        return null;
    }

    @Override
    public List<Moneda> getAllMonedas() {
        List<Moneda> lstMonedas;

        if (this.cache.isEmpty()) {
            try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT m.id, m.nombre, m.abreviatura, m.simbolo, m.base, MAX(tc.id) idtipocambio, tc.valoractual "
                    + "FROM monedas m INNER JOIN tiposcambio tc ON m.id = tc.idmoneda "
                    + "GROUP BY m.id, m.nombre, m.abreviatura, tc.valoractual, m.simbolo, m.base;")) {

                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    Moneda objMoneda = new Moneda(
                            resultSet.getInt("id"),
                            resultSet.getString("nombre"),
                            resultSet.getString("abreviatura"),
                            resultSet.getString("simbolo"),
                            resultSet.getBigDecimal("valoractual"),
                            resultSet.getBoolean("base"));
                    this.cache.put(objMoneda.getId(), objMoneda);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Collection collection = this.cache.values();

        lstMonedas = new ArrayList<>(collection);

        return lstMonedas;
    }

    @Override
    public List<Moneda> getAllMonedasCambio() {
        List<Moneda> lstMonedasReturn = new ArrayList<>();

        if (this.cache.isEmpty()) {
            this.getAllMonedas();
        }

        List<Moneda> lstMonedas = new ArrayList<>(this.cache.values());
        for (Moneda objMoneda : lstMonedas) {
            if (!objMoneda.isBase()) {
                lstMonedasReturn.add(objMoneda);
            }
        }

        return lstMonedasReturn;
    }

    @Override
    public boolean insert(Moneda objMoneda) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO monedas(nombre, abreviatura, simbolo, base) "
                + "VALUES(?, ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, objMoneda.getNombre());
            preparedStatement.setString(2, objMoneda.getAbreviatura());
            preparedStatement.setString(3, objMoneda.getSimbolo());
            preparedStatement.setBoolean(4, objMoneda.isBase());

            preparedStatement.execute();
            
            ResultSet keySet = preparedStatement.getGeneratedKeys();
            while(keySet.next()){
                objMoneda.setId(keySet.getInt(1));
            }

            this.cache.put(objMoneda.getId(), objMoneda);

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean update(Moneda objMoneda) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE monedas SET nombre = ?, abreviatura = ?, simbolo = ?, base = ? "
                + "WHERE id = ?;")) {
            preparedStatement.setString(1, objMoneda.getNombre());
            preparedStatement.setString(2, objMoneda.getAbreviatura());
            preparedStatement.setString(3, objMoneda.getSimbolo());
            preparedStatement.setBoolean(4, objMoneda.isBase());
            preparedStatement.setInt(5, objMoneda.getId());

            preparedStatement.execute();

            this.cache.replace(objMoneda.getId(), objMoneda);

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean insertTipoCambio(Moneda objMoneda) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO tiposcambio (valoractual, idmoneda) "
                + "VALUES(?, ?);")) {
            preparedStatement.setBigDecimal(1, objMoneda.getValorActual());
            preparedStatement.setInt(2, objMoneda.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean physicalDeleteMoneda(Moneda objMoneda) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM monedas WHERE id = ?;")) {
            preparedStatement.setInt(1, objMoneda.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean physicalDeleteLastTipoCambio(Moneda objMoneda) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM tiposcambio WHERE id = (SELECT MAX(id) FROM tiposcambio tcsub WHERE idmoneda = ?);")) {
            preparedStatement.setInt(1, objMoneda.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
