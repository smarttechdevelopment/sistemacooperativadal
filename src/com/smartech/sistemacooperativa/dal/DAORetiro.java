package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Retiro;
import com.smartech.sistemacooperativa.interfaces.IDAORetiro;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAORetiro implements IDAORetiro {

    private static DAORetiro instance = null;

    private DAORetiro() {
    }

    public static DAORetiro getInstance() {
        if (instance == null) {
            instance = new DAORetiro();
        }

        return instance;
    }

    @Override
    public Retiro get(long id) {

        Retiro objRetiro = null;
        
        long idCuenta = 0;
        long idUsuario = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT r.id, r.monto, r.fecharegistro, r.fechamodificacion, r.estado, r.idcuenta, r.idusuario, r.acreedor, r.habilitado, r.pagado  "
                + " FROM retiros as r "
                + " WHERE r.id = ?;")) {

            preparedStatement.setLong(1, id);
            
            ResultSet resultSet = preparedStatement.executeQuery();

            
            while (resultSet.next()) {

                idCuenta = resultSet.getLong("idcuenta");
                idUsuario = resultSet.getLong("idusuario");

                objRetiro = new Retiro(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,//objCuenta.getObjUsuario(), 
                        null,//objCuenta, 
                        resultSet.getString("acreedor"),
                        new ArrayList<>(),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"));
            }

            if (objRetiro != null) {
                    objRetiro.setObjCuenta(DAOCuenta.getInstance().getCuenta(idCuenta));
                    objRetiro.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
                    objRetiro.setLstPagos(DAOPago.getInstance().getAllPagos(objRetiro));
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return objRetiro;
    }

    @Override
    public Retiro get(Pago objPago) {
        Retiro objRetiro = null;
        
        long idCuenta = 0;
        long idUsuario = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT r.id, r.monto, r.fecharegistro, r.fechamodificacion, r.estado, r.idcuenta, r.idusuario, r.acreedor, r.habilitado, r.pagado  "
                + " FROM retiros as r "
                + " WHERE r.id IN (SELECT dpr.idretiro FROM detallepagoretiro dpr WHERE dpr.idpago = ?);")) {

            preparedStatement.setLong(1, objPago.getId());
            
            ResultSet resultSet = preparedStatement.executeQuery();

            
            while (resultSet.next()) {

                idCuenta = resultSet.getLong("idcuenta");
                idUsuario = resultSet.getLong("idusuario");

                objRetiro = new Retiro(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,//objCuenta.getObjUsuario(), 
                        null,//objCuenta, 
                        resultSet.getString("acreedor"),
                        new ArrayList<>(),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"));
            }

            if (objRetiro != null) {
                    objRetiro.setObjCuenta(DAOCuenta.getInstance().getCuenta(idCuenta));
                    objRetiro.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
                    objRetiro.setLstPagos(DAOPago.getInstance().getAllPagos(objRetiro));
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return objRetiro;
    }
    
    @Override
    public boolean insert(Retiro objRetiro) {
        
        boolean result = false;
        
        try (PreparedStatement objPreparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO retiros (monto,fecharegistro,fechamodificacion,estado,idcuenta,idusuario,acreedor,habilitado,pagado) "
                + "VALUES (?,?,?,?,?,?,?,?,?);",PreparedStatement.RETURN_GENERATED_KEYS)){
            
            objPreparedStatement.setBigDecimal(1, objRetiro.getMonto());
            objPreparedStatement.setTimestamp(2, objRetiro.getFechaRegistro());
            if(objRetiro.getFechaModificacion() == null){
                objPreparedStatement.setNull(3, Types.TIMESTAMP);
            }else{
                objPreparedStatement.setTimestamp(3, objRetiro.getFechaModificacion());
            }
            objPreparedStatement.setBoolean(4, objRetiro.isEstado());
            objPreparedStatement.setLong(5, objRetiro.getObjCuenta().getId());
            objPreparedStatement.setLong(6, objRetiro.getObjUsuario().getId());
            objPreparedStatement.setString(7, objRetiro.getAcreedor());
            objPreparedStatement.setBoolean(8, objRetiro.isHabilitado());
            objPreparedStatement.setBoolean(9, objRetiro.isPagado());
            
            objPreparedStatement.execute();
            
            ResultSet keySet = objPreparedStatement.getGeneratedKeys();
            while(keySet.next()){
                objRetiro.setId(keySet.getLong(1));
            }
            
            result = true;
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean insertDetalleRetiro(Retiro objRetiro, Pago objPago) {
        boolean result = false;
        
        try (PreparedStatement objPreparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO detallepagoretiro (monto,fecharegistro,fechamodificacion,estado,idpago,idretiro) "
                + " VALUES (?,?,?,?,?,?); ")){
            
            objPreparedStatement.setBigDecimal(1, objRetiro.getMonto());
            objPreparedStatement.setTimestamp(2, objRetiro.getFechaRegistro());
            if(objRetiro.getFechaModificacion() == null){
                objPreparedStatement.setNull(3, Types.TIMESTAMP);
            }else{
                objPreparedStatement.setTimestamp(3, objRetiro.getFechaModificacion());
            }
            objPreparedStatement.setBoolean(4, objRetiro.isEstado());
            objPreparedStatement.setLong(5, objPago.getId());
            objPreparedStatement.setLong(6, objRetiro.getId());
            
            objPreparedStatement.execute();
            
            result = true;
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean logicalDeleteRetiro(Retiro objRetiro) {
        boolean result = false;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE retiros "
                + "set estado = false "
                + "WHERE id = ? AND estado is true AND pagado is false;")){
            
            preparedStatement.setLong(1, objRetiro.getId());
            preparedStatement.execute();
            if(preparedStatement.getUpdateCount() > 0){
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean deletePhysicallyAllDetalleRetiroByRetiro(Retiro objRetiro) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM detallepagoretiro WHERE idretiro = ?;")) {
            preparedStatement.setLong(1, objRetiro.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean deletePhysicallyRetiro(Retiro objRetiro) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM retiros WHERE id = ?;")) {
            preparedStatement.setLong(1, objRetiro.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
    
    @Override
    public boolean update(Retiro objRetiro) {
    
        boolean result = false;
        
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE retiros "
                + " SET monto = ?,"
                + " fecharegistro = ?,"
                + " fechamodificacion = ?,"
                + " estado = ?,"
                + " idcuenta = ?,"
                + " idusuario = ?,"
                + " acreedor = ?,"
                + " habilitado = ?,"
                + " pagado = ? "
                + " WHERE id = ?; ")){
            
            preparedStatement.setBigDecimal(1, objRetiro.getMonto());
            preparedStatement.setTimestamp(2, objRetiro.getFechaRegistro());
            if(objRetiro.getFechaModificacion() == null){
                preparedStatement.setNull(3, Types.TIMESTAMP);
            }else{
                preparedStatement.setTimestamp(3, objRetiro.getFechaModificacion());
            }
            preparedStatement.setBoolean(4, objRetiro.isEstado());
            preparedStatement.setLong(5, objRetiro.getObjCuenta().getId());
            preparedStatement.setLong(6, objRetiro.getObjUsuario().getId());
            preparedStatement.setString(7, objRetiro.getAcreedor());
            preparedStatement.setBoolean(8, objRetiro.isHabilitado());
            preparedStatement.setBoolean(9, objRetiro.isPagado());
            preparedStatement.setLong(10, objRetiro.getId());
            
            preparedStatement.execute();
            
            if(preparedStatement.getUpdateCount() > 0){
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<Retiro> getAllRetiros(Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Retiro> lstRetiros = new ArrayList<>();

        List<Long> lstIdCuenta = new ArrayList<>();
        List<Long> lstIdUsuario = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT r.id, r.monto, r.fecharegistro, r.fechamodificacion, r.estado, r.idcuenta, r.idusuario, r.acreedor, r.habilitado, r.pagado  "
                + " FROM retiros as r "
                + " WHERE r.estado is true AND r.fecharegistro between ? AND ?;")) {

            preparedStatement.setTimestamp(1, fechaDesde);
            preparedStatement.setTimestamp(2, fechaHasta);
            
            ResultSet resultSet = preparedStatement.executeQuery();

            Retiro objRetiro = null;
            while (resultSet.next()) {

                lstIdCuenta.add(resultSet.getLong("idcuenta"));
                lstIdUsuario.add(resultSet.getLong("idusuario"));

                objRetiro = new Retiro(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,//objCuenta.getObjUsuario(), 
                        null,//objCuenta, 
                        resultSet.getString("acreedor"),
                        new ArrayList<>(),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"));
                lstRetiros.add(objRetiro);
            }

            int size = lstRetiros.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    lstRetiros.get(i).setObjCuenta(DAOCuenta.getInstance().getCuenta(lstIdCuenta.get(i)));
                    lstRetiros.get(i).setObjUsuario(DAOUsuario.getInstance().getUsuario(lstIdUsuario.get(i)));
                    lstRetiros.get(i).setLstPagos(DAOPago.getInstance().getAllPagos(lstRetiros.get(i)));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstRetiros;
    }

    
    @Override
    public List<Retiro> getAllRetirosHabilitados(Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Retiro> lstRetiros = new ArrayList<>();

        List<Long> lstIdCuenta = new ArrayList<>();
        List<Long> lstIdUsuario = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT r.id, r.monto, r.fecharegistro, r.fechamodificacion, r.estado, r.idcuenta, r.idusuario, r.acreedor, r.habilitado, r.pagado  "
                + " FROM retiros as r "
                + " WHERE r.estado is true AND r.habilitado is true AND r.pagado is false AND r.fecharegistro between ? AND ?;")) {

            preparedStatement.setTimestamp(1, fechaDesde);
            preparedStatement.setTimestamp(2, fechaHasta);
            
            ResultSet resultSet = preparedStatement.executeQuery();

            Retiro objRetiro = null;
            while (resultSet.next()) {

                lstIdCuenta.add(resultSet.getLong("idcuenta"));
                lstIdUsuario.add(resultSet.getLong("idusuario"));

                objRetiro = new Retiro(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,//objCuenta.getObjUsuario(), 
                        null,//objCuenta, 
                        resultSet.getString("acreedor"),
                        new ArrayList<>(),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"));
                lstRetiros.add(objRetiro);
            }

            int size = lstRetiros.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    lstRetiros.get(i).setObjCuenta(DAOCuenta.getInstance().getCuenta(lstIdCuenta.get(i)));
                    lstRetiros.get(i).setObjUsuario(DAOUsuario.getInstance().getUsuario(lstIdUsuario.get(i)));
                    lstRetiros.get(i).setLstPagos(DAOPago.getInstance().getAllPagos(lstRetiros.get(i)));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstRetiros;
    }

    @Override
    public List<Retiro> getAllRetirosHabilitados(Cuenta objCuenta, Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Retiro> lstRetiros = new ArrayList<>();

        List<Long> lstIdUsuario = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT r.id, r.monto, r.fecharegistro, r.fechamodificacion, r.estado, r.idcuenta, r.idusuario, r.acreedor, r.habilitado, r.pagado  "
                + " FROM retiros as r "
                + " WHERE r.estado is true AND r.habilitado is true AND r.pagado is false AND r.idcuenta = ? AND r.fecharegistro between ? AND ?;")) {
            preparedStatement.setLong(1, objCuenta.getId());
            preparedStatement.setTimestamp(2, fechaDesde);
            preparedStatement.setTimestamp(3, fechaHasta);
            
            ResultSet resultSet = preparedStatement.executeQuery();

            Retiro objRetiro = null;
            while (resultSet.next()) {
                lstIdUsuario.add(resultSet.getLong("idusuario"));

                objRetiro = new Retiro(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,//objCuenta.getObjUsuario(), 
                        objCuenta,//objCuenta, 
                        resultSet.getString("acreedor"),
                        new ArrayList<>(),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"));
                lstRetiros.add(objRetiro);
            }

            int size = lstRetiros.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    lstRetiros.get(i).setObjUsuario(DAOUsuario.getInstance().getUsuario(lstIdUsuario.get(i)));
                    lstRetiros.get(i).setLstPagos(DAOPago.getInstance().getAllPagos(lstRetiros.get(i)));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstRetiros;
    }

    @Override
    public List<Retiro> getAllRetirosPagados(Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Retiro> lstRetiros = new ArrayList<>();

        List<Long> lstIdCuenta = new ArrayList<>();
        List<Long> lstIdUsuario = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT r.id, r.monto, r.fecharegistro, r.fechamodificacion, r.estado, r.idcuenta, r.idusuario, r.acreedor, r.habilitado, r.pagado "
                + " FROM retiros as r "
                + " WHERE r.estado is true AND r.habilitado is true AND r.pagado is true AND r.fecharegistro between ? AND ?;")) {

            preparedStatement.setTimestamp(1, fechaDesde);
            preparedStatement.setTimestamp(2, fechaHasta);
            
            ResultSet resultSet = preparedStatement.executeQuery();

            Retiro objRetiro;
            while (resultSet.next()) {

                lstIdCuenta.add(resultSet.getLong("idcuenta"));
                lstIdUsuario.add(resultSet.getLong("idusuario"));

                objRetiro = new Retiro(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,//objCuenta.getObjUsuario(), 
                        null,//objCuenta, 
                        resultSet.getString("acreedor"),
                        new ArrayList<>(),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"));
                lstRetiros.add(objRetiro);
            }

            int size = lstRetiros.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    lstRetiros.get(i).setObjCuenta(DAOCuenta.getInstance().getCuenta(lstIdCuenta.get(i)));
                    lstRetiros.get(i).setObjUsuario(DAOUsuario.getInstance().getUsuario(lstIdUsuario.get(i)));
                    lstRetiros.get(i).setLstPagos(DAOPago.getInstance().getAllPagos(lstRetiros.get(i)));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstRetiros;
    }

    @Override
    public List<Retiro> getAllRetirosPagados(Cuenta objCuenta, Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Retiro> lstRetiros = new ArrayList<>();
        List<Long> lstIdUsuario = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT r.id, r.monto, r.fecharegistro, r.fechamodificacion, r.estado, r.idcuenta, r.idusuario, r.acreedor, r.habilitado, r.pagado "
                + " FROM retiros as r "
                + " WHERE r.estado is true AND r.habilitado is true AND r.pagado is true AND r.idcuenta = ? AND r.fecharegistro between ? AND ?;")) {
            preparedStatement.setLong(1, objCuenta.getId());
            preparedStatement.setTimestamp(2, fechaDesde);
            preparedStatement.setTimestamp(3, fechaHasta);
            
            ResultSet resultSet = preparedStatement.executeQuery();

            Retiro objRetiro;
            while (resultSet.next()) {
                lstIdUsuario.add(resultSet.getLong("idusuario"));

                objRetiro = new Retiro(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,//objCuenta.getObjUsuario(), 
                        objCuenta,//objCuenta, 
                        resultSet.getString("acreedor"),
                        new ArrayList<>(),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"));
                lstRetiros.add(objRetiro);
            }

            int size = lstRetiros.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    lstRetiros.get(i).setObjUsuario(DAOUsuario.getInstance().getUsuario(lstIdUsuario.get(i)));
                    lstRetiros.get(i).setLstPagos(DAOPago.getInstance().getAllPagos(lstRetiros.get(i)));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstRetiros;
    }
}
