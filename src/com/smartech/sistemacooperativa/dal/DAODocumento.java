package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Documento;
import com.smartech.sistemacooperativa.interfaces.IDAODocumento;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAODocumento implements IDAODocumento{
    
    private static DAODocumento instance = null;
    private Map<Integer, Documento> cache;
    
    private DAODocumento(){
        this.cache = new HashMap<>();
    }
    
    public static DAODocumento getInstance(){
        if(instance == null){
            instance = new DAODocumento();
        }
        
        return instance;
    }

    @Override
    public Documento getDocumento(int id) {
        Documento objDocumento = this.cache.get(id);
        
        if(objDocumento == null){
            this.cache.clear();
            this.getAllDocumentos();
            objDocumento = this.cache.get(id);
        }
        
        return objDocumento;
    }

    @Override
    public List<Documento> getAllDocumentos() {
        List<Documento> lstDocumentos;
        
        if(this.cache.isEmpty()){
            try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT d.id, d.nombre, d.formato, d.tipoarchivo "
                    + "FROM documentos d;")){
                
                ResultSet resultSet = preparedStatement.executeQuery();
                
                while(resultSet.next()){
                    Documento objDocumento = new Documento(
                            resultSet.getInt("id"),
                            resultSet.getString("nombre"),
                            resultSet.getBytes("formato"),
                            resultSet.getString("tipoarchivo"));
                    this.cache.put(objDocumento.getId(), objDocumento);
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        
        Collection collection = this.cache.values();
        
        lstDocumentos = new ArrayList<>(collection);
        
        return lstDocumentos;
    }
}
