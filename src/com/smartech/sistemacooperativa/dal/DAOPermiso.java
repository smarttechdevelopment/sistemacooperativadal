package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Permiso;
import com.smartech.sistemacooperativa.interfaces.IDAOPermiso;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAOPermiso implements IDAOPermiso{
    
    private static DAOPermiso instance = null;
    private Map<Integer, Permiso> cache;

    private DAOPermiso() {
        this.cache = new HashMap<>();
    }

    public static DAOPermiso getInstance() {
        if (instance == null) {
            instance = new DAOPermiso();
        }

        return instance;
    }

    @Override
    public Permiso getPermiso(int id) {
        Permiso objPermiso = this.cache.get(id);
        
        if(objPermiso == null){
            this.cache.clear();
            this.getAllPermisos();
            objPermiso = this.cache.get(id);
        }
        
        return objPermiso;
    }

    @Override
    public List<Permiso> getAllPermisos() {
        List<Permiso> lstPermisos;
        
        if(this.cache.isEmpty()){
            try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id, p.nombre, p.descripcion "
                    + "FROM permisos p;")){
                
                ResultSet resultSet = preparedStatement.executeQuery();
                
                while(resultSet.next()){
                    Permiso objPermiso = new Permiso(
                            resultSet.getInt("id"),
                            resultSet.getString("nombre"),
                            resultSet.getString("descripcion"));
                    this.cache.put(objPermiso.getId(), objPermiso);
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        
        Collection collection = this.cache.values();
        
        lstPermisos = new ArrayList<>(collection);
        
        return lstPermisos;
    }
}
