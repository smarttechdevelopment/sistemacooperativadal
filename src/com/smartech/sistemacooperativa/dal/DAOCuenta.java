/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.TipoCuenta;
import com.smartech.sistemacooperativa.interfaces.IDAOCuenta;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOCuenta implements IDAOCuenta {

    private static DAOCuenta instance = null;

    private DAOCuenta() {
    }

    public static DAOCuenta getInstance() {
        if (instance == null) {
            instance = new DAOCuenta();
        }

        return instance;
    }

    @Override
    public String getLastCodigoCuenta() {
        String result = "";

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT COALESCE((SELECT c.codigo FROM cuentas c "
                + "WHERE c.id = (SELECT MAX(csub.id) id FROM cuentas csub)), '0') codigo;")) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                result = resultSet.getString("codigo");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public Cuenta getCuenta(Long id) {

        Cuenta objCuenta = null;

        List<Long> lstIdsEmpleados = new ArrayList<>();
        List<Integer> lstIdsTasasInteres = new ArrayList<>();
        List<Integer> lstIdsTiposCuenta = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT c.id, c.codigo, c.saldo, c.fecharegistro, "
                + " c.fechamodificacion, c.estado, c.activo, c.idtipocuenta, c.idempleado, c.idtasainteres, c.idusuario, c.bloqueadoentrada, c.bloqueadosalida "
                + " FROM cuentas c "
                + " WHERE c.id = ?;")) {

            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsEmpleados.add(resultSet.getLong("idempleado"));
                lstIdsTasasInteres.add(resultSet.getInt("idtasainteres"));
                lstIdsTiposCuenta.add(resultSet.getInt("idtipocuenta"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));

                objCuenta = new Cuenta(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        resultSet.getBigDecimal("saldo"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("activo"),
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        resultSet.getBoolean("bloqueadoentrada"),
                        resultSet.getBoolean("bloqueadosalida")
                );
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objCuenta != null) {
            DAOEmpleado objDAOEmpleado = DAOEmpleado.getInstance();
            DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();
            DAOTipoCuenta objDAOTipoCuenta = DAOTipoCuenta.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOSocio objDAOSocio = DAOSocio.getInstance();

            objCuenta.setObjEmpleado(objDAOEmpleado.getEmpleado(lstIdsEmpleados.get(0)));
            objCuenta.setObjTasaInteres(objDAOTasaInteres.getTasaInteres(lstIdsTasasInteres.get(0)));
            objCuenta.setObjTipoCuenta(objDAOTipoCuenta.getTipoCuenta(lstIdsTiposCuenta.get(0)));
            objCuenta.setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(0)));
            objCuenta.setLstSocios(objDAOSocio.getAllSociosOnly(objCuenta, false));

        }

        return objCuenta;

    }

    @Override
    public Cuenta getCuenta(String numeroCuenta) {
        Cuenta objCuenta = null;

        List<Long> lstIdsEmpleados = new ArrayList<>();
        List<Integer> lstIdsTasasInteres = new ArrayList<>();
        List<Integer> lstIdsTiposCuenta = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT c.id, c.codigo, c.saldo, c.fecharegistro, "
                + " c.fechamodificacion, c.estado, c.activo, c.idtipocuenta, c.idempleado, c.idtasainteres, c.idusuario, c.bloqueadoentrada, c.bloqueadosalida "
                + " FROM cuentas c "
                + " WHERE c.codigo = ?;")) {

            preparedStatement.setString(1, numeroCuenta);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsEmpleados.add(resultSet.getLong("idempleado"));
                lstIdsTasasInteres.add(resultSet.getInt("idtasainteres"));
                lstIdsTiposCuenta.add(resultSet.getInt("idtipocuenta"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));

                objCuenta = new Cuenta(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        resultSet.getBigDecimal("saldo"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("activo"),
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        resultSet.getBoolean("bloqueadoentrada"),
                        resultSet.getBoolean("bloqueadosalida")
                );
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objCuenta != null) {
            DAOEmpleado objDAOEmpleado = DAOEmpleado.getInstance();
            DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();
            DAOTipoCuenta objDAOTipoCuenta = DAOTipoCuenta.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOSocio objDAOSocio = DAOSocio.getInstance();

            objCuenta.setObjEmpleado(objDAOEmpleado.getEmpleado(lstIdsEmpleados.get(0)));
            objCuenta.setObjTasaInteres(objDAOTasaInteres.getTasaInteres(lstIdsTasasInteres.get(0)));
            objCuenta.setObjTipoCuenta(objDAOTipoCuenta.getTipoCuenta(lstIdsTiposCuenta.get(0)));
            objCuenta.setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(0)));
            objCuenta.setLstSocios(objDAOSocio.getAllSociosOnly(objCuenta, false));

        }

        return objCuenta;
    }

    @Override
    public BigDecimal getSaldoCuentas(Socio objSocio) {
        BigDecimal saldoCuentas = new BigDecimal(0);

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT SUM(c.saldo) saldo "
                + "FROM cuentas c "
                + "INNER JOIN detallecuentasocio dcs on dcs.idcuenta = c.id "
                + "WHERE dcs.idsocio = ?;")) {

            preparedStatement.setLong(1, objSocio.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                saldoCuentas = resultSet.getBigDecimal("saldo");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return saldoCuentas;
    }

    @Override
    public List<Cuenta> getAllCuentas() {
        List<Cuenta> lstCuentas = new ArrayList<>();
        List<Long> lstIdsEmpleados = new ArrayList<>();
        List<Integer> lstIdsTasasInteres = new ArrayList<>();
        List<Integer> lstIdsTiposCuenta = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT c.id, c.codigo, c.saldo, c.fecharegistro, "
                + "c.fechamodificacion, c.estado, c.activo, c.idtipocuenta, c.idempleado, c.idtasainteres, c.idusuario, c.bloqueadoentrada, c.bloqueadosalida "
                + "FROM cuentas c;")) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsEmpleados.add(resultSet.getLong("idempleado"));
                lstIdsTasasInteres.add(resultSet.getInt("idtasainteres"));
                lstIdsTiposCuenta.add(resultSet.getInt("idtipocuenta"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));

                Cuenta objCuenta = new Cuenta(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        resultSet.getBigDecimal("saldo"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("activo"),
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        resultSet.getBoolean("bloqueadoentrada"),
                        resultSet.getBoolean("bloqueadosalida")
                );
                lstCuentas.add(objCuenta);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstCuentas.isEmpty()) {
            DAOEmpleado objDAOEmpleado = DAOEmpleado.getInstance();
            DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();
            DAOTipoCuenta objDAOTipoCuenta = DAOTipoCuenta.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();

            int size = lstCuentas.size();
            for (int i = 0; i < size; i++) {
                lstCuentas.get(i).setObjEmpleado(objDAOEmpleado.getEmpleado(lstIdsEmpleados.get(i)));
                lstCuentas.get(i).setObjTasaInteres(objDAOTasaInteres.getTasaInteres(lstIdsTasasInteres.get(i)));
                lstCuentas.get(i).setObjTipoCuenta(objDAOTipoCuenta.getTipoCuenta(lstIdsTiposCuenta.get(i)));
                lstCuentas.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstCuentas.get(i).setLstSocios(DAOSocio.getInstance().getAllSociosOnly(lstCuentas.get(i), false));
            }
        }

        return lstCuentas;
    }

    @Override
    public List<Cuenta> getAllCuentasHabilitadas() {
        List<Cuenta> lstCuentas = new ArrayList<>();
        List<Long> lstIdsEmpleados = new ArrayList<>();
        List<Integer> lstIdsTasasInteres = new ArrayList<>();
        List<Integer> lstIdsTiposCuenta = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT c.id, c.codigo, c.saldo, c.fecharegistro, "
                + "c.fechamodificacion, c.estado, c.activo, c.idtipocuenta, c.idempleado, c.idtasainteres, c.idusuario, c.bloqueadoentrada, c.bloqueadosalida "
                + "FROM cuentas c "
                + "WHERE c.estado is true AND c.activo is true;")) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsEmpleados.add(resultSet.getLong("idempleado"));
                lstIdsTasasInteres.add(resultSet.getInt("idtasainteres"));
                lstIdsTiposCuenta.add(resultSet.getInt("idtipocuenta"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));

                Cuenta objCuenta = new Cuenta(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        resultSet.getBigDecimal("saldo"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("activo"),
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        resultSet.getBoolean("bloqueadoentrada"),
                        resultSet.getBoolean("bloqueadosalida")
                );
                lstCuentas.add(objCuenta);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstCuentas.isEmpty()) {
            DAOEmpleado objDAOEmpleado = DAOEmpleado.getInstance();
            DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();
            DAOTipoCuenta objDAOTipoCuenta = DAOTipoCuenta.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();

            int size = lstCuentas.size();
            for (int i = 0; i < size; i++) {
                lstCuentas.get(i).setObjEmpleado(objDAOEmpleado.getEmpleado(lstIdsEmpleados.get(i)));
                lstCuentas.get(i).setObjTasaInteres(objDAOTasaInteres.getTasaInteres(lstIdsTasasInteres.get(i)));
                lstCuentas.get(i).setObjTipoCuenta(objDAOTipoCuenta.getTipoCuenta(lstIdsTiposCuenta.get(i)));
                lstCuentas.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstCuentas.get(i).setLstSocios(DAOSocio.getInstance().getAllSociosOnly(lstCuentas.get(i), false));
            }
        }

        return lstCuentas;
    }

    @Override
    public List<Cuenta> getAllCuentas(Socio objSocio) {
        List<Cuenta> lstCuentas = new ArrayList<>();
        List<Long> lstIdsEmpleados = new ArrayList<>();
        List<Integer> lstIdsTasasInteres = new ArrayList<>();
        List<Integer> lstIdsTiposCuenta = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT c.id, c.codigo, c.saldo, c.fecharegistro, "
                + "c.fechamodificacion, c.estado, c.activo, c.idtipocuenta, c.idempleado, c.idtasainteres, c.idusuario, c.bloqueadoentrada, c.bloqueadosalida "
                + "FROM cuentas c INNER JOIN detallecuentasocio dcs ON c.id = dcs.idcuenta "
                + "INNER JOIN socios s ON s.id = dcs.idsocio "
                + "WHERE s.id = ?;")) {
            preparedStatement.setLong(1, objSocio.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsEmpleados.add(resultSet.getLong("idempleado"));
                lstIdsTasasInteres.add(resultSet.getInt("idtasainteres"));
                lstIdsTiposCuenta.add(resultSet.getInt("idtipocuenta"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));

                Cuenta objCuenta = new Cuenta(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        resultSet.getBigDecimal("saldo"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("activo"),
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        resultSet.getBoolean("bloqueadoentrada"),
                        resultSet.getBoolean("bloqueadosalida")
                );
                lstCuentas.add(objCuenta);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstCuentas.isEmpty()) {
            DAOEmpleado objDAOEmpleado = DAOEmpleado.getInstance();
            DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();
            DAOTipoCuenta objDAOTipoCuenta = DAOTipoCuenta.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOSocio objDAOSocio = DAOSocio.getInstance();

            int size = lstCuentas.size();
            for (int i = 0; i < size; i++) {
                lstCuentas.get(i).setObjEmpleado(objDAOEmpleado.getEmpleado(lstIdsEmpleados.get(i)));
                lstCuentas.get(i).setObjTasaInteres(objDAOTasaInteres.getTasaInteres(lstIdsTasasInteres.get(i)));
                lstCuentas.get(i).setObjTipoCuenta(objDAOTipoCuenta.getTipoCuenta(lstIdsTiposCuenta.get(i)));
                lstCuentas.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstCuentas.get(i).setLstSocios(objDAOSocio.getAllSociosOnly(lstCuentas.get(i), false));
            }
        }

        return lstCuentas;
    }

    @Override
    public List<Cuenta> getAllCuentasOnly(Socio objSocio) {
        List<Cuenta> lstCuentas = new ArrayList<>();
        List<Long> lstIdsEmpleados = new ArrayList<>();
        List<Integer> lstIdsTasasInteres = new ArrayList<>();
        List<Integer> lstIdsTiposCuenta = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT c.id, c.codigo, c.saldo, c.fecharegistro, "
                + "c.fechamodificacion, c.estado, c.activo, c.idtipocuenta, c.idempleado, c.idtasainteres, c.idusuario, c.bloqueadoentrada, c.bloqueadosalida "
                + "FROM cuentas c INNER JOIN detallecuentasocio dcs ON c.id = dcs.idcuenta "
                + "INNER JOIN socios s ON s.id = dcs.idsocio "
                + "WHERE s.id = ?;")) {
            preparedStatement.setLong(1, objSocio.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsEmpleados.add(resultSet.getLong("idempleado"));
                lstIdsTasasInteres.add(resultSet.getInt("idtasainteres"));
                lstIdsTiposCuenta.add(resultSet.getInt("idtipocuenta"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));

                Cuenta objCuenta = new Cuenta(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        resultSet.getBigDecimal("saldo"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("activo"),
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        resultSet.getBoolean("bloqueadoentrada"),
                        resultSet.getBoolean("bloqueadosalida")
                );
                lstCuentas.add(objCuenta);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstCuentas.isEmpty()) {
            DAOEmpleado objDAOEmpleado = DAOEmpleado.getInstance();
            DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();
            DAOTipoCuenta objDAOTipoCuenta = DAOTipoCuenta.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();

            int size = lstCuentas.size();
            for (int i = 0; i < size; i++) {
                lstCuentas.get(i).setObjEmpleado(objDAOEmpleado.getEmpleado(lstIdsEmpleados.get(i)));
                lstCuentas.get(i).setObjTasaInteres(objDAOTasaInteres.getTasaInteres(lstIdsTasasInteres.get(i)));
                lstCuentas.get(i).setObjTipoCuenta(objDAOTipoCuenta.getTipoCuenta(lstIdsTiposCuenta.get(i)));
                lstCuentas.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
            }
        }

        return lstCuentas;
    }

    @Override
    public List<Cuenta> getCuentasSocios(TipoCuenta objTipoCuenta, List<Socio> lstSocios) {

        StringBuilder query = new StringBuilder();
        List<Cuenta> lstCuentas = new ArrayList<>();
        List<Long> lstIdsEmpleados = new ArrayList<>();
        List<Integer> lstIdsTasasInteres = new ArrayList<>();
        List<Integer> lstIdsTiposCuenta = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();

        if (lstSocios.size() > 0) {

            query.append(" SELECT c.id, c.codigo, c.saldo, c.fecharegistro, c.fechamodificacion, c.estado, c.activo, c.idempleado, c.idtasainteres, c.idtipocuenta, c.idusuario, c.bloqueadoentrada, c.bloqueadosalida ");
            query.append(" FROM cuentas c ");
            query.append(" WHERE (SELECT COUNT(dcssuba.id) FROM detallecuentasocio dcssuba WHERE dcssuba.idcuenta = c.id) = ? AND c.idtipocuenta = ? AND EXISTS (SELECT dcssubb.idcuenta ");
            query.append(" FROM detallecuentasocio dcssubb ");
            query.append(" WHERE dcssubb.idcuenta = c.id ");
            for (int i = 0; i < lstSocios.size(); i++) {
                query.append(" AND ? IN (SELECT dcssubc.idsocio FROM detallecuentasocio dcssubc WHERE dcssubc.idcuenta = c.id)");
            }
            query.append(");");

            try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(query.toString())) {

                preparedStatement.setInt(1, lstSocios.size());
                preparedStatement.setInt(2, objTipoCuenta.getId());
                int i = 3;
                for (Socio objSocio : lstSocios) {
                    preparedStatement.setLong(i, objSocio.getId());
                    i++;
                }

                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    lstIdsEmpleados.add(resultSet.getLong("idempleado"));
                    lstIdsTasasInteres.add(resultSet.getInt("idtasainteres"));
                    lstIdsTiposCuenta.add(resultSet.getInt("idtipocuenta"));
                    lstIdsUsuarios.add(resultSet.getLong("idusuario"));

                    Cuenta objCuenta = new Cuenta(
                            resultSet.getLong("id"),
                            resultSet.getString("codigo"),
                            resultSet.getBigDecimal("saldo"),
                            resultSet.getTimestamp("fecharegistro"),
                            resultSet.getTimestamp("fechamodificacion"),
                            resultSet.getBoolean("estado"),
                            resultSet.getBoolean("activo"),
                            null,
                            null,
                            null,
                            null,
                            new ArrayList<>(),
                            resultSet.getBoolean("bloqueadoentrada"),
                            resultSet.getBoolean("bloqueadosalida")
                    );
                    lstCuentas.add(objCuenta);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!lstCuentas.isEmpty()) {
                DAOEmpleado objDAOEmpleado = DAOEmpleado.getInstance();
                DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();
                DAOTipoCuenta objDAOTipoCuenta = DAOTipoCuenta.getInstance();
                DAOUsuario objDAOUsuario = DAOUsuario.getInstance();

                int size = lstCuentas.size();
                for (int i = 0; i < size; i++) {
                    lstCuentas.get(i).setObjEmpleado(objDAOEmpleado.getEmpleado(lstIdsEmpleados.get(i)));
                    lstCuentas.get(i).setObjTasaInteres(objDAOTasaInteres.getTasaInteres(lstIdsTasasInteres.get(i)));
                    lstCuentas.get(i).setObjTipoCuenta(objDAOTipoCuenta.getTipoCuenta(lstIdsTiposCuenta.get(i)));
                    lstCuentas.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                    lstCuentas.get(i).setLstSocios(lstSocios);
                }
            }

        }

        return lstCuentas;
    }

    @Override
    public boolean insert(Cuenta objCuenta) {

        boolean resultado = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO cuentas (codigo,saldo,fecharegistro,"
                + "fechamodificacion,estado,activo,idempleado,idtasainteres,idtipocuenta,idusuario) VALUES (?,?,?,?,?,?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, objCuenta.getCodigo());
            preparedStatement.setBigDecimal(2, objCuenta.getSaldo());
            preparedStatement.setTimestamp(3, objCuenta.getFecharegistro());
            preparedStatement.setTimestamp(4, objCuenta.getFechamodificacion());
            preparedStatement.setBoolean(5, objCuenta.isEstado());
            preparedStatement.setBoolean(6, objCuenta.isActivo());
            preparedStatement.setLong(7, objCuenta.getObjEmpleado().getId());
            preparedStatement.setInt(8, objCuenta.getObjTasaInteres().getId());
            preparedStatement.setInt(9, objCuenta.getObjTipoCuenta().getId());
            preparedStatement.setLong(10, objCuenta.getObjUsuario().getId());

            preparedStatement.execute();
            ResultSet keySet = preparedStatement.getGeneratedKeys();
            while (keySet.next()) {
                objCuenta.setId(keySet.getLong(1));
            }

            System.out.println("cuenta id: " + objCuenta.getId());
            resultado = true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultado;
    }

    @Override
    public boolean insertDetalleCuentaSocio(Cuenta objCuenta, Socio objSocio) {

        boolean resultado = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO detallecuentasocio (idsocio, idcuenta)"
                + " VALUES (?,?)")) {

            preparedStatement.setLong(1, objSocio.getId());
            preparedStatement.setLong(2, objCuenta.getId());

            preparedStatement.execute();

            resultado = true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultado;

    }

    @Override
    public boolean update(Cuenta objCuenta) {

        boolean resultado = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE cuentas "
                + " SET codigo=?,saldo=?,fecharegistro=?,fechamodificacion=?,estado=?,activo=?,idempleado=?,idtasainteres=?,idtipocuenta=?,idusuario=?"
                + " WHERE id = ?")) {

            preparedStatement.setString(1, objCuenta.getCodigo());
            preparedStatement.setBigDecimal(2, objCuenta.getSaldo());
            preparedStatement.setTimestamp(3, objCuenta.getFecharegistro());
            preparedStatement.setTimestamp(4, objCuenta.getFechamodificacion());
            preparedStatement.setBoolean(5, objCuenta.isEstado());
            preparedStatement.setBoolean(6, objCuenta.isActivo());
            preparedStatement.setLong(7, objCuenta.getObjEmpleado().getId());
            preparedStatement.setInt(8, objCuenta.getObjTasaInteres().getId());
            preparedStatement.setInt(9, objCuenta.getObjTipoCuenta().getId());
            preparedStatement.setLong(10, objCuenta.getObjUsuario().getId());
            preparedStatement.setLong(11, objCuenta.getId());

            preparedStatement.executeUpdate();

            resultado = true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultado;
    }

    @Override
    public boolean updateTasasInteres(long oldTasaInteresId, long newTasaInteresId) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE cuentas SET idtasainteres = ? WHERE idtasainteres = ?;")) {
            preparedStatement.setLong(1, newTasaInteresId);
            preparedStatement.setLong(2, oldTasaInteresId);

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean updateIntereses(Timestamp fechaInteres) {
        boolean result = false;

        try (CallableStatement callableStatement = ConnectionManager.getConnection().prepareCall("{call sp_updateinteresescuentas(?)}")) {
            callableStatement.setTimestamp(1, fechaInteres);

            callableStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
