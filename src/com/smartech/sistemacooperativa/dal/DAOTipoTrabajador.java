/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.TipoTrabajador;
import com.smartech.sistemacooperativa.interfaces.IDAOTipoTrabajador;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAOTipoTrabajador implements IDAOTipoTrabajador{

    private static DAOTipoTrabajador instance = null;
    private Map<Integer, TipoTrabajador> cache = new HashMap<>();
    
    private DAOTipoTrabajador(){
        
    }
    public static DAOTipoTrabajador getInstance(){
        if(instance == null){
            instance = new DAOTipoTrabajador();
        }
        return instance;
    }
    
    @Override
    public List<TipoTrabajador> getAllTiposTrabajador() {
        List<TipoTrabajador> lstTiposTrabajador;
        
        if(this.cache.isEmpty()){
            
            try(PreparedStatement pst = ConnectionManager.getConnection().prepareStatement("SELECT id, nombre FROM tipostrabajador ORDER BY id;");){
                
                ResultSet rs = pst.executeQuery();
                while(rs.next()){
                    TipoTrabajador objTipoTrabajador = new TipoTrabajador(rs.getInt("id") ,rs.getString("nombre"));
                    this.cache.put(objTipoTrabajador.getId(), objTipoTrabajador);
                }
                
            }catch(Exception e){
                e.printStackTrace();
            }
            
        }
        
        Collection<TipoTrabajador> collection = this.cache.values();
        lstTiposTrabajador = new ArrayList<>(collection);
        
        return lstTiposTrabajador;
    }

    @Override
    public TipoTrabajador getTipoTrabajador(int id) {
        TipoTrabajador objTipoTrabajador = this.cache.get(id);
        
        if(objTipoTrabajador == null){
            this.cache.clear();
            this.getAllTiposTrabajador();
            objTipoTrabajador = this.cache.get(id);
        }
        
        return objTipoTrabajador;
    }
    
}
