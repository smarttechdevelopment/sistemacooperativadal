package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Establecimiento;
import com.smartech.sistemacooperativa.interfaces.IDAOEstablecimiento;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAOEstablecimiento implements IDAOEstablecimiento {

    private static DAOEstablecimiento instance = null;
    private Map<Integer, Establecimiento> cache;

    private DAOEstablecimiento() {
        this.cache = new HashMap<>();
    }

    public static DAOEstablecimiento getInstance() {
        if (instance == null) {
            instance = new DAOEstablecimiento();
        }

        return instance;
    }

    @Override
    public Establecimiento getEstablecimiento(int id) {
        Establecimiento objEstablecimiento = this.cache.get(id);

        if (objEstablecimiento == null) {
            this.cache.clear();
            this.getAllEstablecimientos();
            objEstablecimiento = this.cache.get(id);
        }

        return objEstablecimiento;
    }

    @Override
    public List<Establecimiento> getAllEstablecimientos() {
        List<Establecimiento> lstEstablecimientos;
        Map<Integer, Integer> mapIdsDistritos = new HashMap<>();

        if (this.cache.isEmpty()) {
            try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT e.id, e.nombre, e.codigo, e.direccion, e.iddistrito "
                    + "FROM establecimientos e;")) {

                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    Establecimiento objEstablecimiento = new Establecimiento(
                            resultSet.getInt("id"),
                            resultSet.getString("nombre"),
                            resultSet.getString("codigo"),
                            resultSet.getString("direccion"),
                            null);
                    mapIdsDistritos.put(objEstablecimiento.getId(), resultSet.getInt("iddistrito"));
                    this.cache.put(objEstablecimiento.getId(), objEstablecimiento);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!this.cache.isEmpty()) {
                DAODistrito objDAODistrito = DAODistrito.getInstance();

                for (Map.Entry<Integer, Establecimiento> entry : this.cache.entrySet()) {
                    Establecimiento objEstablecimiento = entry.getValue();
                    objEstablecimiento.setObjDistrito(objDAODistrito.getDistrito(mapIdsDistritos.get(objEstablecimiento.getId())));
                }
            }
        }

        Collection<Establecimiento> collection = this.cache.values();

        lstEstablecimientos = new ArrayList<>(collection);

        return lstEstablecimientos;
    }
}
