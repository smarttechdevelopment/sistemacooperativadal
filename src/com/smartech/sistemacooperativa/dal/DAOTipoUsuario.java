package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Acceso;
import com.smartech.sistemacooperativa.dominio.Permiso;
import com.smartech.sistemacooperativa.dominio.TipoUsuario;
import com.smartech.sistemacooperativa.dominio.Usuario;
import com.smartech.sistemacooperativa.interfaces.IDAOTipoUsuario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Usuario
 */
public class DAOTipoUsuario implements IDAOTipoUsuario {

    private static DAOTipoUsuario instance = null;
    private Map<Integer, TipoUsuario> cache;

    private DAOTipoUsuario() {
        this.cache = new HashMap<>();
    }

    public static DAOTipoUsuario getInstance() {
        if (instance == null) {
            instance = new DAOTipoUsuario();
        }

        return instance;
    }

    @Override
    public TipoUsuario getTipoUsuario(int id) {
        TipoUsuario objTipoUsuario = this.cache.get(id);

        if (objTipoUsuario == null) {
            this.cache.clear();
            this.getAllTiposUsuario();
            objTipoUsuario = this.cache.get(id);
        }

        return objTipoUsuario;
    }

    @Override
    public List<TipoUsuario> getAllTiposUsuario() {
        List<TipoUsuario> lstTiposUsuario;

        if (this.cache.isEmpty()) {
            try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT tu.id, tu.nombre, tu.tipopersona, tu.nivel, tu.accesocaja, tu.montoadvertenciatransaccion, datu.id iddetalleaccesotipousuario, datu.idtipousuario, datu.idacceso "
                    + "FROM tiposusuario tu FULL JOIN detalleaccesotipousuario datu ON tu.id = datu.idtipousuario;")) {

                ResultSet resultSet = preparedStatement.executeQuery();

                DAOAcceso objDAOAcceso = DAOAcceso.getInstance();
                while (resultSet.next()) {
                    TipoUsuario objTipoUsuario = this.cache.get(resultSet.getInt("id"));
                    if (objTipoUsuario == null) {
                        objTipoUsuario = new TipoUsuario(
                                resultSet.getInt("id"),
                                resultSet.getString("nombre"),
                                resultSet.getInt("tipopersona"),
                                resultSet.getInt("nivel"),
                                resultSet.getBoolean("accesocaja"),
                                resultSet.getBigDecimal("montoadvertenciatransaccion"),
                                new ArrayList<>(),
                                new ArrayList<>());
                        this.cache.put(objTipoUsuario.getId(), objTipoUsuario);
                    }
                    Acceso objAcceso = objDAOAcceso.getAcceso(resultSet.getInt("idacceso"));
                    if (objAcceso != null) {
                        objTipoUsuario.getLstAccesos().add(objAcceso);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT tu.id, tu.nombre, tu.tipopersona, tu.nivel, tu.accesocaja, tu.montoadvertenciatransaccion, dptu.id iddetallepermisotipousuario, dptu.nivel nivelpermiso, dptu.idtipousuario, dptu.idpermiso "
                    + "FROM tiposusuario tu INNER JOIN detallepermisotipousuario dptu ON tu.id = dptu.idtipousuario;")) {

                ResultSet resultSet = preparedStatement.executeQuery();

                DAOPermiso objDAOPermiso = DAOPermiso.getInstance();
                while (resultSet.next()) {
                    TipoUsuario objTipoUsuario = this.cache.get(resultSet.getInt("id"));
                    if (objTipoUsuario == null) {
                        objTipoUsuario = new TipoUsuario(
                                resultSet.getInt("id"),
                                resultSet.getString("nombre"),
                                resultSet.getInt("tipopersona"),
                                resultSet.getInt("nivel"),
                                resultSet.getBoolean("accesocaja"),
                                resultSet.getBigDecimal("montoadvertenciatransaccion"),
                                new ArrayList<>(),
                                new ArrayList<>());
                        this.cache.put(objTipoUsuario.getId(), objTipoUsuario);
                    }

                    Permiso objPermiso = objDAOPermiso.getPermiso(resultSet.getInt("idpermiso"));
                    if (objPermiso != null) {
                        objTipoUsuario.getLstPermisos().add(objPermiso);
                        objTipoUsuario.getLstPermisos().get(objTipoUsuario.getLstPermisos().size() - 1).setNivel(resultSet.getInt("nivelpermiso"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Collection collection = this.cache.values();

        lstTiposUsuario = new ArrayList<>(collection);

        return lstTiposUsuario;
    }

    @Override
    public List<TipoUsuario> getAllTiposUsuarioEmpleadosDebajoNivel(Usuario objUsuario) {
        List<TipoUsuario> lstTiposUsuarioReturn = new ArrayList<>();

        if (this.cache.isEmpty()) {
            this.getAllTiposUsuario();
        }

        List<TipoUsuario> lstTiposUsuario = new ArrayList<>(this.cache.values());
        for (TipoUsuario objTipoUsuario : lstTiposUsuario) {
            if (objTipoUsuario.getTipoPersona() != 0 && objTipoUsuario.getNivel() < objUsuario.getObjTipoUsuario().getNivel()) {
                lstTiposUsuarioReturn.add(objTipoUsuario);
            }
        }

        return lstTiposUsuarioReturn;
    }
}
