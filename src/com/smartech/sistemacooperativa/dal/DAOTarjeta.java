package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.Tarjeta;
import com.smartech.sistemacooperativa.interfaces.IDAOTarjeta;
import com.smartech.sistemacooperativa.util.generics.StringUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOTarjeta implements IDAOTarjeta {

    private static DAOTarjeta instance = null;

    private DAOTarjeta() {
    }

    public static DAOTarjeta getInstance() {
        if (instance == null) {
            instance = new DAOTarjeta();
        }

        return instance;
    }

    @Override
    public Tarjeta getTarjeta(long id) {
        Tarjeta objTarjeta = null;
        int idTipoTarjeta = 0;
        long idSocio = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT t.id, t.codigo, "
                + "t.fechaentrega, t.activo, t.fecharegistro, t.fechamodificacion, t.fechabloqueointentos, t.intentos, t.estado, t.idtipotarjeta, t.idsocio "
                + "FROM tarjetas t "
                + "WHERE t.id = ?;")) {
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idTipoTarjeta = resultSet.getInt("idtipotarjeta");
                idSocio = resultSet.getLong("idsocio");

                objTarjeta = new Tarjeta(
                        id,
                        resultSet.getString("codigo"),
                        "",
                        resultSet.getTimestamp("fechaentrega"),
                        resultSet.getBoolean("activo"),
                        resultSet.getInt("intentos"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getTimestamp("fechabloqueointentos"),
                        resultSet.getBoolean("estado"),
                        null,
                        null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objTarjeta != null) {
            objTarjeta.setObjTipoTarjeta(DAOTipoTarjeta.getInstance().getTipoTarjeta(idTipoTarjeta));
            objTarjeta.setObjSocio(DAOSocio.getInstance().getSocio(idSocio, false));
        }

        return objTarjeta;
    }

    @Override
    public Tarjeta getTarjeta(Socio objSocio) {
        Tarjeta objTarjeta = null;
        int idTipoTarjeta = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT MAX(t.id) id, t.codigo, "
                + "t.fechaentrega, t.activo, t.fecharegistro, t.fechamodificacion, t.fechabloqueointentos, t.intentos, t.estado, t.idtipotarjeta, t.idsocio "
                + "FROM tarjetas t INNER JOIN socios s ON s.id = t.idsocio "
                + "WHERE s.id = ? AND t.estado is true "
                + "GROUP BY t.codigo, t.pin, t.fechaentrega, t.activo, t.fecharegistro, t.fechamodificacion, t.fechabloqueointentos, t.intentos, t.estado, t.idtipotarjeta, t.idsocio;")) {
            preparedStatement.setLong(1, objSocio.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idTipoTarjeta = resultSet.getInt("idtipotarjeta");

                objTarjeta = new Tarjeta(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        "",
                        resultSet.getTimestamp("fechaentrega"),
                        resultSet.getBoolean("activo"),
                        resultSet.getInt("intentos"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getTimestamp("fechabloqueointentos"),
                        resultSet.getBoolean("estado"),
                        null,
                        objSocio);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objTarjeta != null) {
            objTarjeta.setObjTipoTarjeta(DAOTipoTarjeta.getInstance().getTipoTarjeta(idTipoTarjeta));
        }

        return objTarjeta;
    }

    @Override
    public Tarjeta getTarjeta(String codigo) {
        Tarjeta objTarjeta = null;
        int idTipoTarjeta = 0;
        long idSocio = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT MAX(t.id) id, t.codigo, "
                + "t.fechaentrega, t.activo, t.fecharegistro, t.fechamodificacion, t.fechabloqueointentos, t.intentos, t.estado, t.idtipotarjeta, t.idsocio "
                + "FROM tarjetas t  "
                + "WHERE t.codigo = ? "
                + "GROUP BY t.codigo, t.fechaentrega, t.activo, t.fecharegistro, t.fechamodificacion, t.fechabloqueointentos, t.intentos, t.estado, t.idtipotarjeta, t.idsocio;")) {
            preparedStatement.setString(1, codigo);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idTipoTarjeta = resultSet.getInt("idtipotarjeta");
                idSocio = resultSet.getLong("idsocio");

                objTarjeta = new Tarjeta(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        "",
                        resultSet.getTimestamp("fechaentrega"),
                        resultSet.getBoolean("activo"),
                        resultSet.getInt("intentos"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getTimestamp("fechabloqueointentos"),
                        resultSet.getBoolean("estado"),
                        null,
                        null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objTarjeta != null) {
            objTarjeta.setObjTipoTarjeta(DAOTipoTarjeta.getInstance().getTipoTarjeta(idTipoTarjeta));
            objTarjeta.setObjSocio(DAOSocio.getInstance().getSocio(idSocio, false));
        }

        return objTarjeta;
    }

    @Override
    public List<Tarjeta> getAllTarjetas(Socio objSocio) {
        List<Tarjeta> lstTarjetas = new ArrayList<>();
        List<Integer> lstIdsTiposTarjeta = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT t.id, t.codigo, "
                + "t.fechaentrega, t.activo, t.fecharegistro, t.fechamodificacion, t.fechabloqueointentos, t.intentos, t.estado, t.idtipotarjeta, t.idsocio "
                + "FROM tarjetas t;")) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsTiposTarjeta.add(resultSet.getInt("idtipotarjeta"));

                Tarjeta objTarjeta = new Tarjeta(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        "",
                        resultSet.getTimestamp("fechaentrega"),
                        resultSet.getBoolean("activo"),
                        resultSet.getInt("intentos"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getTimestamp("fechabloqueointentos"),
                        resultSet.getBoolean("estado"),
                        null,
                        objSocio);
                lstTarjetas.add(objTarjeta);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstTarjetas.isEmpty()) {
            DAOTipoTarjeta objDAOTipoTarjeta = DAOTipoTarjeta.getInstance();

            int size = lstTarjetas.size();
            for (int i = 0; i < size; i++) {
                lstTarjetas.get(i).setObjTipoTarjeta(objDAOTipoTarjeta.getTipoTarjeta(lstIdsTiposTarjeta.get(i)));
            }
        }

        return lstTarjetas;
    }

    @Override
    public boolean insert(Tarjeta objTarjeta) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO tarjetas (codigo, pin, fechaentrega, activo, intentos, fecharegistro, estado, idtipotarjeta, idsocio) "
                + "VALUES(?, crypt(?, gen_salt('md5')), ?, ?, ?, ?, ?, ?, ?);")) {
            preparedStatement.setString(1, objTarjeta.getCodigo());
            preparedStatement.setString(2, StringUtil.decodeString(objTarjeta.getPin()));
            preparedStatement.setTimestamp(3, objTarjeta.getFechaEntrega());
            preparedStatement.setBoolean(4, objTarjeta.isActivo());
            preparedStatement.setInt(5, objTarjeta.getIntentos());
            preparedStatement.setTimestamp(6, objTarjeta.getFechaRegistro());
            preparedStatement.setBoolean(7, objTarjeta.isEstado());
            preparedStatement.setInt(8, objTarjeta.getObjTipoTarjeta().getId());
            preparedStatement.setLong(9, objTarjeta.getObjSocio().getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean update(Tarjeta objTarjeta) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE tarjetas SET codigo = ?, pin = CASE WHEN ? = '' THEN (SELECT pin FROM tarjetas WHERE id = ?) ELSE crypt(?, gen_salt('md5')) END, fechaentrega = ?, activo = ?, intentos = ?, fechamodificacion = now(), fechabloqueointentos = ?, estado = ?, idtipotarjeta = ?, idsocio = ? "
                + "WHERE id = ?;")) {
            preparedStatement.setString(1, objTarjeta.getCodigo());
            preparedStatement.setString(2, StringUtil.decodeString(objTarjeta.getPin()));
            preparedStatement.setLong(3, objTarjeta.getId());
            preparedStatement.setString(4, StringUtil.decodeString(objTarjeta.getPin()));
            preparedStatement.setTimestamp(5, objTarjeta.getFechaEntrega());
            preparedStatement.setBoolean(6, objTarjeta.isActivo());
            preparedStatement.setInt(7, objTarjeta.getIntentos());
            preparedStatement.setTimestamp(8, objTarjeta.getFechaBloqueoIntentos());
            preparedStatement.setBoolean(9, objTarjeta.isEstado());
            preparedStatement.setInt(10, objTarjeta.getObjTipoTarjeta().getId());
            preparedStatement.setLong(11, objTarjeta.getObjSocio().getId());
            preparedStatement.setLong(12, objTarjeta.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean verificar(Tarjeta objTarjeta, String clave) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT t.id "
                + "FROM tarjetas t "
                + "WHERE t.id = ? AND t.pin = crypt(?, pin);")) {
            preparedStatement.setLong(1, objTarjeta.getId());
            preparedStatement.setString(2, StringUtil.decodeString(clave));

            ResultSet resultSet = preparedStatement.executeQuery();

            result = resultSet.next();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean physicalDelete(Tarjeta objTarjeta) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM tarjetas WHERE id = ?;")) {
            preparedStatement.setLong(1, objTarjeta.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public String getUltimoCodigoTarjeta() {
        String result = "";

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT COALESCE((SELECT substring(t.codigo from 9 for 3) FROM tarjetas t "
                + "WHERE t.id = (SELECT MAX(tsub.id) id FROM tarjetas tsub)), '000') codigo;")) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                result = resultSet.getString("codigo");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
