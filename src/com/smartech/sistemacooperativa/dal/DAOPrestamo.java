package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Prestamo;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.interfaces.IDAOPrestamo;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOPrestamo implements IDAOPrestamo {

    private static DAOPrestamo instance = null;

    private DAOPrestamo() {
    }

    public static DAOPrestamo getInstance() {
        if (instance == null) {
            instance = new DAOPrestamo();
        }

        return instance;
    }

    @Override
    public Prestamo getPrestamo(long id) {
        Prestamo objPrestamo = null;
        long idSolicitudPrestamo = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id, p.cuotas, p.monto, "
                + "p.tem, p.ted, p.fecharegistro, p.fechamodificacion, p.estado, p.pagado, p.aprobado, p.idsolicitudprestamo "
                + "FROM prestamos p "
                + "WHERE p.id = ?;")) {
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idSolicitudPrestamo = resultSet.getLong("idsolicitudprestamo");

                objPrestamo = new Prestamo(
                        id,
                        resultSet.getInt("cuotas"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getBigDecimal("tem"),
                        resultSet.getBigDecimal("ted"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("pagado"),
                        null,
                        resultSet.getBoolean("aprobado"),
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objPrestamo != null) {
            objPrestamo.setObjSolicitud(DAOSolicitudPrestamo.getInstance().getSolicitud(idSolicitudPrestamo));
            objPrestamo.setLstCuotas(DAOCuota.getInstance().getAllCuotas(objPrestamo));
        }

        return objPrestamo;
    }

    @Override
    public Prestamo getPrestamoOnly(long id) {
        Prestamo objPrestamo = null;
        long idSolicitudPrestamo = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id, p.cuotas, p.monto, "
                + "p.tem, p.ted, p.fecharegistro, p.fechamodificacion, p.estado, p.pagado, p.aprobado, p.idsolicitudprestamo "
                + "FROM prestamos p "
                + "WHERE p.id = ?;")) {
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idSolicitudPrestamo = resultSet.getLong("idsolicitudprestamo");

                objPrestamo = new Prestamo(
                        id,
                        resultSet.getInt("cuotas"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getBigDecimal("tem"),
                        resultSet.getBigDecimal("ted"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("pagado"),
                        null,
                        resultSet.getBoolean("aprobado"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objPrestamo != null) {
            objPrestamo.setObjSolicitud(DAOSolicitudPrestamo.getInstance().getSolicitud(idSolicitudPrestamo));
        }

        return objPrestamo;
    }

    @Override
    public List<Prestamo> getAllPrestamos(Socio objSocio, boolean pagado) {
        List<Prestamo> lstPrestamos = new ArrayList<>();
        List<Long> lstIdSolicitudesPrestamo = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id, p.cuotas, p.monto, "
                + "p.tem, p.ted, p.fecharegistro, p.fechamodificacion, p.estado, p.pagado, p.aprobado, p.idsolicitudprestamo "
                + "FROM prestamos p "
                + "WHERE p.aprobado is true AND p.pagado = ? AND p.idsolicitudprestamo IN(SELECT sp.id "
                + "FROM solicitudesprestamo sp INNER JOIN detallesolicitudprestamosocio dsps ON sp.id = dsps.idsolicitudprestamo "
                + "WHERE dsps.idsocio = ? AND dsps.fiador is false AND sp.aprobado is true);")) {
            preparedStatement.setBoolean(1, pagado);
            preparedStatement.setLong(2, objSocio.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdSolicitudesPrestamo.add(resultSet.getLong("idsolicitudprestamo"));
                Prestamo objPrestamo = new Prestamo(
                        resultSet.getLong("id"),
                        resultSet.getInt("cuotas"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getBigDecimal("tem"),
                        resultSet.getBigDecimal("ted"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("pagado"),
                        null,
                        resultSet.getBoolean("aprobado"));
                lstPrestamos.add(objPrestamo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPrestamos.isEmpty()) {
            DAOSolicitudPrestamo objDAOSolicitudPrestamo = DAOSolicitudPrestamo.getInstance();
            DAOCuota objDAOCuota = DAOCuota.getInstance();

            int size = lstPrestamos.size();
            for (int i = 0; i < size; i++) {
                lstPrestamos.get(i).setObjSolicitud(objDAOSolicitudPrestamo.getSolicitud(lstIdSolicitudesPrestamo.get(i)));
                lstPrestamos.get(i).setLstCuotas(objDAOCuota.getAllCuotas(lstPrestamos.get(i)));
            }
        }

        return lstPrestamos;
    }

    @Override
    public List<Prestamo> getAllPrestamos(Timestamp fechaInicio, Timestamp fechaFin, boolean pagado) {
        List<Prestamo> lstPrestamos = new ArrayList<>();
        List<Long> lstIdSolicitudesPrestamo = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id, p.cuotas, p.monto, "
                + "p.tem, p.ted, p.fecharegistro, p.fechamodificacion, p.estado, p.pagado, p.aprobado, p.idsolicitudprestamo "
                + "FROM prestamos p "
                + "WHERE p.aprobado is true AND p.pagado = ? AND p.fecharegistro BETWEEN ? AND ?;")) {
            preparedStatement.setBoolean(1, pagado);
            preparedStatement.setTimestamp(2, fechaInicio);
            preparedStatement.setTimestamp(3, fechaFin);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdSolicitudesPrestamo.add(resultSet.getLong("idsolicitudprestamo"));
                Prestamo objPrestamo = new Prestamo(
                        resultSet.getLong("id"),
                        resultSet.getInt("cuotas"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getBigDecimal("tem"),
                        resultSet.getBigDecimal("ted"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("pagado"),
                        null,
                        resultSet.getBoolean("aprobado"));
                lstPrestamos.add(objPrestamo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPrestamos.isEmpty()) {
            DAOSolicitudPrestamo objDAOSolicitudPrestamo = DAOSolicitudPrestamo.getInstance();
            DAOCuota objDAOCuota = DAOCuota.getInstance();

            int size = lstPrestamos.size();
            for (int i = 0; i < size; i++) {
                lstPrestamos.get(i).setObjSolicitud(objDAOSolicitudPrestamo.getSolicitud(lstIdSolicitudesPrestamo.get(i)));
                lstPrestamos.get(i).setLstCuotas(objDAOCuota.getAllCuotas(lstPrestamos.get(i)));
            }
        }

        return lstPrestamos;
    }

    @Override
    public List<Prestamo> getAllPrestamos(Socio objSocio, Timestamp fechaInicio, Timestamp fechaFin, boolean pagado) {
        List<Prestamo> lstPrestamos = new ArrayList<>();
        List<Long> lstIdSolicitudesPrestamo = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id, p.cuotas, p.monto, "
                + "p.tem, p.ted, p.fecharegistro, p.fechamodificacion, p.estado, p.pagado, p.aprobado, p.idsolicitudprestamo "
                + "FROM prestamos p "
                + "WHERE p.aprobado is true AND p.pagado = ? AND p.idsolicitudprestamo IN(SELECT sp.id "
                + "FROM solicitudesprestamo sp INNER JOIN detallesolicitudprestamosocio dsps ON sp.id = dsps.idsolicitudprestamo "
                + "WHERE dsps.idsocio = ? AND dsps.fiador is false AND sp.aprobado is true) AND p.fecharegistro BETWEEN ? AND ?;")) {
            preparedStatement.setBoolean(1, pagado);
            preparedStatement.setLong(2, objSocio.getId());
            preparedStatement.setTimestamp(3, fechaInicio);
            preparedStatement.setTimestamp(4, fechaFin);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdSolicitudesPrestamo.add(resultSet.getLong("idsolicitudprestamo"));
                Prestamo objPrestamo = new Prestamo(
                        resultSet.getLong("id"),
                        resultSet.getInt("cuotas"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getBigDecimal("tem"),
                        resultSet.getBigDecimal("ted"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("pagado"),
                        null,
                        resultSet.getBoolean("aprobado"));
                lstPrestamos.add(objPrestamo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPrestamos.isEmpty()) {
            DAOSolicitudPrestamo objDAOSolicitudPrestamo = DAOSolicitudPrestamo.getInstance();
            DAOCuota objDAOCuota = DAOCuota.getInstance();
            
            int size = lstPrestamos.size();
            for (int i = 0; i < size; i++) {
                lstPrestamos.get(i).setObjSolicitud(objDAOSolicitudPrestamo.getSolicitud(lstIdSolicitudesPrestamo.get(i)));
                lstPrestamos.get(i).setLstCuotas(objDAOCuota.getAllCuotas(lstPrestamos.get(i)));
            }
        }

        return lstPrestamos;
    }

    @Override
    public List<Prestamo> getAllPrestamosAsGarante(Socio objSocio, boolean pagado) {
        List<Prestamo> lstPrestamos = new ArrayList<>();
        List<Long> lstIdSolicitudesPrestamo = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id, p.cuotas, p.monto, "
                + "p.tem, p.ted, p.fecharegistro, p.fechamodificacion, p.estado, p.pagado, p.aprobado, p.idsolicitudprestamo "
                + "FROM prestamos p "
                + "WHERE p.aprobado is true AND p.pagado = ? AND p.idsolicitudprestamo IN(SELECT sp.id "
                + "FROM solicitudesprestamo sp INNER JOIN detallesolicitudprestamosocio dsps ON sp.id = dsps.idsolicitudprestamo "
                + "WHERE dsps.idsocio = ? AND dsps.fiador is true AND sp.aprobado is true);")) {
            preparedStatement.setBoolean(1, pagado);
            preparedStatement.setLong(2, objSocio.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdSolicitudesPrestamo.add(resultSet.getLong("idsolicitudprestamo"));
                Prestamo objPrestamo = new Prestamo(
                        resultSet.getLong("id"),
                        resultSet.getInt("cuotas"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getBigDecimal("tem"),
                        resultSet.getBigDecimal("ted"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("pagado"),
                        null,
                        resultSet.getBoolean("aprobado"));
                lstPrestamos.add(objPrestamo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPrestamos.isEmpty()) {
            DAOSolicitudPrestamo objDAOSolicitudPrestamo = DAOSolicitudPrestamo.getInstance();
            DAOCuota objDAOCuota = DAOCuota.getInstance();

            int size = lstPrestamos.size();
            for (int i = 0; i < size; i++) {
                lstPrestamos.get(i).setObjSolicitud(objDAOSolicitudPrestamo.getSolicitud(lstIdSolicitudesPrestamo.get(i)));
                lstPrestamos.get(i).setLstCuotas(objDAOCuota.getAllCuotas(lstPrestamos.get(i)));
            }
        }

        return lstPrestamos;
    }

    @Override
    public List<Prestamo> getAllPrestamosAsGarante(Socio objSocio, Timestamp fechaInicio, Timestamp fechaFin, boolean pagado) {
        List<Prestamo> lstPrestamos = new ArrayList<>();
        List<Long> lstIdSolicitudesPrestamo = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT p.id, p.cuotas, p.monto, "
                + "p.tem, p.ted, p.fecharegistro, p.fechamodificacion, p.estado, p.pagado, p.aprobado, p.idsolicitudprestamo "
                + "FROM prestamos p "
                + "WHERE p.aprobado is true AND p.pagado = ? AND p.idsolicitudprestamo IN(SELECT sp.id "
                + "FROM solicitudesprestamo sp INNER JOIN detallesolicitudprestamosocio dsps ON sp.id = dsps.idsolicitudprestamo "
                + "WHERE dsps.idsocio = ? AND dsps.fiador is true AND sp.aprobado is true) AND p.fecharegistro BETWEEN ? AND ?;")) {
            preparedStatement.setBoolean(1, pagado);
            preparedStatement.setLong(2, objSocio.getId());
            preparedStatement.setTimestamp(3, fechaInicio);
            preparedStatement.setTimestamp(4, fechaFin);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdSolicitudesPrestamo.add(resultSet.getLong("idsolicitudprestamo"));
                Prestamo objPrestamo = new Prestamo(
                        resultSet.getLong("id"),
                        resultSet.getInt("cuotas"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getBigDecimal("tem"),
                        resultSet.getBigDecimal("ted"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("pagado"),
                        null,
                        resultSet.getBoolean("aprobado"));
                lstPrestamos.add(objPrestamo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstPrestamos.isEmpty()) {
            DAOSolicitudPrestamo objDAOSolicitudPrestamo = DAOSolicitudPrestamo.getInstance();
            DAOCuota objDAOCuota = DAOCuota.getInstance();

            int size = lstPrestamos.size();
            for (int i = 0; i < size; i++) {
                lstPrestamos.get(i).setObjSolicitud(objDAOSolicitudPrestamo.getSolicitud(lstIdSolicitudesPrestamo.get(i)));
                lstPrestamos.get(i).setLstCuotas(objDAOCuota.getAllCuotas(lstPrestamos.get(i)));
            }
        }

        return lstPrestamos;
    }

    @Override
    public boolean insert(Prestamo objPrestamo) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO prestamos(cuotas, monto, tem, ted, fecharegistro, estado, aprobado, idsolicitudprestamo) "
                + "VALUES(?, ?, ?, ?, now(), ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setInt(1, objPrestamo.getCuotas());
            preparedStatement.setBigDecimal(2, objPrestamo.getMonto());
            preparedStatement.setBigDecimal(3, objPrestamo.getTem());
            preparedStatement.setBigDecimal(4, objPrestamo.getTed());
            preparedStatement.setBoolean(5, objPrestamo.isEstado());
            preparedStatement.setBoolean(6, objPrestamo.isAprobado());
            preparedStatement.setLong(7, objPrestamo.getObjSolicitudPrestamo().getId());

            preparedStatement.execute();

            ResultSet keySet = preparedStatement.getGeneratedKeys();

            while (keySet.next()) {
                objPrestamo.setId(keySet.getLong(1));
            }

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean update(Prestamo objPrestamo) {
        boolean result = false;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE prestamos SET cuotas = ?, monto =  ?, tem = ?, ted = ?, fechamodificacion = now(), estado = ?, aprobado = ?, pagado = ?, idsolicitudprestamo = ? "
                + "WHERE id = ?;")){
            preparedStatement.setInt(1, objPrestamo.getCuotas());
            preparedStatement.setBigDecimal(2, objPrestamo.getMonto());
            preparedStatement.setBigDecimal(3, objPrestamo.getTem());
            preparedStatement.setBigDecimal(4, objPrestamo.getTed());
            preparedStatement.setBoolean(5, objPrestamo.isEstado());
            preparedStatement.setBoolean(6, objPrestamo.isAprobado());
            preparedStatement.setBoolean(7, objPrestamo.isPagado());
            preparedStatement.setLong(8, objPrestamo.getObjSolicitudPrestamo().getId());
            preparedStatement.setLong(9, objPrestamo.getId());
            
            preparedStatement.execute();
            
            result = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
    }

    @Override
    public boolean physicalDelete(Prestamo objPrestamo) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM prestamos WHERE id = ?;")) {
            preparedStatement.setLong(1, objPrestamo.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
