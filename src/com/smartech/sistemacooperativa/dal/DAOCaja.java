/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Caja;
import com.smartech.sistemacooperativa.dominio.Establecimiento;
import com.smartech.sistemacooperativa.interfaces.IDAOCaja;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOCaja implements IDAOCaja {

    private static DAOCaja instance = null;
    
    public static DAOCaja getInstance(){
        if(instance == null){
            instance = new DAOCaja();
        }
        return instance;
    }

    @Override
    public Caja getCaja(int id) {
        Caja objCaja = null;
        int idEstablecimiento = 0;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT id, monto, codigo, idestablecimiento, abierto "
                + "FROM cajas "
                + "WHERE id = ?;")){
            preparedStatement.setInt(1, id);
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                idEstablecimiento = resultSet.getInt("idestablecimiento");
                objCaja = new Caja(
                        resultSet.getInt("id"), 
                        resultSet.getBigDecimal("monto"),
                        resultSet.getString("codigo"),
                        null ,
                        resultSet.getBoolean("abierto"));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        if(objCaja != null){
            objCaja.setObjEstablecimiento(DAOEstablecimiento.getInstance().getEstablecimiento(idEstablecimiento));
        }
        
        return objCaja;
    }
    
    @Override
    public List<Caja> getAllCajas(Establecimiento objEstablecimiento) {
        List<Caja> lstCajas = new ArrayList<>();
        
        try (PreparedStatement objPreparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT id, monto, codigo, idestablecimiento, abierto "
                + "FROM cajas "
                + "WHERE idestablecimiento = ? ORDER BY id asc;");){
            
            objPreparedStatement.setInt(1, objEstablecimiento.getId());
            ResultSet objResultSet = objPreparedStatement.executeQuery();
            
            Caja objCaja;
            while(objResultSet.next()){
                objCaja = new Caja(objResultSet.getInt("id"), 
                        objResultSet.getBigDecimal("monto"),objResultSet.getString("codigo"), objEstablecimiento,objResultSet.getBoolean("abierto"));
                
                lstCajas.add(objCaja);
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return lstCajas;
    }    

    @Override
    public List<Caja> getAllCajasAbiertas(Establecimiento objEstablecimiento) {
        List<Caja> lstCajas = new ArrayList<>();
        
        try (PreparedStatement objPreparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT id, monto, codigo, idestablecimiento, abierto "
                + "FROM cajas "
                + "WHERE idestablecimiento = ? and abierto = true ORDER BY id asc;");){
            
            objPreparedStatement.setInt(1, objEstablecimiento.getId());
            ResultSet objResultSet = objPreparedStatement.executeQuery();
            
            Caja objCaja;
            while(objResultSet.next()){
                objCaja = new Caja(objResultSet.getInt("id"), 
                        objResultSet.getBigDecimal("monto"),objResultSet.getString("codigo"), objEstablecimiento,objResultSet.getBoolean("abierto"));
                
                lstCajas.add(objCaja);
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return lstCajas;
    }

    @Override
    public List<Caja> getAllCajasCerradas(Establecimiento objEstablecimiento) {
        List<Caja> lstCajas = new ArrayList<>();
        
        try (PreparedStatement objPreparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT c.id, c.monto, c.codigo, c.idestablecimiento, c.abierto "
                + "FROM cajas c "
                + "WHERE c.idestablecimiento = ? AND c.abierto is false "
                + "AND c.id not in (select hcsub.idcaja from historicocaja hcsub "
                                 + "where hcsub.fechafin is null or hcsub.montofin is null or hcsub.montofin = 0 "
                                 + "order by hcsub.id desc limit 1) "
                + "ORDER BY c.id asc;");){
            
            objPreparedStatement.setInt(1, objEstablecimiento.getId());
            ResultSet objResultSet = objPreparedStatement.executeQuery();
            
            Caja objCaja;
            while(objResultSet.next()){
                objCaja = new Caja(objResultSet.getInt("id"), 
                        objResultSet.getBigDecimal("monto"),objResultSet.getString("codigo"), objEstablecimiento,objResultSet.getBoolean("abierto"));
                
                lstCajas.add(objCaja);
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return lstCajas;
    }

    @Override
    public boolean update(Caja objCaja) {
        boolean result = false;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE cajas "
                + "set monto = ?, "
                + "codigo = ?, "
                + "idestablecimiento = ?, "
                + "abierto = ? "
                + "WHERE id = ?;")){
            
            preparedStatement.setBigDecimal(1, objCaja.getMonto());
            preparedStatement.setString(2, objCaja.getCodigo());
            preparedStatement.setInt(3, objCaja.getObjEstablecimiento().getId());
            preparedStatement.setBoolean(4, objCaja.isAbierto());
            preparedStatement.setLong(5, objCaja.getId());
            
            preparedStatement.execute();
            
            result = true;
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return result;
    }
    
}
