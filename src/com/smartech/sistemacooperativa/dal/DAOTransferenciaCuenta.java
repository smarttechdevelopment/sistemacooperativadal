/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.Transferencia;
import com.smartech.sistemacooperativa.dominio.TransferenciaCuenta;
import com.smartech.sistemacooperativa.interfaces.IDAOTransferenciaCuenta;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAOTransferenciaCuenta implements IDAOTransferenciaCuenta {

    private static DAOTransferenciaCuenta instance = null;

    public DAOTransferenciaCuenta() {
    }

    public static DAOTransferenciaCuenta getInstance() {
        if (instance == null) {
            instance = new DAOTransferenciaCuenta();
        }
        return instance;
    }

    @Override
    public List<TransferenciaCuenta> getAllTransferenciasCuenta(Cuenta objCuenta) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<TransferenciaCuenta> getAllTransferenciasCuenta(Socio objSocio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Transferencia getTransferencia(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Transferencia getTransferencia(Pago objPago) {
        TransferenciaCuenta objTransferenciaCuenta = null;
        long idCuentaOrigen = 0;
        long idCuentaDestino = 0;
        int idMoneda = 0;
        long idUsuario = 0;
        
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT  t.id, t.monto, t.intercuenta, t.fecha, t.fecharegistro, t.estado, t.idmoneda, t.idusuario, tc.origen, tc.idcuenta " 
                + "FROM transferencias t " 
                + "INNER JOIN transferenciacuentas tc ON tc.idtransferencia = t.id "
                + "WHERE t.id IN (SELECT pt.idtransferencia FROM detallepagotransferencia pt WHERE pt.idpago = ?);")){
            preparedStatement.setLong(1, objPago.getId());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                if(resultSet.getBoolean("origen")){
                    objTransferenciaCuenta = new TransferenciaCuenta(
                            resultSet.getLong("id"), 
                            resultSet.getTimestamp("fecha"), 
                            resultSet.getBigDecimal("monto"), 
                            resultSet.getBoolean("estado"),
                            resultSet.getTimestamp("fecharegistro"),  
                            null, // moneda
                            null, // usuario
                            null, // origen 
                            null); // destino
                    idCuentaOrigen = resultSet.getLong("idcuenta");
                }else{
                    idCuentaDestino = resultSet.getLong("idcuenta");
                    idMoneda = resultSet.getInt("idmoneda");
                    idUsuario = resultSet.getLong("idusuario");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if(objTransferenciaCuenta != null){
            objTransferenciaCuenta.setObjCuentaOrigen(DAOCuenta.getInstance().getCuenta(idCuentaOrigen));
            objTransferenciaCuenta.setObjCuentaDestino(DAOCuenta.getInstance().getCuenta(idCuentaDestino));
            objTransferenciaCuenta.setObjMoneda(DAOMoneda.getInstance().getMoneda(idMoneda));
            objTransferenciaCuenta.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
        }
        
        return objTransferenciaCuenta;
    }

    @Override
    public TransferenciaCuenta getTransferenciaCuenta(Transferencia objTransferencia) {
        TransferenciaCuenta objTransferenciaCuenta = null;
        long idCuentaOrigen = 0;
        long idCuentaDestino = 0;
        
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT tc.id, tc.origen, tc.idtransferencia, tc.idcuenta, tc.idusuario " +
                "FROM transferenciacuentas tc " +
                "WHERE tc.idtransferencia = ?")){
            preparedStatement.setLong(1, objTransferencia.getId());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                if(resultSet.getBoolean("origen")){
                    objTransferenciaCuenta = new TransferenciaCuenta(
                            0, objTransferencia.getFecha(), 
                            objTransferencia.getMonto(), 
                            objTransferencia.isEstado(), 
                            objTransferencia.getFechaRegistro(), 
                            objTransferencia.getObjMoneda(), 
                            objTransferencia.getObjUsuario(),
                            null, 
                            null);
                    idCuentaOrigen = resultSet.getLong("idcuenta");
                }else{
                    idCuentaDestino = resultSet.getLong("idcuenta");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if(objTransferenciaCuenta != null){
            objTransferenciaCuenta.setObjCuentaOrigen(DAOCuenta.getInstance().getCuenta(idCuentaOrigen));
            objTransferenciaCuenta.setObjCuentaDestino(DAOCuenta.getInstance().getCuenta(idCuentaDestino));
        }
        
        return objTransferenciaCuenta;
    }

    @Override
    public List<Transferencia> getAllTransferencias(Cuenta objCuenta) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insertDetallePagoTransferenciaCuenta(Pago objPago, TransferenciaCuenta objtraTransferenciaCuenta) {
        boolean result = false;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO detallepagotransferencia "
                + "(monto,fecharegistro,estado,idpago,idtransferencia) "
                + "VALUES (?,?,?,?,?); ")) {
            preparedStatement.setBigDecimal(1, objtraTransferenciaCuenta.getMonto());
            preparedStatement.setTimestamp(2, objPago.getFechaRegistro());
            preparedStatement.setBoolean(3, objPago.isEstado());
            preparedStatement.setLong(4, objPago.getId());
            preparedStatement.setLong(5, objtraTransferenciaCuenta.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<Transferencia> getAllTransferencias(Timestamp fechaDesde, Timestamp fechaFin) {
        List<TransferenciaCuenta> lstTransferenciasCuenta = new ArrayList<>();
        List<Integer> lstIdsMonedas = new ArrayList<>();
        List<Integer> lstIdsUsuarios = new ArrayList<>();
        //List<Long> lstIdsCuentas = new ArrayList<>();
        Map<Long, Map<Long, Boolean>> mapTransferencia = new HashMap<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT t.id, t.monto, t.intercuenta, t.fecha, t.fecharegistro, t.estado, t.idmoneda, t.idusuario, tc.origen, tc.idcuenta "
                + " FROM transferencias as t INNER JOIN transferenciacuentas as tc ON tc.idtransferencia = t.id "
                + " WHERE t.fecharegistro BETWEEN ? AND ?; ")) {
            preparedStatement.setTimestamp(1, fechaDesde);
            preparedStatement.setTimestamp(2, fechaFin);

            ResultSet resultSet = preparedStatement.executeQuery();

            boolean origen;
            TransferenciaCuenta objTransferenciaCuenta;
            while (resultSet.next()) {
                origen = resultSet.getBoolean("origen");
                long id = resultSet.getLong("id");
                lstIdsMonedas.add(resultSet.getInt("idmoneda"));
                lstIdsUsuarios.add(resultSet.getInt("idusuario"));
                objTransferenciaCuenta = new TransferenciaCuenta(
                        id,
                        resultSet.getTimestamp("fecha"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fecharegistro"),
                        null, //objMoneda
                        null, // objUsuario
                        null, //objCuentaOrigen, 
                        null //objCuentaDestino
                );

                if (mapTransferencia.get(id) != null) {
                    Map<Long, Boolean> mapCuenta = mapTransferencia.get(id);
                    mapCuenta.put(resultSet.getLong("idcuenta"), origen);
                } else {
                    lstTransferenciasCuenta.add(objTransferenciaCuenta);
                    Map<Long, Boolean> mapCuenta = new HashMap<>();
                    mapCuenta.put(resultSet.getLong("idcuenta"), origen);
                    mapTransferencia.put(id, mapCuenta);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstTransferenciasCuenta.isEmpty()) {
            DAOMoneda objDAOMoneda = DAOMoneda.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOCuenta objDAOCuenta = DAOCuenta.getInstance();

            int size = lstTransferenciasCuenta.size();
            for (int i = 0; i < size; i++) {
                lstTransferenciasCuenta.get(i).setObjMoneda(objDAOMoneda.getMoneda(lstIdsMonedas.get(i)));
                lstTransferenciasCuenta.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                Map<Long, Boolean> mapCuenta = mapTransferencia.get(lstTransferenciasCuenta.get(i).getId());
                if(mapCuenta.size() != 2){
                    return new ArrayList<>();
                }
                for (Map.Entry<Long, Boolean> entry : mapCuenta.entrySet()) {
                    Boolean value = entry.getValue();
                    if (value) {
                        lstTransferenciasCuenta.get(i).setObjCuentaOrigen(objDAOCuenta.getCuenta(entry.getKey()));
                    } else {
                        lstTransferenciasCuenta.get(i).setObjCuentaDestino(objDAOCuenta.getCuenta(entry.getKey()));
                    }
                }
            }
        }

        return new ArrayList<>(lstTransferenciasCuenta);
    }

    @Override
    public boolean insert(Transferencia objTransferencia) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO transferenciacuentas "
                + "(origen,idtransferencia,idcuenta,idusuario) "
                + "VALUES (?,?,?,?);")) {
            result = DAOTransferencia.getInstance().insert(objTransferencia);
            if (result) {
                if (objTransferencia instanceof TransferenciaCuenta) {
                    TransferenciaCuenta objTransferenciaCuenta = (TransferenciaCuenta) objTransferencia;

                    preparedStatement.setBoolean(1, true);
                    preparedStatement.setLong(2, objTransferencia.getId());
                    preparedStatement.setLong(3, objTransferenciaCuenta.getObjCuentaOrigen().getId());
                    preparedStatement.setLong(4, objTransferenciaCuenta.getObjUsuario().getId());

                    preparedStatement.execute();
                    
                    preparedStatement.clearParameters();

                    preparedStatement.setBoolean(1, false);
                    preparedStatement.setLong(2, objTransferencia.getId());
                    preparedStatement.setLong(3, objTransferenciaCuenta.getObjCuentaDestino().getId());
                    preparedStatement.setLong(4, objTransferenciaCuenta.getObjUsuario().getId());

                    preparedStatement.execute();

                    result = true;
                } else {
                    result = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean update(Transferencia objTransferencia) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE transferenciacuentas "
                + "SET origen = ?, "
                + "idtransferencia = ?, "
                + "idcuenta = ?, "
                + "idusuario = ?"
                + "WHERE idtransferencia = ? and idcuenta = ?;")) {
            result = DAOTransferencia.getInstance().update(objTransferencia);
            if (result) {
                if (objTransferencia instanceof TransferenciaCuenta) {
                    TransferenciaCuenta objTransferenciaCuenta = (TransferenciaCuenta) objTransferencia;

                    preparedStatement.setBoolean(1, true);
                    preparedStatement.setLong(2, objTransferencia.getId());
                    preparedStatement.setLong(3, objTransferenciaCuenta.getObjCuentaOrigen().getId());
                    preparedStatement.setLong(4, objTransferenciaCuenta.getObjUsuario().getId());
                    preparedStatement.setLong(5, objTransferencia.getId());
                    preparedStatement.setLong(6, objTransferenciaCuenta.getObjCuentaOrigen().getId());

                    preparedStatement.clearParameters();

                    preparedStatement.setBoolean(1, false);
                    preparedStatement.setLong(2, objTransferencia.getId());
                    preparedStatement.setLong(3, objTransferenciaCuenta.getObjCuentaDestino().getId());
                    preparedStatement.setLong(4, objTransferenciaCuenta.getObjUsuario().getId());
                    preparedStatement.setLong(5, objTransferencia.getId());
                    preparedStatement.setLong(6, objTransferenciaCuenta.getObjCuentaDestino().getId());

                    preparedStatement.execute();

                    result = true;
                } else {
                    result = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean delete(Transferencia objTransferencia) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM transferenciacuentas "
                + "WHERE idtransferencia = ?;")) {
            preparedStatement.setLong(1, objTransferencia.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
