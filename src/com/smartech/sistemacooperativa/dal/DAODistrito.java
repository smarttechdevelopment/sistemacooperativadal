/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Distrito;
import com.smartech.sistemacooperativa.dominio.Provincia;
import com.smartech.sistemacooperativa.interfaces.IDAODistrito;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAODistrito implements IDAODistrito {

    private static DAODistrito instance = null;
    private Map<Integer, Distrito> cache;

    private DAODistrito() {
        this.cache = new HashMap<>();
    }

    public static DAODistrito getInstance() {
        if (instance == null) {
            instance = new DAODistrito();
        }
        return instance;
    }

    @Override
    public Distrito getDistrito(int id) {
        Distrito objDistrito = this.cache.get(id);

        if (objDistrito == null) {
            this.cache.clear();
            this.getAllDistritos();
            objDistrito = this.cache.get(id);
        }

        return objDistrito;
    }

    @Override
    public List<Distrito> getAllDistritos() {
        List<Distrito> lstDistritos;
        Map<Integer, Integer> mapIdsProvincias = new HashMap<>();

        if (this.cache.isEmpty()) {

            try (PreparedStatement pst = ConnectionManager.getConnection().prepareStatement("SELECT id, nombre, idprovincia FROM distritos ORDER BY id;");) {

                ResultSet rs = pst.executeQuery();
                
                while(rs.next()){
                    Distrito objDistrito = new Distrito(rs.getInt("id"), rs.getString("nombre"), null);
                    mapIdsProvincias.put(objDistrito.getId(), rs.getInt("idprovincia"));
                    this.cache.put(rs.getInt("id"), objDistrito);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            if(!this.cache.isEmpty()){
                DAOProvincia objDAOProvincia = DAOProvincia.getInstance();
                
                for(Map.Entry<Integer,Distrito> entry : this.cache.entrySet()){
                    Distrito objDistrito = entry.getValue();
                    objDistrito.setObjProvincia(objDAOProvincia.getProvincia(mapIdsProvincias.get(objDistrito.getId())));
                }
            }
        }

        Collection<Distrito> collection = this.cache.values();

        lstDistritos = new ArrayList<>(collection);

        return lstDistritos;
    }

    @Override
    public List<Distrito> getAllDistritos(Provincia objProvincia) {
        List<Distrito> lstDistritos = new ArrayList<>();
        
        if(this.cache.isEmpty()){
            this.getAllDistritos();
        }
        
        for(Map.Entry<Integer, Distrito> entry : this.cache.entrySet()){
            Distrito objDistrito = entry.getValue();
            if(objDistrito.getObjProvincia().getId() == objProvincia.getId()){
                lstDistritos.add(objDistrito);
            }
        }
        
        return lstDistritos;
    }

}
