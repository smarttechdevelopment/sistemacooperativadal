package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Cuota;
import com.smartech.sistemacooperativa.dominio.HabilitacionCuotas;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Prestamo;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.interfaces.IDAOCuota;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOCuota implements IDAOCuota {

    private static DAOCuota instance = null;

    private DAOCuota() {
    }

    public static DAOCuota getInstance() {
        if (instance == null) {
            instance = new DAOCuota();
        }

        return instance;
    }

    @Override
    public Cuota getCuota(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Cuota> getAllCuotas(Prestamo objPrestamo) {
        List<Cuota> lstCuotas = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        List<Long> lstIdsPrestamos = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT c.id, c.numero, c.monto, c.interes, c.amortizacion, c.mora, c.deudapendiente, "
                + "c.fechavencimiento, c.fechapago, c.pagado, c.fecharegistro, c.estado, c.idusuario, c.idprestamo "
                + "FROM cuotas c "
                + "WHERE c.idprestamo = ? "
                + "ORDER BY c.numero;")) {
            preparedStatement.setLong(1, objPrestamo.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                lstIdsPrestamos.add(resultSet.getLong("idprestamo"));

                Cuota objCuota = new Cuota(
                        resultSet.getInt("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getInt("numero"),
                        resultSet.getBigDecimal("interes"),
                        resultSet.getBigDecimal("amortizacion"),
                        resultSet.getBigDecimal("mora"),
                        resultSet.getBigDecimal("deudapendiente"),
                        resultSet.getTimestamp("fechavencimiento"),
                        resultSet.getTimestamp("fechapago"),
                        resultSet.getBoolean("pagado"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getBoolean("estado"),
                        null,
                        objPrestamo);
                lstCuotas.add(objCuota);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstCuotas.isEmpty()) {
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOPrestamo objDAOPrestamo = DAOPrestamo.getInstance();

            int size = lstCuotas.size();
            for (int i = 0; i < size; i++) {
                lstCuotas.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstCuotas.get(i).setObjPrestamo(objDAOPrestamo.getPrestamoOnly(lstIdsPrestamos.get(i)));
            }
        }

        return lstCuotas;
    }

    @Override
    public List<Cuota> getAllCuotas(HabilitacionCuotas objHabilitacionCuotas) {
        List<Cuota> lstCuotas = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        List<Long> lstIdsPrestamos = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT c.id, c.numero, c.monto, c.interes, c.amortizacion, c.mora, c.deudapendiente, "
                + "c.fechavencimiento, c.fechapago, c.pagado, c.fecharegistro, c.estado, c.idusuario, c.idprestamo "
                + "FROM cuotas c INNER JOIN detallehabilitacioncuota dhc ON c.id = dhc.idcuota "
                + "WHERE dhc.idhabilitacioncuota = ? "
                + "ORDER BY c.numero;")) {
            preparedStatement.setLong(1, objHabilitacionCuotas.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                lstIdsPrestamos.add(resultSet.getLong("idprestamo"));

                Cuota objCuota = new Cuota(
                        resultSet.getInt("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getInt("numero"),
                        resultSet.getBigDecimal("interes"),
                        resultSet.getBigDecimal("amortizacion"),
                        resultSet.getBigDecimal("mora"),
                        resultSet.getBigDecimal("deudapendiente"),
                        resultSet.getTimestamp("fechavencimiento"),
                        resultSet.getTimestamp("fechapago"),
                        resultSet.getBoolean("pagado"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getBoolean("estado"),
                        null,
                        null);
                lstCuotas.add(objCuota);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstCuotas.isEmpty()) {
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOPrestamo objDAOPrestamo = DAOPrestamo.getInstance();

            int size = lstCuotas.size();
            for (int i = 0; i < size; i++) {
                lstCuotas.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstCuotas.get(i).setObjPrestamo(objDAOPrestamo.getPrestamoOnly(lstIdsPrestamos.get(i)));
            }
        }

        return lstCuotas;
    }

    @Override
    public List<Cuota> getAllCuotasPorPagar(Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Cuota> lstCuotas = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        List<Long> lstIdsPrestamos = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT c.id, c.numero, c.monto, c.interes, c.amortizacion, c.mora, c.deudapendiente, "
                + "c.fechavencimiento, c.fechapago, c.pagado, c.fecharegistro, c.estado, c.idusuario, c.idprestamo "
                + "FROM cuotas c "
                + "WHERE c.pagado is false AND c.fechavencimiento BETWEEN ? AND ? "
                + "ORDER BY c.numero;")) {
            preparedStatement.setTimestamp(1, fechaDesde);
            preparedStatement.setTimestamp(2, fechaHasta);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                lstIdsPrestamos.add(resultSet.getLong("idprestamo"));

                Cuota objCuota = new Cuota(
                        resultSet.getInt("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getInt("numero"),
                        resultSet.getBigDecimal("interes"),
                        resultSet.getBigDecimal("amortizacion"),
                        resultSet.getBigDecimal("mora"),
                        resultSet.getBigDecimal("deudapendiente"),
                        resultSet.getTimestamp("fechavencimiento"),
                        resultSet.getTimestamp("fechapago"),
                        resultSet.getBoolean("pagado"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getBoolean("estado"),
                        null,
                        null);
                lstCuotas.add(objCuota);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstCuotas.isEmpty()) {
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOPrestamo objDAOPrestamo = DAOPrestamo.getInstance();

            int size = lstCuotas.size();
            for (int i = 0; i < size; i++) {
                lstCuotas.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstCuotas.get(i).setObjPrestamo(objDAOPrestamo.getPrestamoOnly(lstIdsPrestamos.get(i)));
            }
        }

        return lstCuotas;
    }

    @Override
    public List<Cuota> getAllCuotasPagadas(Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Cuota> lstCuotas = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        List<Long> lstIdsPrestamos = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT c.id, c.numero, c.monto, c.interes, c.amortizacion, c.mora, c.deudapendiente, "
                + "c.fechavencimiento, c.fechapago, c.pagado, c.fecharegistro, c.estado, c.idusuario, c.idprestamo "
                + "FROM cuotas c "
                + "WHERE c.pagado is true AND c.fechavencimiento BETWEEN ? AND ? "
                + "ORDER BY c.numero;")) {
            preparedStatement.setTimestamp(1, fechaDesde);
            preparedStatement.setTimestamp(2, fechaHasta);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                lstIdsPrestamos.add(resultSet.getLong("idprestamo"));

                Cuota objCuota = new Cuota(
                        resultSet.getInt("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getInt("numero"),
                        resultSet.getBigDecimal("interes"),
                        resultSet.getBigDecimal("amortizacion"),
                        resultSet.getBigDecimal("mora"),
                        resultSet.getBigDecimal("deudapendiente"),
                        resultSet.getTimestamp("fechavencimiento"),
                        resultSet.getTimestamp("fechapago"),
                        resultSet.getBoolean("pagado"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getBoolean("estado"),
                        null,
                        null);
                lstCuotas.add(objCuota);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstCuotas.isEmpty()) {
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOPrestamo objDAOPrestamo = DAOPrestamo.getInstance();

            int size = lstCuotas.size();
            for (int i = 0; i < size; i++) {
                lstCuotas.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstCuotas.get(i).setObjPrestamo(objDAOPrestamo.getPrestamoOnly(lstIdsPrestamos.get(i)));
            }
        }

        return lstCuotas;
    }

    @Override
    public List<Cuota> getAllCuotasPorPagar(Socio objSocio) {
        List<Cuota> lstCuotas = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        List<Long> lstIdsPrestamos = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT c.id, c.numero, c.monto, c.interes, c.amortizacion, c.mora, c.deudapendiente, "
                + "c.fechavencimiento, c.fechapago, c.pagado, c.fecharegistro, c.estado, c.idusuario, c.idprestamo "
                + "FROM cuotas c INNER JOIN prestamos p ON p.id = c.idprestamo "
                + "	INNER JOIN solicitudesprestamo sp ON sp.id = p.idsolicitudprestamo "
                + "	INNER JOIN detallesolicitudprestamosocio dsps ON sp.id = dsps.idsolicitudprestamo AND dsps.fiador is false "
                + "	INNER JOIN socios s on s.id = dsps.idsocio "
                + "WHERE c.pagado is false AND s.id = ? "
                + "ORDER BY c.numero;")) {
            preparedStatement.setLong(1, objSocio.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                lstIdsPrestamos.add(resultSet.getLong("idprestamo"));

                Cuota objCuota = new Cuota(
                        resultSet.getInt("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getInt("numero"),
                        resultSet.getBigDecimal("interes"),
                        resultSet.getBigDecimal("amortizacion"),
                        resultSet.getBigDecimal("mora"),
                        resultSet.getBigDecimal("deudapendiente"),
                        resultSet.getTimestamp("fechavencimiento"),
                        resultSet.getTimestamp("fechapago"),
                        resultSet.getBoolean("pagado"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getBoolean("estado"),
                        null,
                        null);
                lstCuotas.add(objCuota);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstCuotas.isEmpty()) {
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOPrestamo objDAOPrestamo = DAOPrestamo.getInstance();

            int size = lstCuotas.size();
            for (int i = 0; i < size; i++) {
                lstCuotas.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstCuotas.get(i).setObjPrestamo(objDAOPrestamo.getPrestamoOnly(lstIdsPrestamos.get(i)));
            }
        }

        return lstCuotas;
    }

    @Override
    public boolean insert(Cuota objCuota) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO cuotas(numero, monto, interes, amortizacion, deudapendiente, fechavencimiento, pagado, fecharegistro, estado, idusuario, idprestamo) "
                + "VALUES(?, ?, ?, ?, ?, ?, ?, now(), ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setInt(1, objCuota.getNumero());
            preparedStatement.setBigDecimal(2, objCuota.getMonto());
            preparedStatement.setBigDecimal(3, objCuota.getInteres());
            preparedStatement.setBigDecimal(4, objCuota.getAmortizacion());
            preparedStatement.setBigDecimal(5, objCuota.getDeudaPendiente());
            preparedStatement.setTimestamp(6, objCuota.getFechaVencimiento());
            preparedStatement.setBoolean(7, objCuota.isPagado());
            preparedStatement.setBoolean(8, objCuota.isEstado());
            preparedStatement.setLong(9, objCuota.getObjUsuario().getId());
            preparedStatement.setLong(10, objCuota.getObjPrestamo().getId());

            preparedStatement.execute();

            ResultSet keySet = preparedStatement.getGeneratedKeys();

            while (keySet.next()) {
                objCuota.setId(keySet.getLong(1));
            }

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean insertDetallePagoCuota(Cuota objCuota, Pago objPago) {
        boolean result = false;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO detallepagocuota(fecharegistro, estado, idcuota, idpago) "
                + "VALUES(now(), true, ?, ?);")){
            preparedStatement.setLong(1, objCuota.getId());
            preparedStatement.setLong(2, objPago.getId());
            
            preparedStatement.execute();
            
            result = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
    }

    @Override
    public boolean update(Cuota objCuota) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE cuotas SET numero = ?, monto = ?, interes = ?, amortizacion = ?, mora = ?, deudapendiente = ?, "
                + "fechavencimiento = ?, fechapago = ?, pagado = ?, estado = ?, idusuario = ?, idprestamo = ? "
                + "WHERE id = ?;")) {
            preparedStatement.setInt(1, objCuota.getNumero());                    
            preparedStatement.setBigDecimal(2, objCuota.getMonto());
            preparedStatement.setBigDecimal(3, objCuota.getInteres());
            preparedStatement.setBigDecimal(4, objCuota.getAmortizacion());
            preparedStatement.setBigDecimal(5, objCuota.getMora());
            preparedStatement.setBigDecimal(6, objCuota.getDeudaPendiente());
            if (objCuota.getFechaVencimiento() == null) {
                preparedStatement.setNull(7, Types.TIMESTAMP);
            } else {
                preparedStatement.setTimestamp(7, objCuota.getFechaVencimiento());
            }
            if (objCuota.getFechaPago() == null) {
                preparedStatement.setNull(8, Types.TIMESTAMP);
            } else {
                preparedStatement.setTimestamp(8, objCuota.getFechaPago());
            }
            preparedStatement.setBoolean(9, objCuota.isPagado());
            preparedStatement.setBoolean(10, objCuota.isEstado());
            preparedStatement.setLong(11, objCuota.getObjUsuario().getId());
            preparedStatement.setLong(12, objCuota.getObjPrestamo().getId());
            preparedStatement.setLong(13, objCuota.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean physicalDelete(Prestamo objPrestamo) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM cuotas WHERE idprestamo = ?;")) {
            preparedStatement.setLong(1, objPrestamo.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
