package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Deposito;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.interfaces.IDAODeposito;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAODeposito implements IDAODeposito {

    private static DAODeposito instance = null;

    private DAODeposito() {
    }

    public static DAODeposito getInstance() {
        if (instance == null) {
            instance = new DAODeposito();
        }

        return instance;
    }

    @Override
    public Deposito getDeposito(long id) {
        Deposito objDeposito = null;
        long idCuenta = 0;
        long idUsuario = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT d.id,d.monto,d.fecharegistro,d.fechamodificacion,d.dnidepositante, d.estado,d.habilitado,d.pagado,d.idcuenta,d.idusuario,d.acreedor "
                + "FROM depositos as d "
                + "WHERE d.id = ?;")) {
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idCuenta = resultSet.getLong("idcuenta");
                idUsuario = resultSet.getLong("idusuario");
                objDeposito = new Deposito(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getString("dnidepositante"),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        null,
                        resultSet.getString("acreedor"),
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objDeposito != null) {
            objDeposito.setObjCuenta(DAOCuenta.getInstance().getCuenta(idCuenta));
            objDeposito.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objDeposito.setLstPagos(DAOPago.getInstance().getAllPagos(objDeposito));
        }

        return objDeposito;
    }

    @Override
    public Deposito getDeposito(Pago objPago) {
        Deposito objDeposito = null;
        long idCuenta = 0;
        long idUsuario = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT d.id,d.monto,d.fecharegistro,d.fechamodificacion,d.dnidepositante, d.estado,d.habilitado,d.pagado,d.idcuenta,d.idusuario,d.acreedor "
                + "FROM depositos as d "
                + "WHERE d.id IN (SELECT dpd.iddeposito FROM detallepagodeposito dpd WHERE dpd.idpago = ?);")) {
            preparedStatement.setLong(1, objPago.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idCuenta = resultSet.getLong("idcuenta");
                idUsuario = resultSet.getLong("idusuario");
                objDeposito = new Deposito(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getString("dnidepositante"),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        null,
                        resultSet.getString("acreedor"),
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objDeposito != null) {
            objDeposito.setObjCuenta(DAOCuenta.getInstance().getCuenta(idCuenta));
            objDeposito.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objDeposito.setLstPagos(DAOPago.getInstance().getAllPagos(objDeposito));
        }

        return objDeposito;
    }

    @Override
    public boolean insert(Deposito objDeposito) {

        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO depositos (monto, fecharegistro, fechamodificacion, estado, habilitado, pagado, idcuenta, idusuario, acreedor, dnidepositante) "
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setBigDecimal(1, objDeposito.getMonto());
            preparedStatement.setTimestamp(2, objDeposito.getFechaRegistro());
            preparedStatement.setTimestamp(3, objDeposito.getFechaModificacion());
            preparedStatement.setBoolean(4, objDeposito.isEstado());
            preparedStatement.setBoolean(5, objDeposito.isHabilitado());
            preparedStatement.setBoolean(6, objDeposito.isPagado());
            preparedStatement.setLong(7, objDeposito.getObjCuenta().getId());
            preparedStatement.setLong(8, objDeposito.getObjUsuario().getId());
            preparedStatement.setString(9, objDeposito.getAcreedor());
            preparedStatement.setString(10, objDeposito.getDniDepositante());

            preparedStatement.execute();

            ResultSet keySet = preparedStatement.getGeneratedKeys();

            while (keySet.next()) {
                objDeposito.setId(keySet.getLong(1));
            }
            
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean insertDetallePago(Deposito objDeposito, Pago objPago) {

        boolean result = false;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO detallepagodeposito (monto, fecharegistro, fechamodificacion, estado, idpago, iddeposito)"
                + " VALUES (?, ?, ?, ?, ?, ?);")) {

            preparedStatement.setBigDecimal(1, objPago.getMonto());
            preparedStatement.setTimestamp(2, objPago.getFecha());
            if(objPago.getFechaModificacion() == null){
                preparedStatement.setNull(3, Types.TIMESTAMP);
            }else{
                preparedStatement.setTimestamp(3, objPago.getFechaModificacion());
            }
            preparedStatement.setBoolean(4, objPago.isEstado());
            preparedStatement.setLong(5, objPago.getId());
            preparedStatement.setLong(6, objDeposito.getId());

            preparedStatement.execute();
            
            result = true;

        } catch (Exception e) {
            
            e.printStackTrace();
        }
        return result;
    }
    
    @Override
    public boolean update(Deposito objDeposito) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE depositos "
                + " SET monto = ?,"
                + " fecharegistro = ?,"
                + " fechamodificacion = ?,"
                + " estado = ?,"
                + " habilitado = ?,"
                + " pagado = ?,"
                + " idcuenta = ?,"
                + " idusuario = ?, "
                + " acreedor = ?,"
                + " dnidepositante = ? "
                + " WHERE id = ?;")) {

            preparedStatement.setBigDecimal(1, objDeposito.getMonto());
            preparedStatement.setTimestamp(2, objDeposito.getFechaRegistro());
            preparedStatement.setTimestamp(3, objDeposito.getFechaModificacion());
            preparedStatement.setBoolean(4, objDeposito.isEstado());
            preparedStatement.setBoolean(5, objDeposito.isHabilitado());
            preparedStatement.setBoolean(6, objDeposito.isPagado());
            preparedStatement.setLong(7, objDeposito.getObjCuenta().getId());
            preparedStatement.setLong(8, objDeposito.getObjUsuario().getId());
            preparedStatement.setString(9, objDeposito.getAcreedor());
            preparedStatement.setString(10, objDeposito.getDniDepositante());
            preparedStatement.setLong(11, objDeposito.getId());

            preparedStatement.execute();
            if(preparedStatement.getUpdateCount() > 0){
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean logicalDeleteDeposito(Deposito objDeposito) {
        boolean result = false;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE depositos "
                + "SET estado = false "
                + "WHERE id = ? AND estado is true AND pagado is false;")){
            preparedStatement.setLong(1, objDeposito.getId());
            preparedStatement.execute();
            if(preparedStatement.getUpdateCount() > 0){
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean deletePhysicallyDeposito(Deposito objDeposito) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM depositos WHERE id = ?;")) {
            preparedStatement.setLong(1, objDeposito.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean deletePhysicallyAllDetallePagoByDeposito(Deposito objDeposito) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM detallepagodeposito WHERE iddeposito = ?;")) {
            preparedStatement.setLong(1, objDeposito.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Deposito> getAllDepositos(Cuenta objCuenta) {

        List<Deposito> lstDepositos = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT d.id,d.monto,d.fecharegistro,d.fechamodificacion,d.dnidepositante, d.estado,d.habilitado,d.pagado,d.idcuenta,d.idusuario,d.acreedor "
                + " FROM depositos as d WHERE d.idcuenta = ?;")) {
            preparedStatement.setLong(1, objCuenta.getId());
            
            ResultSet resultSet = preparedStatement.executeQuery();

            Deposito objDeposito = null;
            while (resultSet.next()) {
                objDeposito = new Deposito(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        objCuenta.getObjUsuario(),
                        resultSet.getString("dnidepositante"),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        objCuenta,
                        resultSet.getString("acreedor"),
                        new ArrayList<>());
                lstDepositos.add(objDeposito);
            }

            int size = lstDepositos.size();
            if (size > 0) {
                DAOPago objDAOPago = DAOPago.getInstance();
                for (int i = 0; i < size; i++) {
                    lstDepositos.get(i).setLstPagos(objDAOPago.getAllPagos(lstDepositos.get(i)));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lstDepositos;
    }

    @Override
    public List<Deposito> getAllDepositos(Timestamp fechaDesde, Timestamp fechaHasta) {

        List<Deposito> lstDepositos = new ArrayList<>();

        List<Long> lstIdCuenta = new ArrayList<>();
        List<Long> lstIdUsuario = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT d.id,d.monto,d.fecharegistro,d.fechamodificacion,d.dnidepositante, d.estado,d.habilitado,d.pagado,d.idcuenta,d.idusuario,d.acreedor "
                + " FROM depositos as d "
                + " WHERE d.estado is true AND d.fecharegistro between ? AND ?;")) {

            preparedStatement.setTimestamp(1, fechaDesde);
            preparedStatement.setTimestamp(2, fechaHasta);
            
            ResultSet resultSet = preparedStatement.executeQuery();

            Deposito objDeposito = null;
            while (resultSet.next()) {

                lstIdCuenta.add(resultSet.getLong("idcuenta"));
                lstIdUsuario.add(resultSet.getLong("idusuario"));

                objDeposito = new Deposito(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,//objCuenta.getObjUsuario(), 
                        resultSet.getString("dnidepositante"),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        null,//objCuenta, 
                        resultSet.getString("acreedor"),
                        new ArrayList<>()//lstPagos
                );
                lstDepositos.add(objDeposito);
            }

            int size = lstDepositos.size();
            if (size > 0) {
                DAOCuenta objDAOCuenta = DAOCuenta.getInstance();
                DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
                DAOPago objDAOPago = DAOPago.getInstance();
                for (int i = 0; i < size; i++) {
                    lstDepositos.get(i).setObjCuenta(objDAOCuenta.getCuenta(lstIdCuenta.get(i)));
                    lstDepositos.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdUsuario.get(i)));
                    lstDepositos.get(i).setLstPagos(objDAOPago.getAllPagos(lstDepositos.get(i)));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstDepositos;

    }

    @Override
    public List<Deposito> getAllDepositosHabilitados(Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Deposito> lstDepositos = new ArrayList<>();

        List<Long> lstIdCuenta = new ArrayList<>();
        List<Long> lstIdUsuario = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT d.id,d.monto,d.fecharegistro,d.fechamodificacion,d.dnidepositante, d.estado,d.habilitado,d.pagado,d.idcuenta,d.idusuario,d.acreedor "
                + " FROM depositos as d "
                + " WHERE d.estado is true AND d.habilitado is true AND d.pagado is false AND d.fecharegistro BETWEEN ? AND ?;")) {

            preparedStatement.setTimestamp(1, fechaDesde);
            preparedStatement.setTimestamp(2, fechaHasta);
            
            ResultSet resultSet = preparedStatement.executeQuery();

            Deposito objDeposito = null;
            while (resultSet.next()) {

                lstIdCuenta.add(resultSet.getLong("idcuenta"));
                lstIdUsuario.add(resultSet.getLong("idusuario"));

                objDeposito = new Deposito(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,//objCuenta.getObjUsuario(), 
                        resultSet.getString("dnidepositante"),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        null,//objCuenta, 
                        resultSet.getString("acreedor"),
                        new ArrayList<>());
                lstDepositos.add(objDeposito);
            }

            int size = lstDepositos.size();
            if (size > 0) {
                DAOCuenta objDAOCuenta = DAOCuenta.getInstance();
                DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
                DAOPago objDAOPago = DAOPago.getInstance();
                for (int i = 0; i < size; i++) {
                    lstDepositos.get(i).setObjCuenta(objDAOCuenta.getCuenta(lstIdCuenta.get(i)));
                    lstDepositos.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdUsuario.get(i)));
                    lstDepositos.get(i).setLstPagos(objDAOPago.getAllPagos(lstDepositos.get(i)));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstDepositos;
    }

    @Override
    public List<Deposito> getAllDepositosHabilitados(Cuenta objCuenta, Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Deposito> lstDepositos = new ArrayList<>();

        List<Long> lstIdUsuario = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT d.id,d.monto,d.fecharegistro,d.fechamodificacion,d.dnidepositante, d.estado,d.habilitado,d.pagado,d.idcuenta,d.idusuario,d.acreedor "
                + " FROM depositos as d "
                + " WHERE d.estado is true AND d.habilitado is true AND d.pagado is false AND d.idcuenta = ? AND d.fecharegistro BETWEEN ? AND ?;")) {
            preparedStatement.setLong(1, objCuenta.getId());
            preparedStatement.setTimestamp(2, fechaDesde);
            preparedStatement.setTimestamp(3, fechaHasta);
            
            ResultSet resultSet = preparedStatement.executeQuery();

            Deposito objDeposito = null;
            while (resultSet.next()) {
                lstIdUsuario.add(resultSet.getLong("idusuario"));

                objDeposito = new Deposito(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,//objCuenta.getObjUsuario(), 
                        resultSet.getString("dnidepositante"),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        objCuenta,//objCuenta, 
                        resultSet.getString("acreedor"),
                        new ArrayList<>());
                lstDepositos.add(objDeposito);
            }

            int size = lstDepositos.size();
            if (size > 0) {
                DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
                DAOPago objDAOPago = DAOPago.getInstance();
                for (int i = 0; i < size; i++) {
                    lstDepositos.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdUsuario.get(i)));
                    lstDepositos.get(i).setLstPagos(objDAOPago.getAllPagos(lstDepositos.get(i)));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstDepositos;
    }

    @Override
    public List<Deposito> getAllDepositosPagados(Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Deposito> lstDepositos = new ArrayList<>();

        List<Long> lstIdCuenta = new ArrayList<>();
        List<Long> lstIdUsuario = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT d.id,d.monto,d.fecharegistro,d.fechamodificacion,d.dnidepositante, d.estado,d.habilitado,d.pagado,d.idcuenta,d.idusuario,d.acreedor "
                + " FROM depositos as d "
                + " WHERE d.estado is true AND d.habilitado is true AND d.pagado is true AND d.fecharegistro BETWEEN ? AND ?;")) {

            preparedStatement.setTimestamp(1, fechaDesde);
            preparedStatement.setTimestamp(2, fechaHasta);
            
            ResultSet resultSet = preparedStatement.executeQuery();

            Deposito objDeposito = null;
            while (resultSet.next()) {

                lstIdCuenta.add(resultSet.getLong("idcuenta"));
                lstIdUsuario.add(resultSet.getLong("idusuario"));

                objDeposito = new Deposito(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,//objCuenta.getObjUsuario(), 
                        resultSet.getString("dnidepositante"),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        null,//objCuenta, 
                        resultSet.getString("acreedor"),
                        new ArrayList<>());
                lstDepositos.add(objDeposito);
            }

            int size = lstDepositos.size();
            if (size > 0) {
                DAOCuenta objDAOCuenta = DAOCuenta.getInstance();
                DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
                DAOPago objDAOPago = DAOPago.getInstance();
                for (int i = 0; i < size; i++) {
                    lstDepositos.get(i).setObjCuenta(objDAOCuenta.getCuenta(lstIdCuenta.get(i)));
                    lstDepositos.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdUsuario.get(i)));
                    lstDepositos.get(i).setLstPagos(objDAOPago.getAllPagos(lstDepositos.get(i)));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstDepositos;
    }

    @Override
    public List<Deposito> getAllDepositosPagados(Cuenta objCuenta, Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Deposito> lstDepositos = new ArrayList<>();

        List<Long> lstIdUsuario = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT d.id,d.monto,d.fecharegistro,d.fechamodificacion,d.dnidepositante, d.estado,d.habilitado,d.pagado,d.idcuenta,d.idusuario,d.acreedor "
                + " FROM depositos as d "
                + " WHERE d.estado is true AND d.habilitado is true AND d.pagado is true AND AND d.idcuenta = ? AND d.fecharegistro BETWEEN ? AND ?;")) {
            preparedStatement.setLong(1, objCuenta.getId());
            preparedStatement.setTimestamp(2, fechaDesde);
            preparedStatement.setTimestamp(3, fechaHasta);
            
            ResultSet resultSet = preparedStatement.executeQuery();

            Deposito objDeposito = null;
            while (resultSet.next()) {
                lstIdUsuario.add(resultSet.getLong("idusuario"));

                objDeposito = new Deposito(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,//objCuenta.getObjUsuario(), 
                        resultSet.getString("dnidepositante"),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        objCuenta,//objCuenta, 
                        resultSet.getString("acreedor"),
                        new ArrayList<>());
                lstDepositos.add(objDeposito);
            }

            int size = lstDepositos.size();
            if (size > 0) {
                DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
                DAOPago objDAOPago = DAOPago.getInstance();
                for (int i = 0; i < size; i++) {
                    lstDepositos.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdUsuario.get(i)));
                    lstDepositos.get(i).setLstPagos(objDAOPago.getAllPagos(lstDepositos.get(i)));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lstDepositos;
    }
}
