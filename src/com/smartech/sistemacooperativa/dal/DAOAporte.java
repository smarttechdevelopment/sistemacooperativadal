package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.interfaces.IDAOAporte;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOAporte implements IDAOAporte {

    private static DAOAporte instance = null;

    private DAOAporte() {
    }

    public static DAOAporte getInstance() {
        if (instance == null) {
            instance = new DAOAporte();
        }

        return instance;
    }

    @Override
    public Aporte getAporte(long id) {
        Aporte objAporte = null;
        long idTarjeta = 0;
        long idUsuario = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT a.id, a.monto, a.habilitado, "
                + "a.pagado, a.ingreso, a.fecharegistro, a.fechamodificacion, a.estado, a.idusuario, a.idtarjeta, a.retiro "
                + "FROM aportes a "
                + "WHERE a.id = ?;")) {
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idTarjeta = resultSet.getLong("idtarjeta");
                idUsuario = resultSet.getLong("idusuario");
                objAporte = new Aporte(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        resultSet.getBoolean("ingreso"),
                        resultSet.getBoolean("retiro"),
                        null,
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objAporte != null) {
            objAporte.setObjTarjeta(DAOTarjeta.getInstance().getTarjeta(idTarjeta));
            objAporte.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objAporte.setLstPagos(DAOPago.getInstance().getAllPagos(objAporte));
        }

        return objAporte;
    }

    @Override
    public Aporte getAporte(Pago objPago) {
        Aporte objAporte = null;
        long idTarjeta = 0;
        long idUsuario = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT a.id, a.monto, a.habilitado, "
                + "a.pagado, a.ingreso, a.fecharegistro, a.fechamodificacion, a.estado, a.idusuario, a.idtarjeta, a.retiro "
                + "FROM aportes a "
                + "WHERE a.id IN (SELECT dpa.idaporte FROM detallepagoaporte dpa WHERE dpa.idpago = ?);")) {
            preparedStatement.setLong(1, objPago.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idTarjeta = resultSet.getLong("idtarjeta");
                idUsuario = resultSet.getLong("idusuario");
                objAporte = new Aporte(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        resultSet.getBoolean("ingreso"),
                        resultSet.getBoolean("retiro"),
                        null,
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objAporte != null) {
            objAporte.setObjTarjeta(DAOTarjeta.getInstance().getTarjeta(idTarjeta));
            objAporte.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objAporte.setLstPagos(DAOPago.getInstance().getAllPagos(objAporte));
        }

        return objAporte;
    }

    @Override
    public List<Aporte> getAllAportes(Socio objSocio) {
        List<Aporte> lstAportes = new ArrayList<>();
        List<Long> lstIdsTarjetas = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT a.id, a.monto, a.habilitado, "
                + "a.pagado, a.ingreso, a.fecharegistro, a.fechamodificacion, a.estado, a.idusuario, a.idtarjeta, a.retiro "
                + "FROM aportes a INNER JOIN tarjetas t ON a.idtarjeta = t.id "
                + "INNER JOIN socios s ON t.idsocio = s.id "
                + "WHERE s.id = ? AND a.ingreso is false AND a.pagado is true;")) {
            preparedStatement.setLong(1, objSocio.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsTarjetas.add(resultSet.getLong("idtarjeta"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));

                Aporte objAporte = new Aporte(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        resultSet.getBoolean("ingreso"),
                        resultSet.getBoolean("retiro"),
                        null,
                        new ArrayList<>());
                lstAportes.add(objAporte);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstAportes.isEmpty()) {
            DAOTarjeta objDAOTarjeta = DAOTarjeta.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOPago objDAOPago = DAOPago.getInstance();

            int size = lstAportes.size();
            for (int i = 0; i < size; i++) {
                lstAportes.get(i).setObjTarjeta(objDAOTarjeta.getTarjeta(lstIdsTarjetas.get(i)));
                lstAportes.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstAportes.get(i).setLstPagos(objDAOPago.getAllPagos(lstAportes.get(i)));
            }
        }

        return lstAportes;
    }

    // Falta, borra este comentario cuando ya no falte
    public BigDecimal getDeudaAportes(Timestamp fechaUltimoPago) {

        BigDecimal deudaAportes = BigDecimal.ZERO;

//        String statement = "SELECT hc.id, hc.monto, hc.fecharegistro" + 
//                " FROM(SELECT (date_part('month', hcsub.fecharegistro)) mes, max(hcsub.fecharegistro) fecharegistro" 
//                +"	 FROM historicomontosaporte hcsub" 
//                +"	 WHERE date_part('month', hcsub.fecharegistro) > 3 AND date_part('month', hcsub.fecharegistro) < 9" 
//                +"	 GROUP BY mes" 
//                +"	 ORDER BY fecharegistro ASC) AS tabla INNER JOIN historicomontosaporte hc ON hc.fecharegistro = tabla.fecharegistro";
        String statement = "SELECT sum(hc.monto) monto "
                + " FROM(SELECT (date_part('month', hcsub.fecharegistro)) mes, (date_part('year', hcsub.fecharegistro)) anyo, max(hcsub.fecharegistro) fecharegistro "
                + "	 FROM historicomontosaporte hcsub "
                + "	 WHERE hcsub.fecharegistro BETWEEN ? AND now() "
                + "	 GROUP BY mes, anyo "
                + "	 GROUP BY fecharegistro ASC) AS tabla INNER JOIN historicomontosaporte hc ON hc.fecharegistro = tabla.fecharegistro;";

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(statement)) {

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return deudaAportes;
    }

    @Override
    public Aporte getAporteIngresoImpago(Socio objSocio) {
        Aporte objAporte = null;
        long idTarjeta = 0;
        long idUsuario = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT a.id, a.monto, a.fecharegistro, a.fechamodificacion, a.estado, a.habilitado, a.pagado, a.ingreso, a.idusuario, a.idtarjeta, a.retiro "
                + "FROM aportes a INNER JOIN tarjetas t ON a.idtarjeta = t.id "
                + "		INNER JOIN socios s ON s.id = t.idsocio "
                + "WHERE a.pagado is false AND a.ingreso is true AND a.estado is true AND s.id = ?;")) {
            preparedStatement.setLong(1, objSocio.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idTarjeta = resultSet.getLong("idtarjeta");
                idUsuario = resultSet.getLong("idusuario");

                objAporte = new Aporte(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        resultSet.getBoolean("ingreso"),
                        resultSet.getBoolean("retiro"),
                        null,
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objAporte != null) {
            DAOTarjeta objDAOTarjeta = DAOTarjeta.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOPago objDAOPago = DAOPago.getInstance();

            objAporte.setObjTarjeta(objDAOTarjeta.getTarjeta(idTarjeta));
            objAporte.setObjUsuario(objDAOUsuario.getUsuario(idUsuario));
            objAporte.setLstPagos(objDAOPago.getAllPagos(objAporte));
        }

        return objAporte;
    }

    @Override
    public Aporte getLastAporte(Socio objSocio) {
        Aporte objAporte = null;
        Long idTarjeta = new Long(0);
        Long idUsuario = new Long(0);

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT a.id, a.monto, a.habilitado, "
                + "a.pagado, a.ingreso, a.fecharegistro, a.fechamodificacion, a.estado, a.idusuario, a.idtarjeta, a.retiro "
                + "FROM aportes a "
                + "INNER JOIN tarjetas t on t.id = a.idtarjeta "
                + "WHERE t.idsocio = ? "
                + "ORDER BY a.fecharegistro desc "
                + "limit 1;")) {

            preparedStatement.setLong(1, objSocio.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idTarjeta = resultSet.getLong("idtarjeta");
                idUsuario = resultSet.getLong("idusuario");

                objAporte = new Aporte(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        resultSet.getBoolean("ingreso"),
                        resultSet.getBoolean("retiro"),
                        null,
                        new ArrayList<>());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objAporte != null) {
            DAOTarjeta objDAOTarjeta = DAOTarjeta.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOPago objDAOPago = DAOPago.getInstance();

            objAporte.setObjTarjeta(objDAOTarjeta.getTarjeta(idTarjeta));
            objAporte.setObjUsuario(objDAOUsuario.getUsuario(idUsuario));
            objAporte.setLstPagos(objDAOPago.getAllPagos(objAporte));
        }

        return objAporte;
    }

    @Override
    public BigDecimal getSaldoAportes(Socio objSocio) {
        BigDecimal saldoAportes = new BigDecimal(BigInteger.ZERO);

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT COALESCE ((SELECT SUM(a.monto) saldo "
                + "FROM aportes a "
                + "INNER JOIN tarjetas t on t.id = a.idtarjeta "
                + "WHERE t.idsocio = ? AND a.pagado is true AND a.estado is true AND a.retiro is false), 0) saldo;")) {

            preparedStatement.setLong(1, objSocio.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                saldoAportes = resultSet.getBigDecimal("saldo");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return saldoAportes;
    }

    // Habilitados para pago
    @Override
    public List<Aporte> getAllAportesHabilitados(Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Aporte> lstAportes = new ArrayList<>();
        List<Long> lstIdsTarjetas = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT a.id, a.monto, a.habilitado, "
                + "a.pagado, a.ingreso, a.fecharegistro, a.fechamodificacion, a.estado, a.idusuario, a.idtarjeta, a.retiro "
                + "FROM aportes a "
                + "WHERE a.estado is true AND a.habilitado is true AND a.pagado is false AND a.fecharegistro BETWEEN ? AND ?;")) {
            preparedStatement.setTimestamp(1, fechaDesde);
            preparedStatement.setTimestamp(2, fechaHasta);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsTarjetas.add(resultSet.getLong("idtarjeta"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));

                Aporte objAporte = new Aporte(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        resultSet.getBoolean("ingreso"),
                        resultSet.getBoolean("retiro"),
                        null,
                        new ArrayList<>());
                lstAportes.add(objAporte);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstAportes.isEmpty()) {
            DAOTarjeta objDAOTarjeta = DAOTarjeta.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOPago objDAOPago = DAOPago.getInstance();

            int size = lstAportes.size();
            for (int i = 0; i < size; i++) {
                lstAportes.get(i).setObjTarjeta(objDAOTarjeta.getTarjeta(lstIdsTarjetas.get(i)));
                lstAportes.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstAportes.get(i).setLstPagos(objDAOPago.getAllPagos(lstAportes.get(i)));
            }
        }

        return lstAportes;
    }

    @Override
    public List<Aporte> getAllAportesPagados(Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Aporte> lstAportes = new ArrayList<>();
        List<Long> lstIdsTarjetas = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT a.id, a.monto, a.habilitado, "
                + "a.pagado, a.ingreso, a.fecharegistro, a.fechamodificacion, a.estado, a.idusuario, a.idtarjeta, a.retiro "
                + "FROM aportes a "
                + "WHERE a.habilitado is true AND a.pagado is true AND a.fechamodificacion BETWEEN ? AND ?;")) {
            preparedStatement.setTimestamp(1, fechaDesde);
            preparedStatement.setTimestamp(2, fechaHasta);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsTarjetas.add(resultSet.getLong("idtarjeta"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));

                Aporte objAporte = new Aporte(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        resultSet.getBoolean("ingreso"),
                        resultSet.getBoolean("retiro"),
                        null,
                        new ArrayList<>());
                lstAportes.add(objAporte);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstAportes.isEmpty()) {
            DAOTarjeta objDAOTarjeta = DAOTarjeta.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOPago objDAOPago = DAOPago.getInstance();

            int size = lstAportes.size();
            for (int i = 0; i < size; i++) {
                lstAportes.get(i).setObjTarjeta(objDAOTarjeta.getTarjeta(lstIdsTarjetas.get(i)));
                lstAportes.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstAportes.get(i).setLstPagos(objDAOPago.getAllPagos(lstAportes.get(i)));
            }
        }

        return lstAportes;
    }

    @Override
    public List<Aporte> getAllAportesPagados(Socio objSocio, Timestamp fechaDesde, Timestamp fechaHasta) {
        List<Aporte> lstAportes = new ArrayList<>();
        List<Long> lstIdsTarjetas = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT a.id, a.monto, a.habilitado, "
                + "a.pagado, a.ingreso, a.fecharegistro, a.fechamodificacion, a.estado, a.idusuario, a.idtarjeta, a.retiro "
                + "FROM aportes a INNER JOIN tarjetas t ON a.idtarjeta = t.id "
                + "WHERE t.idsocio = ? AND a.habilitado is true AND a.pagado is true AND a.fechamodificacion BETWEEN ? AND ?;")) {
            preparedStatement.setLong(1, objSocio.getId());
            preparedStatement.setTimestamp(2, fechaDesde);
            preparedStatement.setTimestamp(3, fechaHasta);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsTarjetas.add(resultSet.getLong("idtarjeta"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));

                Aporte objAporte = new Aporte(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        resultSet.getBoolean("ingreso"),
                        resultSet.getBoolean("retiro"),
                        null,
                        new ArrayList<>());
                lstAportes.add(objAporte);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstAportes.isEmpty()) {
            DAOTarjeta objDAOTarjeta = DAOTarjeta.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOPago objDAOPago = DAOPago.getInstance();

            int size = lstAportes.size();
            for (int i = 0; i < size; i++) {
                lstAportes.get(i).setObjTarjeta(objDAOTarjeta.getTarjeta(lstIdsTarjetas.get(i)));
                lstAportes.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstAportes.get(i).setLstPagos(objDAOPago.getAllPagos(lstAportes.get(i)));
            }
        }

        return lstAportes;
    }

    @Override
    public boolean insert(Aporte objAporte) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO aportes(monto, habilitado, pagado, ingreso, fecharegistro, estado, idusuario, idtarjeta, retiro) "
                + "VALUES(?, ?, ?, ?, now(), ?, ?, ?, ?);")) {
            preparedStatement.setBigDecimal(1, objAporte.getMonto());
            preparedStatement.setBoolean(2, objAporte.isHabilitado());
            preparedStatement.setBoolean(3, objAporte.isPagado());
            preparedStatement.setBoolean(4, objAporte.isIngreso());
            preparedStatement.setBoolean(5, objAporte.isEstado());
            preparedStatement.setLong(6, objAporte.getObjUsuario().getId());
            preparedStatement.setLong(7, objAporte.getObjTarjeta().getId());
            preparedStatement.setBoolean(8, objAporte.isRetiro());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean update(Aporte objAporte) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE aportes SET monto = ?, habilitado = ?, pagado = ?, ingreso = ?, fechamodificacion = now(), estado = ?, idusuario = ?, idtarjeta = ?, retiro = ? "
                + "WHERE id = ?;")) {
            preparedStatement.setBigDecimal(1, objAporte.getMonto());
            preparedStatement.setBoolean(2, objAporte.isHabilitado());
            preparedStatement.setBoolean(3, objAporte.isPagado());
            preparedStatement.setBoolean(4, objAporte.isIngreso());
            preparedStatement.setBoolean(5, objAporte.isEstado());
            preparedStatement.setLong(6, objAporte.getObjUsuario().getId());
            preparedStatement.setLong(7, objAporte.getObjTarjeta().getId());
            preparedStatement.setBoolean(8, objAporte.isRetiro());
            preparedStatement.setLong(9, objAporte.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean updateAllAportes(Aporte objAporte) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE aportes "
                + "SET monto = ?, habilitado = ?, pagado = ?, ingreso = ?, fechamodificacion = now(), estado = ?, idusuario = ?, idtarjeta = ?, retiro = ? "
                + "WHERE a.idtarjeta = ? AND a.ingreso is false AND a.retiro = false;")) {
            preparedStatement.setBigDecimal(1, objAporte.getMonto());
            preparedStatement.setBoolean(2, objAporte.isHabilitado());
            preparedStatement.setBoolean(3, objAporte.isPagado());
            preparedStatement.setBoolean(4, objAporte.isIngreso());
            preparedStatement.setBoolean(5, objAporte.isEstado());
            preparedStatement.setLong(6, objAporte.getObjUsuario().getId());
            preparedStatement.setLong(7, objAporte.getObjTarjeta().getId());
            preparedStatement.setBoolean(8, objAporte.isRetiro());
            preparedStatement.setLong(9, objAporte.getObjTarjeta().getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean insertDetallePagoAporte(Pago objPago, Aporte objAporte) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO detallepagoaporte(monto, fecharegistro, estado, idaporte, idpago) "
                + "VALUES(?, ?, ?, ?, ?);")) {
            preparedStatement.setBigDecimal(1, objAporte.getMonto());
            preparedStatement.setTimestamp(2, objPago.getFechaRegistro());
            preparedStatement.setBoolean(3, true);
            preparedStatement.setLong(4, objAporte.getId());
            preparedStatement.setLong(5, objPago.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean delete(Aporte objAporte) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE aportes SET estado = false WHERE id = ?;")) {
            preparedStatement.setLong(1, objAporte.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean physicalDelete(Aporte objAporte) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM aportes WHERE id = ?;")) {
            preparedStatement.setLong(1, objAporte.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
