package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.TipoUsuario;
import com.smartech.sistemacooperativa.dominio.Usuario;
import com.smartech.sistemacooperativa.interfaces.IDAOUsuario;
import com.smartech.sistemacooperativa.util.generics.StringUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class DAOUsuario implements IDAOUsuario {

    private static DAOUsuario instance = null;

    private DAOUsuario() {
    }

    public static DAOUsuario getInstance() {
        if (instance == null) {
            instance = new DAOUsuario();
        }

        return instance;
    }

    @Override
    public Usuario getUsuario(String username, String password) {
        Usuario objUsuario = null;
        int idTipoUsuario = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT u.id, u.usuario, u.estado, u.activo, u.idtipousuario "
                + "FROM usuarios u "
                + "WHERE u.usuario = ? AND u.clave = crypt(?, clave);")) {
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, StringUtil.decodeString(password));

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idTipoUsuario = resultSet.getInt("idtipousuario");
                objUsuario = new Usuario(
                        resultSet.getLong("id"),
                        resultSet.getString("usuario"),
                        "",
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("activo"),
                        null,
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objUsuario != null) {
            objUsuario.setObjTipoUsuario(DAOTipoUsuario.getInstance().getTipoUsuario(idTipoUsuario));
            objUsuario.setLstHuellas(DAOHuella.getInstance().getAllHuellas(objUsuario));
        }

        return objUsuario;
    }

    @Override
    public Usuario getUsuario(long id) {
        Usuario objUsuario = null;
        int idTipoUsuario = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT u.id, u.usuario, u.estado, u.activo, u.idtipousuario "
                + "FROM usuarios u "
                + "WHERE u.id = ?;")) {
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idTipoUsuario = resultSet.getInt("idtipousuario");
                objUsuario = new Usuario(
                        resultSet.getLong("id"),
                        resultSet.getString("usuario"),
                        "",
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("activo"),
                        null,
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objUsuario != null) {
            objUsuario.setObjTipoUsuario(DAOTipoUsuario.getInstance().getTipoUsuario(idTipoUsuario));
            objUsuario.setLstHuellas(DAOHuella.getInstance().getAllHuellas(objUsuario));
        }

        return objUsuario;
    }

    @Override
    public List<Usuario> getAllUsuarios(TipoUsuario objTipoUsuario) {
        List<Usuario> lstUsuarios = new ArrayList<>();
        
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT u.id, u.usuario, u.estado, u.activo, u.idtipousuario "
                + "FROM usuarios u "
                + "WHERE u.idtipousuario = ?;")) {
            preparedStatement.setInt(1, objTipoUsuario.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Usuario objUsuario = new Usuario(
                        resultSet.getLong("id"),
                        resultSet.getString("usuario"),
                        "",
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("activo"),
                        objTipoUsuario,
                        new ArrayList<>());
                lstUsuarios.add(objUsuario);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if(!lstUsuarios.isEmpty()){
            DAOHuella objDAOHuella = DAOHuella.getInstance();
            
            int size = lstUsuarios.size();
            for(int i = 0; i < size; i++){
                lstUsuarios.get(i).setLstHuellas(objDAOHuella.getAllHuellas(lstUsuarios.get(i)));
            }
        }
        
        return lstUsuarios;
    }

    @Override
    public List<Usuario> getAllUsuarios(String username, String password) {
        List<Usuario> lstUsuarios = new ArrayList<>();
        List<Integer> lstIdsTiposUsuario = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT u.id, u.usuario, u.estado, u.activo, u.idtipousuario "
                + "FROM usuarios u "
                + "WHERE u.usuario = ? AND u.clave = crypt(?, clave);")) {
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, StringUtil.decodeString(password));

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsTiposUsuario.add(resultSet.getInt("idtipousuario"));
                Usuario objUsuario = new Usuario(
                        resultSet.getLong("id"),
                        resultSet.getString("usuario"),
                        "",
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("activo"),
                        null,
                        new ArrayList<>());
                lstUsuarios.add(objUsuario);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstUsuarios.isEmpty()) {
            DAOTipoUsuario objDAOTipoUsuario = DAOTipoUsuario.getInstance();
            DAOHuella objDAOHuella = DAOHuella.getInstance();
            
            int size = lstUsuarios.size();
            for (int i = 0; i < size; i++) {
                lstUsuarios.get(i).setObjTipoUsuario(objDAOTipoUsuario.getTipoUsuario(lstIdsTiposUsuario.get(i)));
                lstUsuarios.get(i).setLstHuellas(objDAOHuella.getAllHuellas(lstUsuarios.get(i)));
            }
        }

        return lstUsuarios;
    }

    @Override
    public boolean insert(Usuario objUsuario) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO usuarios(usuario, clave, estado, activo, idtipousuario) "
                + "VALUES(?, crypt(?, gen_salt('md5')), ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, objUsuario.getUsername());
            preparedStatement.setString(2, StringUtil.decodeString(objUsuario.getPassword()));
            preparedStatement.setBoolean(3, objUsuario.isEstado());
            preparedStatement.setBoolean(4, objUsuario.isActivo());
            preparedStatement.setInt(5, objUsuario.getObjTipoUsuario().getId());

            preparedStatement.execute();

            ResultSet keySet = preparedStatement.getGeneratedKeys();
            if (keySet.next()) {
                objUsuario.setId(keySet.getLong(1));
            }

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean update(Usuario objUsuario) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE usuarios SET usuario = ?, clave = CASE WHEN ? = '' THEN (SELECT clave FROM usuarios WHERE id = ?) ELSE crypt(?, gen_salt('md5')) END, estado = ?, activo = ?, idtipousuario = ? "
                + "WHERE id = ?;")) {
            preparedStatement.setString(1, objUsuario.getUsername());
            preparedStatement.setString(2, StringUtil.decodeString(objUsuario.getPassword()));
            preparedStatement.setLong(3, objUsuario.getId());
            preparedStatement.setString(4, StringUtil.decodeString(objUsuario.getPassword()));
            preparedStatement.setBoolean(5, objUsuario.isEstado());
            preparedStatement.setBoolean(6, objUsuario.isActivo());
            preparedStatement.setInt(7, objUsuario.getObjTipoUsuario().getId());
            preparedStatement.setLong(8, objUsuario.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean delete(Usuario objUsuario) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE usuarios SET estado = false WHERE id = ?;")) {
            preparedStatement.setLong(1, objUsuario.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean physicalDelete(Usuario objUsuario) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM usuarios WHERE id = ?;")) {
            preparedStatement.setLong(1, objUsuario.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
