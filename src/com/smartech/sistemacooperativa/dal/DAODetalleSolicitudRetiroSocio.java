/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.DetalleSolicitudRetiroSocio;
import com.smartech.sistemacooperativa.dominio.Solicitud;
import com.smartech.sistemacooperativa.interfaces.IDAODetalleSolicitudRetiroSocio;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAODetalleSolicitudRetiroSocio implements IDAODetalleSolicitudRetiroSocio{

    private static DAODetalleSolicitudRetiroSocio instance = null;
    
    public static DAODetalleSolicitudRetiroSocio getInstance(){
        if(instance == null){
            instance = new DAODetalleSolicitudRetiroSocio();
        }
        return instance;
    }
    
    @Override
    public boolean insert(Solicitud objSolicitud, DetalleSolicitudRetiroSocio objDetalleSolicitudRetiroSocio) {
        boolean result = false;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO detallesolicitudretirosocio "
                + "(idsocio,idsolicitudretirosocio,aprueba,idusuario) "
                + "VALUES (?,?,?,?);",PreparedStatement.RETURN_GENERATED_KEYS)){
            
            preparedStatement.setLong(1, objDetalleSolicitudRetiroSocio.getObjSocio().getId());
            preparedStatement.setLong(2, objSolicitud.getId());
            preparedStatement.setBoolean(3, objDetalleSolicitudRetiroSocio.isAprueba());
            preparedStatement.setLong(4, objDetalleSolicitudRetiroSocio.getObjUsuario().getId());
            
            preparedStatement.execute();
            
            ResultSet keySet = preparedStatement.getGeneratedKeys();
            
            while(keySet.next()){
                objDetalleSolicitudRetiroSocio.setId(keySet.getLong(1));
            }
            
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean update(Solicitud objSolicitud, DetalleSolicitudRetiroSocio objDetalleSolicitudRetiroSocio) {
         boolean result = false;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE detallesolicitudretirosocio "
                + "SET idsocio = ?, "
                + "idsolicitudretirosocio = ?, "
                + "aprueba = ?, "
                + "idusuario  = ? "
                + "WHERE id = ?;")){
            
            preparedStatement.setLong(1, objDetalleSolicitudRetiroSocio.getObjSocio().getId());
            preparedStatement.setLong(2, objSolicitud.getId());
            preparedStatement.setBoolean(3, objDetalleSolicitudRetiroSocio.isAprueba());
            preparedStatement.setLong(4, objDetalleSolicitudRetiroSocio.getObjUsuario().getId());
            preparedStatement.setLong(5, objDetalleSolicitudRetiroSocio.getId());
            
            preparedStatement.execute();
            
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<DetalleSolicitudRetiroSocio> getAllDetalleSolicitudRetiroSocio(Solicitud objSolicitud) {
        List<DetalleSolicitudRetiroSocio> lstDetalleSolicitudRetiroSocios = new ArrayList<>();
        List<Long> lstIdsSocios = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT id,idsocio,idsolicitudretirosocio,aprueba,idusuario FROM detallesolicitudretirosocio "
                + "WHERE idsolicitudretirosocio = ?;")){
            
            preparedStatement.setLong(1, objSolicitud.getId());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            DetalleSolicitudRetiroSocio objDetalleSolicitudRetiroSocio;
            while(resultSet.next()){
                lstIdsSocios.add(resultSet.getLong("idsocio"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                objDetalleSolicitudRetiroSocio = new DetalleSolicitudRetiroSocio(
                        resultSet.getLong("id"), 
                        null, // socio
                        null,  // usuario
                        resultSet.getBoolean("aprueba"));
                lstDetalleSolicitudRetiroSocios.add(objDetalleSolicitudRetiroSocio);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if(!lstDetalleSolicitudRetiroSocios.isEmpty()){
            int size = lstDetalleSolicitudRetiroSocios.size();
            for(int i = 0; i < size; i++){
                lstDetalleSolicitudRetiroSocios.get(i).setObjSocio(DAOSocio.getInstance().getSocio(lstIdsSocios.get(i), false));
                lstDetalleSolicitudRetiroSocios.get(i).setObjUsuario(DAOUsuario.getInstance().getUsuario(lstIdsUsuarios.get(i)));
            }
        }
        return lstDetalleSolicitudRetiroSocios;
    }
    
    
}
