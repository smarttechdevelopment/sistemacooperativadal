package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.TipoDocumentoIdentidad;
import com.smartech.sistemacooperativa.dominio.Usuario;
import com.smartech.sistemacooperativa.interfaces.IDAOSocio;
import com.smartech.sistemacooperativa.util.generics.FileUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOSocio implements IDAOSocio {

    private static DAOSocio instance = null;

    private DAOSocio() {
    }

    public static DAOSocio getInstance() {
        if (instance == null) {
            instance = new DAOSocio();
        }

        return instance;
    }
    
    @Override
    public long getUltimoCodigo() {
        long ultimoCodigo = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT COALESCE(MAX(substring(s.codigo from 1 for 4)::int), 1) ultimocodigo "
                + "FROM socios s;")) {
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                ultimoCodigo = resultSet.getInt("ultimocodigo");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ultimoCodigo;
    }

    @Override
    public Socio getSocio(long id, boolean cargarFoto) {
        Socio objSocio = null;
        int idDistrito = 0;
        int idTipoTrabajador = 0;
        long idUsuario = 0;
        int idEstadoCivil = 0;
        int idGradoInstruccion = 0;
        int idTipoDocumentoIdentidad = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT s.id, s.rutafoto, s.codigo, s.nombre, apellidopaterno, s.apellidomaterno, s.fechanacimiento, s.sexo, "
                + "s.direccion, s.origen, s.documentoidentidad, s.ruc, s.telefono, s.celular, s.ocupacion, s.direccionlaboral, s.ingresomensual, s.fechaingreso, s.fecharegistro, s.fechamodificacion, "
                + "s.estado, s.activo, s.idtipotrabajador, s.idusuario, s.iddistrito, s.idgradoinstruccion, s.idestadocivil, s.juridico, s.renunciado, s.idtipodocumentoidentidad "
                + "FROM socios s "
                + "WHERE s.id = ?;")) {
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idDistrito = resultSet.getInt("iddistrito");
                idTipoTrabajador = resultSet.getInt("idtipotrabajador");
                idUsuario = resultSet.getLong("idusuario");
                idEstadoCivil = resultSet.getInt("idestadocivil");
                idGradoInstruccion = resultSet.getInt("idgradoinstruccion");
                idTipoDocumentoIdentidad = resultSet.getInt("idtipodocumentoidentidad");
                objSocio = new Socio(
                        resultSet.getLong("id"),
                        null,
                        resultSet.getString("rutafoto"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"),
                        resultSet.getTimestamp("fechanacimiento"),
                        resultSet.getString("direccion"),
                        resultSet.getString("documentoidentidad"),
                        resultSet.getString("ruc"),
                        resultSet.getString("telefono"),
                        resultSet.getString("celular"),
                        resultSet.getBoolean("sexo"),
                        resultSet.getString("origen"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fechaingreso"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getString("codigo"),
                        resultSet.getString("ocupacion"),
                        resultSet.getString("direccionlaboral"),
                        resultSet.getBigDecimal("ingresomensual"),
                        resultSet.getBoolean("activo"),
                        resultSet.getBoolean("juridico"),
                        resultSet.getBoolean("renunciado"),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objSocio != null) {
            objSocio.setObjDistrito(DAODistrito.getInstance().getDistrito(idDistrito));
            objSocio.setObjTipoTrabajador(DAOTipoTrabajador.getInstance().getTipoTrabajador(idTipoTrabajador));
            objSocio.setLstCuentas(DAOCuenta.getInstance().getAllCuentasOnly(objSocio));
            objSocio.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objSocio.setLstDetalleFamiliarSocio(DAODetalleFamiliarSocio.getInstance().getAllDetalleFamiliarSocio(objSocio));
            objSocio.setObjTarjeta(DAOTarjeta.getInstance().getTarjeta(objSocio));
            objSocio.setObjEstadoCivil(DAOEstadoCivil.getInstance().getEstadoCivil(idEstadoCivil));
            objSocio.setObjGradoInstruccion(DAOGradoInstruccion.getInstance().getGradoInstruccion(idGradoInstruccion));
            objSocio.setObjTipoDocumentoIdentidad(DAOTipoDocumentoIdentidad.getInstance().getTipoDocumentoIdentidad(idTipoDocumentoIdentidad));
            if (cargarFoto) {
                objSocio.setFoto(FileUtil.downloadImageFTP(objSocio.getRutaFoto()));
                FileUtil.deleteTempFile(objSocio.getRutaFoto());
            }
        }

        return objSocio;
    }

    @Override
    public Socio getSocio(Usuario objUsuario, boolean cargarFoto) {
        Socio objSocio = null;
        int idDistrito = 0;
        int idTipoTrabajador = 0;
        int idEstadoCivil = 0;
        int idGradoInstruccion = 0;
        int idTipoDocumentoIdentidad = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT s.id, s.rutafoto, s.codigo, s.nombre, apellidopaterno, s.apellidomaterno, s.fechanacimiento, s.sexo, "
                + "s.direccion, s.origen, s.documentoidentidad, s.ruc, s.telefono, s.celular, s.ocupacion, s.direccionlaboral, s.ingresomensual, s.fechaingreso, s.fecharegistro, s.fechamodificacion, "
                + "s.estado, s.activo, s.idtipotrabajador, s.idusuario, s.iddistrito, s.idgradoinstruccion, s.idestadocivil, s.juridico, s.renunciado, s.idtipodocumentoidentidad "
                + "FROM socios s "
                + "WHERE s.idusuario = ?;")) {
            preparedStatement.setLong(1, objUsuario.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idDistrito = resultSet.getInt("iddistrito");
                idTipoTrabajador = resultSet.getInt("idtipotrabajador");
                idEstadoCivil = resultSet.getInt("idestadocivil");
                idGradoInstruccion = resultSet.getInt("idgradoinstruccion");
                idTipoDocumentoIdentidad = resultSet.getInt("idtipodocumentoidentidad");
                objSocio = new Socio(
                        resultSet.getLong("id"),
                        null,
                        resultSet.getString("rutafoto"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"),
                        resultSet.getTimestamp("fechanacimiento"),
                        resultSet.getString("direccion"),
                        resultSet.getString("documentoidentidad"),
                        resultSet.getString("ruc"),
                        resultSet.getString("telefono"),
                        resultSet.getString("celular"),
                        resultSet.getBoolean("sexo"),
                        resultSet.getString("origen"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fechaingreso"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getString("codigo"),
                        resultSet.getString("ocupacion"),
                        resultSet.getString("direccionlaboral"),
                        resultSet.getBigDecimal("ingresomensual"),
                        resultSet.getBoolean("activo"),
                        resultSet.getBoolean("juridico"),
                        resultSet.getBoolean("renunciado"),
                        null,
                        objUsuario,
                        null,
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objSocio != null) {
            objSocio.setObjDistrito(DAODistrito.getInstance().getDistrito(idDistrito));
            objSocio.setObjTipoTrabajador(DAOTipoTrabajador.getInstance().getTipoTrabajador(idTipoTrabajador));
            objSocio.setLstCuentas(DAOCuenta.getInstance().getAllCuentasOnly(objSocio));
            objSocio.setLstDetalleFamiliarSocio(DAODetalleFamiliarSocio.getInstance().getAllDetalleFamiliarSocio(objSocio));
            objSocio.setObjTarjeta(DAOTarjeta.getInstance().getTarjeta(objSocio));
            objSocio.setObjEstadoCivil(DAOEstadoCivil.getInstance().getEstadoCivil(idEstadoCivil));
            objSocio.setObjGradoInstruccion(DAOGradoInstruccion.getInstance().getGradoInstruccion(idGradoInstruccion));
            objSocio.setObjTipoDocumentoIdentidad(DAOTipoDocumentoIdentidad.getInstance().getTipoDocumentoIdentidad(idTipoDocumentoIdentidad));
            if (cargarFoto) {
                objSocio.setFoto(FileUtil.downloadImageFTP(objSocio.getRutaFoto()));
                FileUtil.deleteTempFile(objSocio.getRutaFoto());
            }
        }

        return objSocio;
    }

    @Override
    public Socio getSocioByDocumentoIdentidad(String documentoidentidad, boolean cargarFoto) {
        Socio objSocio = null;
        int idDistrito = 0;
        int idTipoTrabajador = 0;
        long idUsuario = 0;
        int idEstadoCivil = 0;
        int idGradoInstruccion = 0;
        int idTipoDocumentoIdentidad = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT s.id, s.rutafoto, s.codigo, s.nombre, apellidopaterno, s.apellidomaterno, s.fechanacimiento, s.sexo, "
                + "s.direccion, s.origen, s.documentoidentidad, s.ruc, s.telefono, s.celular, s.ocupacion, s.direccionlaboral, s.ingresomensual, s.fechaingreso, s.fecharegistro, s.fechamodificacion, "
                + "s.estado, s.activo, s.idtipotrabajador, s.idusuario, s.iddistrito, s.idgradoinstruccion, s.idestadocivil, s.juridico, s.renunciado, s.idtipodocumentoidentidad "
                + "FROM socios s "
                + "WHERE s.documentoidentidad = ?;")) {

            preparedStatement.setString(1, documentoidentidad);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idDistrito = resultSet.getInt("iddistrito");
                idTipoTrabajador = resultSet.getInt("idtipotrabajador");
                idUsuario = resultSet.getLong("idusuario");
                idEstadoCivil = resultSet.getInt("idestadocivil");
                idGradoInstruccion = resultSet.getInt("idgradoinstruccion");
                idTipoDocumentoIdentidad = resultSet.getInt("idtipodocumentoidentidad");
                objSocio = new Socio(
                        resultSet.getLong("id"),
                        null,
                        resultSet.getString("rutafoto"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"),
                        resultSet.getTimestamp("fechanacimiento"),
                        resultSet.getString("direccion"),
                        resultSet.getString("documentoidentidad"),
                        resultSet.getString("ruc"),
                        resultSet.getString("telefono"),
                        resultSet.getString("celular"),
                        resultSet.getBoolean("sexo"),
                        resultSet.getString("origen"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fechaingreso"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getString("codigo"),
                        resultSet.getString("ocupacion"),
                        resultSet.getString("direccionlaboral"),
                        resultSet.getBigDecimal("ingresomensual"),
                        resultSet.getBoolean("activo"),
                        resultSet.getBoolean("juridico"),
                        resultSet.getBoolean("renunciado"),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objSocio != null) {
            objSocio.setObjDistrito(DAODistrito.getInstance().getDistrito(idDistrito));
            objSocio.setObjTipoTrabajador(DAOTipoTrabajador.getInstance().getTipoTrabajador(idTipoTrabajador));
            objSocio.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objSocio.setLstCuentas(DAOCuenta.getInstance().getAllCuentasOnly(objSocio));
            objSocio.setObjTarjeta(DAOTarjeta.getInstance().getTarjeta(objSocio));
            objSocio.setObjEstadoCivil(DAOEstadoCivil.getInstance().getEstadoCivil(idEstadoCivil));
            objSocio.setObjGradoInstruccion(DAOGradoInstruccion.getInstance().getGradoInstruccion(idGradoInstruccion));
            objSocio.setObjTipoDocumentoIdentidad(DAOTipoDocumentoIdentidad.getInstance().getTipoDocumentoIdentidad(idTipoDocumentoIdentidad));
            if (cargarFoto) {
                objSocio.setFoto(FileUtil.downloadImageFTP(objSocio.getRutaFoto()));
                FileUtil.deleteTempFile(objSocio.getRutaFoto());
            }
        }

        return objSocio;
    }

    @Override
    public Socio getSocioByDocumentoIdentidad(String documentoIdentidad, TipoDocumentoIdentidad objTipoDocumentoIdentidad, boolean cargarFoto) {
        Socio objSocio = null;
        int idDistrito = 0;
        int idTipoTrabajador = 0;
        long idUsuario = 0;
        int idEstadoCivil = 0;
        int idGradoInstruccion = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT s.id, s.rutafoto, s.codigo, s.nombre, apellidopaterno, s.apellidomaterno, s.fechanacimiento, s.sexo, "
                + "s.direccion, s.origen, s.documentoidentidad, s.ruc, s.telefono, s.celular, s.ocupacion, s.direccionlaboral, s.ingresomensual, s.fechaingreso, s.fecharegistro, s.fechamodificacion, "
                + "s.estado, s.activo, s.idtipotrabajador, s.idusuario, s.iddistrito, s.idgradoinstruccion, s.idestadocivil, s.juridico, s.renunciado, s.idtipodocumentoidentidad "
                + "FROM socios s INNER JOIN tiposdocumentoidentidad tdi ON s.idtipodocumentoidentidad = tdi.id "
                + "WHERE s.documentoidentidad = ? AND tdi.id = ?;")) {
            preparedStatement.setString(1, documentoIdentidad);
            preparedStatement.setInt(2, objTipoDocumentoIdentidad.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idDistrito = resultSet.getInt("iddistrito");
                idTipoTrabajador = resultSet.getInt("idtipotrabajador");
                idUsuario = resultSet.getLong("idusuario");
                idEstadoCivil = resultSet.getInt("idestadocivil");
                idGradoInstruccion = resultSet.getInt("idgradoinstruccion");
                objSocio = new Socio(
                        resultSet.getLong("id"),
                        null,
                        resultSet.getString("rutafoto"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"),
                        resultSet.getTimestamp("fechanacimiento"),
                        resultSet.getString("direccion"),
                        resultSet.getString("documentoidentidad"),
                        resultSet.getString("ruc"),
                        resultSet.getString("telefono"),
                        resultSet.getString("celular"),
                        resultSet.getBoolean("sexo"),
                        resultSet.getString("origen"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fechaingreso"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getString("codigo"),
                        resultSet.getString("ocupacion"),
                        resultSet.getString("direccionlaboral"),
                        resultSet.getBigDecimal("ingresomensual"),
                        resultSet.getBoolean("activo"),
                        resultSet.getBoolean("juridico"),
                        resultSet.getBoolean("renunciado"),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        objTipoDocumentoIdentidad,
                        new ArrayList<>(),
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objSocio != null) {
            objSocio.setObjDistrito(DAODistrito.getInstance().getDistrito(idDistrito));
            objSocio.setObjTipoTrabajador(DAOTipoTrabajador.getInstance().getTipoTrabajador(idTipoTrabajador));
            objSocio.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objSocio.setLstCuentas(DAOCuenta.getInstance().getAllCuentasOnly(objSocio));
            objSocio.setObjTarjeta(DAOTarjeta.getInstance().getTarjeta(objSocio));
            objSocio.setObjEstadoCivil(DAOEstadoCivil.getInstance().getEstadoCivil(idEstadoCivil));
            objSocio.setObjGradoInstruccion(DAOGradoInstruccion.getInstance().getGradoInstruccion(idGradoInstruccion));
            if (cargarFoto) {
                objSocio.setFoto(FileUtil.downloadImageFTP(objSocio.getRutaFoto()));
                FileUtil.deleteTempFile(objSocio.getRutaFoto());
            }
        }

        return objSocio;
    }

    @Override
    public Socio getSocioByRUC(String ruc, boolean cargarFoto) {
        Socio objSocio = null;
        int idDistrito = 0;
        int idTipoTrabajador = 0;
        long idUsuario = 0;
        int idEstadoCivil = 0;
        int idGradoInstruccion = 0;
        int idTipoDocumentoIdentidad = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT s.id, s.rutafoto, s.codigo, s.nombre, apellidopaterno, s.apellidomaterno, s.fechanacimiento, s.sexo, "
                + "s.direccion, s.origen, s.documentoidentidad, s.ruc, s.telefono, s.celular, s.ocupacion, s.direccionlaboral, s.ingresomensual, s.fechaingreso, s.fecharegistro, s.fechamodificacion, "
                + "s.estado, s.activo, s.idtipotrabajador, s.idusuario, s.iddistrito, s.idgradoinstruccion, s.idestadocivil, s.juridico, s.renunciado, s.idtipodocumentoidentidad "
                + "FROM socios s "
                + "WHERE s.ruc = ?;")) {

            preparedStatement.setString(1, ruc);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idDistrito = resultSet.getInt("iddistrito");
                idTipoTrabajador = resultSet.getInt("idtipotrabajador");
                idUsuario = resultSet.getLong("idusuario");
                idEstadoCivil = resultSet.getInt("idestadocivil");
                idGradoInstruccion = resultSet.getInt("idgradoinstruccion");
                idTipoDocumentoIdentidad = resultSet.getInt("idtipodocumentoidentidad");
                objSocio = new Socio(
                        resultSet.getLong("id"),
                        null,
                        resultSet.getString("rutafoto"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"),
                        resultSet.getTimestamp("fechanacimiento"),
                        resultSet.getString("direccion"),
                        resultSet.getString("documentoidentidad"),
                        resultSet.getString("ruc"),
                        resultSet.getString("telefono"),
                        resultSet.getString("celular"),
                        resultSet.getBoolean("sexo"),
                        resultSet.getString("origen"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fechaingreso"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getString("codigo"),
                        resultSet.getString("ocupacion"),
                        resultSet.getString("direccionlaboral"),
                        resultSet.getBigDecimal("ingresomensual"),
                        resultSet.getBoolean("activo"),
                        resultSet.getBoolean("juridico"),
                        resultSet.getBoolean("renunciado"),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objSocio != null) {
            objSocio.setObjDistrito(DAODistrito.getInstance().getDistrito(idDistrito));
            objSocio.setObjTipoTrabajador(DAOTipoTrabajador.getInstance().getTipoTrabajador(idTipoTrabajador));
            objSocio.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objSocio.setLstCuentas(DAOCuenta.getInstance().getAllCuentasOnly(objSocio));
            objSocio.setObjTarjeta(DAOTarjeta.getInstance().getTarjeta(objSocio));
            objSocio.setObjEstadoCivil(DAOEstadoCivil.getInstance().getEstadoCivil(idEstadoCivil));
            objSocio.setObjGradoInstruccion(DAOGradoInstruccion.getInstance().getGradoInstruccion(idGradoInstruccion));
            objSocio.setObjTipoDocumentoIdentidad(DAOTipoDocumentoIdentidad.getInstance().getTipoDocumentoIdentidad(idTipoDocumentoIdentidad));
            if (cargarFoto) {
                objSocio.setFoto(FileUtil.downloadImageFTP(objSocio.getRutaFoto()));
                FileUtil.deleteTempFile(objSocio.getRutaFoto());
            }
        }

        return objSocio;
    }

    @Override
    public Socio getSocioByCodigo(String codigo, boolean cargarFoto) {
        Socio objSocio = null;
        int idDistrito = 0;
        int idTipoTrabajador = 0;
        long idUsuario = 0;
        int idEstadoCivil = 0;
        int idGradoInstruccion = 0;
        int idTipoDocumentoIdentidad = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT s.id, s.rutafoto, s.codigo, s.nombre, apellidopaterno, s.apellidomaterno, s.fechanacimiento, s.sexo, "
                + "s.direccion, s.origen, s.documentoidentidad, s.ruc, s.telefono, s.celular, s.ocupacion, s.direccionlaboral, s.ingresomensual, s.fechaingreso, s.fecharegistro, s.fechamodificacion, "
                + "s.estado, s.activo, s.idtipotrabajador, s.idusuario, s.iddistrito, s.idgradoinstruccion, s.idestadocivil, s.juridico, s.renunciado, s.idtipodocumentoidentidad "
                + "FROM socios s "
                + "WHERE s.codigo = ?;")) {

            preparedStatement.setString(1, codigo);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idDistrito = resultSet.getInt("iddistrito");
                idTipoTrabajador = resultSet.getInt("idtipotrabajador");
                idUsuario = resultSet.getLong("idusuario");
                idEstadoCivil = resultSet.getInt("idestadocivil");
                idGradoInstruccion = resultSet.getInt("idgradoinstruccion");
                idTipoDocumentoIdentidad = resultSet.getInt("idtipodocumentoidentidad");
                objSocio = new Socio(
                        resultSet.getLong("id"),
                        null,
                        resultSet.getString("rutafoto"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"),
                        resultSet.getTimestamp("fechanacimiento"),
                        resultSet.getString("direccion"),
                        resultSet.getString("documentoidentidad"),
                        resultSet.getString("ruc"),
                        resultSet.getString("telefono"),
                        resultSet.getString("celular"),
                        resultSet.getBoolean("sexo"),
                        resultSet.getString("origen"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fechaingreso"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getString("codigo"),
                        resultSet.getString("ocupacion"),
                        resultSet.getString("direccionlaboral"),
                        resultSet.getBigDecimal("ingresomensual"),
                        resultSet.getBoolean("activo"),
                        resultSet.getBoolean("juridico"),
                        resultSet.getBoolean("renunciado"),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objSocio != null) {
            objSocio.setObjDistrito(DAODistrito.getInstance().getDistrito(idDistrito));
            objSocio.setObjTipoTrabajador(DAOTipoTrabajador.getInstance().getTipoTrabajador(idTipoTrabajador));
            objSocio.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objSocio.setLstCuentas(DAOCuenta.getInstance().getAllCuentasOnly(objSocio));
            objSocio.setObjTarjeta(DAOTarjeta.getInstance().getTarjeta(objSocio));
            objSocio.setObjEstadoCivil(DAOEstadoCivil.getInstance().getEstadoCivil(idEstadoCivil));
            objSocio.setObjGradoInstruccion(DAOGradoInstruccion.getInstance().getGradoInstruccion(idGradoInstruccion));
            objSocio.setObjTipoDocumentoIdentidad(DAOTipoDocumentoIdentidad.getInstance().getTipoDocumentoIdentidad(idTipoDocumentoIdentidad));
            if (cargarFoto) {
                objSocio.setFoto(FileUtil.downloadImageFTP(objSocio.getRutaFoto()));
                FileUtil.deleteTempFile(objSocio.getRutaFoto());
            }
        }

        return objSocio;
    }

    @Override
    public List<Socio> getAllSocios(boolean cargarFoto) {
        List<Socio> lstSocios = new ArrayList<>();
        List<Integer> lstIdsDistritos = new ArrayList<>();
        List<Integer> lstIdsTiposTrabajador = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        List<Integer> lstIdsEstadosCiviles = new ArrayList<>();
        List<Integer> lstIdsGradosInstruccion = new ArrayList<>();
        List<Integer> lstIdsTiposDocumentoIdentidad = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT s.id, s.rutafoto, s.codigo, s.nombre, apellidopaterno, s.apellidomaterno, s.fechanacimiento, s.sexo, "
                + "s.direccion, s.origen, s.documentoidentidad, s.ruc, s.telefono, s.celular, s.ocupacion, s.direccionlaboral, s.ingresomensual, s.fechaingreso, s.fecharegistro, s.fechamodificacion, "
                + "s.estado, s.activo, s.idtipotrabajador, s.idusuario, s.iddistrito, s.idgradoinstruccion, s.idestadocivil, s.juridico, s.renunciado, s.idtipodocumentoidentidad "
                + "FROM socios s;")) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsDistritos.add(resultSet.getInt("iddistrito"));
                lstIdsTiposTrabajador.add(resultSet.getInt("idtipotrabajador"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                lstIdsEstadosCiviles.add(resultSet.getInt("idestadocivil"));
                lstIdsGradosInstruccion.add(resultSet.getInt("idgradoinstruccion"));
                lstIdsTiposDocumentoIdentidad.add(resultSet.getInt("idtipodocumentoidentidad"));
                Socio objSocio = new Socio(
                        resultSet.getLong("id"),
                        null,
                        resultSet.getString("rutafoto"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"),
                        resultSet.getTimestamp("fechanacimiento"),
                        resultSet.getString("direccion"),
                        resultSet.getString("documentoidentidad"),
                        resultSet.getString("ruc"),
                        resultSet.getString("telefono"),
                        resultSet.getString("celular"),
                        resultSet.getBoolean("sexo"),
                        resultSet.getString("origen"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fechaingreso"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getString("codigo"),
                        resultSet.getString("ocupacion"),
                        resultSet.getString("direccionlaboral"),
                        resultSet.getBigDecimal("ingresomensual"),
                        resultSet.getBoolean("activo"),
                        resultSet.getBoolean("juridico"),
                        resultSet.getBoolean("renunciado"),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>());
                lstSocios.add(objSocio);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstSocios.isEmpty()) {
            DAODistrito objDAODistrito = DAODistrito.getInstance();
            DAOTipoTrabajador objDAOTipoTrabajador = DAOTipoTrabajador.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOCuenta objDAOCuenta = DAOCuenta.getInstance();
            DAODetalleFamiliarSocio objDAODetalleFamiliarSocio = DAODetalleFamiliarSocio.getInstance();
            DAOTarjeta objDAOTarjeta = DAOTarjeta.getInstance();
            DAOEstadoCivil objDAOEstadoCivil = DAOEstadoCivil.getInstance();
            DAOGradoInstruccion objDAOGradoInstruccion = DAOGradoInstruccion.getInstance();
            DAOTipoDocumentoIdentidad objDAOTipoDocumentoIdentidad = DAOTipoDocumentoIdentidad.getInstance();

            int size = lstSocios.size();
            Socio objSocio;
            for (int i = 0; i < size; i++) {
                objSocio = lstSocios.get(i);
                objSocio.setObjDistrito(objDAODistrito.getDistrito(lstIdsDistritos.get(i)));
                objSocio.setObjTipoTrabajador(objDAOTipoTrabajador.getTipoTrabajador(lstIdsTiposTrabajador.get(i)));
                objSocio.setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                objSocio.setLstCuentas(objDAOCuenta.getAllCuentasOnly(objSocio));
                objSocio.setLstDetalleFamiliarSocio(objDAODetalleFamiliarSocio.getAllDetalleFamiliarSocio(objSocio));
                objSocio.setObjTarjeta(objDAOTarjeta.getTarjeta(objSocio));
                objSocio.setObjGradoInstruccion(objDAOGradoInstruccion.getGradoInstruccion(lstIdsGradosInstruccion.get(i)));
                objSocio.setObjEstadoCivil(objDAOEstadoCivil.getEstadoCivil(lstIdsEstadosCiviles.get(i)));
                objSocio.setObjTipoDocumentoIdentidad(objDAOTipoDocumentoIdentidad.getTipoDocumentoIdentidad(lstIdsTiposDocumentoIdentidad.get(i)));
                if (cargarFoto) {
                    objSocio.setFoto(FileUtil.downloadImageFTP(objSocio.getRutaFoto()));
                    FileUtil.deleteTempFile(objSocio.getRutaFoto());
                }
            }
        }

        return lstSocios;
    }

    @Override
    public List<Socio> getAllSociosActivos(boolean cargarFoto) {
        List<Socio> lstSocios = new ArrayList<>();
        List<Integer> lstIdsDistritos = new ArrayList<>();
        List<Integer> lstIdsTiposTrabajador = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        List<Integer> lstIdsEstadosCiviles = new ArrayList<>();
        List<Integer> lstIdsGradosInstruccion = new ArrayList<>();
        List<Integer> lstIdsTiposDocumentoIdentidad = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT s.id, s.rutafoto, s.codigo, s.nombre, apellidopaterno, s.apellidomaterno, s.fechanacimiento, s.sexo, "
                + "s.direccion, s.origen, s.documentoidentidad, s.ruc, s.telefono, s.celular, s.ocupacion, s.direccionlaboral, s.ingresomensual, s.fechaingreso, s.fecharegistro, s.fechamodificacion, "
                + "s.estado, s.activo, s.idtipotrabajador, s.idusuario, s.iddistrito, s.idgradoinstruccion, s.idestadocivil, s.juridico, s.renunciado, s.idtipodocumentoidentidad "
                + "FROM socios s "
                + "WHERE s.activo is true;")) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsDistritos.add(resultSet.getInt("iddistrito"));
                lstIdsTiposTrabajador.add(resultSet.getInt("idtipotrabajador"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                lstIdsEstadosCiviles.add(resultSet.getInt("idestadocivil"));
                lstIdsGradosInstruccion.add(resultSet.getInt("idgradoinstruccion"));
                lstIdsTiposDocumentoIdentidad.add(resultSet.getInt("idtipodocumentoidentidad"));
                Socio objSocio = new Socio(
                        resultSet.getLong("id"),
                        null,
                        resultSet.getString("rutafoto"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"),
                        resultSet.getTimestamp("fechanacimiento"),
                        resultSet.getString("direccion"),
                        resultSet.getString("documentoidentidad"),
                        resultSet.getString("ruc"),
                        resultSet.getString("telefono"),
                        resultSet.getString("celular"),
                        resultSet.getBoolean("sexo"),
                        resultSet.getString("origen"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fechaingreso"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getString("codigo"),
                        resultSet.getString("ocupacion"),
                        resultSet.getString("direccionlaboral"),
                        resultSet.getBigDecimal("ingresomensual"),
                        resultSet.getBoolean("activo"),
                        resultSet.getBoolean("juridico"),
                        resultSet.getBoolean("renunciado"),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>());
                lstSocios.add(objSocio);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstSocios.isEmpty()) {
            DAODistrito objDAODistrito = DAODistrito.getInstance();
            DAOTipoTrabajador objDAOTipoTrabajador = DAOTipoTrabajador.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOCuenta objDAOCuenta = DAOCuenta.getInstance();
            DAODetalleFamiliarSocio objDAODetalleFamiliarSocio = DAODetalleFamiliarSocio.getInstance();
            DAOTarjeta objDAOTarjeta = DAOTarjeta.getInstance();
            DAOEstadoCivil objDAOEstadoCivil = DAOEstadoCivil.getInstance();
            DAOGradoInstruccion objDAOGradoInstruccion = DAOGradoInstruccion.getInstance();
            DAOTipoDocumentoIdentidad objDAOTipoDocumentoIdentidad = DAOTipoDocumentoIdentidad.getInstance();

            int size = lstSocios.size();
            Socio objSocio;
            for (int i = 0; i < size; i++) {
                objSocio = lstSocios.get(i);
                objSocio.setObjDistrito(objDAODistrito.getDistrito(lstIdsDistritos.get(i)));
                objSocio.setObjTipoTrabajador(objDAOTipoTrabajador.getTipoTrabajador(lstIdsTiposTrabajador.get(i)));
                objSocio.setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                objSocio.setLstCuentas(objDAOCuenta.getAllCuentasOnly(objSocio));
                objSocio.setLstDetalleFamiliarSocio(objDAODetalleFamiliarSocio.getAllDetalleFamiliarSocio(objSocio));
                objSocio.setObjTarjeta(objDAOTarjeta.getTarjeta(objSocio));
                objSocio.setObjGradoInstruccion(objDAOGradoInstruccion.getGradoInstruccion(lstIdsGradosInstruccion.get(i)));
                objSocio.setObjEstadoCivil(objDAOEstadoCivil.getEstadoCivil(lstIdsEstadosCiviles.get(i)));
                objSocio.setObjTipoDocumentoIdentidad(objDAOTipoDocumentoIdentidad.getTipoDocumentoIdentidad(lstIdsTiposDocumentoIdentidad.get(i)));
                if (cargarFoto) {
                    objSocio.setFoto(FileUtil.downloadImageFTP(objSocio.getRutaFoto()));
                    FileUtil.deleteTempFile(objSocio.getRutaFoto());
                }
            }
        }

        return lstSocios;
    }

    @Override
    public List<Socio> getAllSociosPasivos(Timestamp fechaLimite, boolean cargarFoto) {
        List<Socio> lstSocios = new ArrayList<>();
        List<Integer> lstIdsDistritos = new ArrayList<>();
        List<Integer> lstIdsTiposTrabajador = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        List<Integer> lstIdsEstadosCiviles = new ArrayList<>();
        List<Integer> lstIdsGradosInstruccion = new ArrayList<>();
        List<Integer> lstIdsTiposDocumentoIdentidad = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT s.id, s.rutafoto, s.codigo, s.nombre, apellidopaterno, s.apellidomaterno, s.fechanacimiento, s.sexo, "
                + "s.direccion, s.origen, s.documentoidentidad, s.ruc, s.telefono, s.celular, s.ocupacion, s.direccionlaboral, s.ingresomensual, s.fechaingreso, s.fecharegistro, s.fechamodificacion, "
                + "s.estado, s.activo, s.idtipotrabajador, s.idusuario, s.iddistrito, s.idgradoinstruccion, s.idestadocivil, s.juridico, s.renunciado, s.idtipodocumentoidentidad "
                + "FROM socios s INNER JOIN tarjetas t ON t.idsocio = s.id "
                + "INNER JOIN aportes a ON a.idtarjeta = t.id "
                + "WHERE s.activo is true AND "
                + "a.id IN (SELECT asub.id FROM aportes as asub "
                + "WHERE asub.pagado is true AND asub.habilitado is true AND asub.idtarjeta = t.id "
                + "ORDER BY asub.id DESC "
                + "LIMIT 1) "
                + "AND a.fechamodificacion < ?;")) {

            preparedStatement.setTimestamp(1, fechaLimite);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsDistritos.add(resultSet.getInt("iddistrito"));
                lstIdsTiposTrabajador.add(resultSet.getInt("idtipotrabajador"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                lstIdsEstadosCiviles.add(resultSet.getInt("idestadocivil"));
                lstIdsGradosInstruccion.add(resultSet.getInt("idgradoinstruccion"));
                lstIdsTiposDocumentoIdentidad.add(resultSet.getInt("idtipodocumentoidentidad"));
                Socio objSocio = new Socio(
                        resultSet.getLong("id"),
                        null,
                        resultSet.getString("rutafoto"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"),
                        resultSet.getTimestamp("fechanacimiento"),
                        resultSet.getString("direccion"),
                        resultSet.getString("documentoidentidad"),
                        resultSet.getString("ruc"),
                        resultSet.getString("telefono"),
                        resultSet.getString("celular"),
                        resultSet.getBoolean("sexo"),
                        resultSet.getString("origen"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fechaingreso"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getString("codigo"),
                        resultSet.getString("ocupacion"),
                        resultSet.getString("direccionlaboral"),
                        resultSet.getBigDecimal("ingresomensual"),
                        resultSet.getBoolean("activo"),
                        resultSet.getBoolean("juridico"),
                        resultSet.getBoolean("renunciado"),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>());
                lstSocios.add(objSocio);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstSocios.isEmpty()) {
            DAODistrito objDAODistrito = DAODistrito.getInstance();
            DAOTipoTrabajador objDAOTipoTrabajador = DAOTipoTrabajador.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            //DAOCuenta objDAOCuenta = DAOCuenta.getInstance();
            DAODetalleFamiliarSocio objDAODetalleFamiliarSocio = DAODetalleFamiliarSocio.getInstance();
            DAOTarjeta objDAOTarjeta = DAOTarjeta.getInstance();
            DAOEstadoCivil objDAOEstadoCivil = DAOEstadoCivil.getInstance();
            DAOGradoInstruccion objDAOGradoInstruccion = DAOGradoInstruccion.getInstance();
            DAOTipoDocumentoIdentidad objDAOTipoDocumentoIdentidad = DAOTipoDocumentoIdentidad.getInstance();

            int size = lstSocios.size();
            Socio objSocio;
            for (int i = 0; i < size; i++) {
                objSocio = lstSocios.get(i);
                objSocio.setObjDistrito(objDAODistrito.getDistrito(lstIdsDistritos.get(i)));
                objSocio.setObjTipoTrabajador(objDAOTipoTrabajador.getTipoTrabajador(lstIdsTiposTrabajador.get(i)));
                objSocio.setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                //lstSocios.get(i).setLstCuentas(objDAOCuenta.getAllCuentasOnly(lstSocios.get(i)));
                objSocio.setLstDetalleFamiliarSocio(objDAODetalleFamiliarSocio.getAllDetalleFamiliarSocio(objSocio));
                objSocio.setObjTarjeta(objDAOTarjeta.getTarjeta(objSocio));
                objSocio.setObjGradoInstruccion(objDAOGradoInstruccion.getGradoInstruccion(lstIdsGradosInstruccion.get(i)));
                objSocio.setObjEstadoCivil(objDAOEstadoCivil.getEstadoCivil(lstIdsEstadosCiviles.get(i)));
                objSocio.setObjTipoDocumentoIdentidad(objDAOTipoDocumentoIdentidad.getTipoDocumentoIdentidad(lstIdsTiposDocumentoIdentidad.get(i)));
                if (cargarFoto) {
                    objSocio.setFoto(FileUtil.downloadImageFTP(objSocio.getRutaFoto()));
                    FileUtil.deleteTempFile(objSocio.getRutaFoto());
                }
            }
        }

        return lstSocios;
    }

    @Override
    public List<Socio> getAllSocios(Cuenta objCuenta, boolean cargarFoto) {
        List<Socio> lstSocios = new ArrayList<>();
        List<Integer> lstIdsDistritos = new ArrayList<>();
        List<Integer> lstIdsTiposTrabajador = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        List<Integer> lstIdsEstadosCiviles = new ArrayList<>();
        List<Integer> lstIdsGradosInstruccion = new ArrayList<>();
        List<Integer> lstIdsTiposDocumentoIdentidad = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT s.id, s.rutafoto, s.codigo, s.nombre, apellidopaterno, s.apellidomaterno, s.fechanacimiento, s.sexo, "
                + "s.direccion, s.origen, s.documentoidentidad, s.ruc, s.telefono, s.celular, s.ocupacion, s.direccionlaboral, s.ingresomensual, s.fechaingreso, s.fecharegistro, s.fechamodificacion, "
                + "s.estado, s.activo, s.idtipotrabajador, s.idusuario, s.iddistrito, s.idgradoinstruccion, s.idestadocivil, s.juridico, s.renunciado, s.idtipodocumentoidentidad "
                + "FROM socios s INNER JOIN detallecuentasocio dcs ON s.id = dcs.idsocio "
                + "INNER JOIN cuentas c ON c.id = dcs.idcuenta "
                + "WHERE c.id = ?;")) {
            preparedStatement.setLong(1, objCuenta.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsDistritos.add(resultSet.getInt("iddistrito"));
                lstIdsTiposTrabajador.add(resultSet.getInt("idtipotrabajador"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                lstIdsEstadosCiviles.add(resultSet.getInt("idestadocivil"));
                lstIdsGradosInstruccion.add(resultSet.getInt("idgradoinstruccion"));
                lstIdsTiposDocumentoIdentidad.add(resultSet.getInt("idtipodocumentoidentidad"));
                Socio objSocio = new Socio(
                        resultSet.getLong("id"),
                        null,
                        resultSet.getString("rutafoto"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"),
                        resultSet.getTimestamp("fechanacimiento"),
                        resultSet.getString("direccion"),
                        resultSet.getString("documentoidentidad"),
                        resultSet.getString("ruc"),
                        resultSet.getString("telefono"),
                        resultSet.getString("celular"),
                        resultSet.getBoolean("sexo"),
                        resultSet.getString("origen"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fechaingreso"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getString("codigo"),
                        resultSet.getString("ocupacion"),
                        resultSet.getString("direccionlaboral"),
                        resultSet.getBigDecimal("ingresomensual"),
                        resultSet.getBoolean("activo"),
                        resultSet.getBoolean("juridico"),
                        resultSet.getBoolean("renunciado"),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>());
                lstSocios.add(objSocio);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstSocios.isEmpty()) {
            DAODistrito objDAODistrito = DAODistrito.getInstance();
            DAOTipoTrabajador objDAOTipoTrabajador = DAOTipoTrabajador.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOCuenta objDAOCuenta = DAOCuenta.getInstance();
            DAODetalleFamiliarSocio objDAODetalleFamiliarSocio = DAODetalleFamiliarSocio.getInstance();
            DAOTarjeta objDAOTarjeta = DAOTarjeta.getInstance();
            DAOEstadoCivil objDAOEstadoCivil = DAOEstadoCivil.getInstance();
            DAOGradoInstruccion objDAOGradoInstruccion = DAOGradoInstruccion.getInstance();
            DAOTipoDocumentoIdentidad objDAOTipoDocumentoIdentidad = DAOTipoDocumentoIdentidad.getInstance();

            int size = lstSocios.size();
            Socio objSocio;
            for (int i = 0; i < size; i++) {
                objSocio = lstSocios.get(i);
                objSocio.setObjDistrito(objDAODistrito.getDistrito(lstIdsDistritos.get(i)));
                objSocio.setObjTipoTrabajador(objDAOTipoTrabajador.getTipoTrabajador(lstIdsTiposTrabajador.get(i)));
                objSocio.setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                objSocio.setLstCuentas(objDAOCuenta.getAllCuentasOnly(objSocio));
                objSocio.setLstDetalleFamiliarSocio(objDAODetalleFamiliarSocio.getAllDetalleFamiliarSocio(objSocio));
                objSocio.setObjTarjeta(objDAOTarjeta.getTarjeta(objSocio));
                objSocio.setObjGradoInstruccion(objDAOGradoInstruccion.getGradoInstruccion(lstIdsGradosInstruccion.get(i)));
                objSocio.setObjEstadoCivil(objDAOEstadoCivil.getEstadoCivil(lstIdsEstadosCiviles.get(i)));
                objSocio.setObjTipoDocumentoIdentidad(objDAOTipoDocumentoIdentidad.getTipoDocumentoIdentidad(lstIdsTiposDocumentoIdentidad.get(i)));
                if (cargarFoto) {
                    objSocio.setFoto(FileUtil.downloadImageFTP(objSocio.getRutaFoto()));
                    FileUtil.deleteTempFile(objSocio.getRutaFoto());
                }
            }
        }

        return lstSocios;
    }

    @Override
    public List<Socio> getAllSociosOnly(Cuenta objCuenta, boolean cargarFoto) {
        List<Socio> lstSocios = new ArrayList<>();
        List<Integer> lstIdsDistritos = new ArrayList<>();
        List<Integer> lstIdsTiposTrabajador = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        List<Integer> lstIdsEstadosCiviles = new ArrayList<>();
        List<Integer> lstIdsGradosInstruccion = new ArrayList<>();
        List<Integer> lstIdsTiposDocumentoIdentidad = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT s.id, s.rutafoto, s.codigo, s.nombre, apellidopaterno, s.apellidomaterno, s.fechanacimiento, s.sexo, "
                + "s.direccion, s.origen, s.documentoidentidad, s.ruc, s.telefono, s.celular, s.ocupacion, s.direccionlaboral, s.ingresomensual, s.fechaingreso, s.fecharegistro, s.fechamodificacion, "
                + "s.estado, s.activo, s.idtipotrabajador, s.idusuario, s.iddistrito, s.idgradoinstruccion, s.idestadocivil, s.juridico, s.renunciado, s.idtipodocumentoidentidad "
                + "FROM socios s INNER JOIN detallecuentasocio dcs ON s.id = dcs.idsocio "
                + "INNER JOIN cuentas c ON c.id = dcs.idcuenta "
                + "WHERE c.id = ?;")) {
            preparedStatement.setLong(1, objCuenta.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsDistritos.add(resultSet.getInt("iddistrito"));
                lstIdsTiposTrabajador.add(resultSet.getInt("idtipotrabajador"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                lstIdsEstadosCiviles.add(resultSet.getInt("idestadocivil"));
                lstIdsGradosInstruccion.add(resultSet.getInt("idgradoinstruccion"));
                lstIdsTiposDocumentoIdentidad.add(resultSet.getInt("idtipodocumentoidentidad"));
                Socio objSocio = new Socio(
                        resultSet.getLong("id"),
                        null,
                        resultSet.getString("rutafoto"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"),
                        resultSet.getTimestamp("fechanacimiento"),
                        resultSet.getString("direccion"),
                        resultSet.getString("documentoidentidad"),
                        resultSet.getString("ruc"),
                        resultSet.getString("telefono"),
                        resultSet.getString("celular"),
                        resultSet.getBoolean("sexo"),
                        resultSet.getString("origen"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fechaingreso"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getString("codigo"),
                        resultSet.getString("ocupacion"),
                        resultSet.getString("direccionlaboral"),
                        resultSet.getBigDecimal("ingresomensual"),
                        resultSet.getBoolean("activo"),
                        resultSet.getBoolean("juridico"),
                        resultSet.getBoolean("renunciado"),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>());
                lstSocios.add(objSocio);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstSocios.isEmpty()) {
            DAODistrito objDAODistrito = DAODistrito.getInstance();
            DAOTipoTrabajador objDAOTipoTrabajador = DAOTipoTrabajador.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAODetalleFamiliarSocio objDAODetalleFamiliarSocio = DAODetalleFamiliarSocio.getInstance();
            DAOTarjeta objDAOTarjeta = DAOTarjeta.getInstance();
            DAOEstadoCivil objDAOEstadoCivil = DAOEstadoCivil.getInstance();
            DAOGradoInstruccion objDAOGradoInstruccion = DAOGradoInstruccion.getInstance();
            DAOTipoDocumentoIdentidad objDAOTipoDocumentoIdentidad = DAOTipoDocumentoIdentidad.getInstance();

            int size = lstSocios.size();
            Socio objSocio;
            for (int i = 0; i < size; i++) {
                objSocio = lstSocios.get(i);
                objSocio.setObjDistrito(objDAODistrito.getDistrito(lstIdsDistritos.get(i)));
                objSocio.setObjTipoTrabajador(objDAOTipoTrabajador.getTipoTrabajador(lstIdsTiposTrabajador.get(i)));
                objSocio.setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                objSocio.setLstDetalleFamiliarSocio(objDAODetalleFamiliarSocio.getAllDetalleFamiliarSocio(objSocio));
                objSocio.setObjTarjeta(objDAOTarjeta.getTarjeta(objSocio));
                objSocio.setObjGradoInstruccion(objDAOGradoInstruccion.getGradoInstruccion(lstIdsGradosInstruccion.get(i)));
                objSocio.setObjEstadoCivil(objDAOEstadoCivil.getEstadoCivil(lstIdsEstadosCiviles.get(i)));
                objSocio.setObjTipoDocumentoIdentidad(objDAOTipoDocumentoIdentidad.getTipoDocumentoIdentidad(lstIdsTiposDocumentoIdentidad.get(i)));
                if (cargarFoto) {
                    objSocio.setFoto(FileUtil.downloadImageFTP(objSocio.getRutaFoto()));
                    FileUtil.deleteTempFile(objSocio.getRutaFoto());
                }
            }
        }

        return lstSocios;
    }

    @Override
    public boolean insert(Socio objSocio) {
        boolean resultado = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO socios (codigo,rutafoto,nombre,apellidopaterno,"
                + "apellidomaterno,fechanacimiento,sexo,direccion,documentoidentidad,telefono,celular,origen,ocupacion,idgradoinstruccion,"
                + "idestadocivil,direccionlaboral,ingresomensual,fecharegistro,estado,activo,iddistrito,"
                + "idtipotrabajador,idusuario,juridico, idtipodocumentoidentidad, fechaingreso) "
                + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?,?,?,?)",
                PreparedStatement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, objSocio.getCodigo());
            preparedStatement.setString(2, objSocio.getRutaFoto());
            preparedStatement.setString(3, objSocio.getNombre());
            preparedStatement.setString(4, objSocio.getApellidoPaterno());
            preparedStatement.setString(5, objSocio.getApellidoMaterno());
            preparedStatement.setTimestamp(6, objSocio.getFechaNacimiento());
            preparedStatement.setBoolean(7, objSocio.isSexo());
            preparedStatement.setString(8, objSocio.getDireccion());
            preparedStatement.setString(9, objSocio.getDocumentoIdentidad());
            preparedStatement.setString(10, objSocio.getTelefono());
            preparedStatement.setString(11, objSocio.getCelular());
            preparedStatement.setString(12, objSocio.getOrigen());
            preparedStatement.setString(13, objSocio.getOcupacion());
            preparedStatement.setInt(14, objSocio.getObjGradoInstruccion().getId());
            preparedStatement.setInt(15, objSocio.getObjEstadoCivil().getId());
            preparedStatement.setString(16, objSocio.getDireccionLaboral());
            preparedStatement.setBigDecimal(17, objSocio.getIngresoMensual());
            preparedStatement.setBoolean(18, objSocio.isEstado());
            preparedStatement.setBoolean(19, objSocio.isActivo());
            preparedStatement.setInt(20, objSocio.getObjDistrito().getId());
            preparedStatement.setInt(21, objSocio.getObjTipoTrabajador().getId());
            preparedStatement.setLong(22, objSocio.getObjUsuario().getId());
            preparedStatement.setBoolean(23, objSocio.isJuridico());
            preparedStatement.setInt(24, objSocio.getObjTipoDocumentoIdentidad().getId());
            preparedStatement.setTimestamp(25, objSocio.getFechaIngreso());

            preparedStatement.execute();
            ResultSet keySet = preparedStatement.getGeneratedKeys();

            while (keySet.next()) {
                objSocio.setId(keySet.getLong(1));
            }

            resultado = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultado;
    }

    @Override
    public boolean update(Socio objSocio) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE socios SET codigo = ?, rutafoto = ?, nombre = ?, "
                + "apellidopaterno = ?, apellidomaterno = ?, fechanacimiento = ?, sexo = ?, direccion = ?, documentoidentidad = ?, ruc = ?, telefono = ?, celular = ?, "
                + "origen = ?, ocupacion = ?, idgradoinstruccion = ?, idestadocivil = ?, direccionlaboral = ?, ingresomensual = ?, "
                + "fechamodificacion = ?, estado = ?, activo = ?, idtipotrabajador = ?, idusuario = ?, iddistrito = ?, juridico = ?, renunciado = ?, idtipodocumentoidentidad = ?, fechaingreso = ? "
                + "WHERE id = ?;")) {
            preparedStatement.setString(1, objSocio.getCodigo());
            preparedStatement.setString(2, objSocio.getRutaFoto());
            preparedStatement.setString(3, objSocio.getNombre());
            preparedStatement.setString(4, objSocio.getApellidoPaterno());
            preparedStatement.setString(5, objSocio.getApellidoMaterno());
            preparedStatement.setTimestamp(6, objSocio.getFechaNacimiento());
            preparedStatement.setBoolean(7, objSocio.isSexo());
            preparedStatement.setString(8, objSocio.getDireccion());
            preparedStatement.setString(9, objSocio.getDocumentoIdentidad());
            preparedStatement.setString(10, objSocio.getRUC());
            preparedStatement.setString(11, objSocio.getTelefono());
            preparedStatement.setString(12, objSocio.getCelular());
            preparedStatement.setString(13, objSocio.getOrigen());
            preparedStatement.setString(14, objSocio.getOcupacion());
            preparedStatement.setInt(15, objSocio.getObjGradoInstruccion().getId());
            preparedStatement.setInt(16, objSocio.getObjEstadoCivil().getId());
            preparedStatement.setString(17, objSocio.getDireccionLaboral());
            preparedStatement.setBigDecimal(18, objSocio.getIngresoMensual());
            preparedStatement.setTimestamp(19, objSocio.getFechaModificacion());
            preparedStatement.setBoolean(20, objSocio.isEstado());
            preparedStatement.setBoolean(21, objSocio.isActivo());
            preparedStatement.setInt(22, objSocio.getObjTipoTrabajador().getId());
            preparedStatement.setLong(23, objSocio.getObjUsuario().getId());
            preparedStatement.setInt(24, objSocio.getObjDistrito().getId());
            preparedStatement.setBoolean(25, objSocio.isJuridico());
            preparedStatement.setBoolean(26, objSocio.isRenunciado());
            preparedStatement.setInt(27, objSocio.getObjTipoDocumentoIdentidad().getId());
            preparedStatement.setTimestamp(28, objSocio.getFechaIngreso());

            preparedStatement.setLong(29, objSocio.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean physicalDelete(Socio objSocio) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM socios WHERE id = ?;")) {
            preparedStatement.setLong(1, objSocio.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
