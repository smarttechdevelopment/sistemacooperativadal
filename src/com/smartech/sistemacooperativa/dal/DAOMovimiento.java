package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Movimiento;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.interfaces.IDAOMovimiento;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

/**
 *
 * @author Smartech
 */
public class DAOMovimiento implements IDAOMovimiento {

    private static DAOMovimiento instance = null;

    public static DAOMovimiento getInstance() {

        if (instance == null) {
            instance = new DAOMovimiento();
        }
        return instance;
    }

    @Override
    public boolean insert(Movimiento objMovimiento) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO movimientos (fecha, fecharegistro, fechamodificacion, estado, idpago, idcaja, idempleado) "
                + " VALUES (?, ?, ?, ?, ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setTimestamp(1, objMovimiento.getFecha());
            preparedStatement.setTimestamp(2, objMovimiento.getFechaRegistro());
            preparedStatement.setTimestamp(3, objMovimiento.getFechaModificacion());
            preparedStatement.setBoolean(4, objMovimiento.isEstado());
            if (objMovimiento.getObjPago() == null) {
                preparedStatement.setNull(5, Types.INTEGER);
            } else {
                preparedStatement.setLong(5, objMovimiento.getObjPago().getId());
            }

            if (objMovimiento.getObjCaja() == null) {
                preparedStatement.setNull(6, Types.INTEGER);
            } else {
                preparedStatement.setInt(6, objMovimiento.getObjCaja().getId());
            }
            preparedStatement.setLong(7, objMovimiento.getObjEmpleado().getId());

            preparedStatement.execute();

            ResultSet keySet = preparedStatement.getGeneratedKeys();

            while (keySet.next()) {
                objMovimiento.setId(keySet.getLong(1));
            }

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Movimiento getMovimiento(Pago objPago) {
        Movimiento objMovimiento = null;
        long idEmpleado = 0;
        int idCaja = 0;
        
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT m.id, m.fecha, m.fecharegistro, m.fechamodificacion, m.estado, m.idempleado, m.idcaja "
                + "FROM movimientos m INNER JOIN pagos p ON m.idpago = p.id "
                + "INNER JOIN conceptos c ON p.idconcepto = c.id "
                + "WHERE c.id = ? AND p.id = ?;")) {                        
            preparedStatement.setInt(1, objPago.getObjConcepto().getId());
            preparedStatement.setLong(2, objPago.getId());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                idEmpleado = resultSet.getLong("idempleado");
                idCaja = resultSet.getInt("idcaja");                
                objMovimiento = new Movimiento(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fecha"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        objPago,
                        null,
                        null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if(objMovimiento != null){            
            objMovimiento.setObjEmpleado(DAOEmpleado.getInstance().getEmpleado(idEmpleado));
            objMovimiento.setObjCaja(DAOCaja.getInstance().getCaja(idCaja));            
        }
        
        return objMovimiento;
    }
}
