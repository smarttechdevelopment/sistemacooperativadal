package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.DetalleSolicitudPrestamoSocio;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.interfaces.IDAODetalleSolicitudPrestamoSocio;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAODetalleSolicitudPrestamoSocio implements IDAODetalleSolicitudPrestamoSocio {

    private static DAODetalleSolicitudPrestamoSocio instance = null;

    private DAODetalleSolicitudPrestamoSocio() {
    }

    public static DAODetalleSolicitudPrestamoSocio getInstance() {
        if (instance == null) {
            instance = new DAODetalleSolicitudPrestamoSocio();
        }

        return instance;
    }

    @Override
    public List<DetalleSolicitudPrestamoSocio> getAllDetalleSolicitudPrestamoSocio(SolicitudPrestamo objSolicitudPrestamo) {
        List<DetalleSolicitudPrestamoSocio> lstDetalleSolicitudPrestamoSocio = new ArrayList<>();
        List<Long> lstIdsSocios = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT dsps.id, dsps.fiador, dsps.ingresos, dsps.egresos, "
                + "dsps.otrosingresos, dsps.saldoavalable, dsps.idsocio, dsps.idsolicitudprestamo "
                + "FROM detallesolicitudprestamosocio dsps "
                + "WHERE dsps.idsolicitudprestamo = ?;")) {
            preparedStatement.setLong(1, objSolicitudPrestamo.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsSocios.add(resultSet.getLong("idsocio"));
                DetalleSolicitudPrestamoSocio objDetalleSolicitudPrestamoSocio = new DetalleSolicitudPrestamoSocio(
                        resultSet.getLong("id"),
                        resultSet.getBoolean("fiador"),
                        resultSet.getBigDecimal("ingresos"),
                        resultSet.getBigDecimal("egresos"),
                        resultSet.getBigDecimal("otrosingresos"),
                        resultSet.getBigDecimal("saldoavalable"),
                        null,
                        objSolicitudPrestamo);
                lstDetalleSolicitudPrestamoSocio.add(objDetalleSolicitudPrestamoSocio);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstDetalleSolicitudPrestamoSocio.isEmpty()) {
            DAOSocio objDAOSocio = DAOSocio.getInstance();

            int size = lstDetalleSolicitudPrestamoSocio.size();
            for (int i = 0; i < size; i++) {
                if (!lstDetalleSolicitudPrestamoSocio.get(i).isGarante()) {
                    lstDetalleSolicitudPrestamoSocio.get(i).setObjSocio(objDAOSocio.getSocio(lstIdsSocios.get(i), true));
                }else{
                    lstDetalleSolicitudPrestamoSocio.get(i).setObjSocio(objDAOSocio.getSocio(lstIdsSocios.get(i), true));
                }
            }
        }

        return lstDetalleSolicitudPrestamoSocio;
    }

    @Override
    public boolean insert(DetalleSolicitudPrestamoSocio objDetalleSolicitudPrestamoSocio) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO detallesolicitudprestamosocio(fiador, ingresos, egresos, otrosingresos, saldoavalable, idsocio, idsolicitudprestamo) "
                + "VALUES(?, ?, ?, ?, ?, ?, ?);")) {
            preparedStatement.setBoolean(1, objDetalleSolicitudPrestamoSocio.isGarante());
            preparedStatement.setBigDecimal(2, objDetalleSolicitudPrestamoSocio.getIngresos());
            preparedStatement.setBigDecimal(3, objDetalleSolicitudPrestamoSocio.getEgresos());
            preparedStatement.setBigDecimal(4, objDetalleSolicitudPrestamoSocio.getOtrosIngresos());
            preparedStatement.setBigDecimal(5, objDetalleSolicitudPrestamoSocio.getSaldoAvalable());
            preparedStatement.setLong(6, objDetalleSolicitudPrestamoSocio.getObjSocio().getId());
            preparedStatement.setLong(7, objDetalleSolicitudPrestamoSocio.getObjSolicitudPrestamo().getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean physicalDelete(SolicitudPrestamo objSolicitudPrestamo) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM detallesolicitudprestamosocio WHERE idsolicitudprestamo = ?;")) {
            preparedStatement.setLong(1, objSolicitudPrestamo.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
