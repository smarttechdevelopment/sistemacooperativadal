package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.EstadoCivil;
import com.smartech.sistemacooperativa.interfaces.IDAOEstadoCivil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAOEstadoCivil implements IDAOEstadoCivil {

    private static DAOEstadoCivil instance = null;
    private Map<Integer, EstadoCivil> cache;

    private DAOEstadoCivil() {
        this.cache = new HashMap<>();
    }

    public static DAOEstadoCivil getInstance() {
        if (instance == null) {
            instance = new DAOEstadoCivil();
        }

        return instance;
    }

    @Override
    public EstadoCivil getEstadoCivil(int id) {
        EstadoCivil objEstadoCivil = this.cache.get(id);

        if (objEstadoCivil == null) {
            this.cache.clear();
            this.getAllEstadosCiviles();
            objEstadoCivil = this.cache.get(id);
        }

        return objEstadoCivil;
    }

    @Override
    public List<EstadoCivil> getAllEstadosCiviles() {
        List<EstadoCivil> lstEstadosCiviles;

        if (this.cache.isEmpty()) {
            try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareCall("SELECT ec.id, ec.nombre "
                    + "FROM estadosciviles ec;")) {

                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    EstadoCivil objEstadoCivil = new EstadoCivil(
                            resultSet.getInt("id"),
                            resultSet.getString("nombre"));
                    this.cache.put(objEstadoCivil.getId(), objEstadoCivil);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Collection collection = this.cache.values();

        lstEstadosCiviles = new ArrayList<>(collection);

        return lstEstadosCiviles;
    }
}
