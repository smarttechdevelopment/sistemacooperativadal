package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.HistoricoCuenta;
import com.smartech.sistemacooperativa.interfaces.IDAOHistoricoCuenta;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOHistoricoCuenta implements IDAOHistoricoCuenta{

    private static DAOHistoricoCuenta instance = null;

    private DAOHistoricoCuenta() {
    }

    public static DAOHistoricoCuenta getInstance() {
        if (instance == null) {
            instance = new DAOHistoricoCuenta();
        }

        return instance;
    }

    @Override
    public List<HistoricoCuenta> getAllHistoricoCuenta(Cuenta objCuenta) {
        List<HistoricoCuenta> lstHistoricoCuenta = new ArrayList<>();
        List<Long> lstIdsComprobantes = new ArrayList<>();
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT hc.id, hc.saldo, hc.fecha, hc.idcuenta, hc.idcomprobante "
                + "FROM historicocuenta hc "
                + "WHERE hc.idcuenta = ?;")){
            preparedStatement.setLong(1, objCuenta.getId());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                lstIdsComprobantes.add(resultSet.getLong("idcomprobante"));
                HistoricoCuenta objHistoricoCuenta = new HistoricoCuenta(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("saldo"),
                        resultSet.getTimestamp("fecha"),
                        objCuenta,
                        null);
                lstHistoricoCuenta.add(objHistoricoCuenta);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        if(!lstHistoricoCuenta.isEmpty()){
            DAOComprobante objDAOComprobante = DAOComprobante.getInstance();
            
            int size = lstHistoricoCuenta.size();
            for(int i = 0; i < size; i++){
                lstHistoricoCuenta.get(i).setObjComprobante(objDAOComprobante.getComprobante(lstIdsComprobantes.get(i)));
            }
        }
        
        return lstHistoricoCuenta;
    }

    @Override
    public List<HistoricoCuenta> getAllHistoricoCuenta(Cuenta objCuenta, Timestamp fechaDesde, Timestamp fechaHasta) {
        List<HistoricoCuenta> lstHistoricoCuenta = new ArrayList<>();
        List<Long> lstIdsComprobantes = new ArrayList<>();
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT hc.id, hc.saldo, hc.fecha, hc.idcuenta, hc.idcomprobante "
                + "FROM historicocuenta hc "
                + "WHERE hc.idcuenta = ? AND hc.fecha BETWEEN ? AND ?;")){
            preparedStatement.setLong(1, objCuenta.getId());
            preparedStatement.setTimestamp(2, fechaDesde);
            preparedStatement.setTimestamp(3, fechaHasta);
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                lstIdsComprobantes.add(resultSet.getLong("idcomprobante"));
                HistoricoCuenta objHistoricoCuenta = new HistoricoCuenta(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("saldo"),
                        resultSet.getTimestamp("fecha"),
                        objCuenta,
                        null);
                lstHistoricoCuenta.add(objHistoricoCuenta);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        if(!lstHistoricoCuenta.isEmpty()){
            DAOComprobante objDAOComprobante = DAOComprobante.getInstance();
            
            int size = lstHistoricoCuenta.size();
            for(int i = 0; i < size; i++){
                lstHistoricoCuenta.get(i).setObjComprobante(objDAOComprobante.getComprobante(lstIdsComprobantes.get(i)));
            }
        }
        
        return lstHistoricoCuenta;
    }

    @Override
    public boolean insertHistoricoCuenta(HistoricoCuenta objHistoricoCuenta) {
        boolean result = false;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO historicocuenta(saldo, fecha, idcuenta, idcomprobante) "
                + "VALUES(?, ?, ?, ?);")){
            preparedStatement.setBigDecimal(1, objHistoricoCuenta.getSaldo());
            preparedStatement.setTimestamp(2, objHistoricoCuenta.getFecha());
            preparedStatement.setLong(3, objHistoricoCuenta.getObjCuenta().getId());
            preparedStatement.setLong(4, objHistoricoCuenta.getObjComprobante().getId());
            
            preparedStatement.execute();
            
            result = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
    }
}
