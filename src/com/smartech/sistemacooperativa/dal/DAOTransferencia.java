/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Transferencia;
import com.smartech.sistemacooperativa.dominio.TransferenciaCuenta;
import com.smartech.sistemacooperativa.interfaces.IDAOTransferencia;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOTransferencia implements IDAOTransferencia{

    private static DAOTransferencia instance = null;        
    
    private DAOTransferencia(){
    }
    
    protected static DAOTransferencia getInstance(){
        if (instance == null){
            instance = new DAOTransferencia();
        }
        return instance;
    }

    @Override
    public Transferencia getTransferencia(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Transferencia getTransferencia(Pago objPago) {
        Transferencia objTransferencia = null;
        
        objTransferencia =  DAOTransferenciaCuenta.getInstance().getTransferencia(objPago);
        if(objTransferencia == null){
            // transferencia bancaria
        }
        
        return objTransferencia;
    }

    @Override
    public List<Transferencia> getAllTransferencias(Cuenta objCuenta) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Transferencia> getAllTransferencias(Timestamp fechaDesde, Timestamp fechaFin) {
        List<Transferencia> lstTransferencias = new ArrayList<>();
        
        lstTransferencias.addAll(DAOTransferenciaCuenta.getInstance().getAllTransferencias(fechaDesde,fechaFin));
        // lstTransferencias.addAll -> lstTransferenciaCuentaBancaria
        return lstTransferencias;
    }

    @Override
    public boolean insert(Transferencia objTransferencia) {
        boolean result = false;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO transferencias "
                + "(monto,intercuenta,fecha,fecharegistro,estado,idmoneda,idusuario) "
                + "VALUES (?,?,?,?,?,?,?);",PreparedStatement.RETURN_GENERATED_KEYS)){
            
            preparedStatement.setBigDecimal(1, objTransferencia.getMonto());
            preparedStatement.setBoolean(2, objTransferencia instanceof TransferenciaCuenta);
            preparedStatement.setTimestamp(3, objTransferencia.getFecha());
            preparedStatement.setTimestamp(4, objTransferencia.getFechaRegistro());
            preparedStatement.setBoolean(5, objTransferencia.isEstado());
            preparedStatement.setInt(6, objTransferencia.getObjMoneda().getId());
            preparedStatement.setLong(7, objTransferencia.getObjUsuario().getId());
            
            preparedStatement.execute();
            ResultSet keySet = preparedStatement.getGeneratedKeys();
            
            while(keySet.next()){
                objTransferencia.setId(keySet.getLong(1));
            }
            result = true;
            
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
    }

    @Override
    public boolean update(Transferencia objTransferencia) {
        boolean result = false;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE transferencias "
                + "set monto = ?, "
                + "intercuenta = ?, "
                + "fecha = ?, "
                + "fecharegistro = ?, "
                + "estado = ?, "
                + "idmoneda = ?, "
                + "idusuario = ? "
                + "WHERE id = ?; ")){
            
            preparedStatement.setBigDecimal(1, objTransferencia.getMonto());
            preparedStatement.setBoolean(2, objTransferencia instanceof TransferenciaCuenta);
            preparedStatement.setTimestamp(3, objTransferencia.getFecha());
            preparedStatement.setTimestamp(4, objTransferencia.getFechaRegistro());
            preparedStatement.setTimestamp(5, objTransferencia.getFechaModificacion());
            preparedStatement.setBoolean(6, objTransferencia.isEstado());
            preparedStatement.setInt(7, objTransferencia.getObjMoneda().getId());
            preparedStatement.setLong(8, objTransferencia.getObjUsuario().getId());
            preparedStatement.setLong(9, objTransferencia.getId());
            
            preparedStatement.execute();
            
            result = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
    } 

    @Override
    public boolean delete(Transferencia objTransferencia) {
        boolean result = false;
        
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM transferencias "
                + "WHERE id = ?;")){
            
            preparedStatement.setLong(1, objTransferencia.getId());
            
            preparedStatement.execute();
            
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
