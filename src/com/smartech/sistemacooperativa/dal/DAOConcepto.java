package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Concepto;
import com.smartech.sistemacooperativa.interfaces.IDAOConcepto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAOConcepto implements IDAOConcepto {

    private static DAOConcepto instance = null;
    private Map<Integer, Concepto> cache;

    private DAOConcepto() {
        this.cache = new HashMap<>();
    }

    public static DAOConcepto getInstance() {
        if (instance == null) {
            instance = new DAOConcepto();
        }
        return instance;
    }

    @Override
    public Concepto getConcepto(int id) {
        Concepto objConcepto = this.cache.get(id);

        if (objConcepto == null) {
            this.cache.clear();
            this.getAllConceptos();
            objConcepto = this.cache.get(id);
        }

        return objConcepto;
    }

    @Override
    public Concepto getConcepto(String nombre) {
        Concepto objConcepto = null;

        for (Map.Entry<Integer, Concepto> entry : this.cache.entrySet()) {
            if (entry.getValue().getNombre().equals(nombre)) {
                objConcepto = entry.getValue();
                break;
            }
        }

        if (objConcepto == null) {
            this.cache.clear();
            this.getAllConceptos();
            for (Map.Entry<Integer, Concepto> entry : this.cache.entrySet()) {
                if (entry.getValue().getNombre().equals(nombre)) {
                    objConcepto = entry.getValue();
                    break;
                }
            }
        }

        return objConcepto;
    }

    @Override
    public List<Concepto> getAllConceptosOnly() {
        List<Concepto> lstConceptos = new ArrayList<>();
        
        if(this.cache.isEmpty()){
            this.getAllConceptos();
        }
        
        for(Map.Entry<Integer, Concepto> entry : this.cache.entrySet()){
            if(!entry.getValue().isMovimiento()){
                lstConceptos.add(entry.getValue());
            }
        }
        
        return lstConceptos;
    }

    @Override
    public List<Concepto> getAllConceptos() {
        List<Concepto> lstConceptos;

        if (this.cache.isEmpty()) {
            try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT id,nombre,movimiento FROM conceptos;")) {

                ResultSet resultSet = preparedStatement.executeQuery();

                Concepto objConcepto;
                while (resultSet.next()) {
                    objConcepto = new Concepto(
                            resultSet.getInt("id"),
                            resultSet.getString("nombre"),
                            resultSet.getBoolean("movimiento"));
                    this.cache.put(objConcepto.getId(), objConcepto);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Collection collection = this.cache.values();

        lstConceptos = new ArrayList<>(collection);

        return lstConceptos;
    }
}
