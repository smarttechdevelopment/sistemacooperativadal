package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.Solicitud;
import com.smartech.sistemacooperativa.dominio.SolicitudIngreso;
import com.smartech.sistemacooperativa.dominio.TipoSolicitud;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import com.smartech.sistemacooperativa.interfaces.IDAOSolicitudIngreso;
import java.util.Date;

/**
 *
 * @author Smartech
 */
public class DAOSolicitudIngreso implements IDAOSolicitudIngreso {

    private static DAOSolicitudIngreso instance = null;

    private DAOSolicitudIngreso() {
    }

    public static DAOSolicitudIngreso getInstance() {
        if (instance == null) {
            instance = new DAOSolicitudIngreso();
        }

        return instance;
    }

    @Override
    public long getUltimoCodigo(int year) {
        long ultimoCodigo = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT COALESCE(MAX(substring(si.codigo from 1 for 4)::int), 1) ultimocodigo "
                + "FROM solicitudesingreso si "
                + "WHERE substring(si.codigo from 6 for 4) = ?")) {
            preparedStatement.setString(1, String.valueOf(year));

            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                ultimoCodigo = resultSet.getInt("ultimocodigo");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ultimoCodigo;
    }

    @Override
    public SolicitudIngreso getSolicitud(long id) {
        SolicitudIngreso objSolicitud = null;
        int idTipoSolicitud = 0;
        long idSocio = 0;
        long idUsuario = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT s.id, s.codigo, "
                + "s.fechavencimiento, s.fecharegistro, s.fechamodificacion, s.aprobado, s.pagado, s.estado, "
                + "s.idtiposolicitud, s.idsocio, s.idusuario "
                + "FROM solicitudes s "
                + "WHERE s.id = ?;")) {
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idTipoSolicitud = resultSet.getInt("idtiposolicitud");
                idSocio = resultSet.getLong("idsocio");
                idUsuario = resultSet.getLong("idusuario");
                objSolicitud = new SolicitudIngreso(
                        id,
                        resultSet.getString("codigo"),
                        null,
                        null,
                        resultSet.getBoolean("aprobado"),
                        resultSet.getBoolean("pagado"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fechavencimiento"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        null,
                        new ArrayList<>(),
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objSolicitud != null) {
            objSolicitud.setObjSocio(DAOSocio.getInstance().getSocio(idSocio, true));
            objSolicitud.setObjTipoSolicitud(DAOTipoSolicitud.getInstance().getTipoSolicitud(idTipoSolicitud));
            objSolicitud.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objSolicitud.setLstDetalleDocumentoSolicitud(DAODetalleDocumentoSolicitud.getInstance().getAllDetalleDocumentoSolicitud(objSolicitud));
            objSolicitud.setLstAprobaciones(DAOAprobacion.getInstance().getAllAprobaciones(objSolicitud));
        }

        return objSolicitud;
    }

    @Override
    public SolicitudIngreso getSolicitudIngreso(String codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SolicitudIngreso getSolicitudIngresoVigente(String dni) {
        SolicitudIngreso objSolicitudIngreso = null;
        long idSocio = 0;
        int idTipoSolicitud = 0;
        long idUsuario = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareCall("SELECT si.id, si.codigo, si.idtiposolicitud, "
                + "si.idusuario, si.aprobado, si.pagado, si.estado, si.fechavencimiento, si.fecharegistro, si.idsocio "
                + "FROM solicitudesingreso si "
                + "WHERE si.id = (SELECT MAX (sisub.id) idsub "
                + "                 FROM solicitudesingreso sisub INNER JOIN socios s ON sisub.idsocio = s.id "
                + "                 WHERE sisub.pagado is false AND s.documentoidentidad = ? AND sisub.fechavencimiento >= now());")) {
            preparedStatement.setString(1, dni);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idTipoSolicitud = resultSet.getInt("idtiposolicitud");
                idSocio = resultSet.getLong("idsocio");
                idUsuario = resultSet.getLong("idusuario");
                objSolicitudIngreso = new SolicitudIngreso(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        null,//objTipoSolicitud,
                        null,
                        resultSet.getBoolean("aprobado"),
                        resultSet.getBoolean("pagado"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fechavencimiento"),
                        resultSet.getTimestamp("fecharegistro"),
                        new Timestamp(new Date().getTime()),
                        null,//objUsuario,
                        new ArrayList<>(),
                        new ArrayList<>()
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objSolicitudIngreso != null) {
            objSolicitudIngreso.setObjSocio(DAOSocio.getInstance().getSocio(idSocio, true));
            objSolicitudIngreso.setObjTipoSolicitud(DAOTipoSolicitud.getInstance().getTipoSolicitud(idTipoSolicitud));
            objSolicitudIngreso.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objSolicitudIngreso.setLstDetalleDocumentoSolicitud(DAODetalleDocumentoSolicitud.getInstance().getAllDetalleDocumentoSolicitud(objSolicitudIngreso));
            objSolicitudIngreso.setLstAprobaciones(DAOAprobacion.getInstance().getAllAprobaciones(objSolicitudIngreso));
        }

        return objSolicitudIngreso;
    }

    @Override
    public SolicitudIngreso getSolicitudIngresoVigente(Socio objSocio) {
        SolicitudIngreso objSolicitudIngreso = null;
        int idTipoSolicitud = 0;
        long idUsuario = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareCall("SELECT si.id, si.codigo, si.idtiposolicitud, "
                + "si.idusuario, si.aprobado, si.pagado, si.estado, si.fechavencimiento, si.fecharegistro, si.idsocio "
                + "FROM solicitudesingreso si "
                + "WHERE si.id = (SELECT MAX (sisub.id) idsub "
                + "                 FROM solicitudesingreso sisub INNER JOIN socios s ON sisub.idsocio = s.id "
                + "                 WHERE s.id = ?);")) {
            preparedStatement.setLong(1, objSocio.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idTipoSolicitud = resultSet.getInt("idtiposolicitud");
                idUsuario = resultSet.getLong("idusuario");
                objSolicitudIngreso = new SolicitudIngreso(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        null,//objTipoSolicitud,
                        null,
                        resultSet.getBoolean("aprobado"),
                        resultSet.getBoolean("pagado"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fechavencimiento"),
                        resultSet.getTimestamp("fecharegistro"),
                        new Timestamp(new Date().getTime()),
                        null,//objUsuario,
                        new ArrayList<>(),
                        new ArrayList<>()
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objSolicitudIngreso != null) {
            objSolicitudIngreso.setObjSocio(objSocio);
            objSolicitudIngreso.setObjTipoSolicitud(DAOTipoSolicitud.getInstance().getTipoSolicitud(idTipoSolicitud));
            objSolicitudIngreso.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objSolicitudIngreso.setLstDetalleDocumentoSolicitud(DAODetalleDocumentoSolicitud.getInstance().getAllDetalleDocumentoSolicitud(objSolicitudIngreso));
            objSolicitudIngreso.setLstAprobaciones(DAOAprobacion.getInstance().getAllAprobaciones(objSolicitudIngreso));
        }

        return objSolicitudIngreso;
    }

    @Override
    public List<SolicitudIngreso> getAllSolicitudesIngreso(Socio objSocio) {
        List<SolicitudIngreso> lstSolicitudesIngreso = new ArrayList<>();
        SolicitudIngreso objSolicitudIngreso = null;
        long idSocio = 0;
        int idTipoSolicitud = 0;
        long idUsuario = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareCall("SELECT si.id, si.codigo, si.idtiposolicitud, "
                + "si.idusuario, si.aprobado, si.pagado, si.estado, si.fechavencimiento, si.fecharegistro, si.idsocio "
                + "FROM solicitudesingreso si "
                + "WHERE si.idsocio = ? "
                + "ORDER BY si.id asc;")) {
            preparedStatement.setLong(1, objSocio.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idTipoSolicitud = resultSet.getInt("idtiposolicitud");
                idSocio = resultSet.getLong("idsocio");
                idUsuario = resultSet.getLong("idusuario");
                objSolicitudIngreso = new SolicitudIngreso(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        null,//objTipoSolicitud,
                        null,
                        resultSet.getBoolean("aprobado"),
                        resultSet.getBoolean("pagado"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fechavencimiento"),
                        resultSet.getTimestamp("fecharegistro"),
                        new Timestamp(new Date().getTime()),
                        null,//objUsuario,
                        new ArrayList<>(),
                        new ArrayList<>()
                );
                lstSolicitudesIngreso.add(objSolicitudIngreso);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objSolicitudIngreso != null) {
            objSolicitudIngreso.setObjSocio(DAOSocio.getInstance().getSocio(idSocio, true));
            objSolicitudIngreso.setObjTipoSolicitud(DAOTipoSolicitud.getInstance().getTipoSolicitud(idTipoSolicitud));
            objSolicitudIngreso.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objSolicitudIngreso.setLstDetalleDocumentoSolicitud(DAODetalleDocumentoSolicitud.getInstance().getAllDetalleDocumentoSolicitud(objSolicitudIngreso));
            objSolicitudIngreso.setLstAprobaciones(DAOAprobacion.getInstance().getAllAprobaciones(objSolicitudIngreso));
        }

        return lstSolicitudesIngreso;
    }

    @Override
    public List<SolicitudIngreso> getAllSolicitudesIngresoPorPagar(Socio objSocio) {
        List<SolicitudIngreso> lstSolicitudIngresos = new ArrayList<>();
        List<Integer> lstIdsTipoSolicitud = new ArrayList<>();
        List<Integer> lstIdsUsuario = new ArrayList();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT si.id, si.codigo, si.idtiposolicitud, "
                + "si.idusuario, si.aprobado, si.pagado, si.estado, si.fechavencimiento, si.fecharegistro "
                + "FROM solicitudesingreso si INNER JOIN socios s ON si.idsocio = s.id "
                + "WHERE si.id NOT IN (SELECT ddssub.idsolicitudingreso "
                + "FROM detalledocumentosolicitudingreso ddssub "
                + "WHERE aprobado is false) AND si.pagado is false AND s.id = ?;")) {

            preparedStatement.setLong(1, objSocio.getId());

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                SolicitudIngreso objSolicitudIngreso = new SolicitudIngreso(
                        rs.getLong("id"),
                        rs.getString("codigo"),
                        null,//objTipoSolicitud,
                        objSocio,
                        rs.getBoolean("aprobado"),
                        rs.getBoolean("pagado"),
                        rs.getBoolean("estado"),
                        rs.getTimestamp("fechavencimiento"),
                        rs.getTimestamp("fecharegistro"),
                        new Timestamp(new Date().getTime()),
                        null,//objUsuario,
                        new ArrayList<>(),
                        new ArrayList<>()
                );

                lstIdsTipoSolicitud.add(rs.getInt("idtiposolicitud"));
                lstIdsUsuario.add(rs.getInt("idusuario"));

                lstSolicitudIngresos.add(objSolicitudIngreso);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstSolicitudIngresos.isEmpty()) {
            DAOTipoSolicitud objDAOTipoSolicitud = DAOTipoSolicitud.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAODetalleDocumentoSolicitud objDAODetalleDocumentoSolicitud = DAODetalleDocumentoSolicitud.getInstance();
            DAOAprobacion objDAOAprobacion = DAOAprobacion.getInstance();

            int size = lstSolicitudIngresos.size();
            for (int i = 0; i < size; i++) {
                lstSolicitudIngresos.get(i).setObjTipoSolicitud(objDAOTipoSolicitud.getTipoSolicitud(lstIdsTipoSolicitud.get(i)));
                lstSolicitudIngresos.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuario.get(i)));
                lstSolicitudIngresos.get(i).setLstDetalleDocumentoSolicitud(objDAODetalleDocumentoSolicitud.getAllDetalleDocumentoSolicitud(lstSolicitudIngresos.get(i)));
                lstSolicitudIngresos.get(i).setLstAprobaciones(objDAOAprobacion.getAllAprobaciones(lstSolicitudIngresos.get(i)));
            }

        }

        return lstSolicitudIngresos;
    }

    @Override
    public List<SolicitudIngreso> getAllSolicitudesIngresoPorAprobarDocumentos(Socio objSocio) {
        List<SolicitudIngreso> lstSolicitudIngresos = new ArrayList<>();
        List<Integer> lstIdsTipoSolicitud = new ArrayList<>();
        List<Integer> lstIdsUsuario = new ArrayList();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT si.id, si.codigo, si.idtiposolicitud, "
                + "si.idusuario, si.aprobado, si.pagado, si.estado, si.fechavencimiento, si.fecharegistro "
                + "FROM solicitudesingreso si INNER JOIN socios s on si.idsocio = s.id "
                + "WHERE si.id in (SELECT ddssub.idsolicitudingreso "
                + "FROM detalledocumentosolicitudingreso ddssub "
                + "WHERE aprobado is false) AND si.pagado is false AND s.id = ?;")) {

            preparedStatement.setLong(1, objSocio.getId());

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                SolicitudIngreso objSolicitudIngreso = new SolicitudIngreso(
                        rs.getLong("id"),
                        rs.getString("codigo"),
                        null,//objTipoSolicitud,
                        objSocio,
                        rs.getBoolean("aprobado"),
                        rs.getBoolean("pagado"),
                        rs.getBoolean("estado"),
                        rs.getTimestamp("fechavencimiento"),
                        rs.getTimestamp("fecharegistro"),
                        new Timestamp(new Date().getTime()),
                        null,//objUsuario,
                        new ArrayList<>(),
                        new ArrayList<>()
                );

                lstIdsTipoSolicitud.add(rs.getInt("idtiposolicitud"));
                lstIdsUsuario.add(rs.getInt("idusuario"));

                lstSolicitudIngresos.add(objSolicitudIngreso);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstSolicitudIngresos.isEmpty()) {
            DAOTipoSolicitud objDAOTipoSolicitud = DAOTipoSolicitud.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAODetalleDocumentoSolicitud objDAODetalleDocumentoSolicitud = DAODetalleDocumentoSolicitud.getInstance();
            DAOAprobacion objDAOAprobacion = DAOAprobacion.getInstance();

            int size = lstSolicitudIngresos.size();
            for (int i = 0; i < size; i++) {
                lstSolicitudIngresos.get(i).setObjTipoSolicitud(objDAOTipoSolicitud.getTipoSolicitud(lstIdsTipoSolicitud.get(i)));
                lstSolicitudIngresos.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuario.get(i)));
                lstSolicitudIngresos.get(i).setLstDetalleDocumentoSolicitud(objDAODetalleDocumentoSolicitud.getAllDetalleDocumentoSolicitud(lstSolicitudIngresos.get(i)));
                lstSolicitudIngresos.get(i).setLstAprobaciones(objDAOAprobacion.getAllAprobaciones(lstSolicitudIngresos.get(i)));
            }
        }

        return lstSolicitudIngresos;
    }

    @Override
    public List<SolicitudIngreso> getAllSolicitudesIngresoPorPagar() {
        List<SolicitudIngreso> lstSolicitudIngresos = new ArrayList<>();
        List<Integer> lstIdsTipoSolicitud = new ArrayList<>();
        List<Integer> lstIdsUsuario = new ArrayList();
        List<Long> lstIdsSocio = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT si.id, si.idsocio, si.codigo, si.idtiposolicitud, "
                + "si.idusuario, si.aprobado, si.pagado, si.estado, si.fechavencimiento, si.fecharegistro "
                + "FROM solicitudesingreso si INNER JOIN socios s ON si.idsocio = s.id "
                + "WHERE si.id NOT IN (SELECT ddssub.idsolicitudingreso "
                + "FROM detalledocumentosolicitudingreso ddssub "
                + "WHERE aprobado is false) AND si.aprobado is true AND si.pagado is false;")) {

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                SolicitudIngreso objSolicitudIngreso = new SolicitudIngreso(
                        rs.getLong("id"),
                        rs.getString("codigo"),
                        null,//objTipoSolicitud,
                        null,
                        rs.getBoolean("aprobado"),
                        rs.getBoolean("pagado"),
                        rs.getBoolean("estado"),
                        rs.getTimestamp("fechavencimiento"),
                        rs.getTimestamp("fecharegistro"),
                        new Timestamp(new Date().getTime()),
                        null,//objUsuario,
                        new ArrayList<>(),
                        new ArrayList<>()
                );

                lstIdsTipoSolicitud.add(rs.getInt("idtiposolicitud"));
                lstIdsUsuario.add(rs.getInt("idusuario"));
                lstIdsSocio.add(rs.getLong("idsocio"));

                lstSolicitudIngresos.add(objSolicitudIngreso);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstSolicitudIngresos.isEmpty()) {
            DAOTipoSolicitud objDAOTipoSolicitud = DAOTipoSolicitud.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAODetalleDocumentoSolicitud objDAODetalleDocumentoSolicitud = DAODetalleDocumentoSolicitud.getInstance();
            DAOSocio objDAOSocio = DAOSocio.getInstance();
            DAOAprobacion objDAOAprobacion = DAOAprobacion.getInstance();

            int size = lstSolicitudIngresos.size();
            for (int i = 0; i < size; i++) {
                lstSolicitudIngresos.get(i).setObjTipoSolicitud(objDAOTipoSolicitud.getTipoSolicitud(lstIdsTipoSolicitud.get(i)));
                lstSolicitudIngresos.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuario.get(i)));
                lstSolicitudIngresos.get(i).setObjSocio(objDAOSocio.getSocio(lstIdsSocio.get(i), true));
                lstSolicitudIngresos.get(i).setLstDetalleDocumentoSolicitud(objDAODetalleDocumentoSolicitud.getAllDetalleDocumentoSolicitud(lstSolicitudIngresos.get(i)));
                lstSolicitudIngresos.get(i).setLstAprobaciones(objDAOAprobacion.getAllAprobaciones(lstSolicitudIngresos.get(i)));
            }
        }

        return lstSolicitudIngresos;
    }

    @Override
    public List<SolicitudIngreso> getAllSolicitudesIngresoPorAprobarDocumentos() {
        List<SolicitudIngreso> lstSolicitudIngresos = new ArrayList<>();
        List<Integer> lstIdsTipoSolicitud = new ArrayList<>();
        List<Integer> lstIdsUsuario = new ArrayList();
        List<Long> lstIdsSocio = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT si.id, si.codigo, si.idtiposolicitud, "
                + "si.idusuario, si.aprobado, si.pagado, si.estado, si.fechavencimiento, si.fecharegistro, si.idsocio "
                + "FROM solicitudesingreso si INNER JOIN socios s ON si.idsocio = s.id "
                + "WHERE ((si.id IN (SELECT ddssub.idsolicitudingreso "
                + "FROM detalledocumentosolicitudingreso ddssub "
                + "WHERE aprobado is false)) OR si.id IN (SELECT ai.idsolicitudingreso FROM aprobacionesingreso ai WHERE ai.aprobado is false)) AND si.pagado is false;")) {

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                SolicitudIngreso objSolicitudIngreso = new SolicitudIngreso(
                        rs.getLong("id"),
                        rs.getString("codigo"),
                        null,//objTipoSolicitud,
                        null,
                        rs.getBoolean("aprobado"),
                        rs.getBoolean("pagado"),
                        rs.getBoolean("estado"),
                        rs.getTimestamp("fechavencimiento"),
                        rs.getTimestamp("fecharegistro"),
                        new Timestamp(new Date().getTime()),
                        null,//objUsuario,
                        new ArrayList<>(),
                        new ArrayList<>());

                lstIdsTipoSolicitud.add(rs.getInt("idtiposolicitud"));
                lstIdsUsuario.add(rs.getInt("idusuario"));
                lstIdsSocio.add(rs.getLong("idsocio"));

                lstSolicitudIngresos.add(objSolicitudIngreso);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstSolicitudIngresos.isEmpty()) {
            DAOTipoSolicitud objDAOTipoSolicitud = DAOTipoSolicitud.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAODetalleDocumentoSolicitud objDAODetalleDocumentoSolicitud = DAODetalleDocumentoSolicitud.getInstance();
            DAOSocio objDAOSocio = DAOSocio.getInstance();
            DAOAprobacion objDAOAprobacion = DAOAprobacion.getInstance();

            int size = lstSolicitudIngresos.size();
            for (int i = 0; i < size; i++) {
                lstSolicitudIngresos.get(i).setObjTipoSolicitud(objDAOTipoSolicitud.getTipoSolicitud(lstIdsTipoSolicitud.get(i)));
                lstSolicitudIngresos.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuario.get(i)));
                lstSolicitudIngresos.get(i).setObjSocio(objDAOSocio.getSocio(lstIdsSocio.get(i), true));
                lstSolicitudIngresos.get(i).setLstDetalleDocumentoSolicitud(objDAODetalleDocumentoSolicitud.getAllDetalleDocumentoSolicitud(lstSolicitudIngresos.get(i)));
                lstSolicitudIngresos.get(i).setLstAprobaciones(objDAOAprobacion.getAllAprobaciones(lstSolicitudIngresos.get(i)));
            }
        }

        return lstSolicitudIngresos;
    }

    @Override
    public List<SolicitudIngreso> getAllSolicitudesIngreso(Socio objSocio, TipoSolicitud objTipoSolicitud) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<SolicitudIngreso> getAllSolicitudesIngreso(Socio objSocio, TipoSolicitud objTipoSolicitud, boolean aprobado) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<SolicitudIngreso> getAllSolicitudesIngreso(Socio objSocio, TipoSolicitud objTipoSolicitud, boolean aprobado, Timestamp fechaInicio, Timestamp fechaFin) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean insert(Solicitud objSolicitud) {
        boolean resultado = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO solicitudesingreso (fecharegistro, estado, "
                + "idtiposolicitud, idsocio, idusuario, aprobado, codigo, fechavencimiento) "
                + "VALUES (now(),?,?,?,?,?,?,?)",
                PreparedStatement.RETURN_GENERATED_KEYS)) {
            SolicitudIngreso objSolicitudIngreso = (SolicitudIngreso) objSolicitud;

            preparedStatement.setBoolean(1, objSolicitudIngreso.isEstado());
            preparedStatement.setInt(2, objSolicitudIngreso.getObjTipoSolicitud().getId());
            preparedStatement.setLong(3, objSolicitudIngreso.getObjSocio().getId());
            preparedStatement.setLong(4, objSolicitudIngreso.getObjUsuario().getId());
            preparedStatement.setBoolean(5, objSolicitudIngreso.isAprobado());
            preparedStatement.setString(6, objSolicitudIngreso.getCodigo());
            preparedStatement.setTimestamp(7, objSolicitudIngreso.getFechaVencimiento());

            preparedStatement.execute();
            ResultSet keySet = preparedStatement.getGeneratedKeys();

            while (keySet.next()) {
                objSolicitud.setId(keySet.getLong(1));
            }

            resultado = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultado;
    }

    @Override
    public boolean update(Solicitud objSolicitud) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE solicitudesingreso SET codigo = ?, fechavencimiento = ?, "
                + "fechamodificacion = ?, aprobado = ?, pagado = ?, estado = ?, idtiposolicitud = ?, idsocio = ?, idusuario = ? "
                + "WHERE id = ?;")) {
            SolicitudIngreso objSolicitudIngreso = (SolicitudIngreso) objSolicitud;
            
            preparedStatement.setString(1, objSolicitudIngreso.getCodigo());
            preparedStatement.setTimestamp(2, objSolicitudIngreso.getFechaVencimiento());
            preparedStatement.setTimestamp(3, objSolicitudIngreso.getFechaModificacion());
            preparedStatement.setBoolean(4, objSolicitudIngreso.isAprobado());
            preparedStatement.setBoolean(5, objSolicitudIngreso.isPagado());
            preparedStatement.setBoolean(6, objSolicitudIngreso.isEstado());
            preparedStatement.setInt(7, objSolicitudIngreso.getObjTipoSolicitud().getId());
            preparedStatement.setLong(8, objSolicitudIngreso.getObjSocio().getId());
            preparedStatement.setLong(9, objSolicitudIngreso.getObjUsuario().getId());
            preparedStatement.setLong(10, objSolicitudIngreso.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean physicalDelete(Solicitud objSolicitud) {
        boolean result = false;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM solicitudesingreso WHERE id = ?;")){
            preparedStatement.setLong(1, objSolicitud.getId());
            
            preparedStatement.execute();
            
            result = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
    }
}
