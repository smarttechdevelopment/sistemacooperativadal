package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.MovimientoAdministrativo;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.interfaces.IDAOMovimientoAdministrativo;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOMovimientoAdministrativo implements IDAOMovimientoAdministrativo {

    private static DAOMovimientoAdministrativo instance = null;

    private DAOMovimientoAdministrativo() {
    }

    public static DAOMovimientoAdministrativo getInstance() {
        if (instance == null) {
            instance = new DAOMovimientoAdministrativo();
        }

        return instance;
    }

    @Override
    public MovimientoAdministrativo getMovimientoAdministrativo(long id) {
        MovimientoAdministrativo objMovimientoAdministrativo = null;
        long idUsuario = 0;
        long idEmpleado = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT ma.id, ma.monto, ma.descripcion, ma.ingreso, ma.habilitado, ma.pagado, ma.fecharegistro, ma.fechamodificacion, ma.estado, ma.idusuario, ma.idempleado "
                + "FROM movimientosadministrativos ma "
                + "WHERE ma.id = ?;")) {
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idUsuario = resultSet.getLong("idusuario");
                idEmpleado = resultSet.getLong("idempleado");
                objMovimientoAdministrativo = new MovimientoAdministrativo(
                        id,
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getString("descripcion"),
                        resultSet.getBoolean("ingreso"),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        null,
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objMovimientoAdministrativo != null) {
            objMovimientoAdministrativo.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objMovimientoAdministrativo.setObjEmpleado(DAOEmpleado.getInstance().getEmpleado(idEmpleado));
            objMovimientoAdministrativo.setLstPagos(DAOPago.getInstance().getAllPagos(objMovimientoAdministrativo));
        }

        return objMovimientoAdministrativo;
    }

    @Override
    public MovimientoAdministrativo getMovimientoAdministrativo(Pago objPago) {
        MovimientoAdministrativo objMovimientoAdministrativo = null;
        long idUsuario = 0;
        long idEmpleado = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT ma.id, ma.monto, ma.descripcion, ma.ingreso, ma.habilitado, ma.pagado, ma.fecharegistro, ma.fechamodificacion, ma.estado, ma.idusuario, ma.idempleado "
                + "FROM movimientosadministrativos ma "
                + "WHERE ma.id IN (SELECT dpma.idmovimientoadministrativo FROM detallepagomovimientoadministrativo dpma WHERE dpma.idpago = ?);")) {
            preparedStatement.setLong(1, objPago.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idUsuario = resultSet.getLong("idusuario");
                idEmpleado = resultSet.getLong("idempleado");
                objMovimientoAdministrativo = new MovimientoAdministrativo(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getString("descripcion"),
                        resultSet.getBoolean("ingreso"),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        null,
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objMovimientoAdministrativo != null) {
            objMovimientoAdministrativo.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objMovimientoAdministrativo.setObjEmpleado(DAOEmpleado.getInstance().getEmpleado(idEmpleado));
            objMovimientoAdministrativo.setLstPagos(DAOPago.getInstance().getAllPagos(objMovimientoAdministrativo));
        }

        return objMovimientoAdministrativo;
    }

    @Override
    public List<MovimientoAdministrativo> getAllMovimientosAdministrativosHabilitados(Timestamp fechaDesde, Timestamp fechaHasta, boolean ingreso) {
        List<MovimientoAdministrativo> lstMovimientosAdministrativos = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        List<Long> lstIdsEmpleados = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT ma.id, ma.monto, ma.descripcion, ma.ingreso, ma.habilitado, ma.pagado, ma.fecharegistro, ma.fechamodificacion, ma.estado, ma.idusuario, ma.idempleado "
                + "FROM movimientosadministrativos ma "
                + "WHERE ma.estado is true AND ma.habilitado is true AND ma.pagado is false AND ma.ingreso = ? AND ma.fecharegistro BETWEEN ? AND ?;")) {
            preparedStatement.setBoolean(1, ingreso);
            preparedStatement.setTimestamp(2, fechaDesde);
            preparedStatement.setTimestamp(3, fechaHasta);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                lstIdsEmpleados.add(resultSet.getLong("idempleado"));
                MovimientoAdministrativo objMovimientoAdministrativo = new MovimientoAdministrativo(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getString("descripcion"),
                        resultSet.getBoolean("ingreso"),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        null,
                        new ArrayList<>());
                lstMovimientosAdministrativos.add(objMovimientoAdministrativo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstMovimientosAdministrativos.isEmpty()) {
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOEmpleado objDAOEmpleado = DAOEmpleado.getInstance();
            DAOPago objDAOPago = DAOPago.getInstance();

            int size = lstMovimientosAdministrativos.size();
            for (int i = 0; i < size; i++) {
                lstMovimientosAdministrativos.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstMovimientosAdministrativos.get(i).setObjEmpleado(objDAOEmpleado.getEmpleado(lstIdsEmpleados.get(i)));
                lstMovimientosAdministrativos.get(i).setLstPagos(objDAOPago.getAllPagos(lstMovimientosAdministrativos.get(i)));
            }
        }

        return lstMovimientosAdministrativos;
    }

    @Override
    public List<MovimientoAdministrativo> getAllMovimientosAdministrativosPagados(Timestamp fechaDesde, Timestamp fechaHasta, boolean ingreso) {
        List<MovimientoAdministrativo> lstMovimientosAdministrativos = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        List<Long> lstIdsEmpleados = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT ma.id, ma.monto, ma.descripcion, ma.ingreso, ma.habilitado, ma.pagado, ma.fecharegistro, ma.fechamodificacion, ma.estado, ma.idusuario, ma.idempleado "
                + "FROM movimientosadministrativos ma "
                + "WHERE ma.habilitado is true AND ma.pagado is true AND ma.ingreso = ? AND ma.fechamodificacion BETWEEN ? AND ?;")) {
            preparedStatement.setBoolean(1, ingreso);
            preparedStatement.setTimestamp(2, fechaDesde);
            preparedStatement.setTimestamp(3, fechaHasta);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                lstIdsEmpleados.add(resultSet.getLong("idempleado"));
                MovimientoAdministrativo objMovimientoAdministrativo = new MovimientoAdministrativo(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getString("descripcion"),
                        resultSet.getBoolean("ingreso"),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        null,
                        new ArrayList<>());
                lstMovimientosAdministrativos.add(objMovimientoAdministrativo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstMovimientosAdministrativos.isEmpty()) {
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOEmpleado objDAOEmpleado = DAOEmpleado.getInstance();
            DAOPago objDAOPago = DAOPago.getInstance();

            int size = lstMovimientosAdministrativos.size();
            for (int i = 0; i < size; i++) {
                lstMovimientosAdministrativos.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstMovimientosAdministrativos.get(i).setObjEmpleado(objDAOEmpleado.getEmpleado(lstIdsEmpleados.get(i)));
                lstMovimientosAdministrativos.get(i).setLstPagos(objDAOPago.getAllPagos(lstMovimientosAdministrativos.get(i)));
            }
        }

        return lstMovimientosAdministrativos;
    }

    @Override
    public boolean insert(MovimientoAdministrativo objMovimientoAdministrativo) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO movimientosadministrativos(monto, descripcion, ingreso, habilitado, pagado, fecharegistro, estado, idusuario, idempleado) "
                + "VALUES(?, ?, ?, ?, ?, now(), ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setBigDecimal(1, objMovimientoAdministrativo.getMonto());
            preparedStatement.setString(2, objMovimientoAdministrativo.getDescripcion());
            preparedStatement.setBoolean(3, objMovimientoAdministrativo.isIngreso());
            preparedStatement.setBoolean(4, objMovimientoAdministrativo.isHabilitado());
            preparedStatement.setBoolean(5, objMovimientoAdministrativo.isPagado());
            preparedStatement.setBoolean(6, objMovimientoAdministrativo.isEstado());
            preparedStatement.setLong(7, objMovimientoAdministrativo.getObjUsuario().getId());
            preparedStatement.setLong(8, objMovimientoAdministrativo.getObjEmpleado().getId());

            preparedStatement.execute();

            ResultSet keySet = preparedStatement.getGeneratedKeys();

            while (keySet.next()) {
                objMovimientoAdministrativo.setId(keySet.getLong(1));
            }

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean update(MovimientoAdministrativo objMovimientoAdministrativo) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE movimientosadministrativos SET monto = ?, descripcion = ?, ingreso = ?, habilitado = ?, pagado = ?, fechamodificacion = now(), estado = ?, idusuario = ?, idempleado = ? "
                + "WHERE id = ?;")) {
            preparedStatement.setBigDecimal(1, objMovimientoAdministrativo.getMonto());
            preparedStatement.setString(2, objMovimientoAdministrativo.getDescripcion());
            preparedStatement.setBoolean(3, objMovimientoAdministrativo.isIngreso());
            preparedStatement.setBoolean(4, objMovimientoAdministrativo.isHabilitado());
            preparedStatement.setBoolean(5, objMovimientoAdministrativo.isPagado());
            preparedStatement.setBoolean(6, objMovimientoAdministrativo.isEstado());
            preparedStatement.setLong(7, objMovimientoAdministrativo.getObjUsuario().getId());
            preparedStatement.setLong(8, objMovimientoAdministrativo.getObjEmpleado().getId());
            preparedStatement.setLong(9, objMovimientoAdministrativo.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean insertDetallePagoMovimientoAdministrativo(Pago objPago, MovimientoAdministrativo objMovimientoAdministrativo) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO detallepagomovimientoadministrativo(monto, fecharegistro, estado, idmovimientoadministrativo, idpago) "
                + "VALUES(?, now(), ?, ?, ?);")) {
            preparedStatement.setBigDecimal(1, objMovimientoAdministrativo.getMonto());
            preparedStatement.setBoolean(2, objMovimientoAdministrativo.isEstado());
            preparedStatement.setLong(3, objMovimientoAdministrativo.getId());
            preparedStatement.setLong(4, objPago.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean delete(MovimientoAdministrativo objMovimientoAdministrativo) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE movimientosadministrativos SET estado = false WHERE id = ?;")) {
            preparedStatement.setLong(1, objMovimientoAdministrativo.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
