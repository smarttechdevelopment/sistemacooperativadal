package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.TipoPrestamo;
import com.smartech.sistemacooperativa.interfaces.IDAOTipoPrestamo;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAOTipoPrestamo implements IDAOTipoPrestamo{

    private static DAOTipoPrestamo instance = null;
    private Map<Integer, TipoPrestamo> cache;

    private DAOTipoPrestamo() {
        this.cache = new HashMap<>();
    }

    public static DAOTipoPrestamo getInstance() {
        if (instance == null) {
            instance = new DAOTipoPrestamo();
        }

        return instance;
    }

    @Override
    public TipoPrestamo getTipoPrestamo(int id) {
        TipoPrestamo objTipoPrestamo = this.cache.get(id);
        
        if(objTipoPrestamo == null){
            this.cache.clear();
            this.getAllTiposPrestamo();
            objTipoPrestamo = this.cache.get(id);
        }
        
        return objTipoPrestamo;
    }

    @Override
    public List<TipoPrestamo> getAllTiposPrestamo() {
        List<TipoPrestamo> lstTiposPrestamo;
        
        if(this.cache.isEmpty()){
            Map<Integer, Integer> mapIdsMonedas = new HashMap<>();
            try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT tp.id, tp.nombre, tp.idmoneda "
                    + "FROM tiposprestamo tp;")){
                
                ResultSet resultSet = preparedStatement.executeQuery();
                
                while(resultSet.next()){
                    TipoPrestamo objTipoPrestamo = new TipoPrestamo(
                            resultSet.getInt("id"),
                            resultSet.getString("nombre"),
                            null);
                    this.cache.put(objTipoPrestamo.getId(), objTipoPrestamo);
                    mapIdsMonedas.put(objTipoPrestamo.getId(), resultSet.getInt("idmoneda"));
                }
            }catch(Exception e){
                e.printStackTrace();
            }
            
            if(!this.cache.isEmpty()){
                DAOMoneda objDAOMoneda = DAOMoneda.getInstance();
                for(Map.Entry<Integer, TipoPrestamo> entry : this.cache.entrySet()){
                    TipoPrestamo objTipoPrestamo = entry.getValue();
                    objTipoPrestamo.setObjMoneda(objDAOMoneda.getMoneda(mapIdsMonedas.get(objTipoPrestamo.getId())));
                }
            }
        }
        
        Collection collection = this.cache.values();
        
        lstTiposPrestamo = new ArrayList<>(collection);
        
        return lstTiposPrestamo;
    }
}
