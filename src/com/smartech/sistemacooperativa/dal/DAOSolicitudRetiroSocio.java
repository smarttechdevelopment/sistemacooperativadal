package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.DetalleDocumentoSolicitud;
import com.smartech.sistemacooperativa.dominio.Retiro;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.Solicitud;
import com.smartech.sistemacooperativa.dominio.SolicitudRetiroSocio;
import com.smartech.sistemacooperativa.interfaces.IDAOSolicitudRetiroSocio;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOSolicitudRetiroSocio implements IDAOSolicitudRetiroSocio {

    private static DAOSolicitudRetiroSocio instance = null;

    private DAOSolicitudRetiroSocio() {
    }

    public static DAOSolicitudRetiroSocio getInstance() {
        if (instance == null) {
            instance = new DAOSolicitudRetiroSocio();
        }

        return instance;
    }

    @Override
    public long getUltimoCodigo(int year) {
        long ultimoCodigo = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT COALESCE(MAX(substring(si.codigo from 1 for 4)::int), 1) ultimocodigo "
                + "FROM solicitudesretirosocio si "
                + "WHERE substring(si.codigo from 6 for 4) = ?")) {
            preparedStatement.setString(1, String.valueOf(year));

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                ultimoCodigo = resultSet.getInt("ultimocodigo");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ultimoCodigo;
    }

    @Override
    public Solicitud getSolicitud(long id) {
        SolicitudRetiroSocio objSolicitudRetiroSocio = null;
        Long idSocio = new Long(0);
        Long idUsuario = new Long(0);
        int idTipoSolicitud = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT id,estadosocio,codigo,aprobado,estado,fecharegistro,fechamodificacion,idusuario,idtiposolicitud,idsocio "
                + "FROM solicitudesretirosocio "
                + "WHERE estado is true and id = ?"
                + "ORDER BY id desc "
                + "LIMIT 1; ")) {

            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idSocio = resultSet.getLong("idsocio");
                idUsuario = resultSet.getLong("idusuario");
                idTipoSolicitud = resultSet.getInt("idtiposolicitud");

                objSolicitudRetiroSocio = new SolicitudRetiroSocio(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        resultSet.getBoolean("aprobado"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getString("estadosocio"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>(),
                        new ArrayList<>());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objSolicitudRetiroSocio != null) {
            objSolicitudRetiroSocio.setObjSocio(DAOSocio.getInstance().getSocio(idSocio, true));
            objSolicitudRetiroSocio.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objSolicitudRetiroSocio.setObjTipoSolicitud(DAOTipoSolicitud.getInstance().getTipoSolicitud(idTipoSolicitud));
            objSolicitudRetiroSocio.setLstDetalleDocumentoSolicitud(DAODetalleDocumentoSolicitud.getInstance().getAllDetalleDocumentoSolicitud(objSolicitudRetiroSocio));
            objSolicitudRetiroSocio.setLstAprobaciones(DAOAprobacion.getInstance().getAllAprobaciones(objSolicitudRetiroSocio));
            objSolicitudRetiroSocio.setLstDetalleSolicitudRetiroSocios(DAODetalleSolicitudRetiroSocio.getInstance().getAllDetalleSolicitudRetiroSocio(objSolicitudRetiroSocio));

        }

        return objSolicitudRetiroSocio;
    }

    @Override
    public SolicitudRetiroSocio getLastSolicitudRetiroSocio(Socio objSocio) {
        SolicitudRetiroSocio objSolicitudRetiroSocio = null;
        Long idSocio = new Long(0);
        Long idUsuario = new Long(0);
        int idTipoSolicitud = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT id,estadosocio,codigo,aprobado,estado,fecharegistro,fechamodificacion,idusuario,idtiposolicitud,idsocio "
                + "FROM solicitudesretirosocio "
                + "WHERE estado is true and idsocio = ?"
                + "ORDER BY id desc "
                + "LIMIT 1; ")) {

            preparedStatement.setLong(1, objSocio.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idSocio = resultSet.getLong("idsocio");
                idUsuario = resultSet.getLong("idusuario");
                idTipoSolicitud = resultSet.getInt("idtiposolicitud");

                objSolicitudRetiroSocio = new SolicitudRetiroSocio(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        resultSet.getBoolean("aprobado"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getString("estadosocio"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>(),
                        new ArrayList<>());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objSolicitudRetiroSocio != null) {
            objSolicitudRetiroSocio.setObjSocio(DAOSocio.getInstance().getSocio(idSocio, true));
            objSolicitudRetiroSocio.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objSolicitudRetiroSocio.setObjTipoSolicitud(DAOTipoSolicitud.getInstance().getTipoSolicitud(idTipoSolicitud));
            objSolicitudRetiroSocio.setLstDetalleDocumentoSolicitud(DAODetalleDocumentoSolicitud.getInstance().getAllDetalleDocumentoSolicitud(objSolicitudRetiroSocio));
            objSolicitudRetiroSocio.setLstAprobaciones(DAOAprobacion.getInstance().getAllAprobaciones(objSolicitudRetiroSocio));
            objSolicitudRetiroSocio.setLstDetalleSolicitudRetiroSocios(DAODetalleSolicitudRetiroSocio.getInstance().getAllDetalleSolicitudRetiroSocio(objSolicitudRetiroSocio));

        }

        return objSolicitudRetiroSocio;
    }

    @Override
    public List<SolicitudRetiroSocio> getAllSolicitudesRetiroAprobadas() {
        List<SolicitudRetiroSocio> lstSolicitudesRetiroSocio = new ArrayList<>();
        List<Long> lstIdsSocio = new ArrayList<>();
        List<Long> lstIdsUsuario = new ArrayList<>();
        List<Integer> lstIdsTipoSolicitud = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT id,estadosocio,codigo,aprobado,estado,fecharegistro,fechamodificacion,idusuario,idtiposolicitud,idsocio "
                + "FROM solicitudesretirosocio "
                + "WHERE estado is true and aprobado is true "
                + "ORDER BY id desc ")) {

            ResultSet resultSet = preparedStatement.executeQuery();
            SolicitudRetiroSocio objSolicitudRetiroSocio;
            while (resultSet.next()) {
                lstIdsSocio.add(resultSet.getLong("idsocio"));
                lstIdsUsuario.add(resultSet.getLong("idusuario"));
                lstIdsTipoSolicitud.add(resultSet.getInt("idtiposolicitud"));

                objSolicitudRetiroSocio = new SolicitudRetiroSocio(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        resultSet.getBoolean("aprobado"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getString("estadosocio"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>(),
                        new ArrayList<>());
                
                lstSolicitudesRetiroSocio.add(objSolicitudRetiroSocio);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstSolicitudesRetiroSocio.isEmpty()) {
            
            for(int i = 0; i < lstSolicitudesRetiroSocio.size(); i ++){
                lstSolicitudesRetiroSocio.get(i).setObjSocio(DAOSocio.getInstance().getSocio(lstIdsSocio.get(i), true));
                lstSolicitudesRetiroSocio.get(i).setObjUsuario(DAOUsuario.getInstance().getUsuario(lstIdsUsuario.get(i)));
                lstSolicitudesRetiroSocio.get(i).setObjTipoSolicitud(DAOTipoSolicitud.getInstance().getTipoSolicitud(lstIdsTipoSolicitud.get(i)));
                lstSolicitudesRetiroSocio.get(i).setLstDetalleDocumentoSolicitud(DAODetalleDocumentoSolicitud.getInstance().getAllDetalleDocumentoSolicitud(lstSolicitudesRetiroSocio.get(i)));
                lstSolicitudesRetiroSocio.get(i).setLstAprobaciones(DAOAprobacion.getInstance().getAllAprobaciones(lstSolicitudesRetiroSocio.get(i)));
                //lstSolicitudesRetiroSocio.get(i).setLstDetalleSolicitudRetiroSocios(DAODetalleSolicitudRetiroSocio.getInstance().getAllDetalleSolicitudRetiroSocio(lstSolicitudesRetiroSocio.get(i)));
            }
        }
        
        return lstSolicitudesRetiroSocio;
    }

    @Override
    public boolean insert(Solicitud objSolicitud) {
        boolean result = false;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO solicitudesretirosocio "
                + "(estadosocio,codigo,aprobado,estado,fecharegistro,fechamodificacion,idusuario,idtiposolicitud,idsocio) "
                + "VALUES (?,?,?,?,?,?,?,?,?); ", PreparedStatement.RETURN_GENERATED_KEYS)) {

            SolicitudRetiroSocio objSolicitudRetiroSocio = (SolicitudRetiroSocio) objSolicitud;

            preparedStatement.setString(1, objSolicitudRetiroSocio.getEstadoSocio());
            preparedStatement.setString(2, objSolicitudRetiroSocio.getCodigo());
            preparedStatement.setBoolean(3, objSolicitudRetiroSocio.isAprobado());
            preparedStatement.setBoolean(4, objSolicitudRetiroSocio.isEstado());
            preparedStatement.setTimestamp(5, objSolicitudRetiroSocio.getFechaRegistro());
            if (objSolicitudRetiroSocio.getFechaModificacion() != null) {
                preparedStatement.setTimestamp(6, objSolicitudRetiroSocio.getFechaModificacion());
            } else {
                preparedStatement.setNull(6, Types.TIMESTAMP);
            }
            preparedStatement.setLong(7, objSolicitudRetiroSocio.getObjUsuario().getId());
            preparedStatement.setInt(8, objSolicitudRetiroSocio.getObjTipoSolicitud().getId());
            preparedStatement.setLong(9, objSolicitudRetiroSocio.getObjSocio().getId());

            preparedStatement.execute();
            ResultSet keySet = preparedStatement.getGeneratedKeys();
            while (keySet.next()) {
                objSolicitud.setId(keySet.getLong(1));
            }

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean update(Solicitud objSolicitud) {
        boolean result = false;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE solicitudesretirosocio "
                + "SET estadosocio = ?,"
                + "codigo = ?,"
                + "aprobado = ?,"
                + "estado = ?,"
                + "fecharegistro = ?,"
                + "fechamodificacion = ?,"
                + "idusuario = ?,"
                + "idtiposolicitud = ?,"
                + "idsocio = ? "
                + "WHERE id = ?;")) {

            SolicitudRetiroSocio objSolicitudRetiroSocio = (SolicitudRetiroSocio) objSolicitud;

            preparedStatement.setString(1, objSolicitudRetiroSocio.getEstadoSocio());
            preparedStatement.setString(2, objSolicitudRetiroSocio.getCodigo());
            preparedStatement.setBoolean(3, objSolicitudRetiroSocio.isAprobado());
            preparedStatement.setBoolean(4, objSolicitudRetiroSocio.isEstado());
            preparedStatement.setTimestamp(5, objSolicitudRetiroSocio.getFechaRegistro());
            preparedStatement.setTimestamp(6, objSolicitudRetiroSocio.getFechaModificacion());
            preparedStatement.setLong(7, objSolicitudRetiroSocio.getObjUsuario().getId());
            preparedStatement.setInt(8, objSolicitudRetiroSocio.getObjTipoSolicitud().getId());
            preparedStatement.setLong(9, objSolicitudRetiroSocio.getObjSocio().getId());
            preparedStatement.setLong(10, objSolicitudRetiroSocio.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean physicalDelete(Solicitud objSolicitud) {
        boolean result = false;
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE solicitudesretirosocio WHERE id = ? ")) {

            SolicitudRetiroSocio objSolicitudRetiroSocio = (SolicitudRetiroSocio) objSolicitud;

            preparedStatement.setLong(1, objSolicitudRetiroSocio.getId());
            preparedStatement.executeQuery();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
