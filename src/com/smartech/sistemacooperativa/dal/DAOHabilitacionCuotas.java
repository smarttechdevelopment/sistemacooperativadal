package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Cuota;
import com.smartech.sistemacooperativa.dominio.HabilitacionCuotas;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.interfaces.IDAOHabilitacionCuotas;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOHabilitacionCuotas implements IDAOHabilitacionCuotas {

    private static DAOHabilitacionCuotas instance = null;

    private DAOHabilitacionCuotas() {
    }

    public static DAOHabilitacionCuotas getInstance() {
        if (instance == null) {
            instance = new DAOHabilitacionCuotas();
        }

        return instance;
    }

    @Override
    public HabilitacionCuotas getHabilitacionCuotas(long id) {
        HabilitacionCuotas objHabilitacionCuotas = null;
        long idUsuario = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT hc.id, hc.monto, hc.habilitado, hc.pagado, hc.fecharegistro, hc.fechamodificacion, hc.estado, hc.idusuario "
                + "FROM habilitacionescuotas hc "
                + "WHERE hc.id = ?;")) {
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idUsuario = resultSet.getLong("idusuario");
                objHabilitacionCuotas = new HabilitacionCuotas(
                        id,
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        new ArrayList<>(),
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objHabilitacionCuotas != null) {
            objHabilitacionCuotas.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objHabilitacionCuotas.setLstCuotas(DAOCuota.getInstance().getAllCuotas(objHabilitacionCuotas));
            objHabilitacionCuotas.setLstPagos(DAOPago.getInstance().getAllPagos(objHabilitacionCuotas));
        }

        return objHabilitacionCuotas;
    }

    @Override
    public HabilitacionCuotas getHabilitacionCuotas(Pago objPago) {
        HabilitacionCuotas objHabilitacionCuotas = null;
        long idUsuario = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT hc.id, hc.monto, hc.habilitado, hc.pagado, hc.fecharegistro, hc.fechamodificacion, hc.estado, hc.idusuario "
                + "FROM habilitacionescuotas hc "
                + "WHERE hc.id IN (SELECT dhcp.idhabilitacioncuota FROM detallehabilitacioncuotapago dhcp WHERE dhcp.idpago = ?);")) {
            preparedStatement.setLong(1, objPago.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idUsuario = resultSet.getLong("idusuario");
                objHabilitacionCuotas = new HabilitacionCuotas(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        new ArrayList<>(),
                        new ArrayList<>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objHabilitacionCuotas != null) {
            objHabilitacionCuotas.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objHabilitacionCuotas.setLstCuotas(DAOCuota.getInstance().getAllCuotas(objHabilitacionCuotas));
            objHabilitacionCuotas.setLstPagos(DAOPago.getInstance().getAllPagos(objHabilitacionCuotas));
        }

        return objHabilitacionCuotas;
    }

    @Override
    public List<HabilitacionCuotas> getAllHabilitacionesCuotasHabilitados(Timestamp fechaDesde, Timestamp fechaHasta) {
        List<HabilitacionCuotas> lstHabilitacionesCuotas = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT hc.id, hc.monto, hc.habilitado, hc.pagado, hc.fecharegistro, hc.fechamodificacion, hc.estado, hc.idusuario "
                + "FROM habilitacionescuotas hc "
                + "WHERE hc.estado is true AND hc.habilitado is true AND hc.pagado is false AND hc.fecharegistro BETWEEN ? AND ?;")) {
            preparedStatement.setTimestamp(1, fechaDesde);
            preparedStatement.setTimestamp(2, fechaHasta);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                HabilitacionCuotas objHabilitacionCuotas = new HabilitacionCuotas(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        new ArrayList<>(),
                        new ArrayList<>());
                lstHabilitacionesCuotas.add(objHabilitacionCuotas);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstHabilitacionesCuotas.isEmpty()) {
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOCuota objDAOCuota = DAOCuota.getInstance();
            DAOPago objDAOPago = DAOPago.getInstance();

            int size = lstHabilitacionesCuotas.size();
            for (int i = 0; i < size; i++) {
                lstHabilitacionesCuotas.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstHabilitacionesCuotas.get(i).setLstCuotas(objDAOCuota.getAllCuotas(lstHabilitacionesCuotas.get(i)));
                lstHabilitacionesCuotas.get(i).setLstPagos(objDAOPago.getAllPagos(lstHabilitacionesCuotas.get(i)));
            }
        }

        return lstHabilitacionesCuotas;
    }

    @Override
    public List<HabilitacionCuotas> getAllHabilitacionesCuotasPagados(Timestamp fechaDesde, Timestamp fechaHasta) {
        List<HabilitacionCuotas> lstHabilitacionesCuotas = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT hc.id, hc.monto, hc.habilitado, hc.pagado, hc.fecharegistro, hc.fechamodificacion, hc.estado, hc.idusuario "
                + "FROM habilitacionescuotas hc "
                + "WHERE hc.habilitado is true AND hc.pagado is true AND hc.fechamodificacion BETWEEN ? AND ?;")) {
            preparedStatement.setTimestamp(1, fechaDesde);
            preparedStatement.setTimestamp(2, fechaHasta);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                HabilitacionCuotas objHabilitacionCuotas = new HabilitacionCuotas(
                        resultSet.getLong("id"),
                        resultSet.getBigDecimal("monto"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("estado"),
                        null,
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"),
                        new ArrayList<>(),
                        new ArrayList<>());
                lstHabilitacionesCuotas.add(objHabilitacionCuotas);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstHabilitacionesCuotas.isEmpty()) {
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOCuota objDAOCuota = DAOCuota.getInstance();
            DAOPago objDAOPago = DAOPago.getInstance();

            int size = lstHabilitacionesCuotas.size();
            for (int i = 0; i < size; i++) {
                lstHabilitacionesCuotas.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstHabilitacionesCuotas.get(i).setLstCuotas(objDAOCuota.getAllCuotas(lstHabilitacionesCuotas.get(i)));
                lstHabilitacionesCuotas.get(i).setLstPagos(objDAOPago.getAllPagos(lstHabilitacionesCuotas.get(i)));
            }
        }

        return lstHabilitacionesCuotas;
    }

    @Override
    public boolean insert(HabilitacionCuotas objHabilitacionCuotas) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO habilitacionescuotas(monto, habilitado, pagado, fecharegistro, estado, idusuario) "
                + "VALUES(?, ?, ?, now(), ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setBigDecimal(1, objHabilitacionCuotas.getMonto());
            preparedStatement.setBoolean(2, objHabilitacionCuotas.isHabilitado());
            preparedStatement.setBoolean(3, objHabilitacionCuotas.isPagado());
            preparedStatement.setBoolean(4, objHabilitacionCuotas.isEstado());
            preparedStatement.setLong(5, objHabilitacionCuotas.getObjUsuario().getId());

            preparedStatement.execute();

            ResultSet keySet = preparedStatement.getGeneratedKeys();

            while (keySet.next()) {
                objHabilitacionCuotas.setId(keySet.getLong(1));
            }

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean update(HabilitacionCuotas objHabilitacionCuotas) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE habilitacionescuotas SET monto = ?, habilitado = ?, pagado = ?, fechamodificacion = now(), estado = ?, idusuario = ? "
                + "WHERE id = ?;")) {
            preparedStatement.setBigDecimal(1, objHabilitacionCuotas.getMonto());
            preparedStatement.setBoolean(2, objHabilitacionCuotas.isHabilitado());
            preparedStatement.setBoolean(3, objHabilitacionCuotas.isPagado());
            preparedStatement.setBoolean(4, objHabilitacionCuotas.isEstado());
            preparedStatement.setLong(5, objHabilitacionCuotas.getObjUsuario().getId());
            preparedStatement.setLong(6, objHabilitacionCuotas.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean insertDetalleHabilitacionCuota(HabilitacionCuotas objHabilitacionCuotas) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO detallehabilitacioncuota (fecharegistro, estado, idcuota, idhabilitacioncuota) "
                + "VALUES(now(), ?, ?, ?);")) {
            for (Cuota objCuota : objHabilitacionCuotas.getLstCuotas()) {
                preparedStatement.setBoolean(1, objHabilitacionCuotas.isEstado());
                preparedStatement.setLong(2, objCuota.getId());
                preparedStatement.setLong(3, objHabilitacionCuotas.getId());
                
                preparedStatement.execute();
            }
            
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean insertDetalleHabilitacionCuotaPago(Pago objPago, HabilitacionCuotas objHabilitacionCuotas) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO detallehabilitacioncuotapago(monto, fecharegistro, estado, idhabilitacioncuota, idpago) "
                + "VALUES(?, now(), ?, ?, ?);")) {
            preparedStatement.setBigDecimal(1, objHabilitacionCuotas.getMonto());
            preparedStatement.setBoolean(2, objHabilitacionCuotas.isEstado());
            preparedStatement.setLong(3, objHabilitacionCuotas.getId());
            preparedStatement.setLong(4, objPago.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean delete(HabilitacionCuotas objHabilitacionCuotas) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE habilitacionescuotas SET estado = false WHERE id = ?;")) {
            preparedStatement.setLong(1, objHabilitacionCuotas.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
