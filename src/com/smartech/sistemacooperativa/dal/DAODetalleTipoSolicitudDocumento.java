package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.DetalleTipoSolicitudDocumento;
import com.smartech.sistemacooperativa.dominio.Establecimiento;
import com.smartech.sistemacooperativa.dominio.TipoSolicitud;
import com.smartech.sistemacooperativa.interfaces.IDAODetalleTipoSolicitudDocumento;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAODetalleTipoSolicitudDocumento implements IDAODetalleTipoSolicitudDocumento{
    
    private static DAODetalleTipoSolicitudDocumento instance = null;
    
    private DAODetalleTipoSolicitudDocumento(){
    }
    
    public static DAODetalleTipoSolicitudDocumento getInstance(){
        if(instance == null){
            instance = new DAODetalleTipoSolicitudDocumento();
        }
        
        return instance;
    }

    @Override
    public List<DetalleTipoSolicitudDocumento> getAllDetalleTipoSolicitudDocumento(TipoSolicitud objTipoSolicitud) {
        List<DetalleTipoSolicitudDocumento> lstDetalleTipoSolicitudDocumento = new ArrayList<>();
        List<Integer> lstIdsDocumentos = new ArrayList<>();
        List<Integer> lstIdsEstablecimientos = new ArrayList<>();
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT dtsd.id, dtsd.estado, dtsd.activo, dtsd.iddocumento, dtsd.idtiposolicitud, dtsd.idestablecimiento "
                + "FROM detalletiposolicituddocumento dtsd "
                + "WHERE dtsd.idtiposolicitud = ? AND dtsd.estado is true;")){
            preparedStatement.setInt(1, objTipoSolicitud.getId());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                lstIdsDocumentos.add(resultSet.getInt("iddocumento"));
                lstIdsEstablecimientos.add(resultSet.getInt("idestablecimiento"));
                DetalleTipoSolicitudDocumento objDetalleTipoSolicitudDocumento = new DetalleTipoSolicitudDocumento(
                        resultSet.getInt("id"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("activo"),
                        objTipoSolicitud,
                        null,
                        null);
                lstDetalleTipoSolicitudDocumento.add(objDetalleTipoSolicitudDocumento);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        if(!lstDetalleTipoSolicitudDocumento.isEmpty()){
            DAODocumento objDAODocumento = DAODocumento.getInstance();
            DAOEstablecimiento objDAOEstablecimiento = DAOEstablecimiento.getInstance();
            
            int size = lstDetalleTipoSolicitudDocumento.size();
            for(int i = 0; i < size; i++){
                lstDetalleTipoSolicitudDocumento.get(i).setObjDocumento(objDAODocumento.getDocumento(lstIdsDocumentos.get(i)));
                lstDetalleTipoSolicitudDocumento.get(i).setObjEstablecimiento(objDAOEstablecimiento.getEstablecimiento(lstIdsEstablecimientos.get(i)));
            }
        }
        
        return lstDetalleTipoSolicitudDocumento;
    }

    @Override
    public List<DetalleTipoSolicitudDocumento> getAllDetalleTipoSolicitudDocumento(TipoSolicitud objTipoSolicitud, Establecimiento objEstablecimiento) {
        List<DetalleTipoSolicitudDocumento> lstDetalleTipoSolicitudDocumento = new ArrayList<>();
        List<Integer> lstIdsDocumentos = new ArrayList<>();
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT dtsd.id, dtsd.estado, dtsd.activo, dtsd.iddocumento, dtsd.idtiposolicitud, dtsd.idestablecimiento "
                + "FROM detalletiposolicituddocumento dtsd "
                + "WHERE dtsd.idtiposolicitud = ? AND dtsd.idestablecimiento = ? AND dtsd.estado is true;")){
            preparedStatement.setInt(1, objTipoSolicitud.getId());
            preparedStatement.setInt(2, objEstablecimiento.getId());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                lstIdsDocumentos.add(resultSet.getInt("iddocumento"));
                DetalleTipoSolicitudDocumento objDetalleTipoSolicitudDocumento = new DetalleTipoSolicitudDocumento(
                        resultSet.getInt("id"),
                        resultSet.getBoolean("estado"),
                        resultSet.getBoolean("activo"),
                        objTipoSolicitud,
                        null,
                        objEstablecimiento);
                lstDetalleTipoSolicitudDocumento.add(objDetalleTipoSolicitudDocumento);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        if(!lstDetalleTipoSolicitudDocumento.isEmpty()){
            DAODocumento objDAODocumento = DAODocumento.getInstance();
            
            int size = lstDetalleTipoSolicitudDocumento.size();
            for(int i = 0; i < size; i++){
                lstDetalleTipoSolicitudDocumento.get(i).setObjDocumento(objDAODocumento.getDocumento(lstIdsDocumentos.get(i)));
            }
        }
        
        return lstDetalleTipoSolicitudDocumento;
    }
}
