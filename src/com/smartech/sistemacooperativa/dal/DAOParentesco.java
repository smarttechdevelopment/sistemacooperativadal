/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Parentesco;
import com.smartech.sistemacooperativa.interfaces.IDAOParentesco;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAOParentesco implements IDAOParentesco {

    private static DAOParentesco instance = null;
    private Map<Integer, Parentesco> cache;

    private DAOParentesco() {
        this.cache = new HashMap<>();
    }

    public static DAOParentesco getInstance() {
        if (instance == null) {
            instance = new DAOParentesco();
        }
        return instance;
    }

    @Override
    public List<Parentesco> getAllParentescos() {
        List<Parentesco> lstParentescos;

        if (this.cache.isEmpty()) {
            try (PreparedStatement pst = ConnectionManager.getConnection().prepareStatement("SELECT id, nombre FROM parentescos ORDER BY id;");){
               
                ResultSet rs = pst.executeQuery();
                while(rs.next()){
                    Parentesco objParentesco = new Parentesco(rs.getInt("id"), rs.getString("nombre"));
                    this.cache.put(objParentesco.getId(), objParentesco);
                }
                
                
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Collection<Parentesco> collection = this.cache.values();
        lstParentescos = new ArrayList<>(collection);

        return lstParentescos;
    }

    @Override
    public Parentesco getParentesco(int id) {
        Parentesco objParentesco = this.cache.get(id);

        if (objParentesco == null) {
            this.cache.clear();
            this.getAllParentescos();
            objParentesco = this.cache.get(id);
        }

        return objParentesco;
    }

}
