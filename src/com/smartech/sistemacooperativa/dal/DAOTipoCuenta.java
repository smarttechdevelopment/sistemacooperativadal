package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.TipoCuenta;
import com.smartech.sistemacooperativa.interfaces.IDAOTipoCuenta;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAOTipoCuenta implements IDAOTipoCuenta {

    private static DAOTipoCuenta instance = null;
    private Map<Integer, TipoCuenta> cache;

    private DAOTipoCuenta() {
        this.cache = new HashMap<>();
    }

    public static DAOTipoCuenta getInstance() {
        if (instance == null) {
            instance = new DAOTipoCuenta();
        }

        return instance;
    }

    @Override
    public TipoCuenta getTipoCuenta(int id) {
        TipoCuenta objTipoCuenta = this.cache.get(id);

        if (objTipoCuenta == null) {
            this.cache.clear();
            this.getAllTiposCuenta();
            objTipoCuenta = this.cache.get(id);
        }

        return objTipoCuenta;
    }

    @Override
    public List<TipoCuenta> getAllTiposCuenta() {
        List<TipoCuenta> lstTiposCuenta;
        Map<Integer, Integer> mapIdsMonedas = new HashMap<>();

        if (this.cache.isEmpty()) {
            try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT tc.id, tc.nombre, tc.idmoneda, tc.mancomunada, tc.registromanual "
                    + "FROM tiposcuenta tc ;")) {
                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    if (this.cache.get(resultSet.getInt("id")) == null) {
                        TipoCuenta objTipoCuenta = new TipoCuenta(
                                resultSet.getInt("id"),
                                resultSet.getString("nombre"),
                                null,
                                resultSet.getBoolean("mancomunada"),
                                resultSet.getBoolean("registromanual"));
                        this.cache.put(objTipoCuenta.getId(), objTipoCuenta);
                    }
                    mapIdsMonedas.put(this.cache.get(resultSet.getInt("id")).getId(), resultSet.getInt("idmoneda"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!this.cache.isEmpty()) {
                DAOMoneda objDAOMoneda = DAOMoneda.getInstance();

                for (Map.Entry<Integer, Integer> monedasEntry : mapIdsMonedas.entrySet()) {
                    TipoCuenta objTipoCuenta = this.cache.get(monedasEntry.getKey());
                    objTipoCuenta.setObjMoneda(objDAOMoneda.getMoneda(monedasEntry.getValue()));
                }
            }
        }

        Collection collection = this.cache.values();

        lstTiposCuenta = new ArrayList<>(collection);

        return lstTiposCuenta;
    }
}
