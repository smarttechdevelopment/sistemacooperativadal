package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Establecimiento;
import com.smartech.sistemacooperativa.dominio.TipoSolicitud;
import com.smartech.sistemacooperativa.interfaces.IDAOTipoSolicitud;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAOTipoSolicitud implements IDAOTipoSolicitud {

    private static DAOTipoSolicitud instance = null;
    private Map<Integer, TipoSolicitud> cache;

    private DAOTipoSolicitud() {
        this.cache = new HashMap<>();
    }

    public static DAOTipoSolicitud getInstance() {
        if (instance == null) {
            instance = new DAOTipoSolicitud();
        }

        return instance;
    }

    @Override
    public TipoSolicitud getTipoSolicitud(int id) {
        TipoSolicitud objTipoSolicitud = this.cache.get(id);

        if (objTipoSolicitud == null) {
            this.cache.clear();
            this.getAllTiposSolicitud();
            objTipoSolicitud = this.cache.get(id);
        }

        return objTipoSolicitud;
    }

    @Override
    public TipoSolicitud getTipoSolicitud(int id, Establecimiento objEstablecimiento) {
        TipoSolicitud objTipoSolicitud = this.getTipoSolicitud(id);
        if (objTipoSolicitud != null) {
            objTipoSolicitud.setLstDetalleTipoSolicitudDocumento(DAODetalleTipoSolicitudDocumento.getInstance().getAllDetalleTipoSolicitudDocumento(objTipoSolicitud, objEstablecimiento));
        }
        return objTipoSolicitud;
    }

    @Override
    public List<TipoSolicitud> getAllTiposSolicitud(Establecimiento objEstablecimiento) {
        if (this.cache.isEmpty()) {
            this.getAllTiposSolicitud();
        }

        List<TipoSolicitud> lstTiposSolicitud = new ArrayList<>(this.cache.values());

        int size = lstTiposSolicitud.size();
        DAODetalleTipoSolicitudDocumento objDAODetalleTipoSolicitudDocumento = DAODetalleTipoSolicitudDocumento.getInstance();
        for (int i = 0; i < size; i++) {
            lstTiposSolicitud.get(i).setLstDetalleTipoSolicitudDocumento(objDAODetalleTipoSolicitudDocumento.getAllDetalleTipoSolicitudDocumento(lstTiposSolicitud.get(i), objEstablecimiento));
        }

        return lstTiposSolicitud;
    }

    @Override
    public List<TipoSolicitud> getAllTiposSolicitud() {
        List<TipoSolicitud> lstTiposSolicitud;
        Map<Integer, Integer> mapDetalle = new HashMap<>();

        if (this.cache.isEmpty()) {
            try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT ts.id, ts.nombre, ts.estado, dtstu.idtipousuario "
                    + "FROM tipossolicitud ts INNER JOIN detalletiposolicitudtipousuario dtstu ON dtstu.idtiposolicitud = ts.id "
                    + "ORDER BY dtstu.idtipousuario;")) {

                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    TipoSolicitud objTipoSolicitud = this.cache.get(id);
                    if (objTipoSolicitud == null) {
                        objTipoSolicitud = new TipoSolicitud(
                                id,
                                resultSet.getString("nombre"),
                                resultSet.getBoolean("estado"),
                                new ArrayList<>(),
                                new ArrayList<>());
                        this.cache.put(objTipoSolicitud.getId(), objTipoSolicitud);
                    }
                    mapDetalle.put(id, resultSet.getInt("idtipousuario"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!this.cache.isEmpty()) {
                /*DAODetalleTipoSolicitudDocumento objDAODetalleTipoSolicitudDocumento = DAODetalleTipoSolicitudDocumento.getInstance();
                for (Map.Entry<Integer, TipoSolicitud> entry : this.cache.entrySet()) {
                    TipoSolicitud objTipoSolicitud = entry.getValue();
                    objTipoSolicitud.setLstDetalleTipoSolicitudDocumento(objDAODetalleTipoSolicitudDocumento.getAllDetalleTipoSolicitudDocumento(objTipoSolicitud));
                }*/
                DAOTipoUsuario objDAOTipoUsuario = DAOTipoUsuario.getInstance();
                for (Map.Entry<Integer, Integer> entryDetalle : mapDetalle.entrySet()) {
                    TipoSolicitud objTipoSolicitud = this.cache.get(entryDetalle.getKey());
                    objTipoSolicitud.getLstTiposUsuario().add(objDAOTipoUsuario.getTipoUsuario(entryDetalle.getValue()));
                }
            }
        }

        Collection collection = this.cache.values();

        lstTiposSolicitud = new ArrayList<>(collection);

        return lstTiposSolicitud;
    }
}
