package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Acceso;
import com.smartech.sistemacooperativa.interfaces.IDAOAcceso;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAOAcceso implements IDAOAcceso {

    private static DAOAcceso instance = null;
    private Map<Integer, Acceso> cache;

    private DAOAcceso() {
        this.cache = new HashMap<>();
    }

    public static DAOAcceso getInstance() {
        if (instance == null) {
            instance = new DAOAcceso();
        }

        return instance;
    }

    @Override
    public Acceso getAcceso(int id) {
        Acceso objAcceso = this.cache.get(id);
        
        if(objAcceso == null){
            this.cache.clear();
            this.getAllAccesos();
            objAcceso = this.cache.get(id);
        }
        
        return objAcceso;
    }

    @Override
    public List<Acceso> getAllAccesos() {
        List<Acceso> lstAccesos;

        if (this.cache.isEmpty()) {
            try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT a.id, a.nombre, a.descripcion "
                    + "FROM accesos a;")) {

                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    Acceso objAcceso = new Acceso(
                            resultSet.getInt("id"),
                            resultSet.getString("nombre"),
                            resultSet.getString("descripcion"));
                    this.cache.put(objAcceso.getId(), objAcceso);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Collection collection = this.cache.values();
        
        if(collection instanceof List){
            lstAccesos = (List<Acceso>) collection;
        }else{
            lstAccesos = new ArrayList<>(collection);
        }
        
        return lstAccesos;
    }
}
