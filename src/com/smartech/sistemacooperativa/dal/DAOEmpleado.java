package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.Establecimiento;
import com.smartech.sistemacooperativa.dominio.TipoUsuario;
import com.smartech.sistemacooperativa.dominio.Usuario;
import com.smartech.sistemacooperativa.interfaces.IDAOEmpleado;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOEmpleado implements IDAOEmpleado {

    private static DAOEmpleado instance = null;

    private DAOEmpleado() {
    }

    public static DAOEmpleado getInstance() {
        if (instance == null) {
            instance = new DAOEmpleado();
        }

        return instance;
    }

    @Override
    public Empleado getEmpleado(int idTipoUsuario) {
        Empleado objEmpleado = null;
        int idEstablecimiento = 0;
        int idEstadoCivil = 0;
        int idUsuario = 0;
        int idDistrito = 0;
        int idGradoInstruccion = 0;
        int idTipoDocumentoIdentidad = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT e.id, e.nombre, e.apellidopaterno, e.apellidomaterno, e.fechanacimiento, "
                + "e.sexo, e.direccion, e.origen, e.documentoidentidad, e.ruc, e.telefono, e.celular, e.idestadocivil, e.estado, e.activo, e.fecharegistro, e.fechamodificacion, e.idusuario, e.idestablecimiento,"
                + "e.iddistrito, e.idgradoinstruccion, e.idtipodocumentoidentidad "
                + "FROM empleados e inner join usuarios u on e.idusuario = u.id "
                + "inner join tiposusuario tu on u.idtipousuario = tu.id "
                + "WHERE  e.estado = true and u.idtipousuario = ?;")) {
            preparedStatement.setLong(1, idTipoUsuario);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idEstablecimiento = resultSet.getInt("idestablecimiento");
                idEstadoCivil = resultSet.getInt("idestadocivil");
                idUsuario = resultSet.getInt("idusuario");
                idDistrito = resultSet.getInt("iddistrito");
                idGradoInstruccion = resultSet.getInt("idgradoinstruccion");
                idTipoDocumentoIdentidad = resultSet.getInt("idtipodocumentoidentidad");
                objEmpleado = new Empleado(
                        resultSet.getLong("id"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"),
                        resultSet.getTimestamp("fechanacimiento"),
                        resultSet.getString("direccion"),
                        resultSet.getString("documentoidentidad"),
                        resultSet.getString("ruc"),
                        resultSet.getString("telefono"),
                        resultSet.getString("celular"),
                        resultSet.getBoolean("sexo"),
                        resultSet.getString("origen"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("activo"),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if(objEmpleado != null){
            objEmpleado.setObjEstablecimiento(DAOEstablecimiento.getInstance().getEstablecimiento(idEstablecimiento));
            objEmpleado.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objEmpleado.setObjEstadoCivil(DAOEstadoCivil.getInstance().getEstadoCivil(idEstadoCivil));
            objEmpleado.setObjDistrito(DAODistrito.getInstance().getDistrito(idDistrito));
            objEmpleado.setObjGradoInstruccion(DAOGradoInstruccion.getInstance().getGradoInstruccion(idGradoInstruccion));
            objEmpleado.setObjTipoDocumentoIdentidad(DAOTipoDocumentoIdentidad.getInstance().getTipoDocumentoIdentidad(idTipoDocumentoIdentidad));
            
        }

        return objEmpleado;
    }

    @Override
    public Empleado getEmpleado(long id) {
        Empleado objEmpleado = null;
        int idEstablecimiento = 0;
        int idEstadoCivil = 0;
        int idUsuario = 0;
        int idDistrito = 0;
        int idGradoInstruccion = 0;
        int idTipoDocumentoIdentidad = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT e.id, e.nombre, e.apellidopaterno, e.apellidomaterno, e.fechanacimiento, "
                + "e.sexo, e.direccion, e.origen, e.documentoidentidad, e.ruc, e.telefono, e.celular, e.idestadocivil, e.estado, e.activo, e.fecharegistro, e.fechamodificacion, e.idusuario, e.idestablecimiento,"
                + "e.iddistrito, e.idgradoinstruccion, e.idtipodocumentoidentidad "
                + "FROM empleados e "
                + "WHERE e.id = ?;")) {
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idEstablecimiento = resultSet.getInt("idestablecimiento");
                idEstadoCivil = resultSet.getInt("idestadocivil");
                idUsuario = resultSet.getInt("idusuario");
                idDistrito = resultSet.getInt("iddistrito");
                idGradoInstruccion = resultSet.getInt("idgradoinstruccion");
                idTipoDocumentoIdentidad = resultSet.getInt("idtipodocumentoidentidad");
                objEmpleado = new Empleado(
                        resultSet.getLong("id"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"),
                        resultSet.getTimestamp("fechanacimiento"),
                        resultSet.getString("direccion"),
                        resultSet.getString("documentoidentidad"),
                        resultSet.getString("ruc"),
                        resultSet.getString("telefono"),
                        resultSet.getString("celular"),
                        resultSet.getBoolean("sexo"),
                        resultSet.getString("origen"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("activo"),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if(objEmpleado != null){
            objEmpleado.setObjEstablecimiento(DAOEstablecimiento.getInstance().getEstablecimiento(idEstablecimiento));
            objEmpleado.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objEmpleado.setObjEstadoCivil(DAOEstadoCivil.getInstance().getEstadoCivil(idEstadoCivil));
            objEmpleado.setObjDistrito(DAODistrito.getInstance().getDistrito(idDistrito));
            objEmpleado.setObjGradoInstruccion(DAOGradoInstruccion.getInstance().getGradoInstruccion(idGradoInstruccion));
            objEmpleado.setObjTipoDocumentoIdentidad(DAOTipoDocumentoIdentidad.getInstance().getTipoDocumentoIdentidad(idTipoDocumentoIdentidad));
            
        }

        return objEmpleado;
    }

    @Override
    public Empleado getEmpleado(Usuario objUsuario) {
        Empleado objEmpleado = null;
        int idEstablecimiento = 0;
        int idEstadoCivil = 0;
        int idDistrito = 0;
        int idGradoInstruccion = 0;
        int idTipoDocumentoIdentidad = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT e.id, e.nombre, e.apellidopaterno, e.apellidomaterno, e.fechanacimiento, "
                + "e.sexo, e.direccion, e.origen, e.documentoidentidad, e.ruc, e.telefono, e.celular, e.idestadocivil, e.estado, e.activo, e.fecharegistro, e.fechamodificacion, e.idusuario, e.idestablecimiento,"
                + "e.iddistrito, e.idgradoinstruccion, e.idtipodocumentoidentidad "
                + "FROM empleados e "
                + "WHERE e.idusuario = ?;")) {
            preparedStatement.setLong(1, objUsuario.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idEstablecimiento = resultSet.getInt("idestablecimiento");
                idEstadoCivil = resultSet.getInt("idestadocivil");
                idDistrito = resultSet.getInt("iddistrito");
                idGradoInstruccion = resultSet.getInt("idgradoinstruccion");
                idTipoDocumentoIdentidad = resultSet.getInt("idtipodocumentoidentidad");
                objEmpleado = new Empleado(
                        resultSet.getLong("id"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"),
                        resultSet.getTimestamp("fechanacimiento"),
                        resultSet.getString("direccion"),
                        resultSet.getString("documentoidentidad"),
                        resultSet.getString("ruc"),
                        resultSet.getString("telefono"),
                        resultSet.getString("celular"),
                        resultSet.getBoolean("sexo"),
                        resultSet.getString("origen"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("activo"),
                        null,
                        null,
                        null,
                        null,
                        null,
                        objUsuario);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if(objEmpleado != null){
            objEmpleado.setObjEstablecimiento(DAOEstablecimiento.getInstance().getEstablecimiento(idEstablecimiento));
            objEmpleado.setObjEstadoCivil(DAOEstadoCivil.getInstance().getEstadoCivil(idEstadoCivil));
            objEmpleado.setObjDistrito(DAODistrito.getInstance().getDistrito(idDistrito));
            objEmpleado.setObjGradoInstruccion(DAOGradoInstruccion.getInstance().getGradoInstruccion(idGradoInstruccion));
            objEmpleado.setObjTipoDocumentoIdentidad(DAOTipoDocumentoIdentidad.getInstance().getTipoDocumentoIdentidad(idTipoDocumentoIdentidad));
        }

        return objEmpleado;
    }

    @Override
    public List<Empleado> getAllEmpleados() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Empleado> getAllEmpleadosDebajoNivel(Usuario objUsuario, Establecimiento objEstablecimiento) {
        List<Empleado> lstEmpleados = new ArrayList<>();
        List<Integer> lstIdsEstadosCiviles = new ArrayList<>();
        List<Integer> lstIdsDistritos = new ArrayList<>();
        List<Integer> lstIdsGradosInstruccion = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        List<Integer> lstIdsTiposDocumentoIdentidad = new ArrayList<>();
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT e.id, e.nombre, e.apellidopaterno, e.apellidomaterno, e.fechanacimiento, "
                + "e.sexo, e.direccion, e.origen, e.documentoidentidad, e.ruc, e.telefono, e.celular, e.idestadocivil, e.estado, e.activo, e.fecharegistro, e.fechamodificacion, e.idusuario, e.idestablecimiento,"
                + "e.iddistrito, e.idgradoinstruccion, e.idtipodocumentoidentidad "
                + "FROM empleados e INNER JOIN usuarios u ON e.idusuario = u.id "
                + "INNER JOIN tiposusuario tu ON tu.id = u.idtipousuario "
                + "WHERE tu.nivel < ? AND e.idestablecimiento = ? AND e.estado is true;")){
            preparedStatement.setInt(1, objUsuario.getObjTipoUsuario().getNivel());
            preparedStatement.setInt(2, objEstablecimiento.getId());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                lstIdsEstadosCiviles.add(resultSet.getInt("idestadocivil"));
                lstIdsDistritos.add(resultSet.getInt("iddistrito"));
                lstIdsGradosInstruccion.add(resultSet.getInt("idgradoinstruccion"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                lstIdsTiposDocumentoIdentidad.add(resultSet.getInt("idtipodocumentoidentidad"));
                Empleado objEmpleado = new Empleado(
                        resultSet.getLong("id"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"),
                        resultSet.getTimestamp("fechanacimiento"),
                        resultSet.getString("direccion"),
                        resultSet.getString("documentoidentidad"),
                        resultSet.getString("ruc"),
                        resultSet.getString("telefono"),
                        resultSet.getString("celular"),
                        resultSet.getBoolean("sexo"),
                        resultSet.getString("origen"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("activo"),
                        null,
                        null,
                        null,
                        null,
                        objEstablecimiento,
                        null);
                lstEmpleados.add(objEmpleado);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        if(!lstEmpleados.isEmpty()){
            DAOEstadoCivil objDAOEstadoCivil = DAOEstadoCivil.getInstance();
            DAODistrito objDAODistrito = DAODistrito.getInstance();
            DAOGradoInstruccion objDAOGradoInstruccion = DAOGradoInstruccion.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOTipoDocumentoIdentidad objDAOTipoDocumentoIdentidad = DAOTipoDocumentoIdentidad.getInstance();
            
            int size = lstEmpleados.size();
            for(int i = 0; i < size; i++){
                lstEmpleados.get(i).setObjEstadoCivil(objDAOEstadoCivil.getEstadoCivil(lstIdsEstadosCiviles.get(i)));
                lstEmpleados.get(i).setObjDistrito(objDAODistrito.getDistrito(lstIdsDistritos.get(i)));
                lstEmpleados.get(i).setObjGradoInstruccion(objDAOGradoInstruccion.getGradoInstruccion(lstIdsGradosInstruccion.get(i)));
                lstEmpleados.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstEmpleados.get(i).setObjTipoDocumentoIdentidad(objDAOTipoDocumentoIdentidad.getTipoDocumentoIdentidad(lstIdsTiposDocumentoIdentidad.get(i)));
            }
        }
        
        return lstEmpleados;
    }

    @Override
    public List<Empleado> getAllEmpleadosSobreNivel(int nivel, Establecimiento objEstablecimiento) {
        List<Empleado> lstEmpleados = new ArrayList<>();
        List<Integer> lstIdsEstadosCiviles = new ArrayList<>();
        List<Integer> lstIdsDistritos = new ArrayList<>();
        List<Integer> lstIdsGradosInstruccion = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        List<Integer> lstIdsTiposDocumentoIdentidad = new ArrayList<>();
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT e.id, e.nombre, e.apellidopaterno, e.apellidomaterno, e.fechanacimiento, "
                + "e.sexo, e.direccion, e.origen, e.documentoidentidad, e.ruc, e.telefono, e.celular, e.idestadocivil, e.estado, e.activo, e.fecharegistro, e.fechamodificacion, e.idusuario, e.idestablecimiento,"
                + "e.iddistrito, e.idgradoinstruccion, e.idtipodocumentoidentidad "
                + "FROM empleados e INNER JOIN usuarios u ON e.idusuario = u.id "
                + "INNER JOIN tiposusuario tu ON tu.id = u.idtipousuario "
                + "WHERE tu.nivel > ? AND e.idestablecimiento = ? AND e.estado is true;")){
            preparedStatement.setInt(1, nivel);
            preparedStatement.setInt(2, objEstablecimiento.getId());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                lstIdsEstadosCiviles.add(resultSet.getInt("idestadocivil"));
                lstIdsDistritos.add(resultSet.getInt("iddistrito"));
                lstIdsGradosInstruccion.add(resultSet.getInt("idgradoinstruccion"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                lstIdsTiposDocumentoIdentidad.add(resultSet.getInt("idtipodocumentoidentidad"));
                Empleado objEmpleado = new Empleado(
                        resultSet.getLong("id"),
                        resultSet.getString("nombre"),
                        resultSet.getString("apellidopaterno"),
                        resultSet.getString("apellidomaterno"),
                        resultSet.getTimestamp("fechanacimiento"),
                        resultSet.getString("direccion"),
                        resultSet.getString("documentoidentidad"),
                        resultSet.getString("ruc"),
                        resultSet.getString("telefono"),
                        resultSet.getString("celular"),
                        resultSet.getBoolean("sexo"),
                        resultSet.getString("origen"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("activo"),
                        null,
                        null,
                        null,
                        null,
                        objEstablecimiento,
                        null);
                lstEmpleados.add(objEmpleado);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        if(!lstEmpleados.isEmpty()){
            DAOEstadoCivil objDAOEstadoCivil = DAOEstadoCivil.getInstance();
            DAODistrito objDAODistrito = DAODistrito.getInstance();
            DAOGradoInstruccion objDAOGradoInstruccion = DAOGradoInstruccion.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOTipoDocumentoIdentidad objDAOTipoDocumentoIdentidad = DAOTipoDocumentoIdentidad.getInstance();
            
            int size = lstEmpleados.size();
            for(int i = 0; i < size; i++){
                lstEmpleados.get(i).setObjEstadoCivil(objDAOEstadoCivil.getEstadoCivil(lstIdsEstadosCiviles.get(i)));
                lstEmpleados.get(i).setObjDistrito(objDAODistrito.getDistrito(lstIdsDistritos.get(i)));
                lstEmpleados.get(i).setObjGradoInstruccion(objDAOGradoInstruccion.getGradoInstruccion(lstIdsGradosInstruccion.get(i)));
                lstEmpleados.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstEmpleados.get(i).setObjTipoDocumentoIdentidad(objDAOTipoDocumentoIdentidad.getTipoDocumentoIdentidad(lstIdsTiposDocumentoIdentidad.get(i)));
            }
        }
        
        return lstEmpleados;
    }

    @Override
    public boolean insert(Empleado objEmpleado) {
        boolean result = false;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO empleados(nombre, apellidopaterno, apellidomaterno, fechanacimiento, "
                + "sexo, direccion, origen, documentoidentidad, ruc, telefono, celular, idestadocivil, estado, activo, fecharegistro, idusuario, idestablecimiento, iddistrito, idgradoinstruccion, idtipodocumentoidentidad) "
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, true, true, now(), ?, ?, ?, ?, ?);")){
            preparedStatement.setString(1, objEmpleado.getNombre());
            preparedStatement.setString(2, objEmpleado.getApellidoPaterno());
            preparedStatement.setString(3, objEmpleado.getApellidoMaterno());
            preparedStatement.setTimestamp(4, objEmpleado.getFechaNacimiento());
            preparedStatement.setBoolean(5, objEmpleado.isSexo());
            preparedStatement.setString(6, objEmpleado.getDireccion());
            preparedStatement.setString(7, objEmpleado.getOrigen());
            preparedStatement.setString(8, objEmpleado.getDocumentoIdentidad());
            preparedStatement.setString(9, objEmpleado.getRUC());
            preparedStatement.setString(10, objEmpleado.getTelefono());
            preparedStatement.setString(11, objEmpleado.getCelular());
            preparedStatement.setInt(12, objEmpleado.getObjEstadoCivil().getId());
            preparedStatement.setLong(13, objEmpleado.getObjUsuario().getId());            
            preparedStatement.setInt(14, objEmpleado.getObjEstablecimiento().getId());
            preparedStatement.setInt(15, objEmpleado.getObjDistrito().getId());
            preparedStatement.setInt(16, objEmpleado.getObjGradoInstruccion().getId());
            preparedStatement.setInt(17, objEmpleado.getObjTipoDocumentoIdentidad().getId());
                    
            
            preparedStatement.execute();
            
            result = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
    }

    @Override
    public boolean update(Empleado objEmpleado) {
        boolean result = false;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE empleados SET nombre = ?, apellidopaterno = ?, apellidomaterno = ?, fechanacimiento = ?, "
                + "sexo = ?, direccion = ?, origen = ?, documentoidentidad = ?, ruc = ?, telefono = ?, celular = ?, idestadocivil = ?, estado = ?, activo = ?, fechamodificacion = now(), idusuario = ?, idestablecimiento = ?, "
                + "iddistrito = ?, idgradoinstruccion = ?, idtipodocumentoidentidad = ? "
                + "WHERE id = ?;")){
            preparedStatement.setString(1, objEmpleado.getNombre());
            preparedStatement.setString(2, objEmpleado.getApellidoPaterno());
            preparedStatement.setString(3, objEmpleado.getApellidoMaterno());
            preparedStatement.setTimestamp(4, objEmpleado.getFechaNacimiento());
            preparedStatement.setBoolean(5, objEmpleado.isSexo());
            preparedStatement.setString(6, objEmpleado.getDireccion());
            preparedStatement.setString(7, objEmpleado.getOrigen());
            preparedStatement.setString(8, objEmpleado.getDocumentoIdentidad());
            preparedStatement.setString(9, objEmpleado.getRUC());
            preparedStatement.setString(10, objEmpleado.getTelefono());
            preparedStatement.setString(11, objEmpleado.getCelular());
            preparedStatement.setInt(12, objEmpleado.getObjEstadoCivil().getId());
            preparedStatement.setBoolean(13, objEmpleado.isEstado());
            preparedStatement.setBoolean(14, objEmpleado.isActivo());
            preparedStatement.setLong(15, objEmpleado.getObjUsuario().getId());            
            preparedStatement.setInt(16, objEmpleado.getObjEstablecimiento().getId());
            preparedStatement.setInt(17, objEmpleado.getObjDistrito().getId());
            preparedStatement.setInt(18, objEmpleado.getObjGradoInstruccion().getId());
            preparedStatement.setInt(19, objEmpleado.getObjTipoDocumentoIdentidad().getId());
            preparedStatement.setLong(20, objEmpleado.getId());
            
            preparedStatement.execute();
            
            result = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
    }

    @Override
    public boolean delete(Empleado objEmpleado) {
        boolean result = false;
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE empleados SET estado = false "
                + "WHERE id = ?;")){
            preparedStatement.setLong(1, objEmpleado.getId());
            
            preparedStatement.execute();
            
            result = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
    }

}
