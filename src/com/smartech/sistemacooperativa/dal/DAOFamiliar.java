/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Familiar;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.interfaces.IDAOFamiliar;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOFamiliar implements IDAOFamiliar {

    private static DAOFamiliar instance = null;

    public DAOFamiliar() {
    }

    public static DAOFamiliar getInstance() {
        if (instance == null) {
            instance = new DAOFamiliar();
        }
        return instance;
    }

    @Override
    public List<Familiar> getAllFamiliares(Socio objSocio) {
        List<Familiar> lstFamiliares = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(" "
                + "")) {

            preparedStatement.setLong(1, objSocio.getId());

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                /*Familiar objFamiliar = new Familiar(0,
                        nombre,
                        apellidoPaterno,
                        apellidoMaterno,
                        fechaNacimiento,
                        direccion,
                        DNI,
                        RUC,
                        telefono,
                        celular,
                        true,
                        origen,
                        estadoCivil,
                        true,
                        fechaRegistro,
                        fechaModificacion,
                        lstFamiliaresSocio);
                
                lstFamiliares.add(objFamiliar);
                 */
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return lstFamiliares;
    }

    @Override
    public Familiar getFamiliar(int id) {
        Familiar objFamiliar = null;
        int idTipoDocumentoIdentidad = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT f.id, f.nombres, f.apellidopaterno, "
                + "f.apellidomaterno, f.documentoidentidad, f.telefono, f.celular, f.sexo, f.fechanacimiento, f.direccion, f.idtipodocumentoidentidad "
                + "FROM familiares f "
                + "WHERE f.id = ?")) {

            preparedStatement.setLong(1, id);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                idTipoDocumentoIdentidad = rs.getInt("idtipodocumentoidentidad");
                objFamiliar = new Familiar(
                        rs.getLong("id"),
                        rs.getString("nombres"),
                        rs.getString("apellidopaterno"),
                        rs.getString("apellidomaterno"),
                        rs.getTimestamp("fechanacimiento"),
                        rs.getString("direccion"),
                        rs.getString("documentoidentidad"),
                        "-",
                        rs.getString("telefono"),
                        rs.getString("celular"),
                        rs.getBoolean("sexo"),
                        "-",
                        true,
                        null, //fecharegistro
                        null, //fechamodificacion
                        null,
                        null,
                        new ArrayList<>()//lstFamiliaresSocio
                );
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objFamiliar != null) {
            objFamiliar.setObjTipoDocumentoIdentidad(DAOTipoDocumentoIdentidad.getInstance().getTipoDocumentoIdentidad(idTipoDocumentoIdentidad));
        }

        return objFamiliar;
    }

    @Override
    public boolean insert(Familiar objFamiliar) {
        boolean resultado = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO familiares (nombres, apellidopaterno, "
                + "apellidomaterno, documentoidentidad, telefono, celular, sexo, fechanacimiento, direccion, idtipodocumentoidentidad) "
                + "VALUES (?,?,?,?,?,?,?,?,?,?)",
                PreparedStatement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, objFamiliar.getNombre());
            preparedStatement.setString(2, objFamiliar.getApellidoPaterno());
            preparedStatement.setString(3, objFamiliar.getApellidoMaterno());
            preparedStatement.setString(4, objFamiliar.getDocumentoIdentidad());
            preparedStatement.setString(5, objFamiliar.getTelefono());
            preparedStatement.setString(6, objFamiliar.getCelular());
            preparedStatement.setBoolean(7, objFamiliar.isSexo());
            preparedStatement.setTimestamp(8, objFamiliar.getFechaNacimiento());
            preparedStatement.setString(9, objFamiliar.getDireccion());
            preparedStatement.setInt(10, objFamiliar.getObjTipoDocumentoIdentidad().getId());

            preparedStatement.execute();

            ResultSet keySet = preparedStatement.getGeneratedKeys();

            while (keySet.next()) {
                objFamiliar.setId(keySet.getLong(1));
            }

            resultado = true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultado;
    }

}
