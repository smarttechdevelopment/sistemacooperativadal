/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.DetalleFamiliarSocio;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.interfaces.IDAODetalleFamiliarSocio;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAODetalleFamiliarSocio implements IDAODetalleFamiliarSocio {

    private static DAODetalleFamiliarSocio instance = null;

    private DAODetalleFamiliarSocio() {
    }

    public static DAODetalleFamiliarSocio getInstance() {
        if (instance == null) {
            instance = new DAODetalleFamiliarSocio();
        }

        return instance;
    }

    @Override
    public List<DetalleFamiliarSocio> getAllDetalleFamiliarSocio(Socio objSocio) {
        List<DetalleFamiliarSocio> lstFamiliaresSocio = new ArrayList<>();
        List<Integer> lstIdsFamiliares = new ArrayList<>();
        List<Integer> lstIdsParentescos = new ArrayList<>();

        try (PreparedStatement pst = ConnectionManager.getConnection().prepareStatement("SELECT dfs.id, dfs.descripcion, dfs.porcentajebeneficio, dfs.idsocio, dfs.idfamiliar, dfs.idparentesco "
                + "FROM detallefamiliarsocio dfs "
                + "WHERE dfs.idsocio = ?;");) {
            pst.setLong(1, objSocio.getId());

            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                DetalleFamiliarSocio objDetalleFamiliarSocio = new DetalleFamiliarSocio(rs.getInt("id"), rs.getString("descripcion"), rs.getBigDecimal("porcentajebeneficio"), objSocio, null, null);
                lstIdsFamiliares.add(rs.getInt("idfamiliar"));
                lstIdsParentescos.add(rs.getInt("idparentesco"));

                lstFamiliaresSocio.add(objDetalleFamiliarSocio);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstFamiliaresSocio.isEmpty()) {
            DAOFamiliar objDAOFamiliar = DAOFamiliar.getInstance();
            DAOParentesco objDAOParentesco = DAOParentesco.getInstance();

            int size = lstFamiliaresSocio.size();
            for (int i = 0; i < size; i++) {
                lstFamiliaresSocio.get(i).setObjFamiliar(objDAOFamiliar.getFamiliar(lstIdsFamiliares.get(i)));
                lstFamiliaresSocio.get(i).setObjParentesco(objDAOParentesco.getParentesco(lstIdsParentescos.get(i)));
            }
        }

        return lstFamiliaresSocio;
    }

    @Override
    public boolean insert(DetalleFamiliarSocio objDetalleFamiliarSocio) {
        boolean resultado = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO detallefamiliarsocio (descripcion, porcentajebeneficio, idsocio, idfamiliar, idparentesco) "
                + "VALUES (?,?,?,?,?)",
                PreparedStatement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, objDetalleFamiliarSocio.getDescripcion());
            preparedStatement.setBigDecimal(2, objDetalleFamiliarSocio.getPorcentajeBeneficio());
            preparedStatement.setLong(3, objDetalleFamiliarSocio.getObjSocio().getId());
            preparedStatement.setLong(4, objDetalleFamiliarSocio.getObjFamiliar().getId());
            preparedStatement.setLong(5, objDetalleFamiliarSocio.getObjParentesco().getId());

            preparedStatement.execute();

            ResultSet keySet = preparedStatement.getGeneratedKeys();

            while (keySet.next()) {
                objDetalleFamiliarSocio.setId(keySet.getInt(1));
            }

            resultado = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultado;
    }

    @Override
    public boolean physicalDelete(Socio objSocio) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM detallefamiliarsocio WHERE idsocio = ?;")) {
            preparedStatement.setLong(1, objSocio.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
