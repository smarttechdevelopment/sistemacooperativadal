/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.TipoPago;
import com.smartech.sistemacooperativa.interfaces.IDAOTipoPago;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAOTipoPago implements IDAOTipoPago {

    private static DAOTipoPago instance = null;
    private Map<Integer, TipoPago> cache;

    private DAOTipoPago() {
        this.cache = new HashMap<>();
    }

    public static DAOTipoPago getInstance() {
        if (instance == null) {
            instance = new DAOTipoPago();
        }
        return instance;
    }

    @Override
    public boolean insert(TipoPago objTipoPago) {
        boolean resultado = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO tipospago (nombre) VALUES (?);", PreparedStatement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, objTipoPago.getNombre());

            preparedStatement.execute();

            ResultSet keySet = preparedStatement.getGeneratedKeys();

            while (keySet.next()) {
                objTipoPago.setId(keySet.getInt(1));
            }

            resultado = true;

        } catch (Exception e) {
        }
        return resultado;
    }

    @Override
    public TipoPago getTipoPago(int id) {
        TipoPago objTipoPago = this.cache.get(id);
        
        if(objTipoPago == null){
            this.cache.clear();
            this.getAllTipoPago();
            objTipoPago = this.cache.get(id);
        }
        
        return objTipoPago;
    }

    @Override
    public List<TipoPago> getAllTipoPago() {
        List<TipoPago> lstTipoPagos;

        if (this.cache.isEmpty()) {
            try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareCall("SELECT id,nombre FROM tipospago")) {

                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    TipoPago objTipoPago = new TipoPago(
                            resultSet.getInt("id"),
                            resultSet.getString("nombre"));
                    this.cache.put(objTipoPago.getId(), objTipoPago);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        Collection collection = this.cache.values();
        
        lstTipoPagos = new ArrayList<>(collection);

        return lstTipoPagos;
    }
}
