package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.DetalleAccesoTipoUsuario;
import com.smartech.sistemacooperativa.dominio.TipoUsuario;
import com.smartech.sistemacooperativa.interfaces.IDAODetalleAccesoTipoUsuario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAODetalleAccesoTipoUsuario implements IDAODetalleAccesoTipoUsuario{
    
    private static DAODetalleAccesoTipoUsuario instance = null;
    
    private DAODetalleAccesoTipoUsuario(){
        if(instance == null){
            instance = new DAODetalleAccesoTipoUsuario();
        }
        
        instance = new DAODetalleAccesoTipoUsuario();
    }

    @Override
    public List<DetalleAccesoTipoUsuario> getAllDetalleAccesoTipoUsuario(TipoUsuario objTipoUsuario) {
        List<DetalleAccesoTipoUsuario> lstDetalleAccesoTipoUsuario = new ArrayList<>();
        
        try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT datu.id, datu.idacceso, datu.idtipousuario "
                + "FROM detalleaccesotipousuario datu "
                + "WHERE datu.idtipousuario = ?;")){
            preparedStatement.setInt(1, objTipoUsuario.getId());
            
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                DetalleAccesoTipoUsuario objDetalleAccesoTipoUsuario = new DetalleAccesoTipoUsuario(
                        resultSet.getLong("id"),
                        objTipoUsuario,
                        null);
                lstDetalleAccesoTipoUsuario.add(objDetalleAccesoTipoUsuario);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return lstDetalleAccesoTipoUsuario;
    }
}
