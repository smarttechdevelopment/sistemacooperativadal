package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.DetalleDocumentoSolicitud;
import com.smartech.sistemacooperativa.dominio.Solicitud;
import com.smartech.sistemacooperativa.dominio.SolicitudIngreso;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.SolicitudRetiroSocio;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import com.smartech.sistemacooperativa.interfaces.IDAODetalleDocumentoSolicitud;
import com.smartech.sistemacooperativa.util.generics.FileUtil;

/**
 *
 * @author Smartech
 */
public class DAODetalleDocumentoSolicitud implements IDAODetalleDocumentoSolicitud {

    private static DAODetalleDocumentoSolicitud instance = null;

    private DAODetalleDocumentoSolicitud() {
    }

    public static DAODetalleDocumentoSolicitud getInstance() {
        if (instance == null) {
            instance = new DAODetalleDocumentoSolicitud();
        }

        return instance;
    }

    @Override
    public List<DetalleDocumentoSolicitud> getAllDetalleDocumentoSolicitud(Solicitud objSolicitud) {
        List<DetalleDocumentoSolicitud> lstDetalleDocumentoSolicitud = new ArrayList<>();
        List<Integer> lstIdsDocumentos = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();

        String selectQuery = "";
        String ftpDirectory = "";
        if (objSolicitud instanceof SolicitudIngreso) {
            selectQuery = "FROM detalledocumentosolicitudingreso dds INNER JOIN solicitudesingreso s ON s.id = dds.idsolicitudingreso ";
            ftpDirectory = "SolicitudesIngreso";
        }
        if (objSolicitud instanceof SolicitudPrestamo) {
            selectQuery = "FROM detalledocumentosolicitudprestamo dds INNER JOIN solicitudesprestamo s ON s.id = dds.idsolicitudprestamo ";
            ftpDirectory = "SolicitudesPrestamo";
        }
        if (objSolicitud instanceof SolicitudRetiroSocio) {
            selectQuery = "FROM detalledocumentosolicitudretirosocio dds INNER JOIN solicitudesretirosocio s ON s.id = dds.idsolicitudretirosocio ";
            ftpDirectory = "SolicitudesRenuncia";
        }

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT dds.id, dds.aprobado, "
                + "dds.fechaentrega, dds.fechasolicitud, dds.ruta, dds.fechamodificacion, dds.iddocumento, dds." + objSolicitud.getForeignKeyName() + ", dds.idusuario "
                + selectQuery
                + "WHERE s.id = ?;")) {

            preparedStatement.setLong(1, objSolicitud.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsDocumentos.add(resultSet.getInt("iddocumento"));
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                DetalleDocumentoSolicitud objDetalleDocumentoSolicitud = new DetalleDocumentoSolicitud(
                        resultSet.getLong("id"),
                        resultSet.getTimestamp("fechaentrega"),
                        null,
                        resultSet.getString("ruta"),
                        null,
                        objSolicitud,
                        resultSet.getBoolean("aprobado"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getTimestamp("fechasolicitud"),
                        null);
                lstDetalleDocumentoSolicitud.add(objDetalleDocumentoSolicitud);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstDetalleDocumentoSolicitud.isEmpty()) {
            DAODocumento objDAODocumento = DAODocumento.getInstance();
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();

            int size = lstDetalleDocumentoSolicitud.size();
            for (int i = 0; i < size; i++) {
                lstDetalleDocumentoSolicitud.get(i).setObjDocumento(objDAODocumento.getDocumento(lstIdsDocumentos.get(i)));
                lstDetalleDocumentoSolicitud.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstDetalleDocumentoSolicitud.get(i).setRepresentacion(FileUtil.downloadFileFTP(lstDetalleDocumentoSolicitud.get(i).getRuta()));
                FileUtil.deleteTempFile(lstDetalleDocumentoSolicitud.get(i).getRuta());
            }
        }

        return lstDetalleDocumentoSolicitud;
    }

    @Override
    public boolean insert(DetalleDocumentoSolicitud objDetalleDocumentoSolicitud) {
        boolean resultado = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO " + objDetalleDocumentoSolicitud.getObjSolicitud().getDetalleDocumentoTableName() + " (aprobado,"
                + "fechasolicitud," + objDetalleDocumentoSolicitud.getObjSolicitud().getForeignKeyName() + ",iddocumento,idusuario,fechaentrega,ruta) "
                + "VALUES (?,now(),?,?,?,?,?)",
                PreparedStatement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setBoolean(1, objDetalleDocumentoSolicitud.isAprobado());
            preparedStatement.setLong(2, objDetalleDocumentoSolicitud.getObjSolicitud().getId());
            preparedStatement.setInt(3, objDetalleDocumentoSolicitud.getObjDocumento().getId());
            preparedStatement.setLong(4, objDetalleDocumentoSolicitud.getObjUsuario().getId());
            if (objDetalleDocumentoSolicitud.getFechaEntrega() == null) {
                preparedStatement.setNull(5, Types.TIMESTAMP);
            } else {
                preparedStatement.setTimestamp(5, objDetalleDocumentoSolicitud.getFechaEntrega());
            }
            preparedStatement.setString(6, objDetalleDocumentoSolicitud.getRuta());

            preparedStatement.execute();

            ResultSet keySet = preparedStatement.getGeneratedKeys();
            while (keySet.next()) {
                objDetalleDocumentoSolicitud.setId(keySet.getLong(1));
            }

            resultado = true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultado;
    }

    @Override
    public boolean update(DetalleDocumentoSolicitud objDetalleDocumentoSolicitud) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE " + objDetalleDocumentoSolicitud.getObjSolicitud().getDetalleDocumentoTableName() + " SET fechaentrega = ?, "
                + "aprobado = ?, fechasolicitud = ?, fechamodificacion = ?, iddocumento = ?, " + objDetalleDocumentoSolicitud.getObjSolicitud().getForeignKeyName() + " = ?, idusuario = ?, ruta = ? "
                + "WHERE id = ?;")) {
            preparedStatement.setTimestamp(1, objDetalleDocumentoSolicitud.getFechaEntrega());
            preparedStatement.setBoolean(2, objDetalleDocumentoSolicitud.isAprobado());
            preparedStatement.setTimestamp(3, objDetalleDocumentoSolicitud.getFechaSolicitud());
            preparedStatement.setTimestamp(4, objDetalleDocumentoSolicitud.getFechaModificacion());
            preparedStatement.setInt(5, objDetalleDocumentoSolicitud.getObjDocumento().getId());
            preparedStatement.setLong(6, objDetalleDocumentoSolicitud.getObjSolicitud().getId());
            preparedStatement.setLong(7, objDetalleDocumentoSolicitud.getObjUsuario().getId());
            preparedStatement.setString(8, objDetalleDocumentoSolicitud.getRuta());
                    
            preparedStatement.setLong(9, objDetalleDocumentoSolicitud.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean discardAllDetalleDocumentoSolicitud(Solicitud objSolicitud) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE " + objSolicitud.getDetalleDocumentoTableName() + " SET aprobado = false "
                + "WHERE " + objSolicitud.getForeignKeyName() + " = ?;")) {
            preparedStatement.setLong(1, objSolicitud.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean physicalDelete(Solicitud objSolicitud) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM  " + objSolicitud.getDetalleDocumentoTableName() + " WHERE " + objSolicitud.getForeignKeyName() + " = ?;")) {
            preparedStatement.setLong(1, objSolicitud.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
