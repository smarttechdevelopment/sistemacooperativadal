package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.TipoCuenta;
import com.smartech.sistemacooperativa.dominio.TipoInteres;
import com.smartech.sistemacooperativa.interfaces.IDAOTipoInteres;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAOTipoInteres implements IDAOTipoInteres {

    private static DAOTipoInteres instance = null;
    private Map<Integer, TipoInteres> cache;

    private DAOTipoInteres() {
        this.cache = new HashMap<>();
    }

    public static DAOTipoInteres getInstance() {
        if (instance == null) {
            instance = new DAOTipoInteres();
        }

        return instance;
    }

    @Override
    public TipoInteres getTipoInteres(int id) {
        TipoInteres objTipoInteres = this.cache.get(id);

        if (objTipoInteres == null) {
            this.cache.clear();
            this.getAllTiposInteres();
            objTipoInteres = this.cache.get(id);
        }

        return objTipoInteres;
    }

    @Override
    public List<TipoInteres> getAllTiposInteres() {
        List<TipoInteres> lstTiposInteres;

        if (this.cache.isEmpty()) {
            try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT ti.id, ti.nombre, ti.diasplazos, ti.plazofijo, ti.estado, ti.fecharegistro, ti.fechamodificacion "
                    + "FROM tiposinteres ti "
                    + "WHERE ti.estado is true;")) {

                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    TipoInteres objTipoInteres = new TipoInteres(
                            resultSet.getInt("id"),
                            resultSet.getString("nombre"),
                            resultSet.getInt("diasplazos"),
                            resultSet.getBoolean("plazofijo"),
                            resultSet.getBoolean("estado"),
                            resultSet.getTimestamp("fecharegistro"),
                            resultSet.getTimestamp("fechamodificacion"),
                            new ArrayList<>());
                    this.cache.put(objTipoInteres.getId(), objTipoInteres);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!this.cache.isEmpty()) {
                DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();

                for (Map.Entry<Integer, TipoInteres> entry : this.cache.entrySet()) {
                    TipoInteres objTipoInteres = entry.getValue();
                    objTipoInteres.setLstTasasInteres(objDAOTasaInteres.getAllTasasIntereses(objTipoInteres));
                }
            }
        }

        Collection collection = this.cache.values();

        lstTiposInteres = new ArrayList<>(collection);

        return lstTiposInteres;
    }

    @Override
    public List<TipoInteres> getAllTiposInteres(TipoCuenta objTipoCuenta) {        
        List<TipoInteres> lstTiposInteres = new ArrayList<>();
        objTipoCuenta.setLstTiposInteres(lstTiposInteres);

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT ti.id, ti.nombre, ti.diasplazos, ti.plazofijo, ti.estado, ti.fecharegistro, ti.fechamodificacion "
                + " FROM tiposinteres ti "
                + " INNER JOIN detalletipocuentatipointeres dtctp on dtctp.idtipointeres = ti.id"
                + " INNER JOIN tiposcuenta tc on tc.id = dtctp.idtipocuenta"
                + " WHERE tc.id = ? AND ti.estado is true;")) {
            preparedStatement.setInt(1, objTipoCuenta.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                TipoInteres objTipoInteres = new TipoInteres(
                        resultSet.getInt("id"),
                        resultSet.getString("nombre"),
                        resultSet.getInt("diasplazos"),
                        resultSet.getBoolean("plazofijo"),
                        resultSet.getBoolean("estado"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        new ArrayList<>());
                lstTiposInteres.add(objTipoInteres);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if(!lstTiposInteres.isEmpty()){
            DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();
            
            int size = lstTiposInteres.size();
            for (int i = 0; i < size; i++) {
                lstTiposInteres.get(i).setLstTasasInteres(objDAOTasaInteres.getAllTasasIntereses(lstTiposInteres.get(i)));
            }
        }

        return lstTiposInteres;
    }

    @Override
    public boolean insert(TipoInteres objTipoInteres) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO tiposinteres(nombre, diasplazos, plazofijo, fecharegistro, estado) "
                + "VALUES(?, ?, ?, now(), true);", PreparedStatement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, objTipoInteres.getNombre());
            preparedStatement.setInt(2, objTipoInteres.getDiasPlazos());
            preparedStatement.setBoolean(3, objTipoInteres.isPlazoFijo());

            preparedStatement.execute();

            ResultSet keySet = preparedStatement.getGeneratedKeys();
            while (keySet.next()) {
                objTipoInteres.setId(keySet.getInt(1));
            }

            this.cache.put(objTipoInteres.getId(), objTipoInteres);

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean update(TipoInteres objTipoInteres) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE tiposinteres SET nombre = ?, diasplazos = ?, plazofijo = ?, estado = ?, fechamodificacion = now() "
                + "WHERE id = ?;")) {
            preparedStatement.setString(1, objTipoInteres.getNombre());
            preparedStatement.setInt(2, objTipoInteres.getDiasPlazos());
            preparedStatement.setBoolean(3, objTipoInteres.isPlazoFijo());
            preparedStatement.setBoolean(4, objTipoInteres.isEstado());
            preparedStatement.setInt(5, objTipoInteres.getId());
            
            preparedStatement.execute();
            
            this.cache.replace(objTipoInteres.getId(), objTipoInteres);
            
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean delete(TipoInteres objTipoInteres) {
        boolean result = false;
        
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE tiposinteres SET estado = false "
                + "WHERE id = ?;")) {
            preparedStatement.setInt(1, objTipoInteres.getId());
            
            preparedStatement.execute();
            
            this.cache.remove(objTipoInteres.getId());
            
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return result;
    }
}
