package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.TipoDocumentoIdentidad;
import com.smartech.sistemacooperativa.interfaces.IDAOTipoDocumentoIdentidad;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class DAOTipoDocumentoIdentidad implements IDAOTipoDocumentoIdentidad {

    private static DAOTipoDocumentoIdentidad instance = null;
    private Map<Integer, TipoDocumentoIdentidad> cache = new HashMap<>();

    private DAOTipoDocumentoIdentidad() {
    }

    public static DAOTipoDocumentoIdentidad getInstance() {
        if (instance == null) {
            instance = new DAOTipoDocumentoIdentidad();
        }

        return instance;
    }

    @Override
    public TipoDocumentoIdentidad getTipoDocumentoIdentidad(int id) {
        TipoDocumentoIdentidad objTipoDocumentoIdentidad = this.cache.get(id);
        
        if(objTipoDocumentoIdentidad == null){
            this.cache.clear();
            this.getAllTiposDocumentoIdentidad();
            objTipoDocumentoIdentidad = this.cache.get(id);            
        }
        
        return objTipoDocumentoIdentidad;
    }

    @Override
    public List<TipoDocumentoIdentidad> getAllTiposDocumentoIdentidad() {
        List<TipoDocumentoIdentidad> lstTiposDocumentoIdentidad;
        
        if(this.cache.isEmpty()){
            try(PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT tdi.id, tdi.nombre, tdi.longitud, tdi.permiteletras "
                    + "FROM tiposdocumentoidentidad tdi;")){                
                ResultSet resultSet = preparedStatement.executeQuery();
                
                while(resultSet.next()){
                    TipoDocumentoIdentidad objTipoDocumentoIdentidad = new TipoDocumentoIdentidad(
                            resultSet.getInt("id"),
                            resultSet.getString("nombre"),
                            resultSet.getInt("longitud"),
                            resultSet.getBoolean("permiteletras"));
                    this.cache.put(objTipoDocumentoIdentidad.getId(), objTipoDocumentoIdentidad);
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        
        lstTiposDocumentoIdentidad = new ArrayList<>(this.cache.values());
        
        return lstTiposDocumentoIdentidad;
    }
}
