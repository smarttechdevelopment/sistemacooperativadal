package com.smartech.sistemacooperativa.dal;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.Solicitud;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.interfaces.IDAOSolicitudPrestamo;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DAOSolicitudPrestamo implements IDAOSolicitudPrestamo {

    private static DAOSolicitudPrestamo instance = null;

    private DAOSolicitudPrestamo() {
    }

    public static DAOSolicitudPrestamo getInstance() {
        if (instance == null) {
            instance = new DAOSolicitudPrestamo();
        }

        return instance;
    }

    @Override
    public long getUltimoCodigo(int year) {
        long ultimoCodigo = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT COALESCE(MAX(substring(sp.codigo from 1 for 4)::int), 1) ultimocodigo "
                + "FROM solicitudesprestamo sp "
                + "WHERE substring(sp.codigo from 6 for 4) = ?")) {
            preparedStatement.setString(1, String.valueOf(year));

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                ultimoCodigo = resultSet.getInt("ultimocodigo");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ultimoCodigo;
    }

    @Override
    public SolicitudPrestamo getSolicitud(long id) {
        SolicitudPrestamo objSolicitudPrestamo = null;
        long idUsuario = 0;
        int idTipoSolicitud = 0;
        int idTipoPrestamo = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT sp.id, sp.codigo, sp.cuotas, sp.motivo, sp.otrosingresos, sp.nivelgastos, "
                + "sp.motivodesaprobacion, sp.montosolicitado, sp.montoaprobado, sp.fechavencimiento, sp.fecharegistro, sp.fechamodificacion, sp.aprobado, sp.estado, sp.habilitado, sp.pagado, "
                + "sp.idusuario, sp.idtiposolicitud, sp.idtipoprestamo "
                + "FROM solicitudesprestamo sp "
                + "WHERE sp.id = ?;")) {
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idUsuario = resultSet.getLong("idusuario");
                idTipoSolicitud = resultSet.getInt("idtiposolicitud");
                idTipoPrestamo = resultSet.getInt("idtipoprestamo");

                objSolicitudPrestamo = new SolicitudPrestamo(
                        id,
                        resultSet.getString("codigo"),
                        resultSet.getBigDecimal("otrosingresos"),
                        resultSet.getBigDecimal("nivelgastos"),
                        resultSet.getInt("cuotas"),
                        resultSet.getString("motivo"),
                        resultSet.getString("motivodesaprobacion"),
                        resultSet.getBigDecimal("montosolicitado"),
                        resultSet.getBigDecimal("montoaprobado"),
                        resultSet.getTimestamp("fechavencimiento"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("aprobado"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>(),
                        new ArrayList<>(),
                        new ArrayList<>(),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objSolicitudPrestamo != null) {
            objSolicitudPrestamo.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objSolicitudPrestamo.setObjTipoSolicitud(DAOTipoSolicitud.getInstance().getTipoSolicitud(idTipoSolicitud));
            objSolicitudPrestamo.setObjTasaInteres(DAOTasaInteres.getInstance().getTasaInteres(objSolicitudPrestamo));
            objSolicitudPrestamo.setObjTipoPrestamo(DAOTipoPrestamo.getInstance().getTipoPrestamo(idTipoPrestamo));
            objSolicitudPrestamo.setLstDetalleSolicitudPrestamoSocio(DAODetalleSolicitudPrestamoSocio.getInstance().getAllDetalleSolicitudPrestamoSocio(objSolicitudPrestamo));
            objSolicitudPrestamo.setLstDetalleDocumentoSolicitud(DAODetalleDocumentoSolicitud.getInstance().getAllDetalleDocumentoSolicitud(objSolicitudPrestamo));
            objSolicitudPrestamo.setLstAprobaciones(DAOAprobacion.getInstance().getAllAprobaciones(objSolicitudPrestamo));
        }

        return objSolicitudPrestamo;
    }

    @Override
    public SolicitudPrestamo getSolicitudPrestamoVigente(Socio objSocio) {
        SolicitudPrestamo objSolicitudPrestamo = null;
        long idUsuario = 0;
        int idTipoSolicitud = 0;
        int idTipoPrestamo = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT sp.id, sp.codigo, sp.cuotas, sp.idtiposolicitud, sp.otrosingresos, sp.nivelgastos, "
                + "sp.idusuario, sp.aprobado, sp.estado, sp.habilitado, sp.pagado, sp.fechavencimiento, sp.fecharegistro, sp.fechamodificacion, "
                + "sp.motivo, sp.motivodesaprobacion, sp.montosolicitado, sp.montoaprobado, sp.idtipoprestamo "
                + "FROM solicitudesprestamo sp "
                + "WHERE sp.id = (SELECT MAX (spsub.id) idsub "
                + "                 FROM solicitudesprestamo spsub INNER JOIN detallesolicitudprestamosocio dsps ON spsub.id = dsps.idsolicitudprestamo "
                + "                 WHERE spsub.estado is true AND dsps.idsocio = ? AND sp.fechavencimiento >= now() AND (spsub.aprobado is false OR spsub.id IN (SELECT p.id FROM prestamos p WHERE p.pagado is false)));")) {
            preparedStatement.setLong(1, objSocio.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idUsuario = resultSet.getLong("idusuario");
                idTipoSolicitud = resultSet.getInt("idtiposolicitud");
                idTipoPrestamo = resultSet.getInt("idtipoprestamo");

                objSolicitudPrestamo = new SolicitudPrestamo(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        resultSet.getBigDecimal("otrosingresos"),
                        resultSet.getBigDecimal("nivelgastos"),
                        resultSet.getInt("cuotas"),
                        resultSet.getString("motivo"),
                        resultSet.getString("motivodesaprobacion"),
                        resultSet.getBigDecimal("montosolicitado"),
                        resultSet.getBigDecimal("montoaprobado"),
                        resultSet.getTimestamp("fechavencimiento"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("aprobado"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>(),
                        new ArrayList<>(),
                        new ArrayList<>(),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (objSolicitudPrestamo != null) {
            objSolicitudPrestamo.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
            objSolicitudPrestamo.setObjTipoSolicitud(DAOTipoSolicitud.getInstance().getTipoSolicitud(idTipoSolicitud));
            objSolicitudPrestamo.setObjTasaInteres(DAOTasaInteres.getInstance().getTasaInteres(objSolicitudPrestamo));
            objSolicitudPrestamo.setObjTipoPrestamo(DAOTipoPrestamo.getInstance().getTipoPrestamo(idTipoPrestamo));
            objSolicitudPrestamo.setLstDetalleSolicitudPrestamoSocio(DAODetalleSolicitudPrestamoSocio.getInstance().getAllDetalleSolicitudPrestamoSocio(objSolicitudPrestamo));
            objSolicitudPrestamo.setLstDetalleDocumentoSolicitud(DAODetalleDocumentoSolicitud.getInstance().getAllDetalleDocumentoSolicitud(objSolicitudPrestamo));
            objSolicitudPrestamo.setLstAprobaciones(DAOAprobacion.getInstance().getAllAprobaciones(objSolicitudPrestamo));
        }

        return objSolicitudPrestamo;
    }

    @Override
    public SolicitudPrestamo getSolicitudPrestamo(Pago objPago) {
        SolicitudPrestamo objSolicitudPrestamo = null;
        long idUsuario = 0;
        int idTipoSolicitud = 0;
        int idTipoPrestamo = 0;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT sp.id, sp.codigo, sp.cuotas, sp.idtiposolicitud, sp.otrosingresos, sp.nivelgastos, "
                + "sp.idusuario, sp.aprobado, sp.estado, sp.habilitado, sp.pagado, sp.fechavencimiento, sp.fecharegistro, sp.fechamodificacion, "
                + "sp.motivo, sp.motivodesaprobacion, sp.montosolicitado, sp.montoaprobado, sp.idtipoprestamo "
                + "FROM solicitudesprestamo sp "
                + "WHERE sp.id IN (SELECT dpsp.idsolicitudprestamo FROM detallepagosolicitudprestamo dpsp WHERE dpsp.idpago = ?);")) {

            preparedStatement.setLong(1, objPago.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                idUsuario = resultSet.getLong("idusuario");
                idTipoSolicitud = resultSet.getInt("idtiposolicitud");
                idTipoPrestamo = resultSet.getInt("idtipoprestamo");

                objSolicitudPrestamo = new SolicitudPrestamo(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        resultSet.getBigDecimal("otrosingresos"),
                        resultSet.getBigDecimal("nivelgastos"),
                        resultSet.getInt("cuotas"),
                        resultSet.getString("motivo"),
                        resultSet.getString("motivodesaprobacion"),
                        resultSet.getBigDecimal("montosolicitado"),
                        resultSet.getBigDecimal("montoaprobado"),
                        resultSet.getTimestamp("fechavencimiento"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("aprobado"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>(),
                        new ArrayList<>(),
                        new ArrayList<>(),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"));
            }

            if (objSolicitudPrestamo != null) {
                objSolicitudPrestamo.setObjUsuario(DAOUsuario.getInstance().getUsuario(idUsuario));
                objSolicitudPrestamo.setObjTipoSolicitud(DAOTipoSolicitud.getInstance().getTipoSolicitud(idTipoSolicitud));
                objSolicitudPrestamo.setObjTasaInteres(DAOTasaInteres.getInstance().getTasaInteres(objSolicitudPrestamo));
                objSolicitudPrestamo.setObjTipoPrestamo(DAOTipoPrestamo.getInstance().getTipoPrestamo(idTipoPrestamo));
                objSolicitudPrestamo.setLstDetalleSolicitudPrestamoSocio(DAODetalleSolicitudPrestamoSocio.getInstance().getAllDetalleSolicitudPrestamoSocio(objSolicitudPrestamo));
                objSolicitudPrestamo.setLstDetalleDocumentoSolicitud(DAODetalleDocumentoSolicitud.getInstance().getAllDetalleDocumentoSolicitud(objSolicitudPrestamo));
                objSolicitudPrestamo.setLstAprobaciones(DAOAprobacion.getInstance().getAllAprobaciones(objSolicitudPrestamo));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return objSolicitudPrestamo;
    }

    @Override
    public List<SolicitudPrestamo> getAllSolicitudesPrestamoPorAprobar() {
        List<SolicitudPrestamo> lstSolicitudesPrestamo = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        List<Integer> lstIdsTiposSolicitud = new ArrayList<>();
        List<Integer> lstIdsTiposPrestamos = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT sp.id, sp.codigo, sp.cuotas, sp.motivo, sp.motivodesaprobacion, sp.montosolicitado, sp.otrosingresos, sp.nivelgastos, "
                + "sp.montoaprobado, sp.fechavencimiento, sp.fecharegistro, sp.fechamodificacion, sp.aprobado, sp.estado, sp.habilitado, sp.pagado, sp.idusuario, sp.idtiposolicitud, sp.idtipoprestamo "
                + "FROM solicitudesprestamo sp "
                + "WHERE sp.aprobado is false AND sp.estado is true AND sp.fechavencimiento >= NOW();")) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                lstIdsTiposSolicitud.add(resultSet.getInt("idtiposolicitud"));
                lstIdsTiposPrestamos.add(resultSet.getInt("idtipoprestamo"));

                SolicitudPrestamo objSolicitudPrestamo = new SolicitudPrestamo(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        resultSet.getBigDecimal("otrosingresos"),
                        resultSet.getBigDecimal("nivelgastos"),
                        resultSet.getInt("cuotas"),
                        resultSet.getString("motivo"),
                        resultSet.getString("motivodesaprobacion"),
                        resultSet.getBigDecimal("montosolicitado"),
                        resultSet.getBigDecimal("montoaprobado"),
                        resultSet.getTimestamp("fechavencimiento"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("aprobado"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>(),
                        new ArrayList<>(),
                        new ArrayList<>(),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"));
                lstSolicitudesPrestamo.add(objSolicitudPrestamo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstSolicitudesPrestamo.isEmpty()) {
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOTipoSolicitud objDAOTipoSolicitud = DAOTipoSolicitud.getInstance();
            DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();
            DAOTipoPrestamo objDAOTipoPrestamo = DAOTipoPrestamo.getInstance();
            DAODetalleSolicitudPrestamoSocio objDAODetalleSolicitudPrestamoSocio = DAODetalleSolicitudPrestamoSocio.getInstance();
            DAODetalleDocumentoSolicitud objDAODetalleDocumentoSolicitud = DAODetalleDocumentoSolicitud.getInstance();
            DAOAprobacion objDAOAprobacion = DAOAprobacion.getInstance();

            int size = lstSolicitudesPrestamo.size();
            for (int i = 0; i < size; i++) {
                lstSolicitudesPrestamo.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstSolicitudesPrestamo.get(i).setObjTipoSolicitud(objDAOTipoSolicitud.getTipoSolicitud(lstIdsTiposSolicitud.get(i)));
                lstSolicitudesPrestamo.get(i).setObjTasaInteres(objDAOTasaInteres.getTasaInteres(lstSolicitudesPrestamo.get(i)));
                lstSolicitudesPrestamo.get(i).setObjTipoPrestamo(objDAOTipoPrestamo.getTipoPrestamo(lstIdsTiposPrestamos.get(i)));
                lstSolicitudesPrestamo.get(i).setLstDetalleSolicitudPrestamoSocio(objDAODetalleSolicitudPrestamoSocio.getAllDetalleSolicitudPrestamoSocio(lstSolicitudesPrestamo.get(i)));
                lstSolicitudesPrestamo.get(i).setLstDetalleDocumentoSolicitud(objDAODetalleDocumentoSolicitud.getAllDetalleDocumentoSolicitud(lstSolicitudesPrestamo.get(i)));
                lstSolicitudesPrestamo.get(i).setLstAprobaciones(objDAOAprobacion.getAllAprobaciones(lstSolicitudesPrestamo.get(i)));
            }
        }

        return lstSolicitudesPrestamo;
    }

    @Override
    public List<SolicitudPrestamo> getAllSolicitudesPrestamosPorAprobarAsFiador(Socio objSocio) {
        List<SolicitudPrestamo> lstSolicitudesPrestamo = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        List<Integer> lstIdsTiposSolicitud = new ArrayList<>();
        List<Integer> lstIdsTiposPrestamos = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT DISTINCT sp.id, sp.codigo, sp.cuotas, sp.motivo, sp.motivodesaprobacion, sp.montosolicitado, sp.otrosingresos, sp.nivelgastos, "
                + "sp.montoaprobado, sp.fechavencimiento, sp.fecharegistro, sp.fechamodificacion, sp.aprobado, sp.estado, sp.habilitado, sp.pagado, sp.idusuario, sp.idtiposolicitud, sp.idtipoprestamo "
                + "FROM solicitudesprestamo sp INNER JOIN detallesolicitudprestamosocio dsps ON sp.id = dsps.idsolicitudprestamo "
                + "WHERE dsps.idsocio = ? AND dsps.fiador is true AND sp.aprobado is false;")) {
            preparedStatement.setLong(1, objSocio.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                lstIdsTiposSolicitud.add(resultSet.getInt("idtiposolicitud"));
                lstIdsTiposPrestamos.add(resultSet.getInt("idtipoprestamo"));

                SolicitudPrestamo objSolicitudPrestamo = new SolicitudPrestamo(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        resultSet.getBigDecimal("otrosingresos"),
                        resultSet.getBigDecimal("nivelgastos"),
                        resultSet.getInt("cuotas"),
                        resultSet.getString("motivo"),
                        resultSet.getString("motivodesaprobacion"),
                        resultSet.getBigDecimal("montosolicitado"),
                        resultSet.getBigDecimal("montoaprobado"),
                        resultSet.getTimestamp("fechavencimiento"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("aprobado"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>(),
                        new ArrayList<>(),
                        new ArrayList<>(),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"));
                lstSolicitudesPrestamo.add(objSolicitudPrestamo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!lstSolicitudesPrestamo.isEmpty()) {
            DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
            DAOTipoSolicitud objDAOTipoSolicitud = DAOTipoSolicitud.getInstance();
            DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();
            DAOTipoPrestamo objDAOTipoPrestamo = DAOTipoPrestamo.getInstance();
            DAODetalleSolicitudPrestamoSocio objDAODetalleSolicitudPrestamoSocio = DAODetalleSolicitudPrestamoSocio.getInstance();
            DAODetalleDocumentoSolicitud objDAODetalleDocumentoSolicitud = DAODetalleDocumentoSolicitud.getInstance();
            DAOAprobacion objDAOAprobacion = DAOAprobacion.getInstance();

            int size = lstSolicitudesPrestamo.size();
            for (int i = 0; i < size; i++) {
                lstSolicitudesPrestamo.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                lstSolicitudesPrestamo.get(i).setObjTipoSolicitud(objDAOTipoSolicitud.getTipoSolicitud(lstIdsTiposSolicitud.get(i)));
                lstSolicitudesPrestamo.get(i).setObjTasaInteres(objDAOTasaInteres.getTasaInteres(lstSolicitudesPrestamo.get(i)));
                lstSolicitudesPrestamo.get(i).setObjTipoPrestamo(objDAOTipoPrestamo.getTipoPrestamo(lstIdsTiposPrestamos.get(i)));
                lstSolicitudesPrestamo.get(i).setLstDetalleSolicitudPrestamoSocio(objDAODetalleSolicitudPrestamoSocio.getAllDetalleSolicitudPrestamoSocio(lstSolicitudesPrestamo.get(i)));
                lstSolicitudesPrestamo.get(i).setLstDetalleDocumentoSolicitud(objDAODetalleDocumentoSolicitud.getAllDetalleDocumentoSolicitud(lstSolicitudesPrestamo.get(i)));
                lstSolicitudesPrestamo.get(i).setLstAprobaciones(objDAOAprobacion.getAllAprobaciones(lstSolicitudesPrestamo.get(i)));
            }
        }

        return lstSolicitudesPrestamo;
    }

    @Override
    public List<SolicitudPrestamo> getAllSolicitudesPrestamoHabilitados(Timestamp fechaDesde, Timestamp fechaHasta) {
        List<SolicitudPrestamo> lstSolicitudesPrestamo = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        List<Integer> lstIdsTiposSolicitud = new ArrayList<>();
        List<Integer> lstIdsTiposPrestamos = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT sp.id, sp.codigo, sp.cuotas, sp.motivo, sp.motivodesaprobacion, sp.montosolicitado, sp.otrosingresos, sp.nivelgastos, "
                + "sp.montoaprobado, sp.fechavencimiento, sp.fecharegistro, sp.fechamodificacion, sp.aprobado, sp.estado, sp.habilitado, sp.pagado, sp.idusuario, sp.idtiposolicitud, sp.idtipoprestamo "
                + "FROM solicitudesprestamo sp "
                + "WHERE sp.estado is true AND sp.habilitado is true AND sp.pagado is false AND sp.fecharegistro BETWEEN ? AND ?;")) {
            preparedStatement.setTimestamp(1, fechaDesde);
            preparedStatement.setTimestamp(2, fechaHasta);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                lstIdsTiposSolicitud.add(resultSet.getInt("idtiposolicitud"));
                lstIdsTiposPrestamos.add(resultSet.getInt("idtipoprestamo"));

                SolicitudPrestamo objSolicitudPrestamo = new SolicitudPrestamo(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        resultSet.getBigDecimal("otrosingresos"),
                        resultSet.getBigDecimal("nivelgastos"),
                        resultSet.getInt("cuotas"),
                        resultSet.getString("motivo"),
                        resultSet.getString("motivodesaprobacion"),
                        resultSet.getBigDecimal("montosolicitado"),
                        resultSet.getBigDecimal("montoaprobado"),
                        resultSet.getTimestamp("fechavencimiento"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("aprobado"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>(),
                        new ArrayList<>(),
                        new ArrayList<>(),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"));
                lstSolicitudesPrestamo.add(objSolicitudPrestamo);
            }

            if (!lstSolicitudesPrestamo.isEmpty()) {
                DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
                DAOTipoSolicitud objDAOTipoSolicitud = DAOTipoSolicitud.getInstance();
                DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();
                DAOTipoPrestamo objDAOTipoPrestamo = DAOTipoPrestamo.getInstance();
                DAODetalleSolicitudPrestamoSocio objDAODetalleSolicitudPrestamoSocio = DAODetalleSolicitudPrestamoSocio.getInstance();
                DAODetalleDocumentoSolicitud objDAODetalleDocumentoSolicitud = DAODetalleDocumentoSolicitud.getInstance();
                DAOAprobacion objDAOAprobacion = DAOAprobacion.getInstance();

                int size = lstSolicitudesPrestamo.size();
                for (int i = 0; i < size; i++) {
                    lstSolicitudesPrestamo.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                    lstSolicitudesPrestamo.get(i).setObjTipoSolicitud(objDAOTipoSolicitud.getTipoSolicitud(lstIdsTiposSolicitud.get(i)));
                    lstSolicitudesPrestamo.get(i).setObjTasaInteres(objDAOTasaInteres.getTasaInteres(lstSolicitudesPrestamo.get(i)));
                    lstSolicitudesPrestamo.get(i).setObjTipoPrestamo(objDAOTipoPrestamo.getTipoPrestamo(lstIdsTiposPrestamos.get(i)));
                    lstSolicitudesPrestamo.get(i).setLstDetalleSolicitudPrestamoSocio(objDAODetalleSolicitudPrestamoSocio.getAllDetalleSolicitudPrestamoSocio(lstSolicitudesPrestamo.get(i)));
                    lstSolicitudesPrestamo.get(i).setLstDetalleDocumentoSolicitud(objDAODetalleDocumentoSolicitud.getAllDetalleDocumentoSolicitud(lstSolicitudesPrestamo.get(i)));
                    lstSolicitudesPrestamo.get(i).setLstAprobaciones(objDAOAprobacion.getAllAprobaciones(lstSolicitudesPrestamo.get(i)));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return lstSolicitudesPrestamo;
    }

    @Override
    public List<SolicitudPrestamo> getAllSolicitudesPrestamoPagados(Timestamp fechaDesde, Timestamp fechaHasta) {
        List<SolicitudPrestamo> lstSolicitudesPrestamo = new ArrayList<>();
        List<Long> lstIdsUsuarios = new ArrayList<>();
        List<Integer> lstIdsTiposSolicitud = new ArrayList<>();
        List<Integer> lstIdsTiposPrestamos = new ArrayList<>();

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("SELECT sp.id, sp.codigo, sp.cuotas, sp.motivo, sp.motivodesaprobacion, sp.montosolicitado, sp.otrosingresos, sp.nivelgastos, "
                + "sp.montoaprobado, sp.fechavencimiento, sp.fecharegistro, sp.fechamodificacion, sp.aprobado, sp.estado, sp.habilitado, sp.pagado, sp.idusuario, sp.idtiposolicitud, sp.idtipoprestamo "
                + "FROM solicitudesprestamo sp "
                + "WHERE sp.estado is true AND sp.habilitado is true AND sp.pagado is true AND sp.fecharegistro BETWEEN ? AND ?;")) {
            preparedStatement.setTimestamp(1, fechaDesde);
            preparedStatement.setTimestamp(2, fechaHasta);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                lstIdsUsuarios.add(resultSet.getLong("idusuario"));
                lstIdsTiposSolicitud.add(resultSet.getInt("idtiposolicitud"));
                lstIdsTiposPrestamos.add(resultSet.getInt("idtipoprestamo"));

                SolicitudPrestamo objSolicitudPrestamo = new SolicitudPrestamo(
                        resultSet.getLong("id"),
                        resultSet.getString("codigo"),
                        resultSet.getBigDecimal("otrosingresos"),
                        resultSet.getBigDecimal("nivelgastos"),
                        resultSet.getInt("cuotas"),
                        resultSet.getString("motivo"),
                        resultSet.getString("motivodesaprobacion"),
                        resultSet.getBigDecimal("montosolicitado"),
                        resultSet.getBigDecimal("montoaprobado"),
                        resultSet.getTimestamp("fechavencimiento"),
                        resultSet.getTimestamp("fecharegistro"),
                        resultSet.getTimestamp("fechamodificacion"),
                        resultSet.getBoolean("aprobado"),
                        resultSet.getBoolean("estado"),
                        null,
                        null,
                        null,
                        null,
                        new ArrayList<>(),
                        new ArrayList<>(),
                        new ArrayList<>(),
                        new ArrayList<>(),
                        resultSet.getBoolean("habilitado"),
                        resultSet.getBoolean("pagado"));
                lstSolicitudesPrestamo.add(objSolicitudPrestamo);
            }

            if (!lstSolicitudesPrestamo.isEmpty()) {
                DAOUsuario objDAOUsuario = DAOUsuario.getInstance();
                DAOTipoSolicitud objDAOTipoSolicitud = DAOTipoSolicitud.getInstance();
                DAOTasaInteres objDAOTasaInteres = DAOTasaInteres.getInstance();
                DAOTipoPrestamo objDAOTipoPrestamo = DAOTipoPrestamo.getInstance();
                DAODetalleSolicitudPrestamoSocio objDAODetalleSolicitudPrestamoSocio = DAODetalleSolicitudPrestamoSocio.getInstance();
                DAODetalleDocumentoSolicitud objDAODetalleDocumentoSolicitud = DAODetalleDocumentoSolicitud.getInstance();
                DAOAprobacion objDAOAprobacion = DAOAprobacion.getInstance();

                int size = lstSolicitudesPrestamo.size();
                for (int i = 0; i < size; i++) {
                    lstSolicitudesPrestamo.get(i).setObjUsuario(objDAOUsuario.getUsuario(lstIdsUsuarios.get(i)));
                    lstSolicitudesPrestamo.get(i).setObjTipoSolicitud(objDAOTipoSolicitud.getTipoSolicitud(lstIdsTiposSolicitud.get(i)));
                    lstSolicitudesPrestamo.get(i).setObjTasaInteres(objDAOTasaInteres.getTasaInteres(lstSolicitudesPrestamo.get(i)));
                    lstSolicitudesPrestamo.get(i).setObjTipoPrestamo(objDAOTipoPrestamo.getTipoPrestamo(lstIdsTiposPrestamos.get(i)));
                    lstSolicitudesPrestamo.get(i).setLstDetalleSolicitudPrestamoSocio(objDAODetalleSolicitudPrestamoSocio.getAllDetalleSolicitudPrestamoSocio(lstSolicitudesPrestamo.get(i)));
                    lstSolicitudesPrestamo.get(i).setLstDetalleDocumentoSolicitud(objDAODetalleDocumentoSolicitud.getAllDetalleDocumentoSolicitud(lstSolicitudesPrestamo.get(i)));
                    lstSolicitudesPrestamo.get(i).setLstAprobaciones(objDAOAprobacion.getAllAprobaciones(lstSolicitudesPrestamo.get(i)));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return lstSolicitudesPrestamo;
    }

    @Override
    public boolean insert(Solicitud objSolicitud) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO solicitudesprestamo(codigo, otrosingresos, nivelgastos, cuotas, motivo, motivodesaprobacion, montosolicitado, "
                + "montoaprobado, fechavencimiento, fecharegistro, aprobado, estado, idusuario, idtiposolicitud, idtipoprestamo, habilitado, pagado) "
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS)) {
            SolicitudPrestamo objSolicitudPrestamo = (SolicitudPrestamo) objSolicitud;

            preparedStatement.setString(1, objSolicitudPrestamo.getCodigo());
            preparedStatement.setBigDecimal(2, objSolicitudPrestamo.getOtrosIngresos());
            preparedStatement.setBigDecimal(3, objSolicitudPrestamo.getNivelGastos());
            preparedStatement.setInt(4, objSolicitudPrestamo.getCuotas());
            preparedStatement.setString(5, objSolicitudPrestamo.getMotivo());
            preparedStatement.setString(6, objSolicitudPrestamo.getMotivoDesaprobacion());
            preparedStatement.setBigDecimal(7, objSolicitudPrestamo.getMontoSolicitado());
            preparedStatement.setBigDecimal(8, objSolicitudPrestamo.getMontoAprobado());
            preparedStatement.setTimestamp(9, objSolicitudPrestamo.getFechaVencimiento());
            preparedStatement.setTimestamp(10, objSolicitudPrestamo.getFechaRegistro());
            preparedStatement.setBoolean(11, objSolicitudPrestamo.isAprobado());
            preparedStatement.setBoolean(12, objSolicitudPrestamo.isEstado());
            preparedStatement.setLong(13, objSolicitudPrestamo.getObjUsuario().getId());
            preparedStatement.setInt(14, objSolicitudPrestamo.getObjTipoSolicitud().getId());
            preparedStatement.setInt(15, objSolicitudPrestamo.getObjTipoPrestamo().getId());
            preparedStatement.setBoolean(16, objSolicitudPrestamo.isHabilitado());
            preparedStatement.setBoolean(17, objSolicitudPrestamo.isPagado());

            preparedStatement.execute();

            ResultSet keySet = preparedStatement.getGeneratedKeys();
            while (keySet.next()) {
                objSolicitudPrestamo.setId(keySet.getLong(1));
            }

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean update(Solicitud objSolicitud) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("UPDATE solicitudesprestamo SET codigo = ?, otrosingresos = ?, nivelgastos = ?, cuotas = ?, motivo = ?, motivodesaprobacion = ?, "
                + "montosolicitado = ?, montoaprobado = ?, fechavencimiento = ?, fechamodificacion = now(), aprobado = ?, estado = ?, idusuario = ?, idtiposolicitud = ?, idtipoprestamo = ?, habilitado = ?, pagado = ? "
                + "WHERE id = ?;")) {
            SolicitudPrestamo objSolicitudPrestamo = (SolicitudPrestamo) objSolicitud;

            preparedStatement.setString(1, objSolicitudPrestamo.getCodigo());
            preparedStatement.setBigDecimal(2, objSolicitudPrestamo.getOtrosIngresos());
            preparedStatement.setBigDecimal(3, objSolicitudPrestamo.getNivelGastos());
            preparedStatement.setInt(4, objSolicitudPrestamo.getCuotas());
            preparedStatement.setString(5, objSolicitudPrestamo.getMotivo());
            preparedStatement.setString(6, objSolicitudPrestamo.getMotivoDesaprobacion());
            preparedStatement.setBigDecimal(7, objSolicitudPrestamo.getMontoSolicitado());
            preparedStatement.setBigDecimal(8, objSolicitudPrestamo.getMontoAprobado());
            preparedStatement.setTimestamp(9, objSolicitudPrestamo.getFechaVencimiento());
            preparedStatement.setBoolean(10, objSolicitudPrestamo.isAprobado());
            preparedStatement.setBoolean(11, objSolicitudPrestamo.isEstado());
            preparedStatement.setLong(12, objSolicitudPrestamo.getObjUsuario().getId());
            preparedStatement.setInt(13, objSolicitudPrestamo.getObjTipoSolicitud().getId());
            preparedStatement.setInt(14, objSolicitudPrestamo.getObjTipoPrestamo().getId());
            preparedStatement.setBoolean(15, objSolicitudPrestamo.isHabilitado());
            preparedStatement.setBoolean(16, objSolicitudPrestamo.isPagado());

            preparedStatement.setLong(17, objSolicitudPrestamo.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean insertDetallePagoSolicitudPrestamo(Pago objPago, SolicitudPrestamo objSolicitudPrestamo) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("INSERT INTO detallepagosolicitudprestamo(monto, fecharegistro, estado, idsolicitudprestamo, idpago) "
                + "VALUES(?, now(), ?, ?, ?);")) {
            preparedStatement.setBigDecimal(1, objSolicitudPrestamo.getMontoAprobado());
            preparedStatement.setBoolean(2, objSolicitudPrestamo.isEstado());
            preparedStatement.setLong(3, objSolicitudPrestamo.getId());
            preparedStatement.setLong(4, objPago.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean physicalDelete(Solicitud objSolicitud) {
        boolean result = false;

        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement("DELETE FROM solicitudesprestamo WHERE id = ?;")) {
            preparedStatement.setLong(1, objSolicitud.getId());

            preparedStatement.execute();

            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
